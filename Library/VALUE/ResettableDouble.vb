﻿
''' <summary>
''' Represents an object whose underlying types are <see cref="Double"></see> value types, 
''' but can also be assigned a new value like a reference type.
''' </summary>
''' <remarks>
''' This is useful with SCPI value that require resetting to a known state.
''' </remarks>
<Serializable()>
Public Class ResettableDouble
    Inherits ResettableValue(Of Double)

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    Public Sub New()
        MyBase.new()
        Me._units = New isr.Core.FormattedUnits
        Me._displayUnits = New isr.Core.FormattedUnits
        Me._tolerance = Single.Epsilon
    End Sub

#End Region

#Region " EQUALS "

    Public Overrides Function GetHashCode() As Integer
        Return MyBase.GetHashCode()
    End Function

#End Region

#Region " DISPLAY "

    ''' <summary>
    ''' Gets the caption for displaying the value.
    ''' </summary>
    Public Overloads ReadOnly Property Caption() As String
        Get
            If Me._value.HasValue Then
                Return isr.Core.FormattedUnits.ToCaption(Me._units.ValueFormat, Me._value.Value * Me._units.ScaleFactor)
            Else
                Return ""
            End If
        End Get
    End Property

    ''' <summary>
    ''' Gets the caption for displaying the scaled value.
    ''' </summary>
    Public Overloads ReadOnly Property DisplayCaption() As String
        Get
            If Me._value.HasValue Then
                Return isr.Core.FormattedUnits.ToCaption(Me._displayUnits.ValueFormat, Me._value.Value * Me._displayUnits.ScaleFactor)
            Else
                Return ""
            End If
        End Get
    End Property

#End Region

#Region " EDITED VALUE "

    Private _editedValue As Double
    ''' <summary>
    ''' Gets or sets the edited value to be used for setting a new value.
    ''' </summary>
    Public Property EditedValue() As Double
        Get
            Return Me._editedValue
        End Get
        Set(ByVal value As Double)
            Me._editedValue = value
        End Set
    End Property

    ''' <summary>
    ''' Parses the scaled value to create a <see cref="EditedValue">new unscaled value</see>.
    ''' </summary>
    ''' <param name="value">The scaled value.</param>
    ''' <returns>Returns the new unscaled value</returns>
    Public Function ParseScaledValue(ByVal value As String) As Double
        If Double.TryParse(value, Globalization.NumberStyles.Number Or Globalization.NumberStyles.AllowExponent,
                          Globalization.CultureInfo.CurrentCulture, Me._editedValue) Then
            Me._editedValue *= Me._displayUnits.ScaleValue
        Else
            Throw New CastException("Failed parsing the scales value '" & value & "'")
        End If
        Return Me._editedValue
    End Function

    ''' <summary>
    ''' Parses the value to create a <see cref="EditedValue">new unscaled value</see>.
    ''' </summary>
    ''' <param name="value">The scaled value.</param>
    ''' <returns>Returns the new unscaled value</returns>
    Public Function ParseValue(ByVal value As String) As Double
        If Not Double.TryParse(value, Globalization.NumberStyles.Number Or Globalization.NumberStyles.AllowExponent,
                          Globalization.CultureInfo.CurrentCulture, Me._editedValue) Then
            Throw New CastException("Failed parsing the value '" & value & "'")
        End If
        Return Me._editedValue
    End Function

#End Region

#Region " VERIFIY "

    ''' <summary>
    ''' Gets a value indicating whether this instance is verified.
    ''' </summary>
    ''' <value>
    ''' 
    ''' <c>True</c> if this instance is verified; otherwise, <c>False</c>.
    ''' 
    ''' </value>
    Public Overrides ReadOnly Property IsVerified() As Boolean?
        Get
            If ActualValue.HasValue Then
                Return Me.Approximates(ActualValue.Value)
            Else
                Return New Boolean?
            End If
        End Get
    End Property

#End Region

#Region " UNITS AND SCALES "

    Private _units As isr.Core.FormattedUnits
    ''' <summary>
    ''' Gets or sets the units for this value.
    ''' These are the units of the unscaled value. 
    ''' </summary>
    Public Property Units() As isr.Core.FormattedUnits
        Get
            Return Me._units
        End Get
        Set(ByVal value As isr.Core.FormattedUnits)
            Me._units = value
        End Set
    End Property

    Private _displayUnits As isr.Core.FormattedUnits
    ''' <summary>
    ''' Gets or sets the Display Units.
    ''' </summary>
    Public Property DisplayUnits() As isr.Core.FormattedUnits
        Get
            If Me._displayUnits Is Nothing Then
                Return Me._units
            Else
                Return Me._displayUnits
            End If
        End Get
        Set(ByVal value As isr.Core.FormattedUnits)
            Me._displayUnits = value
        End Set
    End Property

#End Region

#Region " EQUALS "

    ''' <summary>Indicates whether the current <see cref="T:ResettableDouble"></see> value is equal to 
    ''' a specified object.</summary>
    ''' <returns><c>True</c> if the other parameter is equal to the current <see cref="T:ResettableDouble"></see> value; 
    ''' otherwise, <c>False</c>. 
    ''' </returns>
    ''' <param name="obj">An object.</param>
    ''' <FilterPriority>1</FilterPriority> 
    Public Overrides Function Equals(ByVal obj As Object) As Boolean
        Return obj IsNot Nothing AndAlso (Object.ReferenceEquals(Me, obj) OrElse
                                          (Me.GetType() Is obj.GetType AndAlso Me.Equals(CType(obj, ResettableDouble))))
    End Function

    ''' <summary>Indicates whether the current <see cref="T:ResettableDouble"></see> value is equal to 
    ''' a specified object.</summary>
    ''' <returns><c>True</c> if the other parameter is equal to the current <see cref="T:ResettableDouble"></see> value; 
    ''' otherwise, <c>False</c>. 
    ''' </returns>
    ''' <param name="value">An object.</param>
    ''' <FilterPriority>1</FilterPriority>
    Public Overridable Overloads Function Equals(ByVal value As ResettableDouble) As Boolean
        Return value IsNot Nothing AndAlso (Object.ReferenceEquals(value, Me) OrElse
                                           (ScpiDouble.Approximates(Me.Value, value.Value, Me.Tolerance) AndAlso
                                            ScpiDouble.Approximates(Me.ActualValue, value.ActualValue, Me.Tolerance) AndAlso
                                            ScpiDouble.Approximates(Me.ClearValue, value.ClearValue, Me.Tolerance) AndAlso
                                            ScpiDouble.Approximates(Me.PresetValue, value.PresetValue, Me.Tolerance) AndAlso
                                            ScpiDouble.Approximates(Me.ResetValue, value.ResetValue, Me.Tolerance)))
    End Function

    ''' <summary>Indicates whether the current <see cref="T:ResettableDouble"></see> value is equal to 
    ''' a specified object.</summary>
    ''' <returns><c>True</c> if the other parameter is equal to the current <see cref="T:ResettableDouble"></see> value; 
    ''' otherwise, <c>False</c>. 
    ''' </returns>
    ''' <param name="value">An object.</param>
    ''' <FilterPriority>1</FilterPriority>
    Public Overridable Overloads Function Equals(ByVal value As Double) As Boolean
        Return Me.Value.HasValue AndAlso ScpiDouble.Approximates(Me.Value, value, Me.Tolerance)
    End Function

    Public Overloads Function Equals(ByVal value As Single) As Boolean
        Return Me.Equals(CDbl(value))
    End Function

    Public Overloads Function Equals(ByVal value As Integer) As Boolean
        Return Me.Equals(CDbl(value))
    End Function

    Public Overloads Function Equals(ByVal value As Short) As Boolean
        Return Me.Equals(CDbl(value))
    End Function

    Public Overloads Function Equals(ByVal value As Long) As Boolean
        Return Me.Equals(CDbl(value))
    End Function

#End Region

#Region " CASTS "
#If False Then
  ''' <summary>Creates a new <see cref="T:ResettableDouble"></see> object initialized to a specified value.</summary>
  ''' <returns>A <see cref="T:ResettableDouble"></see> object whose <see cref="P:ResettableDouble.Value"></see> 
  ''' property is initialized with the value parameter.</returns>
  ''' <param name="value">A value type.</param>
  Public Overloads Shared Widening Operator CType(ByVal value As Nullable(Of Double)) As ResettableDouble
    Return FromNullableDouble(value)
  End Operator

  ''' <summary>Creates a new <see cref="T:ResettableDouble"></see> object initialized to a specified value.</summary>
  ''' <returns>A <see cref="T:ResettableDouble"></see> object whose <see cref="P:ResettableDouble.Value"></see> 
  ''' property is initialized with the value parameter.</returns>
  ''' <param name="value">A value type.</param>
  Public Shared Function FromNullableDouble(ByVal value As Nullable(Of Double)) As ResettableDouble
    If value.HasValue Then
      Return New ResettableDouble(value.Value)
    Else
      Return New ResettableDouble()
    End If
  End Function

  ''' <summary>Returns the value of a specified <see cref="T:ResettableDouble"></see> value.</summary>
  ''' <returns>The value of the <see cref="P:ResettableDouble.Value"></see> property for the value parameter.</returns>
  ''' <param name="value">A <see cref="T:ResettableDouble"></see> value.</param>
  Public Overloads Shared Narrowing Operator CType(ByVal value As ResettableDouble) As Nullable(Of Double)
    Return value.Value
  End Operator

  ''' <summary>Returns the value of a specified <see cref="T:ResettableDouble"></see> value.</summary>
  ''' <returns>The value of the <see cref="P:ResettableDouble.Value"></see> property for the value parameter.</returns>
  ''' <param name="value">A <see cref="T:ResettableDouble"></see> value.</param>
  Public Shared Function ToNullableDouble(ByVal value As ResettableValue(Of Double)) As Nullable(Of Double)
    Return value.Value
  End Function
#End If

#End Region

#Region " OPERATORS "

    ''' <summary>Returns True if the specified <see cref="T:ResettableDouble"></see> value
    ''' equals the <paramref name="left">value</paramref>.</summary>
    ''' <returns>The value of the <see cref="P:ResettableDouble.Value"></see> property for the value parameter.</returns>
    ''' <param name="left">A <see cref="T:Boolean"></see> value.</param>
    ''' <param name="right">A <see cref="T:ResettableDouble"></see> value.</param>
    Public Overloads Shared Operator =(ByVal left As Double, ByVal right As ResettableDouble) As Boolean
        Return right.Equals(left)
    End Operator

    ''' <summary>Returns True if the specified <see cref="T:ResettableDouble"></see> value
    ''' equals the <paramref name="right">value</paramref>.</summary>
    ''' <returns>The value of the <see cref="P:ResettableDouble.Value"></see> property for the value parameter.</returns>
    ''' <param name="left">A <see cref="T:ResettableDouble"></see> value.</param>
    ''' <param name="right">A <see cref="T:Boolean"></see> value.</param>
    Public Overloads Shared Operator =(ByVal left As ResettableDouble, ByVal right As Double) As Boolean
        Return left.Equals(right)
    End Operator

    ''' <summary>Returns True if the specified <see cref="T:ResettableDouble"></see> value
    ''' equals the <paramref name="right">value</paramref>.</summary>
    ''' <returns>The value of the <see cref="P:ResettableDouble.Value"></see> property for the value parameter.</returns>
    ''' <param name="left">A <see cref="T:ResettableDouble"></see> value.</param>
    ''' <param name="right">A <see cref="T:Boolean"></see> value.</param>
    Public Overloads Shared Operator =(ByVal left As ResettableDouble, ByVal right As ResettableDouble) As Boolean
        Return left.Equals(right)
    End Operator

    Public Overloads Shared Operator <>(ByVal left As Double, ByVal right As ResettableDouble) As Boolean
        Return Not (left = right)
    End Operator

    Public Overloads Shared Operator <>(ByVal left As ResettableDouble, ByVal right As Double) As Boolean
        Return Not (left = right)
    End Operator

    Public Overloads Shared Operator <>(ByVal left As ResettableDouble, ByVal right As ResettableDouble) As Boolean
        Return Not (left = right)
    End Operator

    ''' <summary>
    ''' Return 0 if the two values are equal.
    ''' </summary>
    ''' <param name="left"></param>
    ''' <param name="right"></param>
    Public Overloads Shared Function Compare(ByVal left As ScpiValue(Of Double), ByVal right As Double) As Integer
        If left.Value.HasValue Then
            Return left.Value.Value.CompareTo(right)
        Else
            Return 0
        End If
    End Function

    ''' <summary>Returns True if the specified <see cref="T:ResettableDouble"></see> value
    ''' is less than the <paramref name="right">value</paramref>.</summary>
    ''' <returns>The value of the <see cref="P:ResettableDouble.Value"></see> property for the value parameter.</returns>
    ''' <param name="left">A <see cref="T:ResettableDouble"></see> value.</param>
    ''' <param name="right">A <see cref="T:Boolean"></see> value.</param>
    Public Overloads Shared Operator <(ByVal left As ResettableDouble, ByVal right As Double) As Boolean
        Return ResettableDouble.Compare(left, right) < 0
    End Operator

    Public Overloads Shared Operator >(ByVal left As ResettableDouble, ByVal right As Double) As Boolean
        Return ResettableDouble.Compare(left, right) > 0
    End Operator

#End Region

#Region " RANGE "

    Friend _maximum As Nullable(Of Double)
    ''' <summary>
    ''' Gets or sets the maximum allowed value.
    ''' </summary>
    Public Property [Maximum]() As Nullable(Of Double)
        Get
            Return Me._maximum
        End Get
        Set(ByVal value As Nullable(Of Double))
            Me._maximum = value
        End Set
    End Property

    Friend _minimum As Nullable(Of Double)
    ''' <summary>
    ''' Gets or sets the minimum allowed value.
    ''' </summary>
    Public Property [Minimum]() As Nullable(Of Double)
        Get
            Return Me._minimum
        End Get
        Set(ByVal value As Nullable(Of Double))
            Me._minimum = value
        End Set
    End Property

    ''' <summary>
    ''' Returns true if the value is in range.
    ''' </summary>
    Public Function IsInRange(ByVal value As Double) As Boolean
        If Me._minimum.HasValue Then
            If Me._maximum.HasValue Then
                Return value <= Me._maximum.Value AndAlso value >= Me._minimum.Value
            Else
                Return value >= Me._minimum.Value
            End If
        ElseIf Me._maximum.HasValue Then
            Return value <= Me._maximum.Value
        Else
            Return True
        End If
    End Function

    ''' <summary>
    ''' Returns true if the value is in range.
    ''' </summary>
    Public Function IsInRange(ByVal value As Double?) As Boolean?
        If value.HasValue Then
            Return Me.IsInRange(value.Value)
        Else
            Return New Boolean?
        End If
    End Function

    ''' <summary>
    ''' Returns true if the value is in range.
    ''' </summary>
    Public Function IsInRange() As Boolean?
        Return IsInRange(Me.Value)
    End Function

    ''' <summary>
    ''' Returns a value within the entity range.
    ''' </summary>
    Public Function BoundValue(ByVal value As Double) As Double
        If Me._minimum.HasValue Then
            If Me._maximum.HasValue Then
                If value >= Me._minimum Then
                    If value <= Me._maximum Then
                        Return value
                    Else
                        Return Me._maximum.Value
                    End If
                Else
                    Return Me._minimum.Value
                End If
            Else
                If value >= Me._minimum Then
                    Return value
                Else
                    Return Me._minimum.Value
                End If
            End If
        ElseIf Me._maximum.HasValue Then
            If value <= Me._maximum Then
                Return value
            Else
                Return Me._maximum.Value
            End If
        Else
            Return value
        End If
    End Function

    ''' <summary>
    ''' Returns a value within the entity range.
    ''' </summary>
    Public Function BoundValue(ByVal value As Double?) As Double?
        If value.HasValue Then
            Return Me.BoundValue(value.Value)
        Else
            Return value
        End If
    End Function

    ''' <summary>
    ''' Returns a value within the entity range.
    ''' </summary>
    Public Function BoundValue() As Double?
        Return BoundValue(Me.Value)
    End Function

#End Region

#Region " TOLERANCE "

    Private _tolerance As Double
    ''' <summary>
    ''' Gets or sets the tolerance level for comparisons.
    ''' </summary>
    Public Property Tolerance() As Double
        Get
            Return Me._tolerance
        End Get
        Set(ByVal value As Double)
            Me._tolerance = value
        End Set
    End Property

    ''' <summary>
    ''' Returns true if this value approximates the given value
    ''' with the set <see cref="Tolerance">tolerance</see>
    ''' </summary>
    Public Function Approximates(ByVal value As Double) As Boolean?
        If Me.Value.HasValue Then
            Return Math.Abs(value - Me.Value.Value) < Me.Tolerance
        Else
            Return New Boolean?
        End If
    End Function

    ''' <summary>
    ''' Returns true if this value approximates the given value
    ''' with the set <see cref="Tolerance">tolerance</see>
    ''' </summary>
    Public Function Approximates(ByVal value As Single) As Boolean?
        If Me.Value.HasValue Then
            Return Math.Abs(value - Me.Value.Value) < Me.Tolerance
        Else
            Return New Boolean?
        End If
    End Function

#End Region

End Class

