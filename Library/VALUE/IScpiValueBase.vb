﻿''' <summary>
''' Specifies the contract implemented by SCPI values independent of their type.
''' </summary>
''' <remarks>
''' This is useful with SCPI values.
''' </remarks>
''' <license>
''' (c) 2011 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="01/27/2011" by="David" revision="6.0.4044.x">
''' Created
''' </history>
Public Interface IScpiValueBase

#Region " COMMANDS "

  ''' <summary>
  ''' Gets or sets the getter command.
  ''' </summary>
  ''' <value>
  ''' The getter command.
  ''' </value>
  Property GetterCommand() As String

  ''' <summary>
  ''' Gets or sets the setter command.
  ''' </summary>
  ''' <value>
  ''' The setter command.
  ''' </value>
  Property SetterCommand() As String

  ''' <summary>
  ''' Gets or sets the setter command format.
  ''' </summary>
  ''' <value>
  ''' The setter command format.
  ''' </value>
  Property SetterCommandFormat() As String

#End Region

#Region " EQUALS "

  ''' <summary>Indicates whether the current <see cref="T:ScpiValue"></see> value is equal to 
  ''' a specified object.</summary>
  ''' <returns><c>True</c> if the other parameter is equal to the current <see cref="T:ScpiValue"></see> value; 
  ''' otherwise, <c>False</c>. 
  ''' </returns>
  ''' <param name="obj">An object.</param>
  ''' <FilterPriority>1</FilterPriority>
  <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1720:IdentifiersShouldNotContainTypeNames", MessageId:="obj")> 
  Function Equals(ByVal obj As Object) As Boolean

  ''' <summary>Retrieves the hash code of the object returned by the 
  ''' <see cref="P:ScpiValue.Value"></see> property.</summary>
  ''' <returns>The hash code of the object returned by the 
  ''' <see cref="P:ScpiValue.Value"></see> property if the 
  ''' <see cref="P:ScpiValue.Value.HasValue"></see> property is true, 
  ''' or zero if the <see cref="P:ScpiValue.Value.HasValue"></see> property is false. </returns>
  ''' <FilterPriority>1</FilterPriority>
  Function GetHashCode() As Integer

#End Region

#Region " FORMAT "

  ''' <summary>
  ''' Gets the caption for displaying this information.
  ''' </summary>
      ReadOnly Property DisplayCaption() As String

  ''' <summary>Returns the text representation of the value of the current <see cref="T:ScpiValue"></see> 
  ''' object.</summary>
  ''' <returns>The text representation of the value of the current <see cref="T:ScpiValue"></see> 
  ''' object if the <see cref="P:ScpiValue.Value.HasValue"></see> property is true, or an empty string ("") if the 
  ''' <see cref="P:ScpiValue.Value.HasValue"></see> property is false.</returns>
  ''' <FilterPriority>1</FilterPriority>
  Function ToString() As String

#End Region

#Region " VALUES "

  ''' <summary>
  ''' Gets a value indicating whether this instance is verified.
  ''' </summary>
  ''' <value>
  ''' 
  ''' <c>True</c> if this instance is verified; otherwise, <c>False</c>.
  ''' 
  ''' </value>
  ReadOnly Property IsVerified() As Boolean?

#End Region

End Interface
