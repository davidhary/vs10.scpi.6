﻿''' <summary>
''' Represents an object whose underlying types are string values.
''' </summary>
''' <remarks>
''' This is useful with SCPI values.
''' </remarks>
''' <license>
''' (c) 2011 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="01/27/2011" by="David" revision="6.0.4044.x">
''' Created
''' </history>
Public Class ScpiString
    Inherits ScpiValueBase
    Implements IScpiString

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    Public Sub New()
        MyBase.new()
        Me._value = ""
        Me._actualValue = ""
    End Sub

    Public Sub New(ByVal value As String)
        Me.New()
        Me._value = value
        Me._actualValue = value
    End Sub

#End Region

#Region " EQUALS "

    ''' <summary>Indicates whether the current <see cref="T:ScpiString"></see> value is equal to 
    ''' a specified object.</summary>
    ''' <returns><c>True</c> if the other parameter is equal to the current <see cref="T:ScpiString"></see> value; 
    ''' otherwise, <c>False</c>. 
    ''' </returns>
    ''' <param name="obj">An object.</param>
    ''' <FilterPriority>1</FilterPriority>
    Public Overrides Function Equals(ByVal obj As Object) As Boolean
        Return obj IsNot Nothing AndAlso (Object.ReferenceEquals(Me, obj) OrElse
                                          (Me.GetType() Is obj.GetType AndAlso Me.Equals(CType(obj, ScpiString))))
    End Function

    ''' <summary>Indicates whether the current <see cref="T:ScpiString"></see> value is equal to 
    ''' a specified object.</summary>
    ''' <returns><c>True</c> if the other parameter is equal to the current <see cref="T:ScpiString"></see> value; 
    ''' otherwise, <c>False</c>. 
    ''' </returns>
    ''' <param name="value">An object.</param>
    ''' <FilterPriority>1</FilterPriority>
    Public Overridable Overloads Function Equals(ByVal value As IScpiString) As Boolean Implements IScpiString.Equals
        Return value IsNot Nothing AndAlso (Object.ReferenceEquals(value, Me) OrElse
                                            (Me.Value.Equals(value.Value) AndAlso Me.ActualValue.Equals(value.ActualValue)))
    End Function

    ''' <summary>Indicates whether the current <see cref="T:ScpiString"></see> value is equal to 
    ''' a specified object.</summary>
    ''' <returns><c>True</c> if the other parameter is equal to the current <see cref="T:ScpiString"></see> value; 
    ''' otherwise, <c>False</c>. 
    ''' </returns>
    ''' <param name="value">An object.</param>
    ''' <FilterPriority>1</FilterPriority>
    Public Overridable Overloads Function Equals(ByVal value As String) As Boolean Implements IScpiString.Equals
        Return Me.Value.Equals(value)
    End Function

    ''' <summary>Retrieves the hash code of the object returned by the 
    ''' <see cref="P:ScpiString.Value"></see> property.</summary>
    ''' <returns>The hash code of the object returned by the 
    ''' <see cref="P:ScpiString.Value"></see> property if the 
    ''' <see cref="P:ScpiString.Value"></see> is not null, 
    ''' or zero if the <see cref="P:ScpiString.Value"></see> is null. </returns>
    ''' <FilterPriority>1</FilterPriority>
    Public Overrides Function GetHashCode() As Integer
        Dim hashCode As Integer
        If Me.Value IsNot Nothing Then
            hashCode = Me._value.GetHashCode
        End If
        If Me.ActualValue IsNot Nothing Then
            hashCode = hashCode Xor Me._actualValue.GetHashCode
        End If
        Return hashCode
    End Function

#End Region

#Region " FORMAT "

    ''' <summary>Returns the text representation of the value of the current <see cref="T:ScpiString"></see> 
    ''' object.</summary>
    ''' <returns>The text representation of the value of the current <see cref="T:ScpiString"></see> 
    ''' object if the <see cref="P:ScpiString.Value"></see> is nothing, or an empty string ("") if the 
    ''' <see cref="P:ScpiString.Value"></see> is nothing.</returns>
    ''' <FilterPriority>1</FilterPriority>
    Public Overrides Function ToString() As String
        If String.IsNullOrWhiteSpace(Me.Value) Then
            Return ""
        Else
            Return Me.Value
        End If
    End Function

#End Region

#Region " VALUES "

    Friend _value As String
    ''' <summary>
    ''' Gets or sets the value.
    ''' </summary>
    ''' <value>
    ''' The value.
    ''' </value>
    Public Property [Value]() As String Implements IScpiString.Value
        Get
            Return Me._value
        End Get
        Set(ByVal value As String)
            Me._value = value
        End Set
    End Property

    Friend _actualValue As String
    ''' <summary>
    ''' Gets or sets the actual value.
    ''' </summary>
    ''' <value>
    ''' The actual value.
    ''' </value>
    Public Property ActualValue() As String Implements IScpiString.ActualValue
        Get
            Return Me._actualValue
        End Get
        Set(ByVal value As String)
            Me._actualValue = value
        End Set
    End Property

    ''' <summary>
    ''' Gets a value indicating whether this instance is verified.
    ''' </summary>
    ''' <value>
    ''' 
    ''' <c>True</c> if this instance is verified; otherwise, <c>False</c>.
    ''' 
    ''' </value>
    Public Overrides ReadOnly Property IsVerified() As Boolean?
        Get
            If Value IsNot Nothing AndAlso ActualValue IsNot Nothing Then
                Return Value.Equals(ActualValue)
            Else
                Return New Boolean?
            End If
        End Get
    End Property

#End Region

#Region " OPERATORS "

    ''' <summary>Creates a new <see cref="T:ScpiString"></see> object initialized to a specified value.</summary>
    ''' <returns>A <see cref="T:ScpiString"></see> object whose <see cref="P:ScpiString.Value"></see> 
    ''' property is initialized with the value parameter.</returns>
    ''' <param name="value">A value type.</param>
    Public Shared Widening Operator CType(ByVal value As String) As ScpiString
        If value IsNot Nothing Then
            Return New ScpiString(value)
        Else
            Return New ScpiString()
        End If
    End Operator

    ''' <summary>Returns the value of a specified <see cref="T:ScpiString"></see> value.</summary>
    ''' <returns>The value of the <see cref="P:ScpiString.Value"></see> property for the value parameter.</returns>
    ''' <param name="value">A <see cref="T:ScpiString"></see> value.</param>
    Public Shared Narrowing Operator CType(ByVal value As ScpiString) As String
        Return value.Value
    End Operator

    ''' <summary>
    ''' Returns True if the specified <see cref="T:ScpiString"></see> value
    ''' equals the <paramref name="left">value</paramref>.</summary>
    ''' <returns>The value of the <see cref="P:ScpiString.Value"></see> property for the value parameter.</returns>
    ''' <param name="left">A <see cref="T:Boolean"></see> value.</param>
    ''' <param name="right">A <see cref="T:ScpiString"></see> value.</param>
    Public Shared Operator =(ByVal left As String, ByVal right As ScpiString) As Boolean
        If right.Value IsNot Nothing Then
            Return right.Value.Equals(left)
        Else
            Return False
        End If
    End Operator

    ''' <summary>Returns True if the specified <see cref="T:ScpiString"></see> value
    ''' equals the <paramref name="right">value</paramref>.</summary>
    ''' <returns>The value of the <see cref="P:ScpiString.Value"></see> property for the value parameter.</returns>
    ''' <param name="left">A <see cref="T:ScpiString"></see> value.</param>
    ''' <param name="right">A <see cref="T:Boolean"></see> value.</param>
    Public Shared Operator =(ByVal left As ScpiString, ByVal right As String) As Boolean
        If left.Value IsNot Nothing Then
            Return left.Value.Equals(right)
        Else
            Return False
        End If
    End Operator

    ''' <summary>Returns True if the specified <see cref="T:ScpiString"></see> value
    ''' equals the <paramref name="right">value</paramref>.</summary>
    ''' <returns>The value of the <see cref="P:ScpiString.Value"></see> property for the value parameter.</returns>
    ''' <param name="left">A <see cref="T:ScpiString"></see> value.</param>
    ''' <param name="right">A <see cref="T:Boolean"></see> value.</param>
    Public Shared Operator =(ByVal left As ScpiString, ByVal right As ScpiString) As Boolean
        Return left.Equals(right)
    End Operator

    Public Shared Operator <>(ByVal left As String, ByVal right As ScpiString) As Boolean
        Return Not (left = right)
    End Operator

    Public Shared Operator <>(ByVal left As ScpiString, ByVal right As String) As Boolean
        Return Not (left = right)
    End Operator

    Public Shared Operator <>(ByVal left As ScpiString, ByVal right As ScpiString) As Boolean
        Return Not (left = right)
    End Operator

#End Region

End Class

