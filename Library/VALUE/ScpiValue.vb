''' <summary>
''' Represents an object whose underlying types are value types, but can also be assigned 
''' a new value like a reference type.
''' </summary>
''' <typeparam name="T"></typeparam>
''' <remarks>
''' This is useful with SCPI values.
''' </remarks>
''' <license>
''' (c) 2011 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="01/27/2011" by="David" revision="6.0.4044.x">
''' Created
''' </history>
<Serializable()>
Public Class ScpiValue(Of T As {Structure})
    Inherits ScpiValueBase
    Implements IScpiValue(Of T)

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    Public Sub New()
        MyBase.new()
        Me._value = New Nullable(Of T)
        Me._actualValue = New Nullable(Of T)
    End Sub

    Public Sub New(ByVal value As T)
        Me.New()
        Me._value = New Nullable(Of T)(value)
        Me._actualValue = New Nullable(Of T)(value)
    End Sub

#End Region

#Region " EQUALS "

    ''' <summary>Indicates whether the current <see cref="T:ScpiValue"></see> value is equal to 
    ''' a specified object.</summary>
    ''' <returns><c>True</c> if the other parameter is equal to the current <see cref="T:ScpiValue"></see> value; 
    ''' otherwise, <c>False</c>. 
    ''' </returns>
    ''' <param name="obj">An object.</param>
    ''' <FilterPriority>1</FilterPriority>
    Public Overrides Function Equals(ByVal obj As Object) As Boolean
        Return obj IsNot Nothing AndAlso (Object.ReferenceEquals(Me, obj) OrElse
                                          (Me.GetType() Is obj.GetType AndAlso Me.Equals(CType(obj, IScpiValue(Of T)))))
    End Function

    ''' <summary>Indicates whether the current <see cref="T:ScpiValue"></see> value is equal to 
    ''' a specified object.</summary>
    ''' <returns><c>True</c> if the other parameter is equal to the current <see cref="T:ScpiValue"></see> value; 
    ''' otherwise, <c>False</c>. 
    ''' </returns>
    ''' <param name="other">The other object being compared.</param>
    ''' <FilterPriority>1</FilterPriority>
    Public Overridable Overloads Function Equals(ByVal other As IScpiValue(Of T)) As Boolean Implements IEquatable(Of IScpiValue(Of T)).Equals
        Return other IsNot Nothing AndAlso (Object.ReferenceEquals(other, Me) OrElse
                                            (Me.Value.Equals(other.Value) AndAlso Me.ActualValue.Equals(other.ActualValue)))
    End Function

    ''' <summary>Indicates whether the current <see cref="T:ScpiValue"></see> value is equal to 
    ''' a specified object.</summary>
    ''' <returns><c>True</c> if the other parameter is equal to the current <see cref="T:ScpiValue"></see> value; 
    ''' otherwise, <c>False</c>. 
    ''' </returns>
    ''' <param name="value">An object.</param>
    ''' <FilterPriority>1</FilterPriority>
    Public Overridable Overloads Function Equals(ByVal value As T) As Boolean
        Return Me.Value.Equals(value)
    End Function

    ''' <summary>Retrieves the hash code of the object returned by the 
    ''' <see cref="P:ScpiValue.Value"></see> property.</summary>
    ''' <returns>The hash code of the object returned by the 
    ''' <see cref="P:ScpiValue.Value"></see> property if the 
    ''' <see cref="P:ScpiValue.Value.HasValue"></see> property is true, 
    ''' or zero if the <see cref="P:ScpiValue.Value.HasValue"></see> property is false. </returns>
    ''' <FilterPriority>1</FilterPriority>
    Public Overrides Function GetHashCode() As Integer
        Dim hashCode As Integer
        If Me.Value IsNot Nothing Then
            hashCode = Me._value.GetHashCode
        End If
        If Me.ActualValue IsNot Nothing Then
            hashCode = hashCode Xor Me._actualValue.GetHashCode
        End If
        Return hashCode
    End Function

#End Region

#Region " FORMAT "

    ''' <summary>Returns the text representation of the value of the current <see cref="T:ScpiValue"></see> 
    ''' object.</summary>
    ''' <returns>The text representation of the value of the current <see cref="T:ScpiValue"></see> 
    ''' object if the <see cref="P:ScpiValue.Value.HasValue"></see> property is true, or an empty string ("") if the 
    ''' <see cref="P:ScpiValue.Value.HasValue"></see> property is false.</returns>
    ''' <FilterPriority>1</FilterPriority>
    Public Overrides Function ToString() As String
        Return Me._value.ToString
    End Function

#End Region

#Region " VALUES "

    Private _FormatterParser As Core.IFormatterParser(Of T)
    ''' <summary>
    ''' Gets or sets the formatter parser.
    ''' </summary>
    ''' <value>
    ''' The formatter parser.
    ''' </value>
    Property FormatterParser() As Core.IFormatterParser(Of T) Implements IScpiValue(Of T).FormatterParser
        Get
            Return Me._FormatterParser
        End Get
        Set(ByVal value As Core.IFormatterParser(Of T))
            Me._FormatterParser = value
        End Set
    End Property

    Friend _value As Nullable(Of T)
    ''' <summary>
    ''' Gets or sets the value.
    ''' </summary>
    ''' <value>
    ''' The value.
    ''' </value>
    Public Property [Value]() As Nullable(Of T) Implements IScpiValue(Of T).Value
        Get
            Return Me._value
        End Get
        Set(ByVal value As Nullable(Of T))
            Me._value = value
        End Set
    End Property

    Friend _actualValue As Nullable(Of T)
    ''' <summary>
    ''' Gets or sets the actual value.
    ''' </summary>
    ''' <value>
    ''' The actual value.
    ''' </value>
    Public Property ActualValue() As Nullable(Of T) Implements IScpiValue(Of T).ActualValue
        Get
            Return Me._actualValue
        End Get
        Set(ByVal value As Nullable(Of T))
            Me._actualValue = value
        End Set
    End Property

    ''' <summary>
    ''' Gets a value indicating whether this instance is verified.
    ''' </summary>
    ''' <value>
    ''' 
    ''' <c>True</c> if this instance is verified; otherwise, <c>False</c>.
    ''' 
    ''' </value>
    Public Overrides ReadOnly Property IsVerified() As Boolean?
        Get
            If Value.HasValue AndAlso ActualValue.HasValue Then
                Return Value.Value.Equals(ActualValue.Value)
            Else
                Return New Boolean?
            End If
        End Get
    End Property

#End Region

#Region " CASTS "
#If False Then
  ''' <summary>Creates a new <see cref="T:ScpiValue"></see> object initialized to a specified value.</summary>
  ''' <returns>A <see cref="T:ScpiValue"></see> object whose <see cref="P:ScpiValue.Value"></see> 
  ''' property is initialized with the value parameter.</returns>
  ''' <param name="value">A value type.</param>
  Public Shared Widening Operator CType(ByVal value As Nullable(Of T)) As ScpiValue(Of T)
    If value.HasValue Then
      Return New ScpiValue(Of T)(value.Value)
    Else
      Return New ScpiValue(Of T)()
    End If
  End Operator

  ''' <summary>Returns the value of a specified <see cref="T:ScpiValue"></see> value.</summary>
  ''' <returns>The value of the <see cref="P:ScpiValue.Value"></see> property for the value parameter.</returns>
  ''' <param name="value">A <see cref="T:ScpiValue"></see> value.</param>
  Public Shared Narrowing Operator CType(ByVal value As ScpiValue(Of T)) As Nullable(Of T)
    Return value.Value
  End Operator

  ''' <summary>Returns the value of a specified <see cref="T:ScpiValue"></see> value.</summary>
  ''' <returns>The value of the <see cref="P:ScpiValue.Value"></see> property for the value parameter.</returns>
  ''' <param name="value">A <see cref="T:ScpiValue"></see> value.</param>
  Public Shared Narrowing Operator CType(ByVal value As ScpiValue(Of T)) As T
    Return value.Value.Value
  End Operator
#End If
#End Region

#Region " OPERATORS "

    ''' <summary>Returns True if the specified <see cref="T:ScpiValue"></see> value
    ''' equals the <paramref name="left">value</paramref>.</summary>
    ''' <returns>The value of the <see cref="P:ScpiValue.Value"></see> property for the value parameter.</returns>
    ''' <param name="left">A <see cref="T:Boolean"></see> value.</param>
    ''' <param name="right">A <see cref="T:ScpiValue"></see> value.</param>
    Public Shared Operator =(ByVal left As T, ByVal right As ScpiValue(Of T)) As Boolean
        If right.Value.HasValue Then
            Return right.Value.Equals(left)
        Else
            Return False
        End If
    End Operator

    ''' <summary>Returns True if the specified <see cref="T:ScpiValue"></see> value
    ''' equals the <paramref name="right">value</paramref>.</summary>
    ''' <returns>The value of the <see cref="P:ScpiValue.Value"></see> property for the value parameter.</returns>
    ''' <param name="left">A <see cref="T:ScpiValue"></see> value.</param>
    ''' <param name="right">A <see cref="T:Boolean"></see> value.</param>
    Public Shared Operator =(ByVal left As ScpiValue(Of T), ByVal right As T) As Boolean
        If left.Value.HasValue Then
            Return left.Value.Equals(right)
        Else
            Return False
        End If
    End Operator

    ''' <summary>Returns True if the specified <see cref="T:ScpiValue"></see> value
    ''' equals the <paramref name="right">value</paramref>.</summary>
    ''' <returns>The value of the <see cref="P:ScpiValue.Value"></see> property for the value parameter.</returns>
    ''' <param name="left">A <see cref="T:ScpiValue"></see> value.</param>
    ''' <param name="right">A <see cref="T:Boolean"></see> value.</param>
    Public Shared Operator =(ByVal left As ScpiValue(Of T), ByVal right As ScpiValue(Of T)) As Boolean
        Return left.Equals(right)
    End Operator

    Public Shared Operator <>(ByVal left As T, ByVal right As ScpiValue(Of T)) As Boolean
        Return Not (left = right)
    End Operator

    Public Shared Operator <>(ByVal left As ScpiValue(Of T), ByVal right As T) As Boolean
        Return Not (left = right)
    End Operator

    Public Shared Operator <>(ByVal left As ScpiValue(Of T), ByVal right As ScpiValue(Of T)) As Boolean
        Return Not (left = right)
    End Operator

#End Region

End Class
