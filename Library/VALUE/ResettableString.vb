﻿Public Class ResettableString
    Inherits ScpiString
    Implements IResettable, IScpiString

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    Public Sub New()
        MyBase.new()
    End Sub

#End Region

#Region " IRESETTABLE "

    ''' <summary>
    ''' Sets the value to the clear execution state.
    ''' </summary>
    Public Overridable Function ClearExecutionState() As Boolean Implements IResettable.ClearExecutionState
        If Me.ClearValue IsNot Nothing Then
            Me.Value = Me.ClearValue
            Me.ActualValue = Me.Value
        End If
        Return True
    End Function

    ''' <summary>
    ''' Sets the value to the preset known state value.
    ''' </summary>
    Public Overridable Function PresetKnownState() As Boolean Implements IResettable.PresetKnownState
        If Me.PresetValue IsNot Nothing Then
            Me.Value = Me.PresetValue
            Me.ActualValue = Me.Value
        End If
        Return True
    End Function

    ''' <summary>
    ''' Sets the value to the known state value.
    ''' </summary>
    Public Overridable Function ResetKnownState() As Boolean Implements IResettable.ResetKnownState
        If Me.ResetValue IsNot Nothing Then
            Me.Value = Me.ResetValue
            Me.ActualValue = Me.Value
        End If
        Return True
    End Function

#End Region

#Region " VALUES "

    Friend _clearValue As String
    ''' <summary>
    ''' Gets or sets the value to use when
    ''' <see cref="ClearExecutionState">clearing execution state</see>
    ''' </summary>
    ''' <value>A <see cref="System.String">nullable value</see></value>
    Public Property ClearValue() As String
        Get
            Return Me._clearValue
        End Get
        Set(ByVal value As String)
            Me._clearValue = value
        End Set
    End Property

    Friend _presetValue As String
    ''' <summary>
    ''' Gets or sets the value to use when
    ''' <see cref="PresetKnownState">presetting to a known state</see>
    ''' </summary>
    ''' <value>A <see cref="System.String">nullable value</see></value>
    Public Property PresetValue() As String
        Get
            Return Me._presetValue
        End Get
        Set(ByVal value As String)
            Me._presetValue = value
        End Set
    End Property

    Friend _resetValue As String
    ''' <summary>
    ''' Gets or sets the value to use when
    ''' <see cref="ResetKnownState">resetting to a known state</see>
    ''' </summary>
    ''' <value>A <see cref="System.String">nullable value</see></value>
    Public Property ResetValue() As String
        Get
            Return Me._resetValue
        End Get
        Set(ByVal value As String)
            Me._resetValue = value
        End Set
    End Property

#End Region

End Class
