''' <summary>
''' Represents an object whose underlying types are value types, but can also be assigned 
''' a new value like a reference type.
''' </summary>
''' <typeparam name="T"></typeparam>
''' <remarks>
''' This is useful with SCPI values that require reseting to a known state.
''' </remarks>
''' <license>
''' (c) 2011 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="03/24/2008" by="David" revision="2.0.3003.x">
''' Created
''' </history>
''' <history date="01/27/2011" by="David" revision="6.0.4044.x">
''' Inherits the <see cref="T:ScpiValue">SCPI Value</see>.
''' </history>
<Serializable()>
Public Class ResettableValue(Of T As {Structure})
    Inherits ScpiValue(Of T)
    Implements IResettableValue(Of T)

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    Public Sub New()
        MyBase.new()
        Me._resetValue = New Nullable(Of T)
        Me._presetValue = New Nullable(Of T)
        Me._clearValue = New Nullable(Of T)
    End Sub

#End Region

#Region " IRESETTABLE "

    ''' <summary>
    ''' Sets the value to the clear execution state.
    ''' </summary>
    Public Overridable Function ClearExecutionState() As Boolean Implements IResettable.ClearExecutionState
        If Me.ClearValue.HasValue Then
            Me.Value = Me.ClearValue.Value
            Me.ActualValue = Me.Value
        End If
        Return True
    End Function

    ''' <summary>
    ''' Sets the value to the preset known state value.
    ''' </summary>
    Public Overridable Function PresetKnownState() As Boolean Implements IResettable.PresetKnownState
        If Me.PresetValue.HasValue Then
            Me.Value = Me.PresetValue.Value
            Me.ActualValue = Me.Value
        End If
        Return True
    End Function

    ''' <summary>
    ''' Sets the value to the known state value.
    ''' </summary>
    Public Overridable Function ResetKnownState() As Boolean Implements IResettable.ResetKnownState
        If Me.ResetValue.HasValue Then
            Me.Value = Me.ResetValue.Value
            Me.ActualValue = Me.Value
        End If
        Return True
    End Function

#End Region

#Region " EQUALS "

    ''' <summary>Indicates whether the current <see cref="T:ResettableValue"></see> value is equal to 
    ''' a specified object.</summary>
    ''' <returns><c>True</c> if the other parameter is equal to the current <see cref="T:ResettableValue"></see> value; 
    ''' otherwise, <c>False</c>. 
    ''' </returns>
    ''' <param name="obj">An object.</param>
    ''' <FilterPriority>1</FilterPriority>
    Public Overrides Function Equals(ByVal obj As Object) As Boolean
        Return obj IsNot Nothing AndAlso (Object.ReferenceEquals(Me, obj) OrElse
                                          (Me.GetType() Is obj.GetType AndAlso Me.Equals(CType(obj, IResettableValue(Of T)))))
    End Function

    ''' <summary>Indicates whether the current <see cref="T:ResettableValue"></see> value is equal to 
    ''' a specified object.</summary>
    ''' <returns><c>True</c> if the other parameter is equal to the current <see cref="T:ResettableValue"></see> value; 
    ''' otherwise, <c>False</c>. 
    ''' </returns>
    ''' <param name="other">The other object being compared.</param>
    ''' <FilterPriority>1</FilterPriority>
    Public Overridable Overloads Function Equals(ByVal other As IResettableValue(Of T)) As Boolean Implements IEquatable(Of IResettableValue(Of T)).Equals
        Return other IsNot Nothing AndAlso (Object.ReferenceEquals(other, Me) OrElse
                                            (MyBase.Equals(other) AndAlso Me.ResetValue.Equals(other.ResetValue) AndAlso
                                             Me.PresetValue.Equals(other.PresetValue) AndAlso
                                             Me.PresetValue.Equals(other.PresetValue) AndAlso
                                             Me.ClearValue.Equals(other.ClearValue)))
    End Function

    ''' <summary>Indicates whether the current <see cref="T:ScpiValue"></see> value is equal to 
    ''' a specified object.</summary>
    ''' <returns><c>True</c> if the other parameter is equal to the current <see cref="T:ScpiValue"></see> value; 
    ''' otherwise, <c>False</c>. 
    ''' </returns>
    ''' <param name="value">An object.</param>
    ''' <FilterPriority>1</FilterPriority>
    Public Overridable Overloads Function Equals(ByVal value As T) As Boolean
        Return Me.Value.Equals(value)
    End Function

    ''' <summary>Retrieves the hash code of the object returned by the 
    ''' <see cref="P:ResettableValue.Value"></see> property.</summary>
    ''' <returns>The hash code of the object returned by the 
    ''' <see cref="P:ResettableValue.Value"></see> property if the 
    ''' <see cref="P:ResettableValue.Value.HasValue"></see> property is true, 
    ''' or zero if the <see cref="P:ResettableValue.Value.HasValue"></see> property is false. </returns>
    ''' <FilterPriority>1</FilterPriority>
    Public Overrides Function GetHashCode() As Integer
        Dim hashCode As Integer = MyBase.GetHashCode
        If Me.ClearValue IsNot Nothing Then
            hashCode = hashCode Xor Me._clearValue.GetHashCode
        End If
        If Me.PresetValue IsNot Nothing Then
            hashCode = hashCode Xor Me.PresetValue.GetHashCode
        End If
        If Me.ResetValue IsNot Nothing Then
            hashCode = hashCode Xor Me.ResetValue.GetHashCode
        End If
        Return hashCode
    End Function

#End Region

#Region " VALUES "

    Friend _clearValue As Nullable(Of T)
    ''' <summary>
    ''' Gets or sets the value to use when
    ''' <see cref="ClearExecutionState">clearing execution state</see>
    ''' </summary>
    ''' <value>A <see cref="System.Nullable(of T)">nullable value</see></value>
    Public Property ClearValue() As Nullable(Of T) Implements IResettableValue(Of T).ClearValue
        Get
            Return Me._clearValue
        End Get
        Set(ByVal value As Nullable(Of T))
            Me._clearValue = value
        End Set
    End Property

    Friend _presetValue As Nullable(Of T)
    ''' <summary>
    ''' Gets or sets the value to use when
    ''' <see cref="PresetKnownState">presetting to a known state</see>
    ''' </summary>
    ''' <value>A <see cref="System.Nullable(of T)">nullable value</see></value>
    Public Property PresetValue() As Nullable(Of T) Implements IResettableValue(Of T).PresetValue
        Get
            Return Me._presetValue
        End Get
        Set(ByVal value As Nullable(Of T))
            Me._presetValue = value
        End Set
    End Property

    Friend _resetValue As Nullable(Of T)
    ''' <summary>
    ''' Gets or sets the value to use when
    ''' <see cref="ResetKnownState">resetting to a known state</see>
    ''' </summary>
    ''' <value>A <see cref="System.Nullable(of T)">nullable value</see></value>
    Public Property ResetValue() As Nullable(Of T) Implements IResettableValue(Of T).ResetValue
        Get
            Return Me._resetValue
        End Get
        Set(ByVal value As Nullable(Of T))
            Me._resetValue = value
        End Set
    End Property

#End Region

#Region " CASTS "
#If False Then
  ''' <summary>Creates a new <see cref="T:ResettableValue"></see> object initialized to a specified value.</summary>
  ''' <returns>A <see cref="T:ResettableValue"></see> object whose <see cref="P:ResettableValue.Value"></see> 
  ''' property is initialized with the value parameter.</returns>
  ''' <param name="value">A value type.</param>
  Public Overloads Shared Widening Operator CType(ByVal value As Nullable(Of T)) As ResettableValue(Of T)
    If value.HasValue Then
      Return New ResettableValue(Of T)(value.Value)
    Else
      Return New ResettableValue(Of T)()
    End If
  End Operator

  ''' <summary>Returns the value of a specified <see cref="T:ResettableValue"></see> value.</summary>
  ''' <returns>The value of the <see cref="P:ResettableValue.Value"></see> property for the value parameter.</returns>
  ''' <param name="value">A <see cref="T:ResettableValue"></see> value.</param>
  Public Overloads Shared Narrowing Operator CType(ByVal value As ResettableValue(Of T)) As Nullable(Of T)
    Return value.Value
  End Operator

  ''' <summary>Creates a new <see cref="T:ResettableValue"></see> object initialized to a specified value.</summary>
  ''' <returns>A <see cref="T:ResettableValue"></see> object whose <see cref="P:ResettableValue.Value"></see> 
  ''' property is initialized with the value parameter.</returns>
  ''' <param name="value">A value type.</param>
  Public Overloads Shared Widening Operator CType(ByVal value As ResettableValue(Of T)) As T
    Return value.Value.Value
  End Operator
#End If
#End Region

#Region " OPERATORS "

    ''' <summary>Returns True if the specified <see cref="T:ResettableValue"></see> value
    ''' equals the <paramref name="left">value</paramref>.</summary>
    ''' <returns>The value of the <see cref="P:ResettableValue.Value"></see> property for the value parameter.</returns>
    ''' <param name="left">A <see cref="T:Boolean"></see> value.</param>
    ''' <param name="right">A <see cref="T:ResettableValue"></see> value.</param>
    Public Overloads Shared Operator =(ByVal left As T, ByVal right As ResettableValue(Of T)) As Boolean
        If right.Value.HasValue Then
            Return right.Value.Equals(left)
        Else
            Return False
        End If
    End Operator

    ''' <summary>Returns True if the specified <see cref="T:ResettableValue"></see> value
    ''' equals the <paramref name="right">value</paramref>.</summary>
    ''' <returns>The value of the <see cref="P:ResettableValue.Value"></see> property for the value parameter.</returns>
    ''' <param name="left">A <see cref="T:ResettableValue"></see> value.</param>
    ''' <param name="right">A <see cref="T:Boolean"></see> value.</param>
    Public Overloads Shared Operator =(ByVal left As ResettableValue(Of T), ByVal right As T) As Boolean
        If left.Value.HasValue Then
            Return left.Value.Equals(right)
        Else
            Return False
        End If
    End Operator

    ''' <summary>Returns True if the specified <see cref="T:ResettableValue"></see> value
    ''' equals the <paramref name="right">value</paramref>.</summary>
    ''' <returns>The value of the <see cref="P:ResettableValue.Value"></see> property for the value parameter.</returns>
    ''' <param name="left">A <see cref="T:ResettableValue"></see> value.</param>
    ''' <param name="right">A <see cref="T:Boolean"></see> value.</param>
    Public Overloads Shared Operator =(ByVal left As ResettableValue(Of T), ByVal right As IResettableValue(Of T)) As Boolean
        Return left.Equals(right)
    End Operator

    Public Overloads Shared Operator <>(ByVal left As T, ByVal right As ResettableValue(Of T)) As Boolean
        Return Not right.Equals(left)
    End Operator

    Public Overloads Shared Operator <>(ByVal left As ResettableValue(Of T), ByVal right As T) As Boolean
        Return Not left.Equals(right)
    End Operator

    Public Overloads Shared Operator <>(ByVal left As ResettableValue(Of T), ByVal right As IResettableValue(Of T)) As Boolean
        Return Not left.Equals(right)
    End Operator

#End Region

End Class
