﻿''' <summary>
''' Specifies the contract implemented by SCPI values.
''' </summary>
''' <typeparam name="T"></typeparam>
''' <remarks>
''' This is useful with SCPI values.
''' </remarks>
''' <license>
''' (c) 2011 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="01/27/2011" by="David" revision="6.0.4044.x">
''' Created
''' </history>
Public Interface IScpiValue(Of T As {Structure})
  Inherits IScpiValueBase, IEquatable(Of IScpiValue(Of T))

#Region " VALUES "

  ''' <summary>
  ''' Gets or sets the formatter parser.
  ''' </summary>
  ''' <value>
  ''' The formatter parser.
  ''' </value>
  Property FormatterParser() As Core.IFormatterParser(Of T)

  ''' <summary>
  ''' Gets or sets the value.
  ''' </summary>
  ''' <value>
  ''' The value.
  ''' </value>
  Property [Value]() As Nullable(Of T)

  ''' <summary>
  ''' Gets or sets the actual value.
  ''' </summary>
  ''' <value>
  ''' The actual value.
  ''' </value>
  Property ActualValue() As Nullable(Of T)

#End Region

End Interface

#Region " UNUSED  "
#If False Then
#Region " EQUALS "
  ''' <summary>Indicates whether the current <see cref="T:ScpiValue"></see> value is equal to 
  ''' a specified object.</summary>
  ''' <returns><c>True</c> if the other parameter is equal to the current <see cref="T:ScpiValue"></see> value; 
  ''' otherwise, <c>False</c>. 
  ''' </returns>
  ''' <param name="value">An object.</param>
  ''' <FilterPriority>1</FilterPriority>
  Overloads Function Equals(ByVal value As IScpiValue(Of T)) As Boolean

  ''' <summary>Indicates whether the current <see cref="T:ScpiValue"></see> value is equal to 
  ''' a specified object.</summary>
  ''' <returns><c>True</c> if the other parameter is equal to the current <see cref="T:ScpiValue"></see> value; 
  ''' otherwise, <c>False</c>. 
  ''' </returns>
  ''' <param name="value">An object.</param>
  ''' <FilterPriority>1</FilterPriority>
  Overloads Function Equals(ByVal value As T) As Boolean
#End Region
#End If
#End Region