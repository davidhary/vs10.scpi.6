﻿''' <summary>
''' Specifies the contract implemented by SCPI resettable values.
''' </summary>
''' <typeparam name="T"></typeparam>
''' <remarks>
''' This is useful with SCPI values.
''' </remarks>
''' <license>
''' (c) 2011 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="01/27/2011" by="David" revision="6.0.4044.x">
''' Created
''' </history>
Public Interface IResettableValue(Of T As {Structure})
  Inherits IResettable, IScpiValue(Of T), IEquatable(Of IResettableValue(Of T))

#Region " VALUES "

  ''' <summary>
  ''' Gets or sets the value to use when
  ''' <see cref="ClearExecutionState">clearing execution state</see>
  ''' </summary>
  ''' <value>A <see cref="System.Nullable(of T)">nullable value</see></value>
  Property ClearValue() As Nullable(Of T)

  ''' <summary>
  ''' Gets or sets the value to use when
  ''' <see cref="PresetKnownState">presetting to a known state</see>
  ''' </summary>
  ''' <value>A <see cref="System.Nullable(of T)">nullable value</see></value>
  Property PresetValue() As Nullable(Of T)

  ''' <summary>
  ''' Gets or sets the value to use when
  ''' <see cref="ResetKnownState">resetting to a known state</see>
  ''' </summary>
  ''' <value>A <see cref="System.Nullable(of T)">nullable value</see></value>
  Property ResetValue() As Nullable(Of T)

#End Region


End Interface

#Region " UNUSED "
#If False Then
#Region " EQUALS "

  ''' <summary>Indicates whether the current <see cref="T:ResettableValue"></see> value is equal to 
  ''' a specified object.</summary>
  ''' <returns><c>True</c> if the other parameter is equal to the current <see cref="T:ResettableValue"></see> value; 
  ''' otherwise, <c>False</c>. 
  ''' </returns>
  ''' <param name="value">An object.</param>
  ''' <FilterPriority>1</FilterPriority>
  Overloads Function Equals(ByVal value As IResettableValue(Of T)) As Boolean

  ''' <summary>Indicates whether the current <see cref="T:ResettableValue"></see> value is equal to 
  ''' a specified object.</summary>
  ''' <returns><c>True</c> if the other parameter is equal to the current <see cref="T:ResettableValue"></see> value; 
  ''' otherwise, <c>False</c>. 
  ''' </returns>
  ''' <param name="value">An object.</param>
  ''' <FilterPriority>1</FilterPriority>
  Overloads Function Equals(ByVal value As T) As Boolean

#End Region
#End If
#End Region