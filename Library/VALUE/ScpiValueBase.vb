﻿''' <summary>
''' Provides a base implementation of the <see cref="IScpiValueBase">scpi value</see>.
''' </summary>
''' <remarks>
''' This is useful with SCPI values.
''' </remarks>
''' <license>
''' (c) 2011 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="01/27/2011" by="David" revision="6.0.4044.x">
''' Created
''' </history>
Public MustInherit Class ScpiValueBase
  Implements IScpiValueBase

#Region " CONSTRUCTORS  and  DESTRUCTORS "

  Protected Sub New()
    MyBase.new()
    Me._GetterCommand = ""
    Me._SetterCommand = ""
    Me._SetterCommandFormat = ""
  End Sub

#End Region

#Region " EQUALS "

  ''' <summary>Indicates whether the current <see cref="T:ScpiString"></see> value is equal to 
  ''' a specified object.</summary>
  ''' <returns><c>True</c> if the other parameter is equal to the current <see cref="T:ScpiString"></see> value; 
  ''' otherwise, <c>False</c>. 
  ''' </returns>
  ''' <param name="obj">An object.</param>
  ''' <FilterPriority>1</FilterPriority>
  Public MustOverride Overloads Function Equals(ByVal obj As Object) As Boolean Implements IScpiValueBase.Equals

  ''' <summary>Retrieves the hash code of the object returned by the 
  ''' <see cref="P:ScpiString.Value"></see> property.</summary>
  ''' <returns>The hash code of the object returned by the 
  ''' <see cref="P:ScpiString.Value"></see> property if the 
  ''' <see cref="P:ScpiString.Value"></see> is not null, 
  ''' or zero if the <see cref="P:ScpiString.Value"></see> is null. </returns>
  ''' <FilterPriority>1</FilterPriority>
  Public MustOverride Overloads Function GetHashCode() As Integer Implements IScpiValueBase.GetHashCode

#End Region

#Region " COMMANDS "

  Dim _GetterCommand As String
  ''' <summary>
  ''' Gets or sets the getter command.
  ''' </summary>
  ''' <value>
  ''' The getter command.
  ''' </value>
  Public Property GetterCommand() As String Implements IScpiValueBase.GetterCommand
    Get
      Return Me._GetterCommand
    End Get
    Set(ByVal value As String)
      Me._GetterCommand = value
    End Set
  End Property

  Dim _SetterCommand As String
  ''' <summary>
  ''' Gets or sets the setter command.
  ''' </summary>
  ''' <value>
  ''' The setter command.
  ''' </value>
  Public Property SetterCommand() As String Implements IScpiValueBase.SetterCommand
    Get
      Return Me._SetterCommand
    End Get
    Set(ByVal value As String)
      Me._SetterCommand = value
    End Set
  End Property

  Private _SetterCommandFormat As String
  ''' <summary>
  ''' Gets or sets the setter command format.
  ''' </summary>
  ''' <value>
  ''' The setter command format.
  ''' </value>
  Property SetterCommandFormat() As String Implements IScpiValueBase.SetterCommandFormat
    Get
      Return Me._SetterCommandFormat
    End Get
    Set(ByVal value As String)
      Me._SetterCommandFormat = value
    End Set
  End Property

#End Region

#Region " FORMAT "

  ''' <summary>
  ''' Gets the caption for displaying this information.
  ''' </summary>
      Public ReadOnly Property DisplayCaption() As String Implements IScpiValueBase.DisplayCaption
    Get
      Return Me.ToString
    End Get
  End Property

  ''' <summary>Returns the text representation of the value of the current <see cref="T:ScpiString"></see> 
  ''' object.</summary>
  ''' <returns>The text representation of the value of the current <see cref="T:ScpiString"></see> 
  ''' object if the <see cref="P:ScpiString.Value"></see> is nothing, or an empty string ("") if the 
  ''' <see cref="P:ScpiString.Value"></see> is nothing.</returns>
  ''' <FilterPriority>1</FilterPriority>
  Public MustOverride Overloads Function ToString() As String Implements IScpiValueBase.ToString

#End Region

#Region " VALUES "

  ''' <summary>
  ''' Gets a value indicating whether this instance is verified.
  ''' </summary>
  ''' <value>
  ''' 
  ''' <c>True</c> if this instance is verified; otherwise, <c>False</c>.
  ''' 
  ''' </value>
  Public MustOverride ReadOnly Property IsVerified() As Boolean? Implements IScpiValueBase.IsVerified

#End Region

End Class


