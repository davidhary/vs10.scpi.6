''' <summary>
''' Defines an Arm Layer for the Trigger SCPI subsystem
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="10/08/2001" by="David" revision="1.0.646.x">
''' Created
''' </history>
Public Class ArmLayer
    Implements IResettable, isr.Core.IKeyable

    Public Sub New(ByVal syntaxHeader As String)
        MyBase.New()

        Me._syntaxHeader = syntaxHeader

        Me._resettableValues = New ResettableCollection(Of IResettable)
        Me._autoDelay = New ResettableValue(Of Boolean)
        Me._resettableValues.Add(Me._autoDelay)
        Me._bypass = New ResettableValue(Of Boolean)
        Me._resettableValues.Add(Me._bypass)
        Me._count = New ResettableValue(Of Integer)
        Me._resettableValues.Add(Me._count)
        Me._delay = New ResettableDouble
        Me._resettableValues.Add(Me._delay)
        Me._inputLineNumber = New ResettableValue(Of Integer)
        Me._resettableValues.Add(Me._inputLineNumber)
        Me._outputLineNumber = New ResettableValue(Of Integer)
        Me._resettableValues.Add(Me._outputLineNumber)
        Me._source = New ResettableValue(Of ArmSource)
        Me._resettableValues.Add(Me._source)
        Me._timerSeconds = New ResettableDouble
        Me._resettableValues.Add(Me._timerSeconds)

        Me._autoDelay.ResetValue = False
        Me._count.ResetValue = 1
        Me._delay.ResetValue = 0
        Me._bypass.ResetValue = True
        Me._inputLineNumber.ResetValue = 1
        Me._outputLineNumber.ResetValue = 2
        Me._source.ResetValue = Scpi.ArmSource.Immediate
        Me._timerSeconds.ResetValue = 0.001

    End Sub

#Region " IRESETTABLE "

    ''' <summary>
    ''' Holds all resettable values for this function.
    ''' </summary>
    Private _resettableValues As ResettableCollection(Of IResettable)

    ''' <summary>Clears the queues and resets all registers to zero.</summary>
    Public Function ClearExecutionState() As Boolean Implements IResettable.ClearExecutionState
        Return Me._resettableValues.ClearExecutionState()
    End Function

    ''' <summary>Returns subsystem to its preset values.</summary>
    Public Function PresetKnownState() As Boolean Implements IResettable.PresetKnownState
        Return Me._resettableValues.PresetKnownState()
    End Function

    ''' <summary>Restore member properties to the following RST or System Preset values:<para>
    ''' </para>
    ''' </summary>
    Public Function ResetKnownState() As Boolean Implements IResettable.ResetKnownState
        Return Me._resettableValues.ResetKnownState()
    End Function

#End Region

    Private _autoDelay As ResettableValue(Of Boolean)
    ''' <summary>Gets the auto Delay cached value.
    ''' </summary>
    Public Property AutoDelay() As ResettableValue(Of Boolean)
        Get
            Return Me._autoDelay
        End Get
        Set(ByVal value As ResettableValue(Of Boolean))
            Me._autoDelay = value
        End Set
    End Property

    Private _count As ResettableValue(Of Integer)
    ''' <summary>Gets or sets the Count.</summary>
    Public Property Count() As ResettableValue(Of Integer)
        Get
            Return Me._count
        End Get
        Set(ByVal value As ResettableValue(Of Integer))
            Me._count = value
        End Set
    End Property

    Private _delay As ResettableDouble
    ''' <summary>Gets or sets the Delay.</summary>
    Public Property Delay() As ResettableDouble
        Get
            Return Me._delay
        End Get
        Set(ByVal value As ResettableDouble)
            Me._delay = value
        End Set
    End Property

    Private _bypass As ResettableValue(Of Boolean)
    ''' <summary>
    ''' Gets or sets the bypass (Acceptor) or Enable (source) of the arm layer.
    ''' </summary>
    Public Property Bypass() As ResettableValue(Of Boolean)
        Get
            Return Me._bypass
        End Get
        Set(ByVal value As ResettableValue(Of Boolean))
            Me._bypass = value
        End Set
    End Property

    Private _inputLineNumber As ResettableValue(Of Integer)
    ''' <summary>
    ''' Gets or sets the input line number of the active layer.
    ''' </summary>
    Public Property InputLineNumber() As ResettableValue(Of Integer)
        Get
            Return Me._inputLineNumber
        End Get
        Set(ByVal value As ResettableValue(Of Integer))
            Me._inputLineNumber = value
        End Set
    End Property

    Private _outputLineNumber As ResettableValue(Of Integer)
    ''' <summary>
    ''' Gets or sets the output line number of the active layer.
    ''' </summary>
    Public Property OutputLineNumber() As ResettableValue(Of Integer)
        Get
            Return Me._outputLineNumber
        End Get
        Set(ByVal value As ResettableValue(Of Integer))
            Me._outputLineNumber = value
        End Set
    End Property

    Private _source As ResettableValue(Of ArmSource)
    ''' <summary>
    ''' Gets or sets the source immediate, manual, bus, trigger link, 
    ''' start test or stop test ior timer depending on the instrument..
    ''' </summary>
    Public Property Source() As ResettableValue(Of ArmSource)
        Get
            Return Me._source
        End Get
        Set(ByVal value As ResettableValue(Of ArmSource))
            Me._source = value
        End Set
    End Property

    Private _syntaxHeader As String
    ''' <summary>
    ''' Gets or sets the header is used in the scpi commands.
    ''' </summary>
    Public Property SyntaxHeader() As String Implements isr.Core.IKeyable.UniqueKey
        Get
            Return Me._syntaxHeader
        End Get
        Set(ByVal value As String)
            Me._syntaxHeader = value
        End Set
    End Property

    Private _timerSeconds As ResettableDouble
    ''' <summary>
    ''' Requests a programmed timer interval.
    ''' </summary>
    Public Property TimerSeconds() As ResettableDouble
        Get
            Return Me._timerSeconds
        End Get
        Set(ByVal value As ResettableDouble)
            Me._timerSeconds = value
        End Set
    End Property

End Class
