''' <summary>
''' Defines a limit for the SCPI Calculate SCPI subsystem
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="10/08/2001" by="David" revision="1.0.646.x">
''' Created
''' </history>
Public Class CalculateLimit
    Implements IResettable, isr.Core.IKeyable

    Public Sub New(ByVal syntaxHeader As String)
        MyBase.New()
        Me._syntaxHeader = syntaxHeader

        Me._resettableValues = New ResettableCollection(Of IResettable)
        Me._complianceBits = New ResettableValue(Of Integer)
        Me._resettableValues.Add(Me._complianceBits)
        Me._incomplianceFailCondition = New ResettableValue(Of Boolean)
        Me._resettableValues.Add(Me._incomplianceFailCondition)
        Me._failureBits = New ResettableValue(Of Integer)
        Me._resettableValues.Add(Me._failureBits)
        Me._limitFailed = New ResettableValue(Of Boolean)
        Me._resettableValues.Add(Me._limitFailed)
        Me._lowerLimit = New ResettableDouble
        Me._resettableValues.Add(Me._lowerLimit)
        Me._lowerLimitFailureBits = New ResettableValue(Of Integer)
        Me._resettableValues.Add(Me._lowerLimitFailureBits)
        Me._passBits = New ResettableValue(Of Integer)
        Me._resettableValues.Add(Me._passBits)
        Me._state = New ResettableValue(Of Boolean)
        Me._resettableValues.Add(Me._state)
        Me._upperLimit = New ResettableDouble
        Me._resettableValues.Add(Me._upperLimit)
        Me._upperLimitFailureBits = New ResettableValue(Of Integer)()
        Me._resettableValues.Add(Me._upperLimitFailureBits)

        Me.ComplianceBits.ResetValue = 15
        Me.IncomplianceFailCondition.ResetValue = True
        Me.FailureBits.ResetValue = 15
        Me.LowerLimit.ResetValue = -1
        Me.LowerLimitFailureBits.ResetValue = 15
        Me.PassBits.ResetValue = 15
        Me.State.ResetValue = False
        Me.UpperLimit.ResetValue = 1
        Me.UpperLimitFailureBits.ResetValue = 15

    End Sub

#Region " IRESETTABLE "

    ''' <summary>
    ''' Holds all resettable values for this function.
    ''' </summary>
    Private _resettableValues As ResettableCollection(Of IResettable)

    ''' <summary>Clears the queues and resets all registers to zero.</summary>
    Public Function ClearExecutionState() As Boolean Implements IResettable.ClearExecutionState
        Return Me._resettableValues.ClearExecutionState()
    End Function

    ''' <summary>Returns subsystem to its preset values.</summary>
    Public Function PresetKnownState() As Boolean Implements IResettable.PresetKnownState
        Return Me._resettableValues.PresetKnownState()
    End Function

    ''' <summary>Restore member properties to the following RST or System Preset values:<para>
    ''' </para>
    ''' </summary>
    Public Function ResetKnownState() As Boolean Implements IResettable.ResetKnownState
        Return Me._resettableValues.ResetKnownState()
    End Function

#End Region

    Private _complianceBits As ResettableValue(Of Integer)
    ''' <summary>
    '''  Gets or sets the Output fail bit pattern for compliance (15)
    ''' </summary>
    Public Property ComplianceBits() As ResettableValue(Of Integer)
        Get
            Return Me._complianceBits
        End Get
        Set(ByVal value As ResettableValue(Of Integer))
            Me._complianceBits = value
        End Set
    End Property

    Private _incomplianceFailCondition As ResettableValue(Of Boolean)
    ''' <summary>
    ''' Gets or sets the Compliance fail condition (In/Out) (in)
    ''' </summary>
    Public Property IncomplianceFailCondition() As ResettableValue(Of Boolean)
        Get
            Return Me._incomplianceFailCondition
        End Get
        Set(ByVal value As ResettableValue(Of Boolean))
            Me._incomplianceFailCondition = value
        End Set
    End Property

    Private _failureBits As ResettableValue(Of Integer)
    ''' <summary>
    ''' Gets or sets the Output fail bit pattern (15)
    ''' </summary>
    Public Property FailureBits() As ResettableValue(Of Integer)
        Get
            Return Me._failureBits
        End Get
        Set(ByVal value As ResettableValue(Of Integer))
            Me._failureBits = value
        End Set
    End Property

    Private _limitFailed As ResettableValue(Of Boolean)
    ''' <summary>
    '''  Gets or sets the limit failure status.
    ''' </summary>
    Public Property LimitFailed() As ResettableValue(Of Boolean)
        Get
            Return Me._limitFailed
        End Get
        Set(ByVal value As ResettableValue(Of Boolean))
            Me._limitFailed = value
        End Set
    End Property

    Private _lowerLimit As ResettableDouble
    ''' <summary>
    ''' Gets or sets the Lower limit (-1)
    ''' </summary>
    Public Property LowerLimit() As ResettableDouble
        Get
            Return Me._lowerLimit
        End Get
        Set(ByVal value As ResettableDouble)
            Me._lowerLimit = value
        End Set
    End Property

    Private _lowerLimitFailureBits As ResettableValue(Of Integer)
    ''' <summary>
    ''' Gets or sets the Output fail bit pattern for lower limit (15)
    ''' </summary>
    Public Property LowerLimitFailureBits() As ResettableValue(Of Integer)
        Get
            Return Me._lowerLimitFailureBits
        End Get
        Set(ByVal value As ResettableValue(Of Integer))
            Me._lowerLimitFailureBits = value
        End Set
    End Property

    Private _passBits As ResettableValue(Of Integer)
    ''' <summary>
    ''' Gets or sets the Output pass bit pattern (15)
    ''' </summary>
    Public Property PassBits() As ResettableValue(Of Integer)
        Get
            Return Me._passBits
        End Get
        Set(ByVal value As ResettableValue(Of Integer))
            Me._passBits = value
        End Set
    End Property

    Private _state As ResettableValue(Of Boolean)
    ''' <summary>
    ''' Gets or sets the Limit on/off state (off).
    ''' </summary>
    Public Property State() As ResettableValue(Of Boolean)
        Get
            Return Me._state
        End Get
        Set(ByVal value As ResettableValue(Of Boolean))
            Me._state = value
        End Set
    End Property

    Private _syntaxHeader As String
    ''' <summary>
    ''' Gets or sets the header is used in the scpi commands.
    ''' </summary>
    Public Property SyntaxHeader() As String Implements isr.Core.IKeyable.UniqueKey
        Get
            Return Me._syntaxHeader
        End Get
        Set(ByVal value As String)
            Me._syntaxHeader = value
        End Set
    End Property

    Private _upperLimit As ResettableDouble
    ''' <summary>
    ''' Gets or sets the Upper limit (1)
    ''' </summary>
    Public Property UpperLimit() As ResettableDouble
        Get
            Return Me._upperLimit
        End Get
        Set(ByVal value As ResettableDouble)
            Me._upperLimit = value
        End Set
    End Property

    Private _upperLimitFailureBits As ResettableValue(Of Integer)
    ''' <summary>
    ''' Gets or sets the Output fail bit pattern for upper limit (15)
    ''' </summary>
    Public Property UpperLimitFailureBits() As ResettableValue(Of Integer)
        Get
            Return Me._upperLimitFailureBits
        End Get
        Set(ByVal value As ResettableValue(Of Integer))
            Me._upperLimitFailureBits = value
        End Set
    End Property

End Class
