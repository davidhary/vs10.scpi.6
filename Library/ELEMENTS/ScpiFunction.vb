Imports isr.Core
''' <summary>
''' Defines a SCPI function such as current, voltage.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="10/08/2001" by="David" revision="1.0.646.x">
''' Created
''' </history>
Public Class ScpiFunction
    Implements IResettable, IKeyable

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>
    ''' Constructs this class.
    ''' </summary>
    ''' <param name="syntaxHeader">Specifies the sytanx header for the function.</param>
    Private Sub New(ByVal syntaxHeader As String)
        MyBase.New()
        Me._defaultLineFrequency = 60
        Me._syntaxHeader = syntaxHeader
        Me._units = New isr.Core.FormattedUnits()

        Me._resettableValues = New ResettableCollection(Of IResettable)
        Me._autoMode = New ResettableValue(Of Boolean)
        Me._resettableValues.Add(Me._autoMode)
        Me._autoRange = New ResettableValue(Of Boolean)
        Me._resettableValues.Add(Me._autoRange)
        Me._compliance = New ResettableDouble
        Me._resettableValues.Add(Me._compliance)
        Me._level = New ResettableDouble
        Me._resettableValues.Add(Me._level)
        Me._integrationPeriod = New ResettableDouble
        Me._resettableValues.Add(Me._integrationPeriod)
        Me._powerLineCycles = New ResettableDouble
        Me._resettableValues.Add(Me._powerLineCycles)
        Me._protectionLevel = New ResettableDouble
        Me._resettableValues.Add(Me._protectionLevel)
        Me._range = New ResettableDouble
        Me._resettableValues.Add(Me._range)
        Me._startLevel = New ResettableDouble
        Me._resettableValues.Add(Me._startLevel)
        Me._stopLevel = New ResettableDouble
        Me._resettableValues.Add(Me._stopLevel)
        Me._sweepMode = New ResettableValue(Of SweepMode)
        Me._resettableValues.Add(Me._sweepMode)

        Me._integrationPeriod.Units = New isr.Core.FormattedUnits(PhysicalUnit.Time, UnitsScale.None)
        Me._autoMode.ResetValue = True

    End Sub

    ''' <summary>
    ''' Constructs this class.
    ''' </summary>
    ''' <param name="syntaxHeader">Specifies the sytanx header for the function.</param>
    ''' <param name="modality">Defines the <see cref="SenseFunctionModes">SCPI function or element</see>.</param>
    Public Sub New(ByVal syntaxHeader As String, ByVal modality As Scpi.SenseFunctionModes)
        Me.New(syntaxHeader)
        Me._senseModality = modality
        Select Case modality
            Case Scpi.SenseFunctionModes.Current, Scpi.SenseFunctionModes.CurrentAC, Scpi.SenseFunctionModes.CurrentDC
                Me._units = New isr.Core.FormattedUnits(PhysicalUnit.Current, UnitsScale.None)
            Case Scpi.SenseFunctionModes.Continuity
                Me._units = New isr.Core.FormattedUnits(PhysicalUnit.Resistance, UnitsScale.None)
            Case Scpi.SenseFunctionModes.FourWireResistance, Scpi.SenseFunctionModes.Resistance
                Me._units = New isr.Core.FormattedUnits(PhysicalUnit.Resistance, UnitsScale.None)
            Case Scpi.SenseFunctionModes.Temperature
                Me._units = New isr.Core.FormattedUnits(PhysicalUnit.CelsiusTemperature, UnitsScale.None)
            Case Scpi.SenseFunctionModes.Voltage, Scpi.SenseFunctionModes.VoltageAC, Scpi.SenseFunctionModes.VoltageDC
                Me._units = New isr.Core.FormattedUnits(PhysicalUnit.Voltage, UnitsScale.None)
            Case Scpi.SenseFunctionModes.Frequency
                Me._units = New isr.Core.FormattedUnits(PhysicalUnit.Frequency, UnitsScale.None)
            Case Else
                Me._units = New isr.Core.FormattedUnits(PhysicalUnit.None, UnitsScale.None)
        End Select

        Me._level.Units = New isr.Core.FormattedUnits(Me._units)
        Me._protectionLevel.Units = New isr.Core.FormattedUnits(Me._units)
        Me._range.Units = New isr.Core.FormattedUnits(Me._units)
        Me._startLevel.Units = New isr.Core.FormattedUnits(Me._units)
        Me._stopLevel.Units = New isr.Core.FormattedUnits(Me._units)


    End Sub

    ''' <summary>
    ''' Constructs this class.
    ''' </summary>
    ''' <param name="syntaxHeader">Specifies the sytanx header for the function.</param>
    ''' <param name="sourceModality">Defines the <see cref="SourceFunctionMode">SCPI function or element</see>.</param>
    Public Sub New(ByVal syntaxHeader As String, ByVal sourceModality As Scpi.SourceFunctionMode)
        Me.New(syntaxHeader)
        Me._sourceModality = sourceModality
        Me._isSource = True
        Select Case sourceModality
            Case SourceFunctionMode.Current, SourceFunctionMode.CurrentAC, SourceFunctionMode.CurrentDC
                Me._units = New isr.Core.FormattedUnits(PhysicalUnit.Current, UnitsScale.None)
                Me._compliance.Units = New isr.Core.FormattedUnits(PhysicalUnit.Voltage, UnitsScale.None)
            Case SourceFunctionMode.Voltage, SourceFunctionMode.VoltageAC, SourceFunctionMode.VoltageDC
                Me._units = New isr.Core.FormattedUnits(PhysicalUnit.Voltage, UnitsScale.None)
                Me._compliance.Units = New isr.Core.FormattedUnits(PhysicalUnit.Current, UnitsScale.None)
            Case Else
                Me._units = New isr.Core.FormattedUnits(PhysicalUnit.None, UnitsScale.None)
        End Select

        Me._level.Units = New isr.Core.FormattedUnits(Me._units)
        Me._protectionLevel.Units = New isr.Core.FormattedUnits(Me._units)
        Me._range.Units = New isr.Core.FormattedUnits(Me._units)
        Me._startLevel.Units = New isr.Core.FormattedUnits(Me._units)
        Me._stopLevel.Units = New isr.Core.FormattedUnits(Me._units)

    End Sub

#End Region

#Region " IRESETTABLE "

    ''' <summary>
    ''' Holds all resettable values for this function.
    ''' </summary>
    Private _resettableValues As Scpi.ResettableCollection(Of Scpi.IResettable)

    ''' <summary>Clears the queues and resets all registers to zero.</summary>
    Public Function ClearExecutionState() As Boolean Implements IResettable.ClearExecutionState
        Return Me._resettableValues.ClearExecutionState()
    End Function

    ''' <summary>Returns subsystem to its preset values.</summary>
    Public Function PresetKnownState() As Boolean Implements IResettable.PresetKnownState
        Return Me._resettableValues.PresetKnownState()
    End Function

    ''' <summary>Restore member properties to the following RST or System Preset values:<para>
    ''' </para>
    ''' </summary>
    Public Function ResetKnownState() As Boolean Implements IResettable.ResetKnownState
        Return Me._resettableValues.ResetKnownState()
    End Function

#End Region

#Region " MANAGEMENT VALUES "

    Private _isSource As Boolean
    Public ReadOnly Property IsSourceFunction() As Boolean
        Get
            Return Me._isSource
        End Get
    End Property

    Private _defaultLineFrequency As Integer
    ''' <summary>
    ''' Gets or sets the power line frequency.
    ''' </summary>
    Public ReadOnly Property LineFrequency() As Double
        Get
            If SystemSubsystem.SystemLineFrequency.HasValue Then
                Return SystemSubsystem.SystemLineFrequency.Value
            Else
                Return Me._defaultLineFrequency
            End If
        End Get
    End Property

    Private _senseModality As Scpi.SenseFunctionModes
    Public ReadOnly Property SenseModality() As Scpi.SenseFunctionModes
        Get
            Return Me._senseModality
        End Get
    End Property

    Private _sourceModality As Scpi.SourceFunctionMode
    Public ReadOnly Property SourceModality() As Scpi.SourceFunctionMode
        Get
            Return Me._sourceModality
        End Get
    End Property

    ' the header is used in the scpi commands.
    Private _syntaxHeader As String
    Public Property SyntaxHeader() As String Implements IKeyable.UniqueKey
        Get
            Return Me._syntaxHeader
        End Get
        Set(ByVal value As String)
            Me._syntaxHeader = value
        End Set
    End Property

    Private _units As isr.Core.FormattedUnits
    ''' <summary>
    ''' Gets or sets the units for this function.
    ''' </summary>
    Public Property Units() As isr.Core.FormattedUnits
        Get
            Return Me._units
        End Get
        Set(ByVal value As isr.Core.FormattedUnits)
            Me._units = value
        End Set
    End Property

#End Region

#Region " INSTRUMENT VALUES "

    Private _autoMode As ResettableValue(Of Boolean)
    ''' <summary>Gets or sets the condition for auto current Mode.</summary>
    Public Property AutoMode() As ResettableValue(Of Boolean)
        Get
            Return Me._autoMode
        End Get
        Set(ByVal value As ResettableValue(Of Boolean))
            Me._autoMode = value
        End Set
    End Property

    Private _autoRange As ResettableValue(Of Boolean)
    ''' <summary>Gets or sets the condition for auto current range.</summary>
    Public Property AutoRange() As ResettableValue(Of Boolean)
        Get
            Return Me._autoRange
        End Get
        Set(ByVal value As ResettableValue(Of Boolean))
            Me._autoRange = value
        End Set
    End Property

    Private _compliance As ResettableDouble
    Public Property Compliance() As ResettableDouble
        Get
            Return Me._compliance
        End Get
        Set(ByVal value As ResettableDouble)
            Me._compliance = value
        End Set
    End Property

    Private _level As ResettableDouble
    Public Property Level() As ResettableDouble
        Get
            Return Me._level
        End Get
        Set(ByVal value As ResettableDouble)
            Me._level = value
        End Set
    End Property

    ' the function sweep mode, e.g., Fixed, List, Sweep
    Private _sweepMode As ResettableValue(Of SweepMode)
    Public Property SweepMode() As ResettableValue(Of SweepMode)
        Get
            Return Me._sweepMode
        End Get
        Set(ByVal value As ResettableValue(Of SweepMode))
            Me._sweepMode = value
            Me._sweepModeCaption = Syntax.ExtractScpi(value.Value)
        End Set
    End Property

    Private _sweepModeCaption As String
    Public Property SweepModeCaption() As String
        Get
            Return Me._sweepModeCaption
        End Get
        Set(ByVal value As String)
            Me._sweepModeCaption = value
            Dim se As New StringEnumerator(Of SweepMode)
            Me._sweepMode.Value = se.ParseContained(Syntax.DelimitedScpiCommand(Me._sweepModeCaption))
        End Set
    End Property

    Private _integrationPeriod As ResettableDouble
    Public Property IntegrationPeriod() As ResettableDouble
        Get
            Return Me._integrationPeriod
        End Get
        Set(ByVal value As ResettableDouble)
            Me._integrationPeriod = value
        End Set
    End Property

    Private _powerLineCycles As ResettableDouble
    Public Property PowerLineCycles() As ResettableDouble
        Get
            Return Me._powerLineCycles
        End Get
        Set(ByVal value As ResettableDouble)
            Me._powerLineCycles = value
            Me._integrationPeriod.Value = Me._powerLineCycles.Value.Value / Me.LineFrequency
        End Set
    End Property

    Private _protectionLevel As ResettableDouble
    ''' <summary>
    ''' Gets or sets the protection level.
    ''' The import of this function depends on the instrument.
    ''' For instance, the protection level for the 2400 voltage source sets 
    ''' a limit on the maximum voltage.  
    ''' </summary>
    Public Property ProtectionLevel() As ResettableDouble
        Get
            Return Me._protectionLevel
        End Get
        Set(ByVal value As ResettableDouble)
            Me._protectionLevel = value
        End Set
    End Property

    Private _range As ResettableDouble
    Public Property Range() As ResettableDouble
        Get
            Return Me._range
        End Get
        Set(ByVal value As ResettableDouble)
            Me._range = value
            Me._autoRange.Value = False
        End Set
    End Property

    Private _startLevel As ResettableDouble
    ''' <summary>Gets or sets the source start level.</summary>
    Public Property StartLevel() As ResettableDouble
        Get
            Return Me._startLevel
        End Get
        Set(ByVal value As ResettableDouble)
            Me._startLevel = value
        End Set
    End Property

    Private _stopLevel As ResettableDouble
    ''' <summary>Gets or sets the source sweep start level.</summary>
    Public Property StopLevel() As ResettableDouble
        Get
            Return Me._stopLevel
        End Get
        Set(ByVal value As ResettableDouble)
            Me._stopLevel = value
        End Set
    End Property

#End Region

End Class


