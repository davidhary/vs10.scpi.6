''' <summary>Defines a SCPI Trigger Subsystem.</summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="01/15/05" by="David" revision="1.0.1841.x">
''' Created
''' </history>
Public Class TriggerSubsystem
    Inherits Subsystem

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructs this class.</summary>
    ''' <param name="port">Reference to an open <see cref="Scpi.IPort">SCPI Port IO</see>.</param>
    Public Sub New(ByVal port As Scpi.IPort)

        ' instantiate the base class
        MyBase.New(ScpiSyntaxHeader, port)

        Me._autoDelay = New ResettableValue(Of Boolean)
        MyBase.ResettableValues.Add(Me._autoDelay)

        Me._count = New ResettableValue(Of Integer)
        MyBase.ResettableValues.Add(Me._count)

        Me._delay = New ResettableDouble
        MyBase.ResettableValues.Add(Me._delay)

        Me._bypass = New ResettableValue(Of Boolean)
        MyBase.ResettableValues.Add(Me._bypass)

        Me._inputEvent = New ResettableValue(Of TriggerEvent)
        MyBase.ResettableValues.Add(Me._inputEvent)

        Me._inputLineNumber = New ResettableValue(Of Integer)
        MyBase.ResettableValues.Add(Me._inputLineNumber)

        Me._outputEvent = New ResettableValue(Of TriggerEvent)
        MyBase.ResettableValues.Add(Me._outputEvent)

        Me._outputLineNumber = New ResettableValue(Of Integer)
        MyBase.ResettableValues.Add(Me._outputLineNumber)

        Me._source = New ResettableValue(Of ArmSource)
        MyBase.ResettableValues.Add(Me._source)

        Me._timerSeconds = New ResettableDouble
        MyBase.ResettableValues.Add(Me._timerSeconds)

    End Sub

    ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
    ''' <param name="disposing">True if this method releases both managed and unmanaged 
    '''   resources; False if this method releases only unmanaged resources.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by
    '''   its disposing parameter.  If True, the method has been called directly or 
    '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
    '''   If disposing equals False, the method has been called by the 
    '''   runtime from inside the finalizer and you should not reference 
    '''   other objects--only unmanaged resources can be disposed.</remarks>
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)

        If Not MyBase.IsDisposed Then

            Try

                If disposing Then

                    ' Free managed resources when explicitly called


                End If

                ' Free shared unmanaged resources
                ' onDisposeUnmanagedResources
            Finally

                ' Invoke the base class dispose method
                MyBase.Dispose(disposing)

            End Try

        End If

    End Sub

#End Region

#Region " SHARED "

    ''' <summary>
    ''' The SCPI Syntax Header for this subsystem.
    ''' </summary>
    Public Const ScpiSyntaxHeader As String = "TRIG"

    ''' <summary>Initiates operation.</summary>
    ''' <param name="port">Reference to a <see cref="Scpi.IPort">SCPI Port IO</see></param>
    ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
    Public Shared Sub Initiate(ByVal port As Scpi.IPort)

        If port Is Nothing Then
            Throw New ArgumentNullException("port")
        End If
        port.WriteLine(":INIT")

    End Sub

    ''' <summary>Initiates operation.</summary>
    ''' <param name="port">Reference to a <see cref="Scpi.IPort">SCPI Port IO</see></param>
    ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
    Public Shared Sub Abort(ByVal port As Scpi.IPort)

        If port Is Nothing Then
            Throw New ArgumentNullException("port")
        End If
        port.WriteLine(":ABOR")

    End Sub

#End Region

#Region " IRESETTABLE "

    ''' <summary>Clears the queues and resets all registers to zero.
    ''' Sets the subsystem properties to the following CLS default values:<para>
    ''' 
    ''' </para>
    ''' </summary>
    Public Overrides Function ClearExecutionState() As Boolean
        Return MyBase.ClearExecutionState()
    End Function

    ''' <summary>
    ''' Sets subsystem to the following default system preset values:<para>
    ''' </para>
    ''' </summary>
    Public Overrides Function PresetKnownState() As Boolean
        Return MyBase.PresetKnownState()
    End Function

    ''' <summary>Restore member properties to the following RST or System Reset values:<para>
    ''' </para>
    ''' </summary>
    ''' <history>
    ''' </history>
    Public Overrides Function ResetKnownState() As Boolean
        Return MyBase.ResetKnownState()
    End Function

#End Region

#Region " PROPERTIES "

    Private _autoDelay As ResettableValue(Of Boolean)
    ''' <summary>Gets the auto Delay cached value.
    ''' </summary>
    Public ReadOnly Property AutoDelay() As ResettableValue(Of Boolean)
        Get
            Return Me._autoDelay
        End Get
    End Property

    ''' <summary>Gets or sets the condition for auto current Delay.
    ''' </summary>
    Public Property AutoDelay(ByVal access As ResourceAccessLevels) As Boolean
        Get
            MyBase.Controller.Getter(Me._autoDelay, Syntax.AutoDelayCommand, Core.EnumeratedBooleanBase.OnOff, access)
            Return Me._autoDelay.Value.Value
        End Get
        Set(ByVal value As Boolean)
            MyBase.Controller.Setter(Me._autoDelay, Syntax.AutoDelayCommand, value, Core.EnumeratedBooleanBase.OnOff, access)
        End Set
    End Property

    Private _count As ResettableValue(Of Integer)
    Public ReadOnly Property Count() As ResettableValue(Of Integer)
        Get
            Return Me._count
        End Get
    End Property
    ''' <summary>Gets or sets the condition for auto current Count.</summary>
    Public Property Count(ByVal access As ResourceAccessLevels) As Integer
        Get
            MyBase.Controller.Getter(Me._count, Syntax.CountCommand, access)
            Return Me._count.Value.Value
        End Get
        Set(ByVal value As Integer)
            MyBase.Controller.Setter(Me._count, Syntax.CountCommand, value, access)
        End Set
    End Property

    Private _delay As ResettableDouble
    Public ReadOnly Property Delay() As ResettableDouble
        Get
            Return Me._delay
        End Get
    End Property
    ''' <summary>Gets or sets the condition for auto current Delay.</summary>
    Public Property Delay(ByVal access As ResourceAccessLevels) As Double
        Get
            MyBase.Controller.Getter(Me._delay, Syntax.DelayCommand, access)
            Return Me._delay.Value.Value
        End Get
        Set(ByVal value As Double)
            MyBase.Controller.Setter(Me._delay, Syntax.DelayCommand, value, access)
        End Set
    End Property

    Private _bypass As ResettableValue(Of Boolean)
    Public ReadOnly Property Bypass() As ResettableValue(Of Boolean)
        Get
            Return Me._bypass
        End Get
    End Property
    ''' <summary>
    ''' Gets or sets the bypass (Acceptor) or Enable (source) of the arm layer.
    ''' </summary>
    ''' <param name="access"></param>
    Public Property Bypass(ByVal access As ResourceAccessLevels) As Boolean
        Get
            MyBase.Controller.Getter(Me._bypass, Syntax.Direction, isr.Scpi.Syntax.AcceptorSource, access)
            Return Me._bypass.Value.Value
        End Get
        Set(ByVal Value As Boolean)
            MyBase.Controller.Setter(Me._bypass, Syntax.Direction, Value, isr.Scpi.Syntax.AcceptorSource, access)
        End Set
    End Property

    Private _inputEvent As ResettableValue(Of TriggerEvent)
    Public ReadOnly Property InputEvent() As ResettableValue(Of TriggerEvent)
        Get
            Return Me._inputEvent
        End Get
    End Property

    ''' <summary>
    ''' Gets or sets the InputEvent e.g., SOURce, DELay, SENSe, NONE.
    ''' </summary>
    ''' <param name="access"></param>
    Public Property InputEvent(ByVal access As ResourceAccessLevels) As TriggerEvent
        Get
            Me.InputEvent.Value = Syntax.ParseTriggerEvent(MyBase.Controller.Getter(Syntax.Input,
                                                              Syntax.ExtractScpi(Me._inputEvent.Value),
                                                              access))
            Me.InputEvent.ActualValue = Me.InputEvent.Value.Value
            Return Me.InputEvent.Value.Value
        End Get
        Set(ByVal Value As TriggerEvent)
            Me._inputEvent.Value = Syntax.ParseTriggerEvent(MyBase.Controller.Setter(Syntax.Input,
                                                Syntax.ExtractScpi(Me._inputEvent.Value),
                                                 Syntax.ExtractScpi(Value),
                                                 access))
        End Set
    End Property

    Private _inputLineNumber As ResettableValue(Of Integer)
    Public ReadOnly Property InputLineNumber() As ResettableValue(Of Integer)
        Get
            Return Me._inputLineNumber
        End Get
    End Property
    ''' <summary>
    ''' Gets or sets the input line number of the active layer.
    ''' </summary>
    ''' <param name="access"></param>
    Public Property InputLineNumber(ByVal access As ResourceAccessLevels) As Integer
        Get
            MyBase.Controller.Getter(Me._inputLineNumber, Syntax.InputLine, access)
            Return Me._inputLineNumber.Value.Value
        End Get
        Set(ByVal Value As Integer)
            MyBase.Controller.Setter(Me._inputLineNumber, Syntax.InputLine, Value, access)
        End Set
    End Property

    Private _outputEvent As ResettableValue(Of TriggerEvent)
    Public ReadOnly Property OutputEvent() As ResettableValue(Of TriggerEvent)
        Get
            Return Me._outputEvent
        End Get
    End Property

    ''' <summary>
    ''' Gets or sets the OutputEvent e.g., SOURce, DELay, SENSe, NONE.
    ''' </summary>
    ''' <param name="access"></param>
    Public Property OutputEvent(ByVal access As ResourceAccessLevels) As TriggerEvent
        Get
            Me.OutputEvent.Value = Syntax.ParseTriggerEvent(MyBase.Controller.Getter(Syntax.Output,
                                                                Syntax.ExtractScpi(Me.OutputEvent.Value), access))
            Me.OutputEvent.ActualValue = Me.OutputEvent.Value.Value
            Return Me.OutputEvent.Value.Value
        End Get
        Set(ByVal Value As TriggerEvent)
            Me.OutputEvent.Value = Syntax.ParseTriggerEvent(MyBase.Controller.Setter(Syntax.Output,
                                                               Syntax.ExtractScpi(Me.OutputEvent.Value),
                                                               Syntax.ExtractScpi(Value), access))
        End Set
    End Property

    Private _outputLineNumber As ResettableValue(Of Integer)
    Public ReadOnly Property OutputLineNumber() As ResettableValue(Of Integer)
        Get
            Return Me._outputLineNumber
        End Get
    End Property

    ''' <summary>
    ''' Gets or sets the output line number of the active layer.
    ''' </summary>
    ''' <param name="access"></param>
    Public Property OutputLineNumber(ByVal access As ResourceAccessLevels) As Integer
        Get
            MyBase.Controller.Getter(Me.OutputLineNumber, Syntax.OutputLine, access)
            Return Me.OutputLineNumber.Value.Value
        End Get
        Set(ByVal Value As Integer)
            MyBase.Controller.Setter(Me.OutputLineNumber, Syntax.OutputLine, Value, access)
        End Set
    End Property

    Private _source As ResettableValue(Of ArmSource)
    Public ReadOnly Property Source() As ResettableValue(Of ArmSource)
        Get
            Return Me._source
        End Get
    End Property

    ''' <summary>
    ''' Gets or sets the source immediate, manual, bus, trigger link, 
    ''' start test or stop test ior timer depending on the instrument..
    ''' </summary>
    ''' <param name="access"></param>
    Public Property Source(ByVal access As ResourceAccessLevels) As ArmSource
        Get
            Me.Source.Value = Syntax.ParseArmSource(MyBase.Controller.Getter(Syntax.Source,
                                                                      Syntax.ExtractScpi(Me.Source.Value), access))
            Me.Source.ActualValue = Me.Source.Value.Value
            Return Me.Source.Value.Value
        End Get
        Set(ByVal Value As ArmSource)
            Me.Source.Value = Syntax.ParseArmSource(MyBase.Controller.Setter(Syntax.Source,
                                                                      Syntax.ExtractScpi(Me.Source.Value),
                                                                      Syntax.ExtractScpi(Value), access))
        End Set
    End Property

    Private _timerSeconds As ResettableDouble
    Public ReadOnly Property TimerSeconds() As ResettableDouble
        Get
            Return Me._timerSeconds
        End Get
    End Property
    ''' <summary>
    ''' Requests a programmed timer interval.
    ''' </summary>
    Public Property TimerSeconds(ByVal access As ResourceAccessLevels) As Double
        Get
            MyBase.Controller.Getter(Me._timerSeconds, Syntax.Timer, access)
            Return Me._timerSeconds.Value.Value
        End Get
        Set(ByVal value As Double)
            MyBase.Controller.Setter(Me._timerSeconds, Syntax.Timer, value, access)
        End Set
    End Property

#End Region

#Region " METHODS "

    ''' <summary>Initiates operation.</summary>
    ''' <returns>True if success or false if error.</returns>
    Public Function Abort() As Boolean

        TriggerSubsystem.Abort(MyBase.Controller.Port)
        Return Not MyBase.Controller.Port.HadError

    End Function

    ''' <summary>Clears any pending triggers.</summary>
    ''' <returns>True if success or false if error.</returns>
    Public Function ClearTriggers() As Boolean

        MyBase.Controller.Execute(Syntax.ClearCommand)
        Return Not MyBase.Controller.Port.HadError

    End Function

    ''' <summary>Initiates operation.</summary>
    ''' <returns>True if success or false if error.</returns>
    Public Function Initiate() As Boolean

        TriggerSubsystem.Initiate(MyBase.Controller.Port)
        Return Not MyBase.Controller.Port.HadError

    End Function

#End Region

End Class
