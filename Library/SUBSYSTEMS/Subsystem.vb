''' <summary>Defines a SCPI Base Subsystem.</summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="01/15/05" by="David" revision="1.0.1841.x">
''' Created
''' </history>
Public MustInherit Class Subsystem

  Implements IDisposable, IResettable, ISubsystem

#Region " CONSTRUCTORS  and  DESTRUCTORS "

  ''' <summary>
  ''' Constructor for a sub system.
  ''' </summary>
  ''' <param name="syntaxHeader">Specifies the system name or syntax header.</param>
  ''' <param name="instrument">Reference to the interface to the instrument.</param>
    Protected Sub New(ByVal syntaxHeader As String, ByVal instrument As Scpi.IPort)

    ' instantiate the base class
    MyBase.New()

    Me._controller = New Controller(instrument)
    Me._controller.SyntaxHeader = syntaxHeader
    Me._resettableValues = New Scpi.ResettableCollection(Of Scpi.IResettable)

  End Sub

  ''' <summary>Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup.</summary>
  ''' <remarks>Do not make this method Overridable (virtual) because a derived 
  '''   class should not be able to override this method.</remarks>
  Public Sub Dispose() Implements IDisposable.Dispose

    ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

    ' this disposes all child classes.
    Dispose(True)

    ' Take this object off the finalization(Queue) and prevent finalization code 
    ' from executing a second time.
    GC.SuppressFinalize(Me)

  End Sub

        ''' <summary>
        ''' Gets or sets the dispose status sentinel of the base class.  This applies to the derived class
        ''' provided proper implementation.
        ''' </summary>
    Public Property IsDisposed() As Boolean

 
  ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
  ''' <param name="disposing">True if this method releases both managed and unmanaged 
  '''   resources; False if this method releases only unmanaged resources.</param>
  ''' <remarks>Executes in two distinct scenarios as determined by
  '''   its disposing parameter.  If True, the method has been called directly or 
  '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
  '''   If disposing equals False, the method has been called by the 
  '''   runtime from inside the finalizer and you should not reference 
  '''   other objects--only unmanaged resources can be disposed.</remarks>
  Protected Overridable Sub Dispose(ByVal disposing As Boolean)

    If Not Me.IsDisposed Then

      Try

        If disposing Then

          ' Free managed resources when explicitly called
          If Me._controller IsNot Nothing Then
            Me._controller.Dispose()
            Me._controller = Nothing
          End If

        End If

        ' Free shared unmanaged resources

      Finally

        ' set the sentinel indicating that the class was disposed.
        Me.IsDisposed = True

      End Try

    End If

  End Sub

  ''' <summary>This destructor will run only if the Dispose method 
  '''   does not get called. It gives the base class the opportunity to 
  '''   finalize. Do not provide destructors in types derived from this class.</summary>
    Protected Overrides Sub Finalize()
        Try
            ' Do not re-create Dispose clean-up code here.
            ' Calling Dispose(false) is optimal for readability and maintainability.
            Dispose(False)
        Finally
            ' The compiler automatically adds a call to the base class finalizer 
            ' that satisfies the rule: FinalizersShouldCallBaseClassFinalizer.
            MyBase.Finalize()
        End Try
    End Sub

#End Region

#Region " I SUBSYSTEM "

  Private _controller As IController
  ''' <summary>
  ''' Gets reference to the SCPI controller for accessing the acutal instrument.
  ''' </summary>
      Public ReadOnly Property Controller() As IController Implements ISubsystem.Controller
    Get
      Return Me._controller
    End Get
  End Property

  ''' <summary>
  ''' Gets the syntax header.
  ''' </summary>
  ''' 
  Public ReadOnly Property SyntaxHeader() As String
    Get
      Return Me.Controller.SyntaxHeader
    End Get
  End Property

#End Region

#Region " IRESETTABLE "

  Private _resettableValues As Scpi.ResettableCollection(Of Scpi.IResettable)
  ''' <summary>
  ''' Holds all resettable values for this subsystem.
  ''' </summary>
    Protected ReadOnly Property ResettableValues() As Scpi.ResettableCollection(Of Scpi.IResettable)
    Get
      Return Me._resettableValues
    End Get
  End Property

  ''' <summary>Clears the queues and resets all registers to zero.
  ''' Sets the subsystem properties to the CLS default values.
  ''' </summary>
  Public Overridable Function ClearExecutionState() As Boolean Implements IResettable.ClearExecutionState
    Return Me._resettableValues.ClearExecutionState()
  End Function

  ''' <summary>
  ''' Sets subsystem to its default system preset values.
  ''' </summary>
  Public Overridable Function PresetKnownState() As Boolean Implements IResettable.PresetKnownState
    Return Me._resettableValues.PresetKnownState()
  End Function

  ''' <summary>Returns the subsystem to its default known state by setting properties
  '''   to default values.</summary>
  Public Overridable Function ResetKnownState() As Boolean Implements IResettable.ResetKnownState
    Return Me._resettableValues.ResetKnownState()
  End Function

#End Region

End Class

#Region " UNUSED "
#If False Then
#Region " EXECUTE "

  ''' <summary>
  ''' Executes a command based on the <paramref name="command">SCPI command</paramref> syntax.
  ''' </summary>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}' where 
  ''' the first item is the <see cref="Name">SCPI system name</see> and 
  ''' the second item is a <paramref name="command">command</paramref>.
  ''' </param>
    Public Sub Execute(ByVal subHeader As String)
    Me.Controller.Port.WriteLine(Syntax.BuildExecute(MyBase.SyntaxHeader, subHeader))
  End Sub

  ''' <summary>
  ''' Executes a command based on the <paramref name="command">SCPI command</paramref> syntax.
  ''' </summary>
  ''' <param name="modalityName">Specifies the modality name.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2}' where 
  ''' the first item is the <see cref="Name">SCPI system name</see>, 
  ''' the second item is the <paramref name="modalityName">modality name</paramref> and 
  ''' the second item is a <paramref name="command">command</paramref>.
  ''' </param>
    Public Sub Execute(ByVal modalityName As String, ByVal subHeader As String)
    Me.Controller.Port.WriteLine(Syntax.BuildExecute(MyBase.SyntaxHeader, modalityName, subHeader))
  End Sub

  Public Sub Execute(ByVal modalityName As String, ByVal subHeader As String, ByVal isVerifyOperationComplete As Boolean, ByVal isRaiseDeviceErrors As Boolean)
    Me.Controller.Port.WriteLine(Syntax.BuildExecute(MyBase.SyntaxHeader, modalityName, subHeader), isVerifyOperationComplete, isRaiseDeviceErrors)
  End Sub

#End Region

#Region " STRING "

#Region " QUERRIES "

  ''' <summary>
  ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
  ''' </summary>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}?' where 
  ''' the first item is the <see cref="Name">SCPI system name</see> and 
  ''' the second item is a <paramref name="command">command</paramref>.
  ''' </param>
      Public Function QueryTrimEnd(ByVal subHeader As String) As String
    Return Me.Controller.QueryTrimEnd(Syntax.BuildQuery(MyBase.SyntaxHeader, subHeader))
  End Function

  ''' <summary>
  ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
  ''' </summary>
  ''' <param name="modalityName">Specifies the modality name.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2}?' where 
  ''' the first item is the <see cref="Name">SCPI system name</see>, 
  ''' the second item is the <paramref name="modalityName">modality name</paramref> and 
  ''' the thrid item is a <paramref name="command">command</paramref>.
  ''' </param>
      Public Function QueryTrimEnd(ByVal modalityName As String, ByVal subHeader As String) As String
    Return Me.Controller.QueryTrimEnd(Syntax.BuildQuery(MyBase.SyntaxHeader, modalityName, subHeader))
  End Function

#End Region

#Region " GETTERS w/ ACCESS "

  ''' <summary>
  ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
  ''' </summary>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}?' where 
  ''' the first item is the <see cref="Name">SCPI system name</see> and 
  ''' the second item is a <paramref name="command">command</paramref>
  ''' </param>
  ''' <param name="existingValue">The existing value to use for forcing instrument access if null or empty.</param>
  ''' <param name="access">The access <see cref="isr.Scpi.ResourceAccessLevels">level</see> to the instrument.</param>
      Public Function Getter(ByVal subHeader As String, 
                         ByVal existingValue As String, 
                         ByVal access As ResourceAccessLevels) As String
    If access.IsDeviceAccess OrElse String.IsNullOrWhiteSpace(existingValue) Then
      existingValue = Me.QueryTrimEnd(subHeader)
    End If
    Return existingValue
  End Function

  ''' <summary>
  ''' Gets a value from the instrument <see cref="Name">subsystem</see> <paramref name="modalityName">modality</paramref> and <paramref name="subHeader">element</paramref>.
  ''' </summary>
  ''' <param name="modalityName">Specifies the modality name.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2}?' where 
  ''' the first item is the <see cref="Name">SCPI system name</see>, 
  ''' the second item is the <paramref name="modalityName">modality name</paramref> and 
  ''' the third item is a <paramref name="command">command</paramref>
  ''' </param>
  ''' <param name="existingValue">The existing value to use for forcing instrument access if null or empty.</param>
      Public Function Getter(ByVal modalityName As String, ByVal subHeader As String, 
                         ByVal existingValue As String, 
                         ByVal access As ResourceAccessLevels) As String
    If access.IsDeviceAccess OrElse String.IsNullOrWhiteSpace(existingValue) Then
      existingValue = Me.QueryTrimEnd(modalityName, subHeader)
    End If
    Return existingValue
  End Function

#End Region

#Region " GETTERS w/ ACCESS w/ ELEMENT  "

  ''' <summary>
  ''' Gets a value from the instrument <see cref="Name">subsystem</see> <paramref name="subHeader">element</paramref>.
  ''' Uses or updates the nullable value getter command.
  ''' </summary>
  ''' <param name="element">Specifies the <see cref="IScpiValueBase">existing element</see> where the getter command is stored.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}?' where 
  ''' the first item is the <see cref="Name">SCPI system name</see> and 
  ''' the second item is a <paramref name="command">command</paramref>
  ''' </param>
  ''' <param name="existingValue">The existing value to use for forcing instrument access if null or empty.</param>
  ''' <param name="access">Specifies the desired access level to the instrument.</param>
      Public Function Getter(ByVal element As IScpiValueBase, ByVal subHeader As String, 
                         ByVal existingValue As String, 
                         ByVal access As ResourceAccessLevels) As String
    If access.IsDeviceAccess OrElse String.IsNullOrWhiteSpace(existingValue) Then
      If String.IsNullOrWhiteSpace(element.GetterCommand) Then
        element.GetterCommand = Syntax.BuildQuery(MyBase.SyntaxHeader, subHeader)
      End If
      existingValue = Me.Controller.QueryTrimEnd(element.GetterCommand)
      ' value = Me.QueryTrimEnd(subHeader)
    End If
    Return existingValue
  End Function

  ''' <summary>
  ''' Gets a value from the instrument <see cref="Name">subsystem</see> <paramref name="modalityName">modality</paramref> and <paramref name="subHeader">element</paramref>.
  ''' </summary>
  ''' <param name="element">Specifies the <see cref="IScpiValueBase">existing element</see> where the getter command is stored.</param>
  ''' <param name="modalityName">Specifies the modality name.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2}?' where 
  ''' the first item is the <see cref="Name">SCPI system name</see>, 
  ''' the second item is the <paramref name="modalityName">modality name</paramref> and 
  ''' the third item is a <paramref name="command">command</paramref>
  ''' </param>
  ''' <param name="existingValue">The existing value to use for forcing instrument access if null or empty.</param>
      Public Function Getter(ByVal element As IScpiValueBase, ByVal modalityName As String, ByVal subHeader As String, 
                         ByVal existingValue As String, 
                         ByVal access As ResourceAccessLevels) As String
    If access.IsDeviceAccess OrElse String.IsNullOrWhiteSpace(existingValue) Then
      If String.IsNullOrWhiteSpace(element.GetterCommand) Then
        element.GetterCommand = Syntax.BuildQuery(MyBase.SyntaxHeader, modalityName, subHeader)
      End If
      ' value = Me.QueryTrimEnd(modalityName, subHeader)
      existingValue = Me.Controller.QueryTrimEnd(element.GetterCommand)
    End If
    Return existingValue
  End Function

#End Region

#Region " GETTERS w/ ACCESS w/ SCPI VALUE "

  ''' <summary>
  ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
  ''' Uses or updates the nullable value getter command.
  ''' </summary>
  ''' <param name="element">Specifies the <see cref="IScpiValueBase">existing element</see> where the getter command is stored.</param>
  ''' <param name="access">The desired <see cref="ResourceAccessLevels">access level</see> to the instrument.</param>
      Public Function Getter(ByVal element As IScpiString, ByVal access As ResourceAccessLevels) As IScpiString
    If access.IsDeviceAccess OrElse String.IsNullOrWhiteSpace(element.Value) Then
      element.Value = Me.Controller.QueryTrimEnd(element.GetterCommand)
      element.ActualValue = element.Value
    End If
    Return element
  End Function

  ''' <summary>
  ''' Gets the instrument <see cref="Name">subsystem</see> <paramref name="subHeader">element</paramref> value.
  ''' Applies readings to the <paramref name="element">existing element</paramref>
  ''' </summary>
  ''' <param name="element">Specifies the existing element where the instrument readings are applied.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}?' where 
  ''' the first item is the <see cref="Name">SCPI system name</see> and 
  ''' the second item is a <paramref name="command">command</paramref>
  ''' </param>
  ''' <param name="access">The desired <see cref="ResourceAccessLevels">access level</see> to the instrument.</param>
      Public Function Getter(ByVal element As IScpiString, ByVal subHeader As String, ByVal access As ResourceAccessLevels) As IScpiString
    element.Value = MyBase.controller.Getter(CType(element, IScpiValueBase), subHeader, element.Value, access)
    element.ActualValue = element.Value
    Return element
  End Function

  ''' <summary>
  ''' Gets the instrument <see cref="Name">subsystem</see> <paramref name="modalityName">modality</paramref> <paramref name="subHeader">element</paramref> value.
  ''' Applies readings to the <paramref name="element">existing element</paramref>
  ''' </summary>
  ''' <param name="element">Specifies the existing element where the instrument readings are applied.</param>
  ''' <param name="modalityName">Specifies the modality name.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2}?' where 
  ''' the first item is the <see cref="Name">SCPI system name</see>, 
  ''' the second item is the <paramref name="modalityName">modality name</paramref> and 
  ''' the third item is a <paramref name="command">command</paramref>
  ''' </param>
  ''' <param name="access">The desired <see cref="ResourceAccessLevels">access level</see> to the instrument.</param>
      Public Function Getter(ByVal element As IScpiString, ByVal modalityName As String, ByVal subHeader As String, 
                         ByVal access As ResourceAccessLevels) As IScpiString
    element.Value = MyBase.controller.Getter(element, modalityName, subHeader, element.Value, access)
    element.ActualValue = element.Value
    Return element
  End Function

#End Region

#Region " SETTERS "

  ''' <summary>
  ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
  ''' </summary>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1} {2}' where 
  ''' the first item is the <see cref="Name">SCPI system name</see>, 
  ''' the second item is a <paramref name="command">command</paramref>
  ''' the third item represents a <paramref name="value">numeric value</paramref>.
  ''' </param>
  ''' <param name="value">The value to set in the instrument.</param>
      Public Function Setter(ByVal subHeader As String, ByVal value As String, ByVal verify As Boolean) As Boolean
    Dim scpiMessage As String = Syntax.BuildCommand(MyBase.SyntaxHeader, subHeader, value)
    Me.Controller.Port.WriteLine(scpiMessage)
    If verify Then
      Dim actual As String = Me.QueryTrimEnd(subHeader)
      Return Not String.IsNullOrWhiteSpace(actual) AndAlso (value = actual)
    Else
      Return True
    End If
  End Function

  ''' <summary>
  ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
  ''' </summary>
  ''' <param name="modalityName">Specifies the modality name.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2} {3}' where 
  ''' the first item is the <see cref="Name">SCPI system name</see>, 
  ''' the second item is the <paramref name="modalityName">modality name</paramref>, 
  ''' the third item is a <paramref name="command">command</paramref>
  ''' the fourth item represents a <paramref name="value">numeric value</paramref>.
  ''' </param>
  ''' <param name="value">The value to set in the instrument.</param>
      Public Function Setter(ByVal modalityName As String, ByVal subHeader As String, ByVal value As String, ByVal verify As Boolean) As Boolean
    Dim scpiMessage As String = Syntax.BuildCommand(MyBase.SyntaxHeader, modalityName, subHeader, value)
    Me.Controller.Port.WriteLine(scpiMessage)
    If verify Then
      Dim actual As String = Me.QueryTrimEnd(modalityName, subHeader)
      Return Not String.IsNullOrWhiteSpace(actual) AndAlso (value = actual)
    Else
      Return True
    End If
  End Function

#End Region

#Region " SETTERS w/ ACCESS "

  ''' <summary>
  ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
  ''' </summary>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1} {2}' where 
  ''' the first item is the <see cref="Name">SCPI system name</see> 
  ''' the second item is a <paramref name="command">command</paramref> and 
  ''' the third item is the <paramref name="value">value</paramref>.
  ''' </param>
  ''' <param name="existingValue">Specifies the existing element value where the instrument.</param>
  ''' <param name="access">The desired <see cref="ResourceAccessLevels">access level</see> to the instrument.</param>
      Public Function Setter(ByVal subHeader As String, 
                         ByVal existingValue As String, ByVal value As String, 
                         ByVal access As ResourceAccessLevels) As String
    If access.IsVerifyAccess Then
      Throw New ArgumentException("Verification not supported by this method", "access")
    End If
    If access.IsDeviceAccess OrElse Not String.Equals(value, existingValue, StringComparison.OrdinalIgnoreCase) Then
      Me.Controller.Port.WriteLine(Syntax.BuildCommand(MyBase.SyntaxHeader, subHeader, value))
    End If
    Return value
  End Function

  ''' <summary>
  ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
  ''' </summary>
  ''' <param name="modalityName">Specifies the modality name.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2} {3}' where 
  ''' the first item is the <see cref="Name">SCPI system name</see>, 
  ''' the second item is the <paramref name="modalityName">modality name</paramref>, 
  ''' the second item is a <paramref name="command">command</paramref> and
  ''' the fourth item represents a <paramref name="value">numeric value</paramref>.
  ''' </param>
  ''' <param name="existingValue">Specifies the existing element value where the instrument.</param>
  ''' <param name="access">The desired <see cref="ResourceAccessLevels">access level</see> to the instrument.</param>
      Public Function Setter(ByVal modalityName As String, ByVal subHeader As String, 
                         ByVal existingValue As String, ByVal value As String, 
                         ByVal access As ResourceAccessLevels) As String
    If access.IsVerifyAccess Then
      Throw New ArgumentException("Verification not supported by this method", "access")
    End If
    If access.IsDeviceAccess OrElse Not String.Equals(value, existingValue, StringComparison.OrdinalIgnoreCase) Then
      Me.Controller.Port.WriteLine(Syntax.BuildCommand(MyBase.SyntaxHeader, modalityName, subHeader, value))
    End If
    Return value
  End Function

#End Region

#Region " SETTERS w/ ACCESS w/ ELEMENT "

  ''' <summary>
  ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
  ''' </summary>
  ''' <param name="element">Specifies the <see cref="IScpiValueBase">existing element</see> where the getter command is stored.</param>
  ''' <param name="existingValue">Specifies the existing element value where the instrument.</param>
  ''' <param name="value">Specifies the value to be set using the element <see cref="IScpiValueBase.SetterCommandFormat">command</see>.</param>
  ''' <param name="access">The desired <see cref="ResourceAccessLevels">access level</see> to the instrument.</param>
      Public Function Setter(ByVal element As IScpiValueBase, 
                         ByVal existingValue As String, ByVal value As String, 
                         ByVal access As ResourceAccessLevels) As String
    If access.IsVerifyAccess Then
      Throw New ArgumentException("Verification not supported by this method", "access")
    End If
    If access.IsDeviceAccess OrElse Not String.Equals(value, existingValue, StringComparison.OrdinalIgnoreCase) Then
      If Not String.IsNullOrWhiteSpace(element.SetterCommandFormat) Then
        element.SetterCommand = String.Format(Globalization.CultureInfo.InvariantCulture, element.SetterCommandFormat, value)
      End If
      Me.Controller.Port.WriteLine(element.SetterCommand)
    End If
    Return value
  End Function

  ''' <summary>
  ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
  ''' </summary>
  ''' <param name="element">Specifies the <see cref="IScpiValueBase">existing element</see> where the getter command is stored.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1} {2}' where 
  ''' the first item is the <see cref="Name">SCPI system name</see> 
  ''' the second item is a <paramref name="command">command</paramref> and 
  ''' the third item is the <paramref name="value">value</paramref>.
  ''' </param>
  ''' <param name="existingValue">Specifies the existing element value where the instrument.</param>
  ''' <param name="access">The desired <see cref="ResourceAccessLevels">access level</see> to the instrument.</param>
      Public Function Setter(ByVal element As IScpiValueBase, ByVal subHeader As String, 
                         ByVal existingValue As String, ByVal value As String, 
                         ByVal access As ResourceAccessLevels) As String
    If access.IsVerifyAccess Then
      Throw New ArgumentException("Verification not supported by this method", "access")
    End If
    If access.IsDeviceAccess OrElse Not String.Equals(value, existingValue, StringComparison.OrdinalIgnoreCase) Then
      element.SetterCommand = Syntax.BuildCommand(MyBase.SyntaxHeader, subHeader, value)
      Me.Controller.Port.WriteLine(element.SetterCommand)
    End If
    Return value
  End Function

  ''' <summary>
  ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
  ''' </summary>
  ''' <param name="element">Specifies the <see cref="IScpiValueBase">existing element</see> where the getter command is stored.</param>
  ''' <param name="modalityName">Specifies the modality name.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2} {3}' where 
  ''' the first item is the <see cref="Name">SCPI system name</see>, 
  ''' the second item is the <paramref name="modalityName">modality name</paramref>, 
  ''' the second item is a <paramref name="command">command</paramref> and
  ''' the fourth item represents a <paramref name="value">numeric value</paramref>.
  ''' </param>
  ''' <param name="existingValue">Specifies the existing element value where the instrument.</param>
  ''' <param name="access">The desired <see cref="ResourceAccessLevels">access level</see> to the instrument.</param>
      Public Function Setter(ByVal element As IScpiValueBase, ByVal modalityName As String, ByVal subHeader As String, 
                         ByVal existingValue As String, ByVal value As String, 
                         ByVal access As ResourceAccessLevels) As String
    If access.IsVerifyAccess Then
      Throw New ArgumentException("Verification not supported by this method", "access")
    End If
    If access.IsDeviceAccess OrElse Not String.Equals(value, existingValue, StringComparison.OrdinalIgnoreCase) Then
      element.SetterCommand = Syntax.BuildCommand(MyBase.SyntaxHeader, modalityName, subHeader, value)
      Me.Controller.Port.WriteLine(element.SetterCommand)
    End If
    Return value
  End Function

#End Region

#Region " SETTERS w/ ACCESS w/ SCPI VALUE  "

  ''' <summary>
  ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
  ''' Applies readings to the <paramref name="element">existing element</paramref>
  ''' Assume element has both <see cref="IScpiValueBase.GetterCommand">getter</see> and 
  ''' <see cref="IScpiValueBase.SetterCommand">setter</see> commands specified.
  ''' </summary>
  ''' <param name="element">Specifies the element holding the existing value, which is updated.</param>
  ''' <param name="value">The value to set in the instrument.</param>
  ''' <param name="access">The desired <see cref="ResourceAccessLevels">access level</see> to the instrument.</param>
      Public Function Setter(ByVal element As IScpiString, ByVal value As String, ByVal access As ResourceAccessLevels) As IScpiString
    If access.IsDeviceAccess OrElse (value <> element.Value) Then
      element.Value = Me.Setter(CType(element, IScpiValueBase), element.Value, value, access And Not ResourceAccessLevels.Verify)
    End If
    If access.IsVerifyAccess Then
      MyBase.controller.Getter(element, ResourceAccessLevels.Device)
      element.Value = value
    ElseIf access.IsCacheAccess Then
      element.ActualValue = value
    End If
    Return element
  End Function

  ''' <summary>
  ''' Sets the instrument <see cref="Name">subsystem</see> <paramref name="subHeader">element</paramref> value.
  ''' Applies readings to the <paramref name="element">existing element</paramref>
  ''' </summary>
  ''' <param name="element">Specifies the existing element where the instrument readings are applied.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1} {2}' where 
  ''' the first item is the <see cref="Name">SCPI system name</see> 
  ''' the second item is a <paramref name="command">command</paramref> and 
  ''' the third item is the <paramref name="value">value</paramref>.
  ''' </param>
  ''' <param name="value">The value to set in the instrument.</param>
  ''' <param name="access">The desired <see cref="ResourceAccessLevels">access level</see> to the instrument.</param>
    ''' <remarks>
  ''' Use the following code in the calling methd if verification is used:
  ''' <code>
  ''' If Not Me.Elements.IsVerified.GetValueOrDefault(False) Then
  '''   If String.IsNullOrWhiteSpace(element.Value) OrElse String.IsNullOrWhiteSpace(element.ActualValue) Then
  '''     return "Failed reading values from the instrument."
  '''   Else
  '''     return String.Format(Globalization.CultureInfo.CurrentCulture, "Instrument settings applied using the command '{0}' did not verify. Expected={1}. Actual={2}.", Me.Elements.Value, Me.Elements.ActualValue.Value)
  '''   End If
  ''' End If
  ''' </code>
  ''' </remarks>
  Public Function Setter(ByVal element As IScpiString, 
                         ByVal subHeader As String, ByVal value As String, ByVal access As ResourceAccessLevels) As IScpiString
    element.Value = Me.Setter(CType(element, IScpiValueBase), subHeader, element.Value, value, access And Not isr.Scpi.ResourceAccessLevels.Verify)
    If access.IsVerifyAccess Then
      MyBase.controller.Getter(element, subHeader, ResourceAccessLevels.Device)
      element.Value = value
    ElseIf access.IsCacheAccess Then
      element.ActualValue = value
    End If
    Return element
  End Function

  ''' <summary>
  ''' Sets the instrument <see cref="Name">subsystem</see> <paramref name="modalityName">modality</paramref> <paramref name="subHeader">element</paramref> value.
  ''' Applies readings to the <paramref name="element">existing element</paramref>
  ''' </summary>
  ''' <param name="element">Specifies the existing element where the instrument readings are applied.</param>
  ''' <param name="modalityName">Specifies the modality name.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2} {3}' where 
  ''' the first item is the <see cref="Name">SCPI system name</see>, 
  ''' the second item is the <paramref name="modalityName">modality name</paramref>, 
  ''' the second item is a <paramref name="command">command</paramref> and
  ''' the fourth item represents a <paramref name="value">numeric value</paramref>.
  ''' </param>
  ''' <param name="value">The value to set in the instrument.</param>
  ''' <param name="access">The desired <see cref="ResourceAccessLevels">access level</see> to the instrument.</param>
    ''' <remarks>
  ''' Use the following code in the calling methd if verification is used:
  ''' <code>
  ''' If Not Me.Elements.IsVerified.GetValueOrDefault(False) Then
  '''   If String.IsNullOrWhiteSpace(element.Value) OrElse String.IsNullOrWhiteSpace(element.ActualValue) Then
  '''     return "Failed reading values from the instrument."
  '''   Else
  '''     return String.Format(Globalization.CultureInfo.CurrentCulture, "Instrument settings applied using the command '{0}' did not verify. Expected={1}. Actual={2}.", Me.Elements.Value, Me.Elements.ActualValue.Value)
  '''   End If
  ''' End If
  ''' </code>
  ''' </remarks>
  Public Function Setter(ByVal element As IScpiString, 
                         ByVal modalityName As String, ByVal subHeader As String, ByVal value As String, 
                         ByVal access As ResourceAccessLevels) As IScpiString
    element.Value = Me.Setter(CType(element, IScpiValueBase), modalityName, subHeader, element.Value, value, access And Not isr.Scpi.ResourceAccessLevels.Verify)
    If access.IsVerifyAccess Then
      Me.Getter(element, modalityName, subHeader, ResourceAccessLevels.Device)
      element.Value = value
    ElseIf access.IsCacheAccess Then
      element.ActualValue = value
    End If
    Return element
  End Function

#End Region

#End Region

#Region " BOOLEAN "

#Region " QUERRIES "

  ''' <summary>
  ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
  ''' </summary>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}?' where 
  ''' the first item is the <see cref="Name">SCPI system name</see> and 
  ''' the second item is a <paramref name="command">command</paramref>.
  ''' </param>
  ''' <param name="formatter">The <see cref="isr.Core.IFormatter(of boolean)">boolean formatter.</see></param>
      Public Function QueryBoolean(ByVal subHeader As String, ByVal parser As isr.Core.IParser(of boolean)) As Nullable(Of Boolean)) As Nullable(Of Boolean)
    Return Me.Controller.QueryBoolean(Syntax.BuildQuery(MyBase.SyntaxHeader, subHeader), boolFormat)
  End Function

  ''' <summary>
  ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
  ''' </summary>
  ''' <param name="modalityName">Specifies the modality name.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2}?' where 
  ''' the first item is the <see cref="Name">SCPI system name</see>, 
  ''' the second item is the <paramref name="modalityName">modality name</paramref> and 
  ''' the thrid item is a <paramref name="command">command</paramref>.
  ''' </param>
  ''' <param name="formatter">The <see cref="isr.Core.IFormatter(of boolean)">boolean formatter.</see></param>
      Public Function QueryBoolean(ByVal modalityName As String, ByVal subHeader As String, ByVal parser As isr.Core.IParser(of boolean)) As Nullable(Of Boolean)) As Nullable(Of Boolean)
    Return Me.Controller.QueryBoolean(Syntax.BuildQuery(MyBase.SyntaxHeader, modalityName, subHeader), boolFormat)
  End Function

#End Region

#Region " GETTERS w/ ACCESS "

  ''' <summary>
  ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
  ''' </summary>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}?' where 
  ''' the first item is the <see cref="Name">SCPI system name</see> and 
  ''' the second item is a <paramref name="command">command</paramref>
  ''' </param>
  ''' <param name="existingValue">The existing value. The instrument is accessed if this value has no value.</param>
  ''' <param name="formatter">The <see cref="isr.Core.IFormatter(of boolean)">boolean formatter.</see></param>
  ''' <param name="access">The desired <see cref="ResourceAccessLevels">access level</see> to the instrument.</param>
      Public Function Getter(ByVal subHeader As String, 
                         ByVal existingValue As Nullable(Of Boolean), 
                         ByVal formatter As isr.Core.IFormatter(of boolean), 
                         ByVal access As ResourceAccessLevels) As Nullable(Of Boolean)
    If access.IsDeviceAccess OrElse Not existingValue.HasValue Then
      existingValue = Me.QueryBoolean(subHeader, boolFormat)
    End If
    Return existingValue
  End Function

  ''' <summary>
  ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
  ''' </summary>
  ''' <param name="modalityName">Specifies the modality name.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2}?' where 
  ''' the first item is the <see cref="Name">SCPI system name</see>, 
  ''' the second item is the <paramref name="modalityName">modality name</paramref> and
  ''' the second item is a <paramref name="command">command</paramref>.
  ''' </param>
  ''' <param name="existingValue">The existing value. The instrument is accessed if this value has no value.</param>
  ''' <param name="formatter">The <see cref="isr.Core.IFormatter(of boolean)">boolean formatter.</see></param>
  ''' <param name="access">The desired <see cref="ResourceAccessLevels">access level</see> to the instrument.</param>
      Public Function Getter(ByVal modalityName As String, ByVal subHeader As String, 
                         ByVal existingValue As Nullable(Of Boolean), 
                         ByVal formatter As isr.Core.IFormatter(of boolean), ByVal access As ResourceAccessLevels) As Nullable(Of Boolean)
    If access.IsDeviceAccess OrElse Not existingValue.HasValue Then
      existingValue = Me.QueryBoolean(modalityName, subHeader, boolFormat)
    End If
    Return existingValue
  End Function

#End Region

#Region " GETTERS w/ ACCESS w/ ELEMENT "

  ''' <summary>
  ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
  ''' Uses or updates the nullable value getter command.
  ''' </summary>
  ''' <param name="element">Specifies the <see cref="IScpiValueBase">existing element</see> where the getter command is stored.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}?' where 
  ''' the first item is the <see cref="Name">SCPI system name</see> and 
  ''' the second item is a <paramref name="command">command</paramref>
  ''' </param>
  ''' <param name="existingValue">The existing value. The instrument is accessed if this value has no value.</param>
  ''' <param name="formatter">The <see cref="isr.Core.IFormatter(of boolean)">boolean formatter.</see></param>
  ''' <param name="access">The desired <see cref="ResourceAccessLevels">access level</see> to the instrument.</param>
      Public Function Getter(ByVal element As IScpiValueBase, 
                         ByVal subHeader As String, ByVal existingValue As Nullable(Of Boolean), 
                         ByVal formatter As isr.Core.IFormatter(of boolean), ByVal access As ResourceAccessLevels) As Nullable(Of Boolean)
    If access.IsDeviceAccess OrElse Not existingValue.HasValue Then
      If String.IsNullOrWhiteSpace(element.GetterCommand) Then
        element.GetterCommand = Syntax.BuildQuery(MyBase.SyntaxHeader, subHeader)
      End If
      ' existingValue = Me.QueryBoolean(subHeader)
      existingValue = Me.Controller.QueryBoolean(element.GetterCommand, boolFormat)
    End If
    Return existingValue
  End Function

  ''' <summary>
  ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
  ''' Uses or updates the nullable value getter command.
  ''' </summary>
  ''' <param name="element">Specifies the <see cref="IScpiValueBase">existing element</see> where the getter command is stored.</param>
  ''' <param name="modalityName">Specifies the modality name.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2}?' where 
  ''' the first item is the <see cref="Name">SCPI system name</see>, 
  ''' the second item is the <paramref name="modalityName">modality name</paramref> and
  ''' the second item is a <paramref name="command">command</paramref>.
  ''' </param>
  ''' <param name="existingValue">The existing value. The instrument is accessed if this value has no value.</param>
  ''' <param name="formatter">The <see cref="isr.Core.IFormatter(of boolean)">boolean formatter.</see></param>
  ''' <param name="access">The desired <see cref="ResourceAccessLevels">access level</see> to the instrument.</param>
      Public Function Getter(ByVal element As IScpiValueBase, 
                         ByVal modalityName As String, ByVal subHeader As String, 
                         ByVal existingValue As Nullable(Of Boolean), 
                         ByVal formatter As isr.Core.IFormatter(of boolean), ByVal access As ResourceAccessLevels) As Nullable(Of Boolean)
    If access.IsDeviceAccess OrElse Not existingValue.HasValue Then
      If String.IsNullOrWhiteSpace(element.GetterCommand) Then
        element.GetterCommand = Syntax.BuildQuery(MyBase.SyntaxHeader, modalityName, subHeader)
      End If
      ' existingValue = Me.QueryBoolean(modalityName, subHeader)
      existingValue = Me.Controller.QueryBoolean(element.GetterCommand, boolFormat)
    End If
    Return existingValue
  End Function

#End Region

#Region " GETTERS w/ ACCESS w/ SCPI VALUE "

  ''' <summary>
  ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
  ''' Uses or updates the nullable value getter command.
  ''' </summary>
  ''' <param name="element">Specifies the <see cref="IScpiValueBase">existing element</see> where the getter command is stored.</param>
  ''' <param name="formatter">The <see cref="isr.Core.IFormatter(of boolean)">boolean formatter.</see></param>
  ''' <param name="access">The desired <see cref="ResourceAccessLevels">access level</see> to the instrument.</param>
      Public Function Getter(ByVal element As IScpiValue(Of Boolean), 
                         ByVal formatter As isr.Core.IFormatter(of boolean), ByVal access As ResourceAccessLevels) As IScpiValue(Of Boolean)
    If access.IsDeviceAccess OrElse Not element.Value.HasValue Then
      element.Value = Me.Controller.QueryBoolean(element.GetterCommand, boolFormat)
      element.ActualValue = element.Value
    End If
    Return element
  End Function

  ''' <summary>
  ''' Gets a value from the instrument <see cref="Name">subsystem</see> <paramref name="subHeader">element</paramref>.
  ''' Applies readings to the <paramref name="element">existing element</paramref>
  ''' </summary>
  ''' <param name="element">Specifies the existing element where the instrument readings are applied.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}?' where 
  ''' the first item is the <see cref="Name">SCPI system name</see> and 
  ''' the second item is a <paramref name="command">command</paramref>
  ''' </param>
  ''' <param name="formatter">The <see cref="isr.Core.IFormatter(of boolean)">boolean formatter.</see></param>
  ''' <param name="access">The desired <see cref="ResourceAccessLevels">access level</see> to the instrument.</param>
      Public Function Getter(ByVal element As IScpiValue(Of Boolean), 
                         ByVal subHeader As String, 
                         ByVal formatter As isr.Core.IFormatter(of boolean), ByVal access As ResourceAccessLevels) As IScpiValue(Of Boolean)
    If access.IsDeviceAccess OrElse Not element.Value.HasValue Then
      If String.IsNullOrWhiteSpace(element.GetterCommand) Then
        element.GetterCommand = Syntax.BuildQuery(MyBase.SyntaxHeader, subHeader)
      End If
      ' element.Value = Me.QueryBoolean(subHeader)
      element.Value = Me.Controller.QueryBoolean(element.GetterCommand, boolFormat)
      element.ActualValue = element.Value
    End If
    Return element
  End Function

  ''' <summary>
  ''' Gets a value from the instrument <see cref="Name">subsystem</see> <paramref name="modalityName">modality</paramref> and <paramref name="subHeader">element</paramref>.
  ''' Applies readings to the <paramref name="element">existing element</paramref>
  ''' </summary>
  ''' <param name="element">Specifies the existing element where the instrument readings are applied.</param>
  ''' <param name="modalityName">Specifies the modality name.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2}?' where 
  ''' the first item is the <see cref="Name">SCPI system name</see>, 
  ''' the second item is the <paramref name="modalityName">modality name</paramref> and
  ''' the second item is a <paramref name="command">command</paramref>.
  ''' </param>
  ''' <param name="formatter">The <see cref="isr.Core.IFormatter(of boolean)">boolean formatter.</see></param>
  ''' <param name="access">The desired <see cref="ResourceAccessLevels">access level</see> to the instrument.</param>
      Public Function Getter(ByVal element As IScpiValue(Of Boolean), 
                         ByVal modalityName As String, ByVal subHeader As String, 
                         ByVal formatter As isr.Core.IFormatter(of boolean), ByVal access As ResourceAccessLevels) As IScpiValue(Of Boolean)
    element.Value = Me.Getter(element, modalityName, subHeader, element.Value, boolFormat, access)
    element.ActualValue = element.Value
    Return element
  End Function

#End Region

#Region " SETTERS "

  ''' <summary>
  ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
  ''' </summary>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1} {2}' where 
  ''' the first item is the <see cref="Name">SCPI system name</see>, 
  ''' the second item is a <paramref name="command">command</paramref>
  ''' the third item represents a <paramref name="value">numeric value</paramref>.
  ''' </param>
  ''' <param name="value">The value to set in the instrument.</param>
  ''' <param name="formatter">The <see cref="isr.Core.IFormatter(of boolean)">boolean formatter.</see></param>
      Public Function Setter(ByVal subHeader As String, ByVal value As Boolean, ByVal formatter As isr.Core.IFormatter(of boolean), ByVal verify As Boolean) As Boolean
    Dim scpiMessage As String = Syntax.BuildCommand(MyBase.SyntaxHeader, subHeader, value, boolFormat)
    Me.Controller.Port.WriteLine(scpiMessage)
    If verify Then
      Dim actual As Nullable(Of Boolean) = Me.QueryBoolean(scpiMessage, boolFormat)
      Return actual.HasValue AndAlso (value = actual.Value)
    Else
      Return True
    End If
  End Function

  ''' <summary>
  ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
  ''' </summary>
  ''' <param name="modalityName">Specifies the modality name.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2} {3}' where 
  ''' the first item is the <see cref="Name">SCPI system name</see>, 
  ''' the second item is the <paramref name="modalityName">modality name</paramref>, 
  ''' the third item is a <paramref name="command">command</paramref>
  ''' the fourth item represents a <paramref name="value">numeric value</paramref>.
  ''' </param>
  ''' <param name="value">The value to set in the instrument.</param>
  ''' <param name="formatter">The <see cref="isr.Core.IFormatter(of boolean)">boolean formatter.</see></param>
      Public Function Setter(ByVal modalityName As String, ByVal subHeader As String, ByVal formatter As isr.Core.IFormatter(of boolean), ByVal value As Boolean, ByVal verify As Boolean) As Boolean
    Dim scpiMessage As String = Syntax.BuildCommand(MyBase.SyntaxHeader, modalityName, subHeader, value, boolFormat)
    Me.Controller.Port.WriteLine(scpiMessage)
    If verify Then
      Dim actual As Nullable(Of Boolean) = Me.QueryBoolean(modalityName, subHeader, boolFormat)
      Return actual.HasValue AndAlso (value = actual.Value)
    Else
      Return True
    End If
  End Function

#End Region

#Region " SETTERS w/ ACCESS "

  ''' <summary>
  ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
  ''' </summary>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1} {2}' where 
  ''' the first item is the <see cref="Name">SCPI system name</see> 
  ''' the second item is a <paramref name="command">command</paramref> and 
  ''' the third item is the <paramref name="value">value</paramref>.
  ''' </param>
  ''' <param name="existingValue">The existing value. The instrument is accessed if this value has no value or is not equal to the specified <paramref name="value"/>.</param>
  ''' <param name="formatter">The <see cref="isr.Core.IFormatter(of boolean)">boolean formatter.</see></param>
  ''' <param name="access">The desired <see cref="ResourceAccessLevels">access level</see> to the instrument.</param>
      Public Function Setter(ByVal subHeader As String, ByVal existingValue As Nullable(Of Boolean), 
                         ByVal value As Boolean, ByVal formatter As isr.Core.IFormatter(of boolean), ByVal access As ResourceAccessLevels) As Nullable(Of Boolean)
    If access.IsVerifyAccess Then
      Throw New ArgumentException("Verification not supported by this method", "access")
    End If
    If access.IsDeviceAccess OrElse (value <> existingValue) Then
      Me.Controller.Port.WriteLine(Syntax.BuildCommand(MyBase.SyntaxHeader, subHeader, value, boolFormat))
    End If
    Return New Nullable(Of Boolean)(value)
  End Function

  ''' <summary>
  ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
  ''' </summary>
  ''' <param name="modalityName">Specifies the modality name.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2} {3}' where 
  ''' the first item is the <see cref="Name">SCPI system name</see>, 
  ''' the second item is the <paramref name="modalityName">modality name</paramref>, 
  ''' the second item is a <paramref name="command">command</paramref> and
  ''' the fourth item represents a <paramref name="value">numeric value</paramref>.
  ''' </param>
  ''' <param name="existingValue">The existing value. The instrument is accessed if this value has no value or is not equal to the specified <paramref name="value"/>.</param>
  ''' <param name="value">The value to set in the instrument.</param>
  ''' <param name="formatter">The <see cref="isr.Core.IFormatter(of boolean)">boolean formatter.</see></param>
  ''' <param name="access">The desired <see cref="ResourceAccessLevels">access level</see> to the instrument.</param>
      Public Function Setter(ByVal modalityName As String, ByVal subHeader As String, 
                         ByVal existingValue As Nullable(Of Boolean), ByVal value As Boolean, 
                         ByVal formatter As isr.Core.IFormatter(of boolean), ByVal access As ResourceAccessLevels) As Nullable(Of Boolean)
    If access.IsVerifyAccess Then
      Throw New ArgumentException("Verification not supported by this method", "access")
    End If
    If access.IsDeviceAccess OrElse (value <> existingValue) Then
      Me.Controller.Port.WriteLine(Syntax.BuildCommand(MyBase.SyntaxHeader, modalityName, subHeader, value, boolFormat))
    End If
    Return New Nullable(Of Boolean)(value)
  End Function

#End Region

#Region " SETTERS w/ ACCESS w/ ELEMENT "

  ''' <summary>
  ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
  ''' </summary>
  ''' <param name="element">Specifies the <see cref="IScpiValueBase">existing element</see> where the getter command is stored.</param>
  ''' <param name="value">Specifies the value to be set using the element <see cref="IScpiValueBase.SetterCommandFormat">command</see>.</param>
  ''' <param name="existingValue">The existing value. The instrument is accessed if this value has no value or is not equal to the specified <paramref name="value"/>.</param>
  ''' <param name="formatter">The <see cref="isr.Core.IFormatter(of boolean)">boolean formatter.</see></param>
  ''' <param name="access">The desired <see cref="ResourceAccessLevels">access level</see> to the instrument.</param>
      Public Function Setter(ByVal element As IScpiValueBase, 
                         ByVal value As Boolean, ByVal existingValue As Boolean, 
                         ByVal formatter As isr.Core.IFormatter(of boolean), ByVal access As ResourceAccessLevels) As Boolean
    If access.IsVerifyAccess Then
      Throw New ArgumentException("Verification not supported by this method", "access")
    End If
    If access.IsDeviceAccess OrElse value <> existingValue Then
      If Not String.IsNullOrWhiteSpace(element.SetterCommandFormat) Then
        element.SetterCommand = String.Format(Globalization.CultureInfo.InvariantCulture, 
                                        element.SetterCommandFormat, value.ConvertToScpiValue(boolFormat))
      End If
      Me.Controller.Port.WriteLine(element.SetterCommand)
    End If
    Return value
  End Function

  ''' <summary>
  ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
  ''' Uses or updates the nullable value getter command.
  ''' </summary>
  ''' <param name="element">Specifies the <see cref="IScpiValueBase">existing element</see> where the getter command is stored.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1} {2}' where 
  ''' the first item is the <see cref="Name">SCPI system name</see> 
  ''' the second item is a <paramref name="command">command</paramref> and 
  ''' the third item is the <paramref name="value">value</paramref>.
  ''' </param>
  ''' <param name="value">The value which to set.</param>
  ''' <param name="existingValue">The existing value. The instrument is accessed if this value has no value or is not equal to the specified <paramref name="value"/>.</param>
  ''' <param name="formatter">The <see cref="isr.Core.IFormatter(of boolean)">boolean formatter.</see></param>
  ''' <param name="access">The desired <see cref="ResourceAccessLevels">access level</see> to the instrument.</param>
      Public Function Setter(ByVal element As IScpiValueBase, 
                         ByVal subHeader As String, ByVal existingValue As Nullable(Of Boolean), 
                         ByVal value As Boolean, ByVal formatter As isr.Core.IFormatter(of boolean), ByVal access As ResourceAccessLevels) As Nullable(Of Boolean)
    If access.IsVerifyAccess Then
      Throw New ArgumentException("Verification not supported by this method", "access")
    End If
    If access.IsDeviceAccess OrElse (value <> existingValue) Then
      element.SetterCommand = Syntax.BuildCommand(MyBase.SyntaxHeader, subHeader, value, boolFormat)
      Me.Controller.Port.WriteLine(element.SetterCommand)
    End If
    Return New Nullable(Of Boolean)(value)
  End Function

  ''' <summary>
  ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
  ''' Uses or updates the nullable value getter command.
  ''' </summary>
  ''' <param name="element">Specifies the <see cref="IScpiValueBase">existing element</see> where the getter command is stored.</param>
  ''' <param name="modalityName">Specifies the modality name.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2} {3}' where 
  ''' the first item is the <see cref="Name">SCPI system name</see>, 
  ''' the second item is the <paramref name="modalityName">modality name</paramref>, 
  ''' the second item is a <paramref name="command">command</paramref> and
  ''' the fourth item represents a <paramref name="value">numeric value</paramref>.
  ''' </param>
  ''' <param name="existingValue">The existing value. The instrument is accessed if this value has no value or is not equal to the specified <paramref name="value"/>.</param>
  ''' <param name="value">The value to set in the instrument.</param>
  ''' <param name="formatter">The <see cref="isr.Core.IFormatter(of boolean)">boolean formatter.</see></param>
  ''' <param name="access">The desired <see cref="ResourceAccessLevels">access level</see> to the instrument.</param>
      Public Function Setter(ByVal element As IScpiValueBase, 
                         ByVal modalityName As String, ByVal subHeader As String, 
                         ByVal existingValue As Nullable(Of Boolean), ByVal value As Boolean, 
                         ByVal formatter As isr.Core.IFormatter(of boolean), ByVal access As ResourceAccessLevels) As Nullable(Of Boolean)
    If access.IsVerifyAccess Then
      Throw New ArgumentException("Verification not supported by this method", "access")
    End If
    If access.IsDeviceAccess OrElse (value <> existingValue) Then
      element.SetterCommand = Syntax.BuildCommand(MyBase.SyntaxHeader, modalityName, subHeader, value, boolFormat)
      Me.Controller.Port.WriteLine(element.SetterCommand)
    End If
    Return New Nullable(Of Boolean)(value)
  End Function

#End Region

#Region " SETTERS w/ ACCESS w/ SCPI VALUE "

  ''' <summary>
  ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
  ''' Applies readings to the <paramref name="element">existing element</paramref>
  ''' Assume element has both <see cref="IScpiValueBase.GetterCommand">getter</see> and 
  ''' <see cref="IScpiValueBase.SetterCommand">setter</see> commands specified.
  ''' </summary>
  ''' <param name="element">Specifies the element holding the existing value, which is updated.</param>
  ''' <param name="value">The value to set in the instrument.</param>
  ''' <param name="formatter">The <see cref="isr.Core.IFormatter(of boolean)">boolean formatter.</see></param>
  ''' <param name="access">The desired <see cref="ResourceAccessLevels">access level</see> to the instrument.</param>
      Public Function Setter(ByVal element As IScpiValue(Of Boolean), ByVal value As Boolean, 
                         ByVal formatter As isr.Core.IFormatter(of boolean), ByVal access As ResourceAccessLevels) As IScpiValue(Of Boolean)
    If access.IsDeviceAccess OrElse (value <> element.Value) Then
      element.Value = Me.Setter(CType(element, IScpiValueBase), element.Value.Value, value, boolFormat, access And Not ResourceAccessLevels.Verify)
    End If
    If access.IsVerifyAccess Then
      Me.Getter(element, boolFormat, ResourceAccessLevels.Device)
      element.Value = value
    ElseIf access.IsCacheAccess Then
      element.ActualValue = value
    End If
    Return element
  End Function

  ''' <summary>
  ''' Sets the instrument <see cref="Name">subsystem</see> <paramref name="subHeader">element</paramref> value.
  ''' Applies readings to the <paramref name="element">existing element</paramref>
  ''' </summary>
  ''' <param name="element">Specifies the existing element where the instrument readings are applied.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1} {2}' where 
  ''' the first item is the <see cref="Name">SCPI system name</see> 
  ''' the second item is a <paramref name="command">command</paramref> and 
  ''' the third item is the <paramref name="value">value</paramref>.
  ''' </param>
  ''' <param name="formatter">The <see cref="isr.Core.IFormatter(of boolean)">boolean formatter.</see></param>
  ''' <param name="access">The desired <see cref="ResourceAccessLevels">access level</see> to the instrument.</param>
    ''' <remarks>
  ''' Use the following code in the calling methd if verification is used:
  ''' <code>
  ''' If Not element.IsVerified.GetValueOrDefault(False) Then
  '''   If element.Value.HasValue AndAlso element.ActualValue.HasValue Then
  '''     return String.Format(Globalization.CultureInfo.CurrentCulture, 
  '''            "Instrument settings applied using the command '{0}' did not verify. Expected={1}. Actual={2}.", 
  '''            element.Value, element.ActualValue.Value)
  '''   Else
  '''     return "Failed reading values from the instrument."
  '''   End If
  ''' End If
  ''' </code>
  ''' </remarks>
  Public Function Setter(ByVal element As IScpiValue(Of Boolean), 
                         ByVal subHeader As String, 
                         ByVal value As Boolean, 
                         ByVal formatter As isr.Core.IFormatter(of boolean), ByVal access As ResourceAccessLevels) As IScpiValue(Of Boolean)
    element.Value = Me.Setter(CType(element, IScpiValueBase), subHeader, element.Value, value, boolFormat, access And Not ResourceAccessLevels.Verify)
    If access.IsVerifyAccess Then
      Me.Getter(element, subHeader, boolFormat, ResourceAccessLevels.Device)
      element.Value = value
    ElseIf access.IsCacheAccess Then
      element.ActualValue = value
    End If
    Return element
  End Function

  ''' <summary>
  ''' Sets the instrument <see cref="Name">subsystem</see> <paramref name="modalityName">modality</paramref> <paramref name="subHeader">element</paramref> value.
  ''' </summary>
  ''' <param name="element">Specifies the existing element where the instrument readings are applied.</param>
  ''' <param name="modalityName">Specifies the modality name.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2} {3}' where 
  ''' the first item is the <see cref="Name">SCPI system name</see>, 
  ''' the second item is the <paramref name="modalityName">modality name</paramref>, 
  ''' the second item is a <paramref name="command">command</paramref> and
  ''' the fourth item represents a <paramref name="value">numeric value</paramref>.
  ''' </param>
  ''' <param name="formatter">The <see cref="isr.Core.IFormatter(of boolean)">boolean formatter.</see></param>
  ''' <param name="access">The desired <see cref="ResourceAccessLevels">access level</see> to the instrument.</param>
    ''' <remarks>
  ''' Use the following code in the calling methd if verification is used:
  ''' <code>
  ''' If Not element.IsVerified.GetValueOrDefault(False) Then
  '''   If element.Value.HasValue AndAlso element.ActualValue.HasValue Then
  '''     return String.Format(Globalization.CultureInfo.CurrentCulture, 
  '''            "Instrument settings applied using the command '{0}' did not verify. Expected={1}. Actual={2}.", 
  '''            element.Value, element.ActualValue.Value)
  '''   Else
  '''     return "Failed reading values from the instrument."
  '''   End If
  ''' End If
  ''' </code>
  ''' </remarks>
  Public Function Setter(ByVal element As IScpiValue(Of Boolean), 
                         ByVal modalityName As String, ByVal subHeader As String, 
                         ByVal value As Boolean, 
                         ByVal formatter As isr.Core.IFormatter(of boolean), ByVal access As ResourceAccessLevels) As IScpiValue(Of Boolean)
    element.Value = Me.Setter(CType(element, IScpiValueBase), modalityName, subHeader, element.Value, value, boolFormat, access And Not ResourceAccessLevels.Verify)
    If access.IsVerifyAccess Then
      Me.Getter(element, modalityName, subHeader, ResourceAccessLevels.Device)
      element.Value = value
    ElseIf access.IsCacheAccess Then
      element.ActualValue = value
    End If
    Return element
  End Function

#End Region

#End Region

#Region " DOUBLE "

#Region " QUERRIES: DOUBLE "

  ''' <summary>
  ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
  ''' </summary>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}?' where 
  ''' the first item is the <see cref="Name">SCPI system name</see> and 
  ''' the second item is a <paramref name="command">command</paramref>.
  ''' </param>
      Public Function QueryDouble(ByVal subHeader As String) As Nullable(Of Double)
    Return Me.Controller.QueryDouble(Syntax.BuildQuery(MyBase.SyntaxHeader, subHeader))
  End Function

  ''' <summary>
  ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
  ''' </summary>
  ''' <param name="modalityName">Specifies the modality name.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2}?' where 
  ''' the first item is the <see cref="Name">SCPI system name</see>, 
  ''' the second item is the <paramref name="modalityName">modality name</paramref> and 
  ''' the thrid item is a <paramref name="command">command</paramref>.
  ''' </param>
      Public Function QueryDouble(ByVal modalityName As String, ByVal subHeader As String) As Nullable(Of Double)
    Return Me.Controller.QueryDouble(Syntax.BuildQuery(MyBase.SyntaxHeader, modalityName, subHeader))
  End Function

#End Region

#Region " GETTERS w/ ACCESS "

  ''' <summary>
  ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
  ''' </summary>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}?' where 
  ''' the first item is the <see cref="Name">SCPI system name</see> and 
  ''' the second item is a <paramref name="command">command</paramref>
  ''' </param>
  ''' <param name="existingValue">Specifies the existing value.</param>
      Public Function Getter(ByVal subHeader As String, ByVal existingValue As Nullable(Of Double), 
                         ByVal access As ResourceAccessLevels) As Nullable(Of Double)
    If access.IsDeviceAccess OrElse Not existingValue.HasValue Then
      existingValue = Me.QueryDouble(subHeader)
    End If
    Return existingValue
  End Function

  ''' <summary>
  ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
  ''' </summary>
  ''' <param name="modalityName">Specifies the modality name.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2}?' where 
  ''' the first item is the <see cref="Name">SCPI system name</see>, 
  ''' the second item is the <paramref name="modalityName">modality name</paramref> and
  ''' the second item is a <paramref name="command">command</paramref>.
  ''' </param>
  ''' <param name="existingValue">Specifies the existing value.</param>
      Public Function Getter(ByVal modalityName As String, ByVal subHeader As String, 
                         ByVal existingValue As Nullable(Of Double), 
                         ByVal access As ResourceAccessLevels) As Nullable(Of Double)
    If access.IsDeviceAccess OrElse Not existingValue.HasValue Then
      existingValue = Me.QueryDouble(modalityName, subHeader)
    End If
    Return existingValue
  End Function

#End Region

#Region " GETTERS w/ ACCESS w/ ELEMENT "

  ''' <summary>
  ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
  ''' Uses or updates the nullable value getter command.
  ''' </summary>
  ''' <param name="element">Specifies the <see cref="IScpiValueBase">existing element</see> where the getter command is stored.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}?' where 
  ''' the first item is the <see cref="Name">SCPI system name</see> and 
  ''' the second item is a <paramref name="command">command</paramref>
  ''' </param>
  ''' <param name="existingValue">Specifies the existing value.</param>
      Public Function Getter(ByVal element As IScpiValueBase, 
                         ByVal subHeader As String, ByVal existingValue As Nullable(Of Double), 
                         ByVal access As ResourceAccessLevels) As Nullable(Of Double)
    If access.IsDeviceAccess OrElse Not existingValue.HasValue Then
      If String.IsNullOrWhiteSpace(element.GetterCommand) Then
        element.GetterCommand = Syntax.BuildQuery(MyBase.SyntaxHeader, subHeader)
      End If
      'existingValue = Me.QueryDouble(subHeader)
      existingValue = Me.Controller.QueryDouble(element.GetterCommand)
    End If
    Return existingValue
  End Function

  ''' <summary>
  ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
  ''' Uses or updates the nullable value getter command.
  ''' </summary>
  ''' <param name="element">Specifies the <see cref="IScpiValueBase">existing element</see> where the getter command is stored.</param>
  ''' <param name="modalityName">Specifies the modality name.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2}?' where 
  ''' the first item is the <see cref="Name">SCPI system name</see>, 
  ''' the second item is the <paramref name="modalityName">modality name</paramref> and
  ''' the second item is a <paramref name="command">command</paramref>.
  ''' </param>
  ''' <param name="existingValue">Specifies the existing value.</param>
      Public Function Getter(ByVal element As IScpiValueBase, 
                         ByVal modalityName As String, ByVal subHeader As String, 
                         ByVal existingValue As Nullable(Of Double), 
                         ByVal access As ResourceAccessLevels) As Nullable(Of Double)
    If access.IsDeviceAccess OrElse Not existingValue.HasValue Then
      If String.IsNullOrWhiteSpace(element.GetterCommand) Then
        element.GetterCommand = Syntax.BuildQuery(MyBase.SyntaxHeader, modalityName, subHeader)
      End If
      ' existingValue = Me.QueryDouble(subHeader)
      existingValue = Me.Controller.QueryDouble(element.GetterCommand)
    End If
    Return existingValue
  End Function

#End Region

#Region " GETTERS w/ ACCESS w/ SCPI VALUE "

  ''' <summary>
  ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
  ''' Uses or updates the nullable value getter command.
  ''' </summary>
  ''' <param name="element">Specifies the <see cref="IScpiValueBase">existing element</see> where the getter command is stored.</param>
      Public Function Getter(ByVal element As IScpiValue(Of Double), ByVal access As ResourceAccessLevels) As IScpiValue(Of Double)
    If access.IsDeviceAccess OrElse Not element.Value.HasValue Then
      element.Value = Me.Controller.QueryDouble(element.GetterCommand)
      element.ActualValue = element.Value
    End If
    Return element
  End Function

  ''' <summary>
  ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
  ''' Applies readings to the <paramref name="element">existing element</paramref>
  ''' </summary>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}?' where 
  ''' the first item is the <see cref="Name">SCPI system name</see> and 
  ''' the second item is a <paramref name="command">command</paramref>
  ''' </param>
  ''' <param name="element">Specifies the element holding the existing value, which is updated.</param>
      Public Function Getter(ByVal element As IScpiValue(Of Double), 
                         ByVal subHeader As String, 
                         ByVal access As ResourceAccessLevels) As IScpiValue(Of Double)
    element.Value = Me.Getter(element, subHeader, element.Value, access)
    element.ActualValue = element.Value
    Return element
  End Function

  ''' <summary>
  ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
  ''' Applies readings to the <paramref name="element">existing element</paramref>
  ''' </summary>
  ''' <param name="modalityName">Specifies the modality name.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2}?' where 
  ''' the first item is the <see cref="Name">SCPI system name</see>, 
  ''' the second item is the <paramref name="modalityName">modality name</paramref> and
  ''' the second item is a <paramref name="command">command</paramref>.
  ''' </param>
  ''' <param name="element">Specifies the element holding the existing value, which is updated.</param>
      Public Function Getter(ByVal element As IScpiValue(Of Double), 
                         ByVal modalityName As String, ByVal subHeader As String, 
                         ByVal access As ResourceAccessLevels) As IScpiValue(Of Double)
    element.Value = Me.Getter(element, modalityName, subHeader, element.Value, access)
    element.ActualValue = element.Value
    Return element
  End Function

#End Region

#Region " SETTERS "

  ''' <summary>
  ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
  ''' </summary>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1} {2}' where 
  ''' the first item is the <see cref="Name">SCPI system name</see>, 
  ''' the second item is a <paramref name="command">command</paramref>
  ''' the third item represents a <paramref name="value">numeric value</paramref>.
  ''' </param>
  ''' <param name="value">The value to set in the instrument.</param>
      Public Function Setter(ByVal subHeader As String, ByVal value As Double, ByVal verify As Boolean) As Boolean
    Dim scpiMessage As String = Syntax.BuildCommand(MyBase.SyntaxHeader, subHeader, value)
    Me.Controller.Port.WriteLine(scpiMessage)
    If verify Then
      Dim actual As Nullable(Of Double) = Me.QueryDouble(scpiMessage)
      Return actual.HasValue AndAlso (value = actual.Value)
    Else
      Return True
    End If
  End Function

  ''' <summary>
  ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
  ''' </summary>
  ''' <param name="modalityName">Specifies the modality name.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2} {3}' where 
  ''' the first item is the <see cref="Name">SCPI system name</see>, 
  ''' the second item is the <paramref name="modalityName">modality name</paramref>, 
  ''' the third item is a <paramref name="command">command</paramref>
  ''' the fourth item represents a <paramref name=" value">numeric value</paramref>.
  ''' </param>
  ''' <param name="value">The value to set in the instrument.</param>
      Public Function Setter(ByVal modalityName As String, ByVal subHeader As String, ByVal value As Double, ByVal verify As Boolean) As Boolean
    Dim scpiMessage As String = Syntax.BuildCommand(MyBase.SyntaxHeader, modalityName, subHeader, value)
    Me.Controller.Port.WriteLine(scpiMessage)
    If verify Then
      Dim actual As Nullable(Of Double) = Me.QueryDouble(modalityName, subHeader)
      Return actual.HasValue AndAlso (value = actual.Value)
    Else
      Return True
    End If
  End Function

#End Region

#Region " SETTERS w/ ACCESS "

  ''' <summary>
  ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
  ''' </summary>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1} {2}' where 
  ''' the first item is the <see cref="Name">SCPI system name</see> 
  ''' the second item is a <paramref name="command">command</paramref> and 
  ''' the third item is the <paramref name="value">value</paramref>.
  ''' </param>
  ''' <param name="existingValue">Specifies the existing value.</param>
      Public Function Setter(ByVal subHeader As String, ByVal existingValue As Nullable(Of Double), 
                         ByVal value As Double, ByVal access As ResourceAccessLevels) As Nullable(Of Double)
    If access.IsVerifyAccess Then
      Throw New ArgumentException("Verification not supported by this method", "access")
    End If
    If access.IsDeviceAccess OrElse Not ScpiDouble.Approximates(value, existingValue, Single.Epsilon) Then
      Me.Controller.Port.WriteLine(Syntax.BuildCommand(MyBase.SyntaxHeader, subHeader, value))
    End If
    Return New Nullable(Of Double)(value)
  End Function

  ''' <summary>
  ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
  ''' </summary>
  ''' <param name="modalityName">Specifies the modality name.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2} {3}' where 
  ''' the first item is the <see cref="Name">SCPI system name</see>, 
  ''' the second item is the <paramref name="modalityName">modality name</paramref>, 
  ''' the second item is a <paramref name="command">command</paramref> and
  ''' the fourth item represents a <paramref name="value">numeric value</paramref>.
  ''' </param>
  ''' <param name="existingValue">Specifies the existing value.</param>
      Public Function Setter(ByVal modalityName As String, ByVal subHeader As String, 
                         ByVal existingValue As Nullable(Of Double), 
                         ByVal value As Double, ByVal access As ResourceAccessLevels) As Nullable(Of Double)
    If access.IsVerifyAccess Then
      Throw New ArgumentException("Verification not supported by this method", "access")
    End If
    If access.IsDeviceAccess OrElse Not ScpiDouble.Approximates(value, existingValue, Single.Epsilon) Then
      Me.Controller.Port.WriteLine(Syntax.BuildCommand(MyBase.SyntaxHeader, modalityName, subHeader, value))
    End If
    Return New Nullable(Of Double)(value)
  End Function

#End Region

#Region " SETTERS w/ ACCESS w/ ELEMENT "

  ''' <summary>
  ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
  ''' </summary>
  ''' <param name="element">Specifies the <see cref="IScpiValueBase">existing element</see> where the getter command is stored.</param>
  ''' <param name="existingValue">Specifies the existing element value where the instrument.</param>
  ''' <param name="value">The value to be set using the element <see cref="IScpiValueBase.SetterCommandFormat">command</see>.</param>
  ''' <param name="access">The desired <see cref="ResourceAccessLevels">access level</see> to the instrument.</param>
      Public Function Setter(ByVal element As IScpiValueBase, 
                         ByVal existingValue As Double, ByVal value As Double, 
                         ByVal access As ResourceAccessLevels) As Double
    If access.IsVerifyAccess Then
      Throw New ArgumentException("Verification not supported by this method", "access")
    End If
    If access.IsDeviceAccess OrElse value <> existingValue Then
      If Not String.IsNullOrWhiteSpace(element.SetterCommandFormat) Then
        element.SetterCommand = String.Format(Globalization.CultureInfo.InvariantCulture, 
                                        element.SetterCommandFormat, value)
      End If
      Me.Controller.Port.WriteLine(element.SetterCommand)
    End If
    Return value
  End Function

  ''' <summary>
  ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
  ''' Uses or updates the nullable value getter command.
  ''' </summary>
  ''' <param name="element">Specifies the <see cref="IScpiValueBase">existing element</see> where the getter command is stored.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1} {2}' where 
  ''' the first item is the <see cref="Name">SCPI system name</see> 
  ''' the second item is a <paramref name="command">command</paramref> and 
  ''' the third item is the <paramref name="value">value</paramref>.
  ''' </param>
  ''' <param name="existingValue">Specifies the existing value.</param>
  ''' <param name="value">The value to set in the instrument.</param>
  ''' <param name="access">The desired <see cref="ResourceAccessLevels">access level</see> to the instrument.</param>
      Public Function Setter(ByVal element As IScpiValueBase, 
                         ByVal subHeader As String, ByVal existingValue As Nullable(Of Double), 
                         ByVal value As Double, ByVal access As ResourceAccessLevels) As Nullable(Of Double)
    If access.IsVerifyAccess Then
      Throw New ArgumentException("Verification not supported by this method", "access")
    End If
    If access.IsDeviceAccess OrElse Not ScpiDouble.Approximates(value, existingValue, Single.Epsilon) Then
      element.SetterCommand = Syntax.BuildCommand(MyBase.SyntaxHeader, subHeader, value)
      Me.Controller.Port.WriteLine(element.SetterCommand)
    End If
    Return New Nullable(Of Double)(value)
  End Function

  ''' <summary>
  ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
  ''' Uses or updates the nullable value getter command.
  ''' </summary>
  ''' <param name="element">Specifies the <see cref="IScpiValueBase">existing element</see> where the getter command is stored.</param>
  ''' <param name="modalityName">Specifies the modality name.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2} {3}' where 
  ''' the first item is the <see cref="Name">SCPI system name</see>, 
  ''' the second item is the <paramref name="modalityName">modality name</paramref>, 
  ''' the second item is a <paramref name="command">command</paramref> and
  ''' the fourth item represents a <paramref name="value">numeric value</paramref>.
  ''' </param>
  ''' <param name="existingValue">Specifies the existing value.</param>
  ''' <param name="value">The value to set in the instrument.</param>
  ''' <param name="access">The desired <see cref="ResourceAccessLevels">access level</see> to the instrument.</param>
      Public Function Setter(ByVal element As IScpiValueBase, 
                         ByVal modalityName As String, ByVal subHeader As String, 
                         ByVal existingValue As Nullable(Of Double), 
                         ByVal value As Double, ByVal access As ResourceAccessLevels) As Nullable(Of Double)
    If access.IsVerifyAccess Then
      Throw New ArgumentException("Verification not supported by this method", "access")
    End If
    If access.IsDeviceAccess OrElse Not ScpiDouble.Approximates(value, existingValue, Single.Epsilon) Then
      element.SetterCommand = Syntax.BuildCommand(MyBase.SyntaxHeader, modalityName, subHeader, value)
      Me.Controller.Port.WriteLine(element.SetterCommand)
    End If
    Return New Nullable(Of Double)(value)
  End Function

#End Region

#Region " SETTERS w/ ACCESS w/ SCPI VALUE "

  ''' <summary>
  ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
  ''' Applies readings to the <paramref name="element">existing element</paramref>
  ''' Assume element has both <see cref="IScpiValueBase.GetterCommand">getter</see> and 
  ''' <see cref="IScpiValueBase.SetterCommand">setter</see> commands specified.
  ''' </summary>
  ''' <param name="element">Specifies the element holding the existing value, which is updated.</param>
  ''' <param name="value">The value to be set using the element <see cref="IScpiValueBase.SetterCommandFormat">command</see>.</param>
  ''' <param name="access">The desired <see cref="ResourceAccessLevels">access level</see> to the instrument.</param>
      Public Function Setter(ByVal element As IScpiValue(Of Double), ByVal value As Double, ByVal access As ResourceAccessLevels) As IScpiValue(Of Double)
    If access.IsDeviceAccess OrElse (value <> element.Value) Then
      element.Value = Me.Setter(CType(element, IScpiValueBase), element.Value.Value, value, access And Not ResourceAccessLevels.Verify)
    End If
    If access.IsVerifyAccess Then
      Me.Getter(element, ResourceAccessLevels.Device)
      element.Value = value
    ElseIf access.IsCacheAccess Then
      element.ActualValue = value
    End If
    Return element
  End Function

  ''' <summary>
  ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
  ''' Applies readings to the <paramref name="element">existing element</paramref>
  ''' </summary>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1} {2}' where 
  ''' the first item is the <see cref="Name">SCPI system name</see> 
  ''' the second item is a <paramref name="command">command</paramref> and 
  ''' the third item is the <paramref name="value">value</paramref>.
  ''' </param>
  ''' <param name="element">Specifies the element holding the existing value, which is updated.</param>
      Public Function Setter(ByVal element As IScpiValue(Of Double), 
                         ByVal subHeader As String, ByVal value As Double, 
                         ByVal access As ResourceAccessLevels) As IScpiValue(Of Double)
    element.Value = Me.Setter(CType(element, IScpiValueBase), subHeader, element.Value, value, access And Not ResourceAccessLevels.Verify)
    ' element.Value = Me.Setter(CType(element, IScpiValueBase), subHeader, element.Value, element.BoundValue(value), access And Not ResourceAccessLevels.Verify)
    If access.IsVerifyAccess Then
      Me.Getter(element, subHeader, ResourceAccessLevels.Device)
      element.Value = value
    ElseIf access.IsCacheAccess Then
      element.ActualValue = value
    End If
    Return element
  End Function

  ''' <summary>
  ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
  ''' Applies readings to the <paramref name="element">existing element</paramref>
  ''' </summary>
  ''' <param name="modalityName">Specifies the modality name.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2} {3}' where 
  ''' the first item is the <see cref="Name">SCPI system name</see>, 
  ''' the second item is the <paramref name="modalityName">modality name</paramref>, 
  ''' the second item is a <paramref name="command">command</paramref> and
  ''' the fourth item represents a <paramref name="value">numeric value</paramref>.
  ''' </param>
  ''' <param name="element">Specifies the element holding the existing value, which is updated.</param>
      Public Function Setter(ByVal element As IScpiValue(Of Double), 
                         ByVal modalityName As String, ByVal subHeader As String, ByVal value As Double, 
                         ByVal access As ResourceAccessLevels) As IScpiValue(Of Double)
    element.Value = Me.Setter(CType(element, IScpiValueBase), modalityName, subHeader, element.Value, value, access And Not ResourceAccessLevels.Verify)
    'element.Value = Me.Setter(CType(element, IScpiValueBase), modalityName, subHeader, element.Value, element.BoundValue(value), access And Not ResourceAccessLevels.Verify)
    If access.IsVerifyAccess Then
      Me.Getter(element, subHeader, ResourceAccessLevels.Device)
      element.Value = value
    ElseIf access.IsCacheAccess Then
      element.ActualValue = value
    End If
    Return element
  End Function

#End Region

#End Region

#Region " INTEGER "

#Region " QUERRIES "

  ''' <summary>
  ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
  ''' </summary>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}?' where 
  ''' the first item is the <see cref="Name">SCPI system name</see> and 
  ''' the second item is a <paramref name="command">command</paramref>.
  ''' </param>
      Public Function QueryInteger(ByVal subHeader As String) As Nullable(Of Integer)
    Return Me.Controller.QueryInteger(Syntax.BuildQuery(MyBase.SyntaxHeader, subHeader))
  End Function

  ''' <summary>
  ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
  ''' </summary>
  ''' <param name="modalityName">Specifies the modality name.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2}?' where 
  ''' the first item is the <see cref="Name">SCPI system name</see>, 
  ''' the second item is the <paramref name="modalityName">modality name</paramref> and 
  ''' the thrid item is a <paramref name="command">command</paramref>.
  ''' </param>
      Public Function QueryInteger(ByVal modalityName As String, ByVal subHeader As String) As Nullable(Of Integer)
    Return Me.Controller.QueryInteger(Syntax.BuildQuery(MyBase.SyntaxHeader, modalityName, subHeader))
  End Function

#End Region

#Region " QUERRIES - INFINITY "

  ''' <summary>
  ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
  ''' </summary>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}?' where 
  ''' the first item is the <see cref="Name">SCPI system name</see> and 
  ''' the second item is a <paramref name="command">command</paramref>.
  ''' </param>
      Public Function QueryInfInteger(ByVal subHeader As String) As Nullable(Of Integer)
    Return Me.Controller.QueryInfInteger(Syntax.BuildQuery(MyBase.SyntaxHeader, subHeader))
  End Function

  ''' <summary>
  ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
  ''' </summary>
  ''' <param name="modalityName">Specifies the modality name.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2}?' where 
  ''' the first item is the <see cref="Name">SCPI system name</see>, 
  ''' the second item is the <paramref name="modalityName">modality name</paramref> and 
  ''' the thrid item is a <paramref name="command">command</paramref>.
  ''' </param>
      Public Function QueryInfInteger(ByVal modalityName As String, ByVal subHeader As String) As Nullable(Of Integer)
    Return Me.Controller.QueryInfInteger(Syntax.BuildQuery(MyBase.SyntaxHeader, modalityName, subHeader))
  End Function

#End Region

#Region " GETTERS "

  ''' <summary>
  ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
  ''' </summary>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}?' where 
  ''' the first item is the <see cref="Name">SCPI system name</see> and 
  ''' the second item is a <paramref name="command">command</paramref>
  ''' </param>
  ''' <param name="existingValue">Specifies the existing value.</param>
      Public Function Getter(ByVal subHeader As String, ByVal existingValue As Nullable(Of Integer), ByVal access As ResourceAccessLevels) As Nullable(Of Int32)
    If access.IsDeviceAccess OrElse Not existingValue.HasValue Then
      existingValue = Me.QueryInteger(subHeader)
    End If
    Return existingValue
  End Function

  ''' <summary>
  ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
  ''' </summary>
  ''' <param name="modalityName">Specifies the modality name.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2}?' where 
  ''' the first item is the <see cref="Name">SCPI system name</see>, 
  ''' the second item is the <paramref name="modalityName">modality name</paramref> and
  ''' the second item is a <paramref name="command">command</paramref>.
  ''' </param>
  ''' <param name="existingValue">Specifies the existing value.</param>
      Public Function Getter(ByVal modalityName As String, ByVal subHeader As String, 
                         ByVal existingValue As Nullable(Of Integer), 
                         ByVal access As ResourceAccessLevels) As Nullable(Of Integer)
    If access.IsDeviceAccess OrElse Not existingValue.HasValue Then
      existingValue = Me.QueryInteger(modalityName, subHeader)
    End If
    Return existingValue
  End Function

  ''' <summary>
  ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
  ''' </summary>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}?' where 
  ''' the first item is the <see cref="Name">SCPI system name</see> and 
  ''' the second item is a <paramref name="command">command</paramref>
  ''' </param>
  ''' <param name="existingValue">Specifies the existing value.</param>
      Public Function GetterInfinity(ByVal subHeader As String, ByVal existingValue As Nullable(Of Integer), 
                                 ByVal access As ResourceAccessLevels) As Nullable(Of Integer)
    If access.IsDeviceAccess OrElse Not existingValue.HasValue Then
      existingValue = Me.QueryInfInteger(subHeader)
    End If
    Return existingValue
  End Function

  ''' <summary>
  ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
  ''' </summary>
  ''' <param name="modalityName">Specifies the modality name.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2}?' where 
  ''' the first item is the <see cref="Name">SCPI system name</see>, 
  ''' the second item is the <paramref name="modalityName">modality name</paramref> and
  ''' the second item is a <paramref name="command">command</paramref>.
  ''' </param>
  ''' <param name="existingValue">Specifies the existing value.</param>
      Public Function GetterInfinity(ByVal modalityName As String, ByVal subHeader As String, 
                                 ByVal existingValue As Nullable(Of Integer), 
                                 ByVal access As ResourceAccessLevels) As Nullable(Of Integer)
    If access.IsDeviceAccess OrElse Not existingValue.HasValue Then
      existingValue = Me.QueryInfInteger(modalityName, subHeader)
    End If
    Return existingValue
  End Function

#End Region

#Region " GETTERS w/ ELEMENT "

  ''' <summary>
  ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
  ''' Uses or updates the nullable value getter command.
  ''' </summary>
  ''' <param name="element">Specifies the <see cref="IScpiValueBase">existing element</see> where the getter command is stored.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}?' where 
  ''' the first item is the <see cref="Name">SCPI system name</see> and 
  ''' the second item is a <paramref name="command">command</paramref>
  ''' </param>
  ''' <param name="existingValue">Specifies the existing value.</param>
      Public Function Getter(ByVal element As IScpiValueBase, 
                         ByVal subHeader As String, ByVal existingValue As Nullable(Of Integer), 
                         ByVal access As ResourceAccessLevels) As Nullable(Of Int32)
    If access.IsDeviceAccess OrElse Not existingValue.HasValue Then
      If String.IsNullOrWhiteSpace(element.GetterCommand) Then
        element.GetterCommand = Syntax.BuildQuery(MyBase.SyntaxHeader, subHeader)
      End If
      ' existingValue = Me.QueryInteger(subHeader)
      existingValue = Me.Controller.QueryInteger(element.GetterCommand)
    End If
    Return existingValue
  End Function

  ''' <summary>
  ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
  ''' Uses or updates the nullable value getter command.
  ''' </summary>
  ''' <param name="element">Specifies the <see cref="IScpiValueBase">existing element</see> where the getter command is stored.</param>
  ''' <param name="modalityName">Specifies the modality name.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2}?' where 
  ''' the first item is the <see cref="Name">SCPI system name</see>, 
  ''' the second item is the <paramref name="modalityName">modality name</paramref> and
  ''' the second item is a <paramref name="command">command</paramref>.
  ''' </param>
  ''' <param name="existingValue">Specifies the existing value.</param>
      Public Function Getter(ByVal element As IScpiValueBase, 
                         ByVal modalityName As String, ByVal subHeader As String, ByVal existingValue As Nullable(Of Integer), 
                         ByVal access As ResourceAccessLevels) As Nullable(Of Integer)
    If access.IsDeviceAccess OrElse Not existingValue.HasValue Then
      If String.IsNullOrWhiteSpace(element.GetterCommand) Then
        element.GetterCommand = Syntax.BuildQuery(MyBase.SyntaxHeader, modalityName, subHeader)
      End If
      ' existingValue = Me.QueryInteger(modalityName, subHeader)
      existingValue = Me.Controller.QueryInteger(element.GetterCommand)
    End If
    Return existingValue
  End Function

  ''' <summary>
  ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
  ''' Uses or updates the nullable value getter command.
  ''' </summary>
  ''' <param name="element">Specifies the <see cref="IScpiValueBase">existing element</see> where the getter command is stored.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}?' where 
  ''' the first item is the <see cref="Name">SCPI system name</see> and 
  ''' the second item is a <paramref name="command">command</paramref>
  ''' </param>
  ''' <param name="existingValue">Specifies the existing value.</param>
      Public Function GetterInfinity(ByVal element As IScpiValueBase, 
                                 ByVal subHeader As String, ByVal existingValue As Nullable(Of Integer), 
                                 ByVal access As ResourceAccessLevels) As Nullable(Of Integer)
    If access.IsDeviceAccess OrElse Not existingValue.HasValue Then
      If String.IsNullOrWhiteSpace(element.GetterCommand) Then
        element.GetterCommand = Syntax.BuildQuery(MyBase.SyntaxHeader, subHeader)
      End If
      ' existingValue = Me.QueryInfInteger(subHeader)
      existingValue = Me.Controller.QueryInfInteger(element.GetterCommand)
    End If
    Return existingValue
  End Function

  ''' <summary>
  ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
  ''' Uses or updates the nullable value getter command.
  ''' </summary>
  ''' <param name="element">Specifies the <see cref="IScpiValueBase">existing element</see> where the getter command is stored.</param>
  ''' <param name="modalityName">Specifies the modality name.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2}?' where 
  ''' the first item is the <see cref="Name">SCPI system name</see>, 
  ''' the second item is the <paramref name="modalityName">modality name</paramref> and
  ''' the second item is a <paramref name="command">command</paramref>.
  ''' </param>
  ''' <param name="existingValue">Specifies the existing value.</param>
      Public Function GetterInfinity(ByVal element As IScpiValueBase, 
                                 ByVal modalityName As String, ByVal subHeader As String, ByVal existingValue As Nullable(Of Integer), 
                                 ByVal access As ResourceAccessLevels) As Nullable(Of Integer)
    If access.IsDeviceAccess OrElse Not existingValue.HasValue Then
      If String.IsNullOrWhiteSpace(element.GetterCommand) Then
        element.GetterCommand = Syntax.BuildQuery(MyBase.SyntaxHeader, modalityName, subHeader)
      End If
      ' existingValue = Me.QueryInfInteger(modalityName, subHeader)
      existingValue = Me.Controller.QueryInfInteger(element.GetterCommand)
    End If
    Return existingValue
  End Function

#End Region

#Region " GETTERS w/ SCPI VALUE "

  ''' <summary>
  ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
  ''' Uses or updates the nullable value getter command.
  ''' </summary>
  ''' <param name="element">Specifies the <see cref="IScpiValueBase">existing element</see> where the getter command is stored.</param>
      Public Function Getter(ByVal element As IScpiValue(Of Integer), ByVal access As ResourceAccessLevels) As IScpiValue(Of Integer)
    If access.IsDeviceAccess OrElse Not element.Value.HasValue Then
      element.Value = Me.Controller.QueryInteger(element.GetterCommand)
      element.ActualValue = element.Value
    End If
    Return element
  End Function

  ''' <summary>
  ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
  ''' Uses or updates the nullable value getter command.
  ''' </summary>
  ''' <param name="element">Specifies the <see cref="IScpiValueBase">existing element</see> where the getter command is stored.</param>
      Public Function GetterInfinity(ByVal element As IScpiValue(Of Integer), ByVal access As ResourceAccessLevels) As IScpiValue(Of Integer)
    If access.IsDeviceAccess OrElse Not element.Value.HasValue Then
      element.Value = Me.Controller.QueryInfInteger(element.GetterCommand)
      element.ActualValue = element.Value
    End If
    Return element
  End Function

  ''' <summary>
  ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
  ''' Applies readings to the <paramref name="element">existing element</paramref>
  ''' </summary>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}?' where 
  ''' the first item is the <see cref="Name">SCPI system name</see> and 
  ''' the second item is a <paramref name="command">command</paramref>
  ''' </param>
  ''' <param name="element">Specifies the element holding the existing value, which is updated.</param>
      Public Function Getter(ByVal element As IScpiValue(Of Integer), 
                         ByVal subHeader As String, 
                         ByVal access As ResourceAccessLevels) As IScpiValue(Of Integer)
    element.Value = Me.Getter(element, subHeader, element.Value, access)
    element.ActualValue = element.Value
    Return element
  End Function

  ''' <summary>
  ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
  ''' Applies readings to the <paramref name="element">existing element</paramref>
  ''' </summary>
  ''' <param name="modalityName">Specifies the modality name.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2}?' where 
  ''' the first item is the <see cref="Name">SCPI system name</see>, 
  ''' the second item is the <paramref name="modalityName">modality name</paramref> and
  ''' the second item is a <paramref name="command">command</paramref>.
  ''' </param>
  ''' <param name="element">Specifies the element holding the existing value, which is updated.</param>
      Public Function Getter(ByVal element As IScpiValue(Of Integer), 
                         ByVal modalityName As String, ByVal subHeader As String, 
                         ByVal access As ResourceAccessLevels) As IScpiValue(Of Integer)
    element.Value = Me.Getter(element, modalityName, subHeader, element.Value, access)
    element.ActualValue = element.Value
    Return element
  End Function

  ''' <summary>
  ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
  ''' Applies readings to the <paramref name="element">existing element</paramref>
  ''' </summary>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}?' where 
  ''' the first item is the <see cref="Name">SCPI system name</see> and 
  ''' the second item is a <paramref name="command">command</paramref>
  ''' </param>
  ''' <param name="element">Specifies the element holding the existing value, which is updated.</param>
      Public Function GetterInfinity(ByVal element As IScpiValue(Of Integer), 
                                 ByVal subHeader As String, ByVal access As ResourceAccessLevels) As IScpiValue(Of Integer)
    element.Value = Me.GetterInfinity(element, subHeader, element.Value, access)
    element.ActualValue = element.Value
    Return element
  End Function

  ''' <summary>
  ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
  ''' Applies readings to the <paramref name="element">existing element</paramref>
  ''' </summary>
  ''' <param name="modalityName">Specifies the modality name.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2}?' where 
  ''' the first item is the <see cref="Name">SCPI system name</see>, 
  ''' the second item is the <paramref name="modalityName">modality name</paramref> and
  ''' the second item is a <paramref name="command">command</paramref>.
  ''' </param>
  ''' <param name="element">Specifies the element holding the existing value, which is updated.</param>
      Public Function GetterInfinity(ByVal element As IScpiValue(Of Integer), 
                                 ByVal modalityName As String, ByVal subHeader As String, 
                                 ByVal access As ResourceAccessLevels) As IScpiValue(Of Integer)
    element.Value = Me.GetterInfinity(element, modalityName, subHeader, element.Value, access)
    element.ActualValue = element.Value
    Return element
  End Function

#End Region

#Region " SETTERS "

  ''' <summary>
  ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
  ''' </summary>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1} {2}' where 
  ''' the first item is the <see cref="Name">SCPI system name</see>, 
  ''' the second item is a <paramref name="command">command</paramref>
  ''' the third item represents a <paramref name="value">numeric value</paramref>.
  ''' </param>
  ''' <param name="value">Specifies the command value.</param>
      Public Function Setter(ByVal subHeader As String, ByVal value As Integer, ByVal verify As Boolean) As Boolean
    Dim scpiMessage As String = Syntax.BuildCommand(MyBase.SyntaxHeader, subHeader, value)
    Me.Controller.Port.WriteLine(scpiMessage)
    If verify Then
      Dim actual As Nullable(Of Integer) = Me.QueryInteger(scpiMessage)
      Return actual.HasValue AndAlso (value = actual.Value)
    Else
      Return True
    End If
  End Function

  ''' <summary>
  ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
  ''' </summary>
  ''' <param name="modalityName">Specifies the modality name.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2} {3}' where 
  ''' the first item is the <see cref="Name">SCPI system name</see>, 
  ''' the second item is the <paramref name="modalityName">modality name</paramref>, 
  ''' the third item is a <paramref name="command">command</paramref>
  ''' the fourth item represents a <paramref name="value">numeric value</paramref>.
  ''' </param>
  ''' <param name="value">Specifies the command value.</param>
      Public Function Setter(ByVal modalityName As String, ByVal subHeader As String, ByVal value As Integer, ByVal verify As Boolean) As Boolean
    Dim scpiMessage As String = Syntax.BuildCommand(MyBase.SyntaxHeader, modalityName, subHeader, value)
    Me.Controller.Port.WriteLine(scpiMessage)
    If verify Then
      Dim actual As Nullable(Of Integer) = Me.QueryInteger(modalityName, subHeader)
      Return actual.HasValue AndAlso (value = actual.Value)
    Else
      Return True
    End If
  End Function

  ''' <summary>
  ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
  ''' </summary>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1} {2}' where 
  ''' the first item is the <see cref="Name">SCPI system name</see>, 
  ''' the second item is a <paramref name="command">command</paramref>
  ''' the third item represents a <paramref name="value">numeric value</paramref>.
  ''' </param>
  ''' <param name="value">Specifies the command value.</param>
      Public Function SetterInfinity(ByVal subHeader As String, ByVal value As Integer, ByVal verify As Boolean) As Boolean
    Dim scpiMessage As String = Syntax.BuildCommand(MyBase.SyntaxHeader, subHeader, value)
    Me.Controller.Port.WriteLine(scpiMessage)
    If verify Then
      Dim actual As Nullable(Of Integer) = Me.QueryInteger(scpiMessage)
      Return actual.HasValue AndAlso (value = actual.Value)
    Else
      Return True
    End If
  End Function

  ''' <summary>
  ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
  ''' </summary>
  ''' <param name="modalityName">Specifies the modality name.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2} {3}' where 
  ''' the first item is the <see cref="Name">SCPI system name</see>, 
  ''' the second item is the <paramref name="modalityName">modality name</paramref>, 
  ''' the third item is a <paramref name="command">command</paramref>
  ''' the fourth item represents a <paramref name="value">numeric value</paramref>.
  ''' </param>
  ''' <param name="value">Specifies the command value.</param>
      Public Function SetterInfinity(ByVal modalityName As String, ByVal subHeader As String, ByVal value As Integer, ByVal verify As Boolean) As Boolean
    Dim scpiMessage As String = Syntax.BuildCommand(MyBase.SyntaxHeader, modalityName, subHeader, value)
    Me.Controller.Port.WriteLine(scpiMessage)
    If verify Then
      Dim actual As Nullable(Of Integer) = Me.QueryInteger(modalityName, subHeader)
      Return actual.HasValue AndAlso (value = actual.Value)
    Else
      Return True
    End If
  End Function

#End Region

#Region " SETTERS w/ ACCESS "

  ''' <summary>
  ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
  ''' </summary>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1} {2}' where 
  ''' the first item is the <see cref="Name">SCPI system name</see> 
  ''' the second item is a <paramref name="command">command</paramref> and 
  ''' the third item is the <paramref name="value">value</paramref>.
  ''' </param>
  ''' <param name="existingValue">Specifies the existing value.</param>
      Public Function Setter(ByVal subHeader As String, ByVal existingValue As Nullable(Of Integer), 
                         ByVal value As Integer, ByVal access As ResourceAccessLevels) As Nullable(Of Integer)
    If access.IsVerifyAccess Then
      Throw New ArgumentException("Verification not supported by this method", "access")
    End If
    If access.IsDeviceAccess OrElse (value <> existingValue) Then
      Me.Controller.Port.WriteLine(Syntax.BuildCommand(MyBase.SyntaxHeader, subHeader, value))
    End If
    Return New Nullable(Of Integer)(value)
  End Function

  ''' <summary>
  ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
  ''' </summary>
  ''' <param name="modalityName">Specifies the modality name.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2} {3}' where 
  ''' the first item is the <see cref="Name">SCPI system name</see>, 
  ''' the second item is the <paramref name="modalityName">modality name</paramref>, 
  ''' the second item is a <paramref name="command">command</paramref> and
  ''' the fourth item represents a <paramref name="value">numeric value</paramref>.
  ''' </param>
  ''' <param name="existingValue">Specifies the existing value.</param>
      Public Function Setter(ByVal modalityName As String, ByVal subHeader As String, 
                         ByVal existingValue As Nullable(Of Integer), 
                         ByVal value As Integer, ByVal access As ResourceAccessLevels) As Nullable(Of Integer)
    If access.IsVerifyAccess Then
      Throw New ArgumentException("Verification not supported by this method", "access")
    End If
    If access.IsDeviceAccess OrElse (value <> existingValue) Then
      Me.Controller.Port.WriteLine(Syntax.BuildCommand(MyBase.SyntaxHeader, modalityName, subHeader, value))
    End If
    Return New Nullable(Of Integer)(value)
  End Function

  ''' <summary>
  ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
  ''' </summary>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1} {2}' where 
  ''' the first item is the <see cref="Name">SCPI system name</see> 
  ''' the second item is a <paramref name="command">command</paramref> and 
  ''' the third item is the <paramref name="value">value</paramref>.
  ''' </param>
  ''' <param name="existingValue">Specifies the existing value.</param>
      Public Function SetterInfinity(ByVal subHeader As String, ByVal existingValue As Nullable(Of Integer), 
                                 ByVal value As Integer, 
                                 ByVal access As ResourceAccessLevels) As Nullable(Of Integer)
    If access.IsVerifyAccess Then
      Throw New ArgumentException("Verification not supported by this method", "access")
    End If
    If access.IsDeviceAccess OrElse (value <> existingValue) Then
      Me.Controller.Port.WriteLine(Syntax.BuildCommand(MyBase.SyntaxHeader, subHeader, value))
    End If
    Return New Nullable(Of Integer)(value)
  End Function

  ''' <summary>
  ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
  ''' </summary>
  ''' <param name="modalityName">Specifies the modality name.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2} {3}' where 
  ''' the first item is the <see cref="Name">SCPI system name</see>, 
  ''' the second item is the <paramref name="modalityName">modality name</paramref>, 
  ''' the second item is a <paramref name="command">command</paramref> and
  ''' the fourth item represents a <paramref name="value">numeric value</paramref>.
  ''' </param>
  ''' <param name="existingValue">Specifies the existing value.</param>
      Public Function SetterInfinity(ByVal modalityName As String, ByVal subHeader As String, 
                                 ByVal existingValue As Nullable(Of Integer), 
                                 ByVal value As Integer, ByVal access As ResourceAccessLevels) As Nullable(Of Integer)
    If access.IsVerifyAccess Then
      Throw New ArgumentException("Verification not supported by this method", "access")
    End If
    If access.IsDeviceAccess OrElse (value <> existingValue) Then
      Me.Controller.Port.WriteLine(Syntax.BuildCommand(MyBase.SyntaxHeader, modalityName, subHeader, value))
    End If
    Return New Nullable(Of Integer)(value)
  End Function

#End Region

#Region " SETTERS w/ ACCESS w/ ELEMENT "

  ''' <summary>
  ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
  ''' </summary>
  ''' <param name="element">Specifies the <see cref="IScpiValueBase">existing element</see> where the getter command is stored.</param>
  ''' <param name="existingValue">Specifies the existing element value where the instrument.</param>
  ''' <param name="value">Specifies the value to be set using the element <see cref="IScpiValueBase.SetterCommandFormat">command</see>.</param>
      Public Function Setter(ByVal element As IScpiValueBase, 
                         ByVal existingValue As Integer, ByVal value As Integer, 
                         ByVal access As ResourceAccessLevels) As Integer
    If access.IsVerifyAccess Then
      Throw New ArgumentException("Verification not supported by this method", "access")
    End If
    If access.IsDeviceAccess OrElse value <> existingValue Then
      If Not String.IsNullOrWhiteSpace(element.SetterCommandFormat) Then
        element.SetterCommand = String.Format(Globalization.CultureInfo.InvariantCulture, 
                                        element.SetterCommandFormat, value)
      End If
      Me.Controller.Port.WriteLine(element.SetterCommand)
    End If
    Return value
  End Function

  ''' <summary>
  ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
  ''' Uses or updates the nullable value getter command.
  ''' </summary>
  ''' <param name="element">Specifies the <see cref="IScpiValueBase">existing element</see> where the getter command is stored.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1} {2}' where 
  ''' the first item is the <see cref="Name">SCPI system name</see> 
  ''' the second item is a <paramref name="command">command</paramref> and 
  ''' the third item is the <paramref name="value">value</paramref>.
  ''' </param>
  ''' <param name="existingValue">Specifies the existing value.</param>
      Public Function Setter(ByVal element As IScpiValueBase, 
                         ByVal subHeader As String, ByVal existingValue As Nullable(Of Integer), ByVal value As Integer, 
                         ByVal access As ResourceAccessLevels) As Nullable(Of Integer)
    If access.IsVerifyAccess Then
      Throw New ArgumentException("Verification not supported by this method", "access")
    End If
    If access.IsDeviceAccess OrElse (value <> existingValue) Then
      element.SetterCommand = Syntax.BuildCommand(MyBase.SyntaxHeader, subHeader, value)
      Me.Controller.Port.WriteLine(element.SetterCommand)
    End If
    Return New Nullable(Of Integer)(value)
  End Function

  ''' <summary>
  ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
  ''' Uses or updates the nullable value getter command.
  ''' </summary>
  ''' <param name="element">Specifies the <see cref="IScpiValueBase">existing element</see> where the getter command is stored.</param>
  ''' <param name="modalityName">Specifies the modality name.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2} {3}' where 
  ''' the first item is the <see cref="Name">SCPI system name</see>, 
  ''' the second item is the <paramref name="modalityName">modality name</paramref>, 
  ''' the second item is a <paramref name="command">command</paramref> and
  ''' the fourth item represents a <paramref name="value">numeric value</paramref>.
  ''' </param>
  ''' <param name="existingValue">Specifies the existing value.</param>
      Public Function Setter(ByVal element As IScpiValueBase, 
                         ByVal modalityName As String, ByVal subHeader As String, ByVal existingValue As Nullable(Of Integer), 
                         ByVal value As Integer, 
                         ByVal access As ResourceAccessLevels) As Nullable(Of Integer)
    If access.IsVerifyAccess Then
      Throw New ArgumentException("Verification not supported by this method", "access")
    End If
    If access.IsDeviceAccess OrElse (value <> existingValue) Then
      element.SetterCommand = Syntax.BuildCommand(MyBase.SyntaxHeader, modalityName, subHeader, value)
      Me.Controller.Port.WriteLine(element.SetterCommand)
    End If
    Return New Nullable(Of Integer)(value)
  End Function

  ''' <summary>
  ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
  ''' Uses or updates the nullable value getter command.
  ''' </summary>
  ''' <param name="element">Specifies the <see cref="IScpiValueBase">existing element</see> where the getter command is stored.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1} {2}' where 
  ''' the first item is the <see cref="Name">SCPI system name</see> 
  ''' the second item is a <paramref name="command">command</paramref> and 
  ''' the third item is the <paramref name="value">value</paramref>.
  ''' </param>
  ''' <param name="existingValue">Specifies the existing value.</param>
      Public Function SetterInfinity(ByVal element As IScpiValueBase, 
                                 ByVal subHeader As String, ByVal existingValue As Nullable(Of Integer), ByVal value As Integer, 
                                 ByVal access As ResourceAccessLevels) As Nullable(Of Integer)
    If access.IsVerifyAccess Then
      Throw New ArgumentException("Verification not supported by this method", "access")
    End If
    If access.IsDeviceAccess OrElse (value <> existingValue) Then
      element.SetterCommand = Syntax.BuildCommand(MyBase.SyntaxHeader, subHeader, value)
      Me.Controller.Port.WriteLine(element.SetterCommand)
    End If
    Return New Nullable(Of Integer)(value)
  End Function

  ''' <summary>
  ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
  ''' Uses or updates the nullable value getter command.
  ''' </summary>
  ''' <param name="element">Specifies the <see cref="IScpiValueBase">existing element</see> where the getter command is stored.</param>
  ''' <param name="modalityName">Specifies the modality name.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2} {3}' where 
  ''' the first item is the <see cref="Name">SCPI system name</see>, 
  ''' the second item is the <paramref name="modalityName">modality name</paramref>, 
  ''' the second item is a <paramref name="command">command</paramref> and
  ''' the fourth item represents a <paramref name="value">numeric value</paramref>.
  ''' </param>
  ''' <param name="existingValue">Specifies the existing value.</param>
      Public Function SetterInfinity(ByVal element As IScpiValueBase, 
                                 ByVal modalityName As String, ByVal subHeader As String, ByVal existingValue As Nullable(Of Integer), 
                                 ByVal value As Integer, 
                                 ByVal access As ResourceAccessLevels) As Nullable(Of Integer)
    If access.IsVerifyAccess Then
      Throw New ArgumentException("Verification not supported by this method", "access")
    End If
    If access.IsDeviceAccess OrElse (value <> existingValue) Then
      element.SetterCommand = Syntax.BuildCommand(MyBase.SyntaxHeader, modalityName, subHeader, value)
      Me.Controller.Port.WriteLine(element.SetterCommand)
    End If
    Return New Nullable(Of Integer)(value)
  End Function

#End Region

#Region " SETTERS w/ SCPI VALUE "

  ''' <summary>
  ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
  ''' Applies readings to the <paramref name="element">existing element</paramref>
  ''' Assume element has both <see cref="IScpiValueBase.GetterCommand">getter</see> and 
  ''' <see cref="IScpiValueBase.SetterCommand">setter</see> commands specified.
  ''' </summary>
  ''' <param name="element">Specifies the element holding the existing value, which is updated.</param>
      Public Function Setter(ByVal element As IScpiValue(Of Integer), ByVal value As Integer, ByVal access As ResourceAccessLevels) As IScpiValue(Of Integer)
    If access.IsDeviceAccess OrElse (value <> element.Value) Then
      element.Value = Me.Setter(CType(element, IScpiValueBase), element.Value.Value, value, access And Not ResourceAccessLevels.Verify)
    End If
    If access.IsVerifyAccess Then
      Me.Getter(element, ResourceAccessLevels.Device)
      element.Value = value
    ElseIf access.IsCacheAccess Then
      element.ActualValue = value
    End If
    Return element
  End Function

  ''' <summary>
  ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
  ''' Applies readings to the <paramref name="element">existing element</paramref>
  ''' </summary>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1} {2}' where 
  ''' the first item is the <see cref="Name">SCPI system name</see> 
  ''' the second item is a <paramref name="command">command</paramref> and 
  ''' the third item is the <paramref name="value">value</paramref>.
  ''' </param>
  ''' <param name="element">Specifies the element holding the existing value, which is updated.</param>
      Public Function Setter(ByVal element As IScpiValue(Of Integer), 
                         ByVal subHeader As String, ByVal value As Integer, 
                         ByVal access As ResourceAccessLevels) As IScpiValue(Of Integer)
    element.Value = Me.Setter(CType(element, IScpiValueBase), subHeader, element.Value, value, access And Not ResourceAccessLevels.Verify)
    If access.IsVerifyAccess Then
      Me.Getter(element, subHeader, ResourceAccessLevels.Device)
      element.Value = value
    ElseIf access.IsCacheAccess Then
      element.ActualValue = value
    End If
    Return element
  End Function

  ''' <summary>
  ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
  ''' Applies readings to the <paramref name="element">existing element</paramref>
  ''' </summary>
  ''' <param name="modalityName">Specifies the modality name.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2} {3}' where 
  ''' the first item is the <see cref="Name">SCPI system name</see>, 
  ''' the second item is the <paramref name="modalityName">modality name</paramref>, 
  ''' the second item is a <paramref name="command">command</paramref> and
  ''' the fourth item represents a <paramref name="value">numeric value</paramref>.
  ''' </param>
  ''' <param name="element">Specifies the element holding the existing value, which is updated.</param>
      Public Function Setter(ByVal element As IScpiValue(Of Integer), 
                         ByVal modalityName As String, ByVal subHeader As String, ByVal value As Integer, 
                         ByVal access As ResourceAccessLevels) As IScpiValue(Of Integer)
    element.Value = Me.Setter(CType(element, IScpiValueBase), modalityName, subHeader, element.Value, value, access And Not ResourceAccessLevels.Verify)
    If access.IsVerifyAccess Then
      Me.Getter(element, subHeader, ResourceAccessLevels.Device)
      element.Value = value
    ElseIf access.IsCacheAccess Then
      element.ActualValue = value
    End If
    Return element
  End Function

  ''' <summary>
  ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
  ''' Applies readings to the <paramref name="element">existing element</paramref>
  ''' </summary>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1} {2}' where 
  ''' the first item is the <see cref="Name">SCPI system name</see> 
  ''' the second item is a <paramref name="command">command</paramref> and 
  ''' the third item is the <paramref name="value">value</paramref>.
  ''' </param>
  ''' <param name="element">Specifies the element holding the existing value, which is updated.</param>
      Public Function SetterInfinity(ByVal element As IScpiValue(Of Integer), 
                                 ByVal subHeader As String, ByVal value As Integer, 
                                 ByVal access As ResourceAccessLevels) As IScpiValue(Of Integer)
    element.Value = Me.SetterInfinity(CType(element, IScpiValueBase), subHeader, element.Value, value, access And Not ResourceAccessLevels.Verify)
    If access.IsVerifyAccess Then
      Me.Getter(element, subHeader, ResourceAccessLevels.Device)
      element.Value = value
    ElseIf access.IsCacheAccess Then
      element.ActualValue = value
    End If
    Return element
  End Function

  ''' <summary>
  ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
  ''' Applies readings to the <paramref name="element">existing element</paramref>
  ''' </summary>
  ''' <param name="modalityName">Specifies the modality name.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2} {3}' where 
  ''' the first item is the <see cref="Name">SCPI system name</see>, 
  ''' the second item is the <paramref name="modalityName">modality name</paramref>, 
  ''' the second item is a <paramref name="command">command</paramref> and
  ''' the fourth item represents a <paramref name="value">numeric value</paramref>.
  ''' </param>
  ''' <param name="element">Specifies the element holding the existing value, which is updated.</param>
      Public Function SetterInfinity(ByVal element As IScpiValue(Of Integer), 
                                 ByVal modalityName As String, ByVal subHeader As String, ByVal value As Integer, 
                                 ByVal access As ResourceAccessLevels) As IScpiValue(Of Integer)
    element.Value = Me.SetterInfinity(CType(element, IScpiValueBase), modalityName, subHeader, element.Value, value, access And Not ResourceAccessLevels.Verify)
    If access.IsVerifyAccess Then
      Me.Getter(element, subHeader, ResourceAccessLevels.Device)
      element.Value = value
    ElseIf access.IsCacheAccess Then
      element.ActualValue = value
    End If
    Return element
  End Function

#End Region

#End Region

  Private _name As String
  ''' <summary>
  ''' Gets the system name or syntax header.
  ''' </summary>
      Public ReadOnly Property Name() As String Implements ISubsystem.SyntaxHeader
    Get
      Return Me._name
    End Get
  End Property


#End If
#End Region
