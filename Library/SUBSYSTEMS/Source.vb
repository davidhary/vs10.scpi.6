''' <summary>Defines a SCPI Source Subsystem.</summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="01/15/05" by="David" revision="1.0.1841.x">
''' Created
''' </history>
Public Class SourceSubsystem
    Inherits Subsystem

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>
    ''' The SCPI Syntax Header for this subsystem.
    ''' </summary>
    Public Const ScpiSyntaxHeader As String = "SOUR"

    ''' <summary>Constructs this class.</summary>
    ''' <param name="port">Reference to an open <see cref="Scpi.IPort">SCPI Port IO</see>.</param>
    Public Sub New(ByVal port As Scpi.IPort)

        ' instantiate the base class
        MyBase.New(ScpiSyntaxHeader, port)

        ' create a new function collection
        Me._functionCollection = New ResettableCollection(Of ScpiFunction)

        Me._autoClear = New ResettableValue(Of Boolean)
        MyBase.ResettableValues.Add(Me._autoClear)
        Me._autoDelay = New ResettableValue(Of Boolean)
        MyBase.ResettableValues.Add(Me._autoDelay)
        Me._delay = New ResettableDouble
        MyBase.ResettableValues.Add(Me._delay)
        Me._deltaArm = New ResettableValue(Of Boolean)
        MyBase.ResettableValues.Add(Me._deltaArm)
        Me._deltaCount = New ResettableValue(Of Integer)
        MyBase.ResettableValues.Add(Me._deltaCount)
        Me._deltaDelay = New ResettableDouble
        MyBase.ResettableValues.Add(Me._deltaDelay)
        Me._deltaHighLevel = New ResettableDouble
        MyBase.ResettableValues.Add(Me._deltaHighLevel)
        Me._deltaAbortOnCompliance = New ResettableValue(Of Boolean)
        MyBase.ResettableValues.Add(Me._deltaAbortOnCompliance)
        Me._functionMode = New ResettableValue(Of SourceFunctionMode)
        MyBase.ResettableValues.Add(Me._functionMode)
        Me._memoryPoints = New ResettableValue(Of Integer)
        MyBase.ResettableValues.Add(Me._memoryPoints)
        Me._sweepPoints = New ResettableValue(Of Integer)
        MyBase.ResettableValues.Add(Me._sweepPoints)

        Me._deltaPresent = New ScpiValue(Of Boolean)

    End Sub

    ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
    ''' <param name="disposing">True if this method releases both managed and unmanaged 
    '''   resources; False if this method releases only unmanaged resources.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by
    '''   its disposing parameter.  If True, the method has been called directly or 
    '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
    '''   If disposing equals False, the method has been called by the 
    '''   runtime from inside the finalizer and you should not reference 
    '''   other objects--only unmanaged resources can be disposed.</remarks>
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)

        If Not MyBase.IsDisposed Then

            Try

                If disposing Then

                    ' Free managed resources when explicitly called


                End If

                ' Free shared unmanaged resources
                ' onDisposeUnmanagedResources
            Finally

                ' Invoke the base class dispose method
                MyBase.Dispose(disposing)

            End Try

        End If

    End Sub

#End Region

#Region " IRESETTABLE "

    ''' <summary>Clears the queues and resets all registers to zero.
    ''' Sets the subsystem properties to the following CLS default values:<para>
    ''' 
    ''' </para>
    ''' </summary>
    Public Overrides Function ClearExecutionState() As Boolean
        MyBase.ClearExecutionState()
        Return Me._functionCollection.ClearExecutionState
    End Function

    ''' <summary>
    ''' Sets subsystem to the following default system preset values:<para>
    ''' </para>
    ''' </summary>
    Public Overrides Function PresetKnownState() As Boolean
        MyBase.PresetKnownState()
        Return Me._functionCollection.PresetKnownState
    End Function

    ''' <summary>Restore member properties to the following RST or System Reset values:<para>
    ''' </para>
    ''' </summary>
    ''' <history>
    ''' </history>
    Public Overrides Function ResetKnownState() As Boolean
        MyBase.ResetKnownState()
        Return Me._functionCollection.ResetKnownState
    End Function

#End Region

#Region " FUNCTIONS "

    ''' <summary>
    ''' Adds a sense function to the collection of sense functions.
    ''' Makes the function the 
    ''' <see cref="ActiveFunction">active function.</see>
    ''' </summary>
    ''' <param name="syntaxHeader">Specifies the function header that is used when addressing 
    ''' this instrument function using the SCPI commands.</param>
    ''' <param name="modality">Defines the <see cref="SourceFunctionMode">SCPI function or element</see>.</param>
    Public Function AddFunction(ByVal syntaxHeader As String, ByVal modality As SourceFunctionMode) As ScpiFunction

        Me._activeFunction = FunctionCollection.Add(New ScpiFunction(syntaxHeader, modality))
        Return Me._activeFunction

    End Function

    Private _functionCollection As ResettableCollection(Of ScpiFunction)
    ''' <summary>
    ''' Gets reference to the collection of sense functions
    ''' </summary>
    Public ReadOnly Property FunctionCollection() As ResettableCollection(Of ScpiFunction)
        Get
            Return Me._functionCollection
        End Get
    End Property

    Private _functionMode As ResettableValue(Of SourceFunctionMode)
    Public ReadOnly Property FunctionMode() As ResettableValue(Of SourceFunctionMode)
        Get
            Return Me._functionMode
        End Get
    End Property
    ''' <summary>Gets or sets the <see cref="SourceFunctionMode">source function mode</see>.</summary>
    ''' <remarks>This method must be set to a non-default value.</remarks>
    Public Property FunctionMode(ByVal access As ResourceAccessLevels) As SourceFunctionMode
        Get
            Me.FunctionMode.Value = Syntax.ParseSourceFunctionMode(MyBase.Controller.Getter(Syntax.FunctionCommand,
                                                              Syntax.ExtractScpi(Me.FunctionMode.Value), access))
            Me.FunctionMode.ActualValue = Me.FunctionMode.Value
            Return Me.FunctionMode.Value.Value
        End Get
        Set(ByVal value As SourceFunctionMode)
            Me.FunctionMode.Value = Syntax.ParseSourceFunctionMode(MyBase.Controller.Setter(Syntax.FunctionCommand,
                                            Syntax.ExtractScpi(Me.FunctionMode.Value),
                                            Syntax.ExtractScpi(value), access))
        End Set
    End Property

#End Region

#Region " ANY FUNCTION "

    ''' <summary>Gets or sets the condition for auto current range.</summary>
    Public Property AutoRange(ByVal scpiFunction As ScpiFunction, ByVal access As ResourceAccessLevels) As Boolean
        Get
            MyBase.Controller.Getter(scpiFunction.AutoRange, scpiFunction.SyntaxHeader, Syntax.AutoRangeCommand, Core.EnumeratedBooleanBase.OnOff, access)
            Return scpiFunction.AutoRange.Value.Value
        End Get
        Set(ByVal value As Boolean)
            MyBase.Controller.Setter(scpiFunction.AutoRange, scpiFunction.SyntaxHeader, Syntax.AutoRangeCommand, value, Core.EnumeratedBooleanBase.OnOff, access)
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the source function Compliance of specified source function.
    ''' </summary>
    ''' <param name="access"></param>
    Public Property Compliance(ByVal scpiFunction As ScpiFunction, ByVal access As ResourceAccessLevels) As Double
        Get
            MyBase.Controller.Getter(scpiFunction.Compliance, scpiFunction.SyntaxHeader, Syntax.ComplianceCommand, access)
            Return scpiFunction.Compliance.Value.Value
        End Get
        Set(ByVal Value As Double)
            MyBase.Controller.Setter(scpiFunction.Compliance, scpiFunction.SyntaxHeader, Syntax.ComplianceCommand, Value, access)
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the source function immediate amplitude level of the specified source function.
    ''' </summary>
    ''' <param name="access"></param>
    Public Property Level(ByVal scpiFunction As ScpiFunction, ByVal access As ResourceAccessLevels) As Double
        Get
            MyBase.Controller.Getter(scpiFunction.Level, scpiFunction.SyntaxHeader, Syntax.LevelCommand, access)
            Return scpiFunction.Level.Value.Value
        End Get
        Set(ByVal Value As Double)
            MyBase.Controller.Setter(scpiFunction.Level, scpiFunction.SyntaxHeader, Syntax.LevelCommand, Value, access)
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the integration period of the specified function in seconds.
    ''' </summary>
    ''' <param name="scpiFunction"></param>
    ''' <param name="access"></param>
    Public Property IntegrationPeriod(ByVal scpiFunction As ScpiFunction, ByVal access As ResourceAccessLevels) As Double
        Get
            MyBase.Controller.Getter(scpiFunction.PowerLineCycles, scpiFunction.SyntaxHeader, "NPLC", access)
            Return scpiFunction.IntegrationPeriod.Value.Value
        End Get
        Set(ByVal Value As Double)
            MyBase.Controller.Setter(scpiFunction.PowerLineCycles, scpiFunction.SyntaxHeader, "NPLC", Value * scpiFunction.LineFrequency, access)
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the upper range of the active sense function.
    ''' </summary>
    ''' <param name="access"></param>
    Public Property Range(ByVal scpiFunction As ScpiFunction, ByVal access As ResourceAccessLevels) As Double
        Get
            MyBase.Controller.Getter(scpiFunction.Range, scpiFunction.SyntaxHeader, "RANG", access)
            Return scpiFunction.Range.Value.Value
        End Get
        Set(ByVal Value As Double)
            MyBase.Controller.Setter(scpiFunction.Range, scpiFunction.SyntaxHeader, "RANG", Value, access)
        End Set
    End Property

#End Region

#Region " ACTIVE FUNCTION "

    Private _activeFunction As ScpiFunction
    ''' <summary>
    ''' Gets the current function.
    ''' </summary>
    Public ReadOnly Property ActiveFunction() As ScpiFunction
        Get
            Return Me._activeFunction
        End Get
    End Property

    ''' <summary>
    ''' Activate the specified function.
    ''' </summary>
    Public Sub ActivateFunction(ByVal value As ScpiFunction, ByVal access As ResourceAccessLevels)
        Me._activeFunction = value
        Me.FunctionMode(access) = Me._activeFunction.SourceModality
    End Sub

    ''' <summary>
    ''' Activate the specified function.
    ''' </summary>
    Public Sub ActivateFunction(ByVal value As SourceFunctionMode, ByVal access As ResourceAccessLevels)
        If value <> SourceFunctionMode.None Then
            For Each scpiFunction As ScpiFunction In Me._functionCollection
                If scpiFunction.SourceModality = value Then
                    Me.ActivateFunction(scpiFunction, access)
                    Return
                End If
            Next
            Throw New FunctionNotDefinedException(value)
        End If
    End Sub

    ''' <summary>
    ''' Activate the specified function.
    ''' </summary>
    Public Sub ActivateFunction(ByVal access As ResourceAccessLevels)
        Me.ActivateFunction(Me.FunctionMode(access), access)
    End Sub

    ''' <summary>Gets or sets the condition for auto current range.</summary>
    Public Property AutoRange(ByVal access As ResourceAccessLevels) As Boolean
        Get
            Return Me.AutoRange(Me._activeFunction, access)
        End Get
        Set(ByVal value As Boolean)
            Me.AutoRange(Me._activeFunction, access) = value
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the source function Compliance of the active source function.
    ''' </summary>
    ''' <param name="access"></param>
    Public Property Compliance(ByVal access As ResourceAccessLevels) As Double
        Get
            Return Me.Compliance(Me._activeFunction, access)
        End Get
        Set(ByVal Value As Double)
            Me.Compliance(Me._activeFunction, access) = Value
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the source function immediate amplitude level of the active source function.
    ''' </summary>
    ''' <param name="access"></param>
    Public Property Level(ByVal access As ResourceAccessLevels) As Double
        Get
            Return Me.Level(Me._activeFunction, access)
        End Get
        Set(ByVal Value As Double)
            Me.Level(Me._activeFunction, access) = Value
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the integration period of the active function in seconds.
    ''' </summary>
    ''' <param name="access"></param>
    Public Property IntegrationPeriod(ByVal access As ResourceAccessLevels) As Double
        Get
            Return Me.IntegrationPeriod(Me._activeFunction, access)
        End Get
        Set(ByVal Value As Double)
            Me.IntegrationPeriod(Me._activeFunction, access) = Value
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the source protection of the active source function.
    ''' </summary>
    ''' <param name="access"></param>
    Public Property ProtectionLevel(ByVal access As ResourceAccessLevels) As Double
        Get
            MyBase.Controller.Getter(Me._activeFunction.ProtectionLevel, Me._activeFunction.SyntaxHeader, Syntax.Protection, access)
            Return Me._activeFunction.ProtectionLevel.Value.Value
        End Get
        Set(ByVal Value As Double)
            MyBase.Controller.Setter(Me._activeFunction.ProtectionLevel, Me._activeFunction.SyntaxHeader, Syntax.Protection, Value, access)
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the upper range of the active sense function.
    ''' </summary>
    ''' <param name="access"></param>
    Public Property Range(ByVal access As ResourceAccessLevels) As Double
        Get
            Return Me.Range(Me._activeFunction, access)
        End Get
        Set(ByVal Value As Double)
            Me.Range(Me._activeFunction, access) = Value
        End Set
    End Property

    ''' <summary>
    ''' Sets the range and level.
    ''' </summary>
    ''' <param name="range">Specifies the source function range.</param>
    ''' <param name="level">Specifies the source function Level.</param>
    ''' <remarks>
    ''' Setting the source function range and level at once helps ensure that the instrument will 
    ''' not cause a failure if the source level is incompatble with the existing range or conversly if 
    ''' the range is lower then the existing level.
    ''' </remarks>
    Public Sub SetRangeAndLevel(ByVal range As Double, ByVal level As Double)

        Dim commandFormat As String = "{0}{1}:RANG {2}; {0}{1} {3}"
        Dim command As String = String.Format(Globalization.CultureInfo.CurrentCulture,
                                              commandFormat, MyBase.SyntaxHeader, Me._activeFunction.SyntaxHeader, range, level)
        MyBase.Controller.Port.WriteLine(command)
        Me._activeFunction.Range.Value = range
        Me._activeFunction.Level.Value = level

    End Sub

    ''' <summary>
    ''' Gets or sets the source function sweep mode e.g., Fixed, Sweep, or List.
    ''' </summary>
    Public Property SweepMode(ByVal access As ResourceAccessLevels) As SweepMode
        Get
            MyBase.Controller.Getter(Me._activeFunction.SyntaxHeader, Syntax.ModeCommand, Me._activeFunction.SweepModeCaption, access)
            Return Me._activeFunction.SweepMode.Value.Value
        End Get
        Set(ByVal value As SweepMode)
            MyBase.Controller.Setter(Me._activeFunction.SyntaxHeader, Syntax.ModeCommand, Me._activeFunction.SweepModeCaption, Syntax.ExtractScpi(value), access)
        End Set
    End Property

    ''' <summary>Gets or sets the source start level.</summary>
    Public Property StartLevel(ByVal access As ResourceAccessLevels) As Double
        Get
            MyBase.Controller.Getter(Me._activeFunction.StartLevel, Me._activeFunction.SyntaxHeader, Syntax.StartLevelCommand, access)
            Return Me._activeFunction.StartLevel.Value.Value
        End Get
        Set(ByVal value As Double)
            MyBase.Controller.Setter(Me._activeFunction.StartLevel, Me._activeFunction.SyntaxHeader, Syntax.StartLevelCommand, value, access)
        End Set
    End Property

    ''' <summary>Gets or sets the source sweep start level.</summary>
    Public Property StopLevel(ByVal access As ResourceAccessLevels) As Double
        Get
            MyBase.Controller.Getter(Me._activeFunction.StopLevel, Me._activeFunction.SyntaxHeader, Syntax.StopLevelCommand, access)
            Return Me._activeFunction.StopLevel.Value.Value
        End Get
        Set(ByVal value As Double)
            MyBase.Controller.Setter(Me._activeFunction.StopLevel, Me._activeFunction.SyntaxHeader, Syntax.StopLevelCommand, value, access)
        End Set
    End Property

#End Region

#Region " DELTA "

    ''' <summary>
    ''' Abort execution of Delta measurements.
    ''' </summary>
    Public Function DeltaAbort() As Boolean

        ' abort the sweep.
        MyBase.Controller.Execute(Syntax.Sweep, Syntax.Abort)

        'Threading.Thread.Sleep(1)

        ' abort the waveform
        'MyBase.Execute(Syntax.WaveModalityName, Syntax.Abort)

        Return Not MyBase.Controller.Port.HadError

    End Function

    Private _deltaAbortOnCompliance As ResettableValue(Of Boolean)
    Public ReadOnly Property DeltaAbortOnCompliance() As ResettableValue(Of Boolean)
        Get
            Return Me._deltaAbortOnCompliance
        End Get
    End Property

    ''' <summary>
    ''' Gets or sets the condition for aborting the test on the source hitting compliance.
    ''' </summary>
    ''' <param name="access"></param>
    Public Property DeltaAbortOnCompliance(ByVal access As ResourceAccessLevels) As Boolean
        Get
            MyBase.Controller.Getter(Me._deltaAbortOnCompliance, Syntax.DeltaModalityName, Syntax.ComplianceAbort, Core.EnumeratedBooleanBase.OnOff, access)
            Return Me._deltaAbortOnCompliance.Value.Value
        End Get
        Set(ByVal value As Boolean)
            MyBase.Controller.Setter(Me._deltaAbortOnCompliance, Syntax.DeltaModalityName, Syntax.ComplianceAbort, value, Core.EnumeratedBooleanBase.OnOff, access)
        End Set
    End Property

    Private _deltaArm As ResettableValue(Of Boolean)
    Public ReadOnly Property DeltaArm() As ResettableValue(Of Boolean)
        Get
            Return Me._deltaArm
        End Get
    End Property

    ''' <summary>Sets the source to auto clear mode.</summary>
    Public Property DeltaArm(ByVal access As ResourceAccessLevels) As Boolean
        Get
            Return MyBase.Controller.Getter(Me.DeltaArm, Syntax.DeltaModalityName, Syntax.ArmCommand, Core.EnumeratedBooleanBase.OneZero, access).Value.Value
        End Get
        Set(ByVal value As Boolean)
            If access.IsCacheAccess Then
                Me.DeltaArm.Value = value
                Me.DeltaArm.ActualValue = value
            Else
                If access.IsDeviceAccess OrElse (value <> Me.DeltaArm) Then
                    If value Then
                        MyBase.Controller.Execute(Syntax.DeltaModalityName, Syntax.ArmCommand)
                    Else
                        MyBase.Controller.Execute(Syntax.SweepModalityName, Syntax.AbortCommand)
                        MyBase.Controller.Execute(Syntax.WaveModalityName, Syntax.AbortCommand)
                    End If
                    Me.DeltaArm.Value = value
                End If
                If access.IsVerifyAccess Then
                    ' set the actual value
                    Me.DeltaArm.ActualValue = Me.DeltaArm(ResourceAccessLevels.Device)
                    ' set the value to the specified value. Now the calling method can check if the value is verified.
                    Me.DeltaArm.Value = value
                End If
            End If
        End Set
    End Property

    Private _deltaHighLevel As ResettableDouble
    Public ReadOnly Property DeltaHighLevel() As ResettableDouble
        Get
            Return Me._deltaHighLevel
        End Get
    End Property
    ''' <summary>Gets or sets the source Delta High level.</summary>
    Public Property DeltaHighLevel(ByVal access As ResourceAccessLevels) As Double
        Get
            MyBase.Controller.Getter(Me._deltaHighLevel, Syntax.DeltaModalityName, Syntax.HighCommand, access)
            Return Me._deltaHighLevel.Value.Value
        End Get
        Set(ByVal value As Double)
            MyBase.Controller.Setter(Me._deltaHighLevel, Syntax.DeltaModalityName, Syntax.HighCommand, value, access)
        End Set
    End Property

    Private _deltaCount As ResettableValue(Of Integer)
    Public ReadOnly Property DeltaCount() As ResettableValue(Of Integer)
        Get
            Return Me._deltaCount
        End Get
    End Property
    ''' <summary>Gets or sets the source delta count.  This is the total number of 
    ''' valid measurements.  A valid measurement is one which is averaged over the required number 
    ''' of average counts.</summary>
    Public Property DeltaCount(ByVal access As ResourceAccessLevels) As Integer
        Get
            MyBase.Controller.GetterInfinity(Me._deltaCount, Syntax.DeltaModalityName, Syntax.CountCommand, access)
            Return Me._deltaCount.Value.Value
        End Get
        Set(ByVal value As Integer)
            value = Math.Min(value, 65536)
            MyBase.Controller.SetterInfinity(Me._deltaCount, Syntax.DeltaModalityName, Syntax.CountCommand, value, access)
        End Set
    End Property

    Private _deltaDelay As ResettableDouble
    ''' <summary>
    ''' Gets the delta delay.
    ''' </summary>
    ''' 
    Public ReadOnly Property DeltaDelay() As ResettableDouble
        Get
            Return Me._deltaDelay
        End Get
    End Property
    ''' <summary>Gets or sets the source delta delay.  
    ''' Allows the current source to settle after changing polarity. Defaults to 2mS for the 6221.
    ''' </summary>
    Public Property DeltaDelay(ByVal access As ResourceAccessLevels) As Double
        Get
            MyBase.Controller.Getter(Me._deltaDelay, Syntax.DeltaModalityName, Syntax.DelayCommand, access)
            Return Me._deltaDelay.Value.Value
        End Get
        Set(ByVal value As Double)
            MyBase.Controller.Setter(Me._deltaDelay, Syntax.DeltaModalityName, Syntax.DelayCommand, value, access)
        End Set
    End Property

    Private _deltaPresent As ScpiValue(Of Boolean)
    ''' <summary>
    ''' Gets the sentinel indicating if the delta instrument is present.
    ''' </summary>
    Public ReadOnly Property DeltaPresent() As ScpiValue(Of Boolean)
        Get
            Return Me._deltaPresent
        End Get
    End Property


    ''' <summary>Gets the condition determining if the 6221 is connected to the 2182 for making
    ''' Delta measurements.</summary>
    Public ReadOnly Property DeltaPresent(ByVal access As ResourceAccessLevels) As Boolean
        Get
            MyBase.Controller.Getter(Me._deltaPresent, Syntax.DeltaModalityName, Syntax.PresentCommand, Core.EnumeratedBooleanBase.OneZero, access)
            Return Me._deltaPresent.Value.Value
        End Get
    End Property

#End Region

#Region " SOURCE CONTROL "

    Private _autoClear As ResettableValue(Of Boolean)
    Public ReadOnly Property AutoClear() As ResettableValue(Of Boolean)
        Get
            Return Me._autoClear
        End Get
    End Property
    ''' <summary>Sets the source to auto clear mode.</summary>
    Public Property AutoClear(ByVal access As ResourceAccessLevels) As Boolean
        Get
            MyBase.Controller.Getter(Me._autoClear, Syntax.AutoClearCommand, Core.EnumeratedBooleanBase.OnOff, access)
            Return Me._autoClear.Value.Value
        End Get
        Set(ByVal value As Boolean)
            MyBase.Controller.Setter(Me._autoClear, Syntax.AutoClearCommand, value, Core.EnumeratedBooleanBase.OnOff, access)
        End Set
    End Property

    Private _autoDelay As ResettableValue(Of Boolean)
    Public ReadOnly Property AutoDelay() As ResettableValue(Of Boolean)
        Get
            Return Me._autoDelay
        End Get
    End Property
    ''' <summary>Gets or sets the condition for auto current Delay.</summary>
    Public Property AutoDelay(ByVal access As ResourceAccessLevels) As Boolean
        Get
            MyBase.Controller.Getter(Me._autoDelay, Syntax.AutoDelayCommand, Core.EnumeratedBooleanBase.OnOff, access)
            Return Me._autoDelay.Value.Value
        End Get
        Set(ByVal value As Boolean)
            MyBase.Controller.Setter(Me._autoDelay, Syntax.AutoDelayCommand, value, Core.EnumeratedBooleanBase.OnOff, access)
        End Set
    End Property

    Private _delay As ResettableDouble
    Public ReadOnly Property Delay() As ResettableDouble
        Get
            Return Me._delay
        End Get
    End Property

    ''' <summary>Gets or sets the condition for auto current Delay.</summary>
    Public Property Delay(ByVal access As ResourceAccessLevels) As Double
        Get
            MyBase.Controller.Getter(Me._delay, Syntax.DelayCommand, access)
            Return Me._delay.Value.Value
        End Get
        Set(ByVal value As Double)
            MyBase.Controller.Setter(Me._delay, Syntax.DelayCommand, value, access)
            Me._autoDelay.Value = False
        End Set
    End Property

#End Region

#Region " MEMORY "

    Private _memoryPoints As ResettableValue(Of Integer)
    Public ReadOnly Property MemoryPoints() As ResettableValue(Of Integer)
        Get
            Return Me._memoryPoints
        End Get
    End Property
    ''' <summary>Gets or sets the source memory points.</summary>
    Public Property MemoryPoints(ByVal access As ResourceAccessLevels) As Integer
        Get
            MyBase.Controller.Getter(Me._memoryPoints, Syntax.MemoryModalityName, Syntax.PointsCommand, access)
            Return Me._memoryPoints.Value.Value
        End Get
        Set(ByVal value As Integer)
            MyBase.Controller.Setter(Me._memoryPoints, Syntax.MemoryModalityName, Syntax.PointsCommand, value, access)
        End Set
    End Property


#End Region

#Region " SWEEP "

    Private _sweepPoints As ResettableValue(Of Integer)
    Public ReadOnly Property SweepPoints() As ResettableValue(Of Integer)
        Get
            Return Me._sweepPoints
        End Get
    End Property
    ''' <summary>Gets or sets the source sweep points.</summary>
    Public Property SweepPoints(ByVal access As ResourceAccessLevels) As Integer
        Get
            MyBase.Controller.Getter(Me._sweepPoints, Syntax.SweepModalityName, Syntax.PointsCommand, access)
            Return Me._sweepPoints.Value.Value
        End Get
        Set(ByVal value As Integer)
            MyBase.Controller.Setter(Me._sweepPoints, Syntax.SweepModalityName, Syntax.PointsCommand, value, access)
        End Set
    End Property

#End Region

End Class
