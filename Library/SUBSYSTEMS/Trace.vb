''' <summary>Defines a SCPI Trace Subsystem.</summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="01/15/05" by="David" revision="1.0.1841.x">
''' Created
''' </history>
Public Class TraceSubsystem
    Inherits Subsystem

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>
    ''' The SCPI Syntax Header for this subsystem.
    ''' </summary>
    Public Const ScpiSyntaxHeader As String = "TRAC"

    ''' <summary>Constructs this class.</summary>
    ''' <param name="port">Reference to an open <see cref="Scpi.IPort">SCPI Port IO</see>.</param>
    Public Sub New(ByVal port As Scpi.IPort)

        ' instantiate the base class
        MyBase.New(ScpiSyntaxHeader, port)

        Me._feedSource = New ResettableValue(Of FeedSource)
        MyBase.ResettableValues.Add(Me._feedSource)

        Me._nextFeedControl = New ResettableValue(Of Boolean)
        MyBase.ResettableValues.Add(Me._nextFeedControl)

        Me._notifyCount = New ResettableValue(Of Integer)
        MyBase.ResettableValues.Add(Me._notifyCount)

        Me._pointsCount = New ResettableValue(Of Integer)
        MyBase.ResettableValues.Add(Me._pointsCount)

        Me._actualPointsCount = New ScpiValue(Of Integer)

        Me._defaultTraceSize = 1024
    End Sub

    ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
    ''' <param name="disposing">True if this method releases both managed and unmanaged 
    '''   resources; False if this method releases only unmanaged resources.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by
    '''   its disposing parameter.  If True, the method has been called directly or 
    '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
    '''   If disposing equals False, the method has been called by the 
    '''   runtime from inside the finalizer and you should not reference 
    '''   other objects--only unmanaged resources can be disposed.</remarks>
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)

        If Not MyBase.IsDisposed Then

            Try

                If disposing Then

                    ' Free managed resources when explicitly called


                End If

                ' Free shared unmanaged resources
                ' onDisposeUnmanagedResources
            Finally

                ' Invoke the base class dispose method
                MyBase.Dispose(disposing)

            End Try

        End If

    End Sub

#End Region

#Region " IRESETTABLE "

    ''' <summary>Clears the queues and resets all registers to zero.
    ''' Sets the subsystem properties to the following CLS default values:<para>
    ''' 
    ''' </para>
    ''' </summary>
    Public Overrides Function ClearExecutionState() As Boolean
        Return MyBase.ClearExecutionState()
    End Function

    ''' <summary>
    ''' Sets subsystem to the following default system preset values:<para>
    ''' </para>
    ''' </summary>
    Public Overrides Function PresetKnownState() As Boolean
        Return MyBase.PresetKnownState()
    End Function

    ''' <summary>Restore member properties to the following RST or System Reset values:<para>
    ''' </para>
    ''' </summary>
    ''' <history>
    ''' </history>
    Public Overrides Function ResetKnownState() As Boolean
        Return MyBase.ResetKnownState()
    End Function

#End Region

#Region " READINGS "

    ''' <summary>
    ''' Requests all readings from the buffer.
    ''' </summary>
    Public Sub QueryReadings()
        MyBase.Controller.Port.QueryTrimEnd(Syntax.BuildQuery(MyBase.SyntaxHeader, Syntax.DataCommand))
    End Sub

    ''' <summary>
    ''' Requests a range of readings.
    ''' </summary>
    ''' <param name="firstPoint"></param>
    ''' <param name="count"></param>
    Public Sub QueryReadings(ByVal firstPoint As Integer, ByVal count As Integer)
        MyBase.Controller.Port.QueryTrimEnd(Syntax.BuildQuery(MyBase.SyntaxHeader, Syntax.DataCommand, firstPoint, count))
    End Sub

    ''' <summary>
    ''' Requests all readings from the buffer.
    ''' </summary>
    Public Sub RequestReadings()
        MyBase.Controller.Port.WriteLine(Syntax.BuildQuery(MyBase.SyntaxHeader, Syntax.DataCommand))
    End Sub

    ''' <summary>
    ''' Requests a range of readings.
    ''' </summary>
    ''' <param name="firstPoint"></param>
    ''' <param name="count"></param>
    Public Sub RequestReadings(ByVal firstPoint As Integer, ByVal count As Integer)
        MyBase.Controller.Port.WriteLine(Syntax.BuildQuery(MyBase.SyntaxHeader, Syntax.DataCommand, firstPoint, count))
    End Sub

#End Region

#Region " BUFFER MANAGEMENT "

    Private _actualPointsCount As ScpiValue(Of Integer)
    ''' <summary>
    ''' Gets the actual number of trace points.
    ''' </summary>
    ''' <param name="access"></param>
    Public ReadOnly Property ActualPointsCount(ByVal access As ResourceAccessLevels) As Integer
        Get
            MyBase.Controller.Getter(Me._actualPointsCount, Syntax.PointsCommand, Syntax.Actual, access)
            Return Me._actualPointsCount.Value.Value
        End Get
    End Property

    Private _feedSource As ResettableValue(Of FeedSource)
    Public ReadOnly Property FeedSource() As ResettableValue(Of FeedSource)
        Get
            Return Me._feedSource
        End Get
    End Property
    ''' <summary>
    ''' Gets or sets the source of readings such as 
    ''' <see cref="Syntax.Sense">sense</see> or 
    ''' <see cref="Syntax.Calculate1">calculate 1</see> or <see cref="Syntax.Calculate2">calculate2</see>
    ''' </summary>
    ''' <param name="access"></param>
    Public Property FeedSource(ByVal access As ResourceAccessLevels) As FeedSource
        Get
            Me.FeedSource.Value = Syntax.ParseFeedSource(MyBase.Controller.Getter(Syntax.Feed, Syntax.ExtractScpi(Me._feedSource.Value), access))
            Me.FeedSource.ActualValue = Me.FeedSource.Value
            Return Me.FeedSource.Value.Value
        End Get
        Set(ByVal Value As FeedSource)
            Me.FeedSource.Value = Syntax.ParseFeedSource(MyBase.Controller.Setter(Syntax.Feed,
                                                                       Syntax.ExtractScpi(Me.FeedSource.Value),
                                                                       Syntax.ExtractScpi(Value), access))
        End Set
    End Property

    Private _nextFeedControl As ResettableValue(Of Boolean)
    Public ReadOnly Property NextFeedControl() As ResettableValue(Of Boolean)
        Get
            Return Me._nextFeedControl
        End Get
    End Property

    ''' <summary>
    ''' Gets or sets the buffer control mode such as 
    ''' <see cref="Syntax.Next">fills buffer and stops</see> or
    ''' <see cref="Syntax.Never">disables buffer storage</see>
    ''' </summary>
    ''' <param name="access"></param>
    Public Property NextFeedControl(ByVal access As ResourceAccessLevels) As Boolean
        Get
            MyBase.Controller.Getter(Me._nextFeedControl, Syntax.Feed, Syntax.Control, Syntax.NextNever, access)
            Return Me._nextFeedControl.Value.Value
        End Get
        Set(ByVal Value As Boolean)
            MyBase.Controller.Setter(Me._nextFeedControl, Syntax.Feed, Syntax.Control, Value, Syntax.NextNever, access)
        End Set
    End Property

    Private _pointsCount As ResettableValue(Of Integer)
    Public ReadOnly Property PointsCount() As ResettableValue(Of Integer)
        Get
            Return Me._pointsCount
        End Get
    End Property

    ''' <summary>
    ''' Gets or sets the number of Trace Points
    ''' </summary>
    ''' <param name="access"></param>
    Public Property PointsCount(ByVal access As ResourceAccessLevels) As Integer
        Get
            Return MyBase.Controller.Getter(Me.PointsCount, Syntax.PointsCommand, access).Value.Value
        End Get
        Set(ByVal Value As Integer)
            If access.IsCacheAccess Then
                Me.PointsCount.Value = Value
                Me.PointsCount.ActualValue = Value
            Else
                Dim feedControlEnabled As Boolean = Me.NextFeedControl(access)
                If feedControlEnabled Then
                    Me.NextFeedControl(access) = False
                End If
                If access.IsDeviceAccess OrElse (Value <> Me.PointsCount) Then
                    MyBase.Controller.Setter(Me.PointsCount, Syntax.PointsCommand, Value, access)
                End If
                If access.IsVerifyAccess Then
                    ' set the actual value
                    Me.PointsCount.ActualValue = Me.PointsCount(access)
                    Me.PointsCount.Value = Value
                End If
                ' check if we need to restore feed control.
                If feedControlEnabled Then
                    Me.NextFeedControl(access) = True
                End If
            End If
            Me._notifyCount.ResetValue = Me.PointsCount.Value.Value \ 2
        End Set
    End Property

    ''' <summary>
    ''' Returns True if periodic notifications are enabled.
    ''' </summary>
    Public ReadOnly Property PeriodicNotifyEnabled() As Boolean
        Get
            Return Me._notifyInterval > 0
        End Get
    End Property

    Private _notifyInterval As Integer
    ''' <summary>
    ''' Gets or sets the interval for notifications.  Set to 0 to
    ''' disable periodic notifications.
    ''' </summary>
    Public Property NotifyInterval() As Integer
        Get
            Return Me._notifyInterval
        End Get
        Set(ByVal value As Integer)
            Me._notifyInterval = value
        End Set
    End Property

    Private _notifyCount As ResettableValue(Of Integer)
    ''' <summary>
    ''' Gets or sets the number of Trace Points
    ''' </summary>
    ''' <param name="access"></param>
    <CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId:="access")>
    Public Property NotifyCount(ByVal access As ResourceAccessLevels) As Integer
        Get
            If Me._notifyCount.Value.HasValue Then
                Return Me._notifyCount.Value.Value
            Else
                Return -1
            End If
        End Get
        Set(ByVal Value As Integer)
            If Value < Me.PointsCount(access) Then
                MyBase.Controller.Setter(Me._notifyCount, Syntax.Notify, Value, access)
            End If
        End Set
    End Property

    ''' <summary>
    ''' Update the notification to notify on the next interval.
    ''' </summary>
    Public Sub NotifyNext(ByVal access As ResourceAccessLevels)
        Me.NotifyCount(access) = Me.NotifyCount(ResourceAccessLevels.Cache) + Me._notifyInterval
    End Sub

#End Region

#Region " CONTROL METHODS "

    ''' <summary>
    ''' Clears the buffer of readings.
    ''' </summary>
    Public Sub ClearBuffer()

        Me._actualPointsCount.Value = 0
        MyBase.Controller.Execute(Syntax.ClearCommand)

    End Sub

    Private _defaultTraceSize As Integer
    ''' <summary>
    ''' Gets or sests the default terace size for reading ASCII data.
    ''' </summary>
    Public Property DefaultTraceSize() As Integer
        Get
            Return Me._defaultTraceSize
        End Get
        Set(ByVal value As Integer)
            Me._defaultTraceSize = value
        End Set
    End Property

    ''' <summary>
    ''' Addresses the instrument to talk and send all the readings stored in the data store (buffer) to 
    ''' the computer.
    ''' </summary>
    Public Function ReadBuffer() As String

        Dim defaultStringSize As Integer = MyBase.Controller.Port.DefaultStringSize
        If Me._defaultTraceSize > defaultStringSize Then
            MyBase.Controller.Port.DefaultStringSize = Me._defaultTraceSize
        End If


        Dim text As New System.Text.StringBuilder
        MyBase.Controller.Execute("DATA?")
        Do
            text.Append(MyBase.Controller.Port.ReadLine())
        Loop Until (MyBase.Controller.Port.SerialPoll And ServiceRequests.MessageAvailable) = 0

        ' restore the default string size.
        If Me._defaultTraceSize > defaultStringSize Then
            MyBase.Controller.Port.DefaultStringSize = defaultStringSize
        End If

        Return text.ToString

    End Function

#End Region

End Class
