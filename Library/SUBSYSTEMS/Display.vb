''' <summary>Defines a Scpi Display Subsystem.</summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="01/15/05" by="David" revision="1.0.1841.x">
''' Created
''' </history>
Public Class DisplaySubsystem
    Inherits Subsystem

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>
    ''' The SCPI Syntax Header for this subsystem.
    ''' </summary>
    Public Const ScpiSyntaxHeader As String = "DISP"

    ''' <summary>Constructs this class.</summary>
    ''' <param name="port">Reference to an open <see cref="Scpi.IPort">SCPI Port IO</see>.</param>
    Public Sub New(ByVal port As Scpi.IPort)

        ' instantiate the base class
        MyBase.New(ScpiSyntaxHeader, port)

        Me._enabled = New ResettableValue(Of Boolean)
        MyBase.ResettableValues.Add(Me._enabled)

    End Sub

    ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
    ''' <param name="disposing">True if this method releases both managed and unmanaged 
    '''   resources; False if this method releases only unmanaged resources.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by
    '''   its disposing parameter.  If True, the method has been called directly or 
    '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
    '''   If disposing equals False, the method has been called by the 
    '''   runtime from inside the finalizer and you should not reference 
    '''   other objects--only unmanaged resources can be disposed.</remarks>
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)

        If Not MyBase.IsDisposed Then

            Try

                If disposing Then

                    ' Free managed resources when explicitly called

                End If

                ' Free shared unmanaged resources
                ' onDisposeUnmanagedResources
            Finally

                ' Invoke the base class dispose method
                MyBase.Dispose(disposing)

            End Try

        End If

    End Sub

#End Region

#Region " IRESETTABLE "

    ''' <summary>Clears the queues and resets all registers to zero.
    ''' Sets the subsystem properties to the following CLS default values:<para>
    ''' 
    ''' </para>
    ''' </summary>
    Public Overrides Function ClearExecutionState() As Boolean
        Return MyBase.ClearExecutionState()
    End Function

    ''' <summary>
    ''' Sets subsystem to the following default system preset values:<para>
    ''' </para>
    ''' </summary>
    Public Overrides Function PresetKnownState() As Boolean
        Return MyBase.PresetKnownState()
    End Function

    ''' <summary>Restore member properties to the following RST or System Reset values:<para>
    ''' </para>
    ''' </summary>
    ''' <history>
    ''' </history>
    Public Overrides Function ResetKnownState() As Boolean
        Return MyBase.ResetKnownState()
    End Function

#End Region

#Region " METHODS  and  PROPERTIES "

    Private _enabled As ResettableValue(Of Boolean)
    ''' <summary>
    ''' Gets the display enable cached value.
    ''' </summary>
    Public ReadOnly Property Enabled() As ResettableValue(Of Boolean)
        Get
            Return Me._enabled
        End Get
    End Property

    ''' <summary>
    ''' Gets or sets the display enable.
    ''' </summary>
    Public Property Enabled(ByVal access As ResourceAccessLevels) As Boolean
        Get
            MyBase.Controller.Getter(Me.Enabled, Syntax.Enable, Core.EnumeratedBooleanBase.OneZero, access)
            Return Me._enabled.Value.Value
        End Get
        Set(ByVal Value As Boolean)
            MyBase.Controller.Setter(Me.Enabled, Syntax.Enable, Value, Core.EnumeratedBooleanBase.OneZero, access)
        End Set
    End Property

#End Region

End Class
