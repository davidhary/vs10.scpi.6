''' <summary>Defines a SCPI System Subsystem.</summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="01/15/05" by="David" revision="1.0.1841.x">
''' Created
''' </history>
Public Class SystemSubsystem
    Inherits Subsystem

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructs this class.</summary>
    ''' <param name="port">Reference to an open <see cref="Scpi.IPort">SCPI Port IO</see>.</param>
    Public Sub New(ByVal port As Scpi.IPort)

        ' instantiate the base class
        MyBase.New(ScpiSyntaxHeader, port)

        Me._autoZero = New ResettableValue(Of Boolean)
        MyBase.ResettableValues.Add(Me._autoZero)
        Me._beeperEnabled = New ResettableValue(Of Boolean)
        MyBase.ResettableValues.Add(Me._beeperEnabled)
        Me._cableGuard = New ResettableValue(Of Boolean)
        MyBase.ResettableValues.Add(Me._cableGuard)
        Me._contactCheckEnabled = New ResettableValue(Of Boolean)
        MyBase.ResettableValues.Add(Me._contactCheckEnabled)
        Me._contactCheckResistance = New ResettableValue(Of Integer)
        MyBase.ResettableValues.Add(Me._contactCheckResistance)
        Me._isRemote = New ResettableValue(Of Boolean)
        MyBase.ResettableValues.Add(Me._isRemote)
        Me._remoteSenseMode = New ResettableValue(Of Boolean)
        MyBase.ResettableValues.Add(Me._remoteSenseMode)

        Me._lastError = New ResettableString
        MyBase.ResettableValues.Add(Me._lastError)

        Me._frontSwitchedCache = New ScpiValue(Of Boolean)
        Me._frontSwitchedCache.GetterCommand = Syntax.BuildQuery(Me.SyntaxHeader, Syntax.FrontSwitchedCommand)

        Me._isRemote.ResetValue = True
        Me._isRemote.ClearValue = True
        Me._isRemote.PresetValue = True

        Me._lastError.GetterCommand = Syntax.BuildQuery(Me.SyntaxHeader, Syntax.ErrorModalityName)  ' ":SYST:ERR?"
        Me._lastError.ClearValue = ""

        Me._lineFrequency = New ScpiValue(Of Integer)
        Me._lineFrequency.GetterCommand = Syntax.BuildQuery(Me.SyntaxHeader, Syntax.LineFrequencySubHeader)

        Me._passwordLocked = New ScpiValue(Of Boolean)

        Me._remoteSenseMode.GetterCommand = Syntax.BuildQuery(Me.SyntaxHeader, Syntax.RemoteSenseCommand)
        Me._remoteSenseMode.FormatterParser = Core.EnumeratedBooleanBase.OnOff

        Me._scpiRevision = New ScpiValue(Of Double)
        Me._scpiRevision.GetterCommand = Syntax.BuildQuery(Me.SyntaxHeader, Syntax.VersionCommand)


    End Sub

    ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
    ''' <param name="disposing">True if this method releases both managed and unmanaged 
    '''   resources; False if this method releases only unmanaged resources.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by
    '''   its disposing parameter.  If True, the method has been called directly or 
    '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
    '''   If disposing equals False, the method has been called by the 
    '''   runtime from inside the finalizer and you should not reference 
    '''   other objects--only unmanaged resources can be disposed.</remarks>
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)

        If Not MyBase.IsDisposed Then

            Try

                If disposing Then

                    ' Free managed resources when explicitly called

                End If

                ' Free shared unmanaged resources
                ' onDisposeUnmanagedResources
            Finally

                ' Invoke the base class dispose method
                MyBase.Dispose(disposing)

            End Try

        End If

    End Sub

#End Region

#Region " SHARED "

    ''' <summary>
    ''' The SCPI Syntax Header for this subsystem.
    ''' </summary>
    Public Const ScpiSyntaxHeader As String = "SYST"

    ''' <summary>Clears the messages from the error queue.</summary>
    ''' <param name="port">Reference to a <see cref="Scpi.IPort">SCPI Port IO</see></param>
    ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
    Public Shared Sub ClearErrorQueue(ByVal port As Scpi.IPort)

        If port Is Nothing Then
            Throw New ArgumentNullException("port")
        End If
        port.WriteLine(Syntax.BuildExecute(ScpiSyntaxHeader, Syntax.ClearCommand))

    End Sub

    ''' <summary>Gets or sets the status of the front (True) rear (switch)</summary>
    ''' <param name="port">Reference to a <see cref="Scpi.IPort">SCPI Port IO</see></param>
    ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
    Public Shared ReadOnly Property FrontSwitched(ByVal port As Scpi.IPort) As Boolean
        Get
            If port Is Nothing Then
                Throw New ArgumentNullException("port")
            End If
            Return port.QueryBoolean(Syntax.BuildQuery(ScpiSyntaxHeader, Syntax.FrontSwitchedCommand), Syntax.FrontRear).Value
        End Get
    End Property

    ''' <summary>Initializes battery backed RAM. This initializes trace, source list, 
    '''   user-defined math, source-memory locations, standard save setups, and all 
    '''   call math expressions.</summary>
    ''' <param name="port">Reference to a <see cref="Scpi.IPort">SCPI Port IO</see></param>
    ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
    Public Shared Sub InitializeMemory(ByVal port As Scpi.IPort)

        If port Is Nothing Then
            Throw New ArgumentNullException("port")
        End If
        port.WriteLine(Syntax.BuildExecute(ScpiSyntaxHeader, "MEM", Syntax.InitCommand))

    End Sub

    ''' <summary>Returns the instrument to states optimized for front panel operations.</summary>
    ''' <param name="port">Reference to a <see cref="Scpi.IPort">SCPI Port IO</see></param>
    ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
    Public Shared Sub Preset(ByVal port As Scpi.IPort)

        If port Is Nothing Then
            Throw New ArgumentNullException("port")
        End If
        port.WriteLine(Syntax.BuildExecute(ScpiSyntaxHeader, Syntax.PresetCommand))

    End Sub

    ''' <summary>Returns the version level of the SCPI standard implemented by the instrument..</summary>
    ''' <param name="port">Reference to a <see cref="Scpi.IPort">SCPI Port IO</see></param>
    ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
    Public Shared Function ReadScpiRevision(ByVal port As Scpi.IPort) As Double
        If port Is Nothing Then
            Throw New ArgumentNullException("port")
        End If
        Return port.QueryDouble(Syntax.BuildQuery(ScpiSyntaxHeader, Syntax.VersionCommand)).Value
    End Function

#End Region

#Region " SERIAL COMM - SHARED "

    ''' <summary>
    ''' Returns a command string to send to the 2182 instrument.
    ''' </summary>
    ''' <param name="remoteCommand">The comand which to send to the remote instrument.</param>
    Private Shared Function buildSerialSendQueryCommand(ByVal remoteCommand As String) As String
        Return String.Format(Globalization.CultureInfo.CurrentCulture, "{0} ""{1}""", SerialSendCommand, remoteCommand)
    End Function

    ''' <summary>
    ''' Returns a command string to send to the 2182 instrument.
    ''' </summary>
    ''' <param name="remoteCommandFormat">The format of the comand which to send to the remote instrument.</param>
    ''' <param name="args">Specifies the command arguments</param>
    Private Shared Function buildSerialSendQueryCommand(ByVal remoteCommandFormat As String, ByVal ParamArray args() As Object) As String
        Return buildSerialSendQueryCommand(String.Format(Globalization.CultureInfo.InvariantCulture, remoteCommandFormat, args))
    End Function

    Public Const SerialSendCommand As String = ":SYST:COMM:SER:SEND"
    Public Const SerialEnterCommand As String = ":SYST:COMM:SER:ENT?"

    ''' <summary>
    ''' Reads all the messages left on the serial buffer.
    ''' </summary>
    ''' <param name="port"></param>
    Public Shared Function SerialFlush(ByVal port As Scpi.IPort) As String

        Dim receivedMessage As String = ""
        Dim buffer As New System.Text.StringBuilder

        SystemSubsystem.SerialSend(port, "*OPC?")
        Threading.Thread.Sleep(200)
        SystemSubsystem.SerialRequestBuffer(port)
        Dim endTime As Date = DateTime.Now.AddMilliseconds(1000)
        Do
            receivedMessage = port.ReadLineTrimEnd()
            buffer.AppendLine(receivedMessage)
        Loop Until receivedMessage = "1" OrElse DateTime.Now > endTime
        Dim lf As String = ""
        endTime = DateTime.Now.AddMilliseconds(100)
        Do Until Not String.IsNullOrWhiteSpace(lf) OrElse endTime < DateTime.Now
            lf = port.ReadLine
        Loop
        Return buffer.ToString

    End Function

    ''' <summary>
    ''' Queries the remote instrument by way of serial communication.
    ''' </summary>
    ''' <param name="port"></param>
    ''' <param name="remoteCommand">The command which to send to the remote instrument.</param>
    ''' <param name="delayMilliseconds">The time to wait from sending the command until
    ''' expecting it to be present on the local instrument bus.</param>
    Public Shared Function SerialQuery(ByVal port As Scpi.IPort,
                                     ByVal remoteCommand As String,
                                     ByVal delayMilliseconds As Integer) As String
        SystemSubsystem.SerialSend(port, remoteCommand)
        Return SystemSubsystem.SerialRequestLineTrimEnd(port, delayMilliseconds)
    End Function

    ''' <summary>
    ''' Reads a message from the remote instrument by way of serial communication.
    ''' An enter command must be send prior to sending this message.
    ''' </summary>
    ''' <param name="port"></param>
    Public Shared Function SerialReadLineTrimEnd(ByVal port As Scpi.IPort) As String

        Dim receivedMessage As String = ""
        If (port.SerialPoll And ServiceRequests.MessageAvailable) <> 0 Then

            ' get the message.
            receivedMessage = port.ReadLineTrimEnd()

        End If

        ' Keithley 6221:
        ' Serial communication leaves a LF character on the bus after responding to a request for data.
        ' The Message Available Bits are not set.
        Dim lf As String = ""
        Dim endTime As DateTime = DateTime.Now.AddMilliseconds(100)
        Do Until Not String.IsNullOrWhiteSpace(lf) OrElse endTime < DateTime.Now
            lf = port.ReadLine
        Loop
        Return receivedMessage

    End Function

    ''' <summary>
    ''' Request data from the remote remote instrument by way of serial communication.
    ''' </summary>
    ''' <param name="port"></param>
    Public Shared Sub SerialRequestBuffer(ByVal port As Scpi.IPort)
        port.WriteLine(SerialEnterCommand)
    End Sub

    ''' <summary>
    ''' Request and read data in the buffer queue of the remote instrument by way of serial communication.
    ''' </summary>
    ''' <param name="port"></param>
    ''' <param name="delayMilliseconds">The time to wait between requests of data.</param>
    Public Shared Function SerialRequestLineTrimEnd(ByVal port As Scpi.IPort, ByVal delayMilliseconds As Integer) As String

        ' issue a request for data already in the buffer queue.
        SystemSubsystem.SerialRequestBuffer(port)

        ' wait some time for the request to reach destination and the remove instrument prepare the reply.
        Threading.Thread.Sleep(delayMilliseconds)

        ' read any reply from the remote instrument.
        Return SystemSubsystem.SerialReadLineTrimEnd(port)


    End Function

    ''' <summary>
    ''' Sends a message to the remote instrument by way of serial communication.
    ''' </summary>
    ''' <param name="remoteCommand">The command which to send to the remote instrument.</param>
    Public Shared Sub SerialSend(ByVal port As Scpi.IPort, ByVal remoteCommand As String)
        Dim queryCommand As String = SystemSubsystem.buildSerialSendQueryCommand(remoteCommand)
        port.WriteLine(queryCommand)
    End Sub

    ''' <summary>
    ''' Sends a message to the remote instrument by way of serial communication.
    ''' </summary>
    ''' <param name="remoteCommandFormat">The format of the comand which to send to the remote instrument.</param>
    ''' <param name="args">Specifies the command arguments</param>
    Public Shared Sub SerialSend(ByVal port As Scpi.IPort, ByVal remoteCommandFormat As String, ByVal ParamArray args() As Object)
        Dim queryCommand As String = SystemSubsystem.buildSerialSendQueryCommand(remoteCommandFormat, args)
        port.WriteLine(queryCommand)
    End Sub

#End Region

#Region " SERIAL COMM "

    Private _lastSerialMessageReceived As String
    ''' <summary>
    ''' Last message received from the remote instrument.
    ''' </summary>
    Public ReadOnly Property LastSerialMessageReceived() As String
        Get
            Return Me._lastSerialMessageReceived
        End Get
    End Property

    Private _lastSerialMessageSent As String
    ''' <summary>
    ''' Last message send from the remote instrument.
    ''' </summary>
    Public ReadOnly Property LastSerialMessageSent() As String
        Get
            Return Me._lastSerialMessageSent
        End Get
    End Property

    Private _serialCommTimedOut As Boolean
    ''' <summary>
    ''' Get the time out condition of the last query.
    ''' </summary>
    Public ReadOnly Property SerialCommTimedOut() As Boolean
        Get
            Return Me._serialCommTimedOut
        End Get
    End Property

    ''' <summary>
    ''' Queries the remote instrument by way of serial communication.
    ''' </summary>
    ''' <param name="remoteCommand">The command which to send to the remote instrument.</param>
    ''' <param name="delayMilliseconds">The time to wait from sending the command until
    ''' expecting it to be present on the local instrument bus.</param>
    ''' <param name="timeoutSeconds">Maximum time to look for a reply from the instrument.</param>
    Public Function SerialQuery(ByVal remoteCommand As String,
                                     ByVal delayMilliseconds As Integer,
                                     ByVal timeoutSeconds As Double) As String

        ' Send query to the remote instrument.
        SystemSubsystem.SerialSend(MyBase.Controller.Port, remoteCommand)

        ' receive the reply from the instrument.
        Return Me.SerialReceive(delayMilliseconds, timeoutSeconds)

    End Function

    ''' <summary>
    ''' Wait for serial operation to complete and return true if '1' was received.
    ''' </summary>
    Public Function SerialQueryOperationCompleted() As Boolean

        Return Me.SerialQuery(Scpi.Syntax.OperationCompletedQueryCommand, 200, 2) = "1"

    End Function

    ''' <summary>
    ''' Receives a message.
    ''' </summary>
    ''' <param name="delayMilliseconds">The time to wait between requests of data.</param>
    ''' <remarks>
    ''' Waits till timeout or data received.  If data received, accumulate data until no data is available.
    ''' </remarks>
    Public Function SerialReceive(ByVal delayMilliseconds As Integer, ByVal timeoutSeconds As Double) As String

        ' initialize monitoring values
        Me._lastSerialMessageReceived = String.Empty
        Me._serialCommTimedOut = False

        ' mark time to wait for timeout
        Dim endTime As Date = DateTime.Now.AddSeconds(timeoutSeconds)

        ' loop until we have a value or timeout
        Do

            ' wait some time for the remote instrument to prepare a reply.
            Threading.Thread.Sleep(delayMilliseconds)

            ' read any reply from the remote instrument.
            Me._lastSerialMessageReceived = SystemSubsystem.SerialRequestLineTrimEnd(MyBase.Controller.Port, delayMilliseconds)

            Me._serialCommTimedOut = String.IsNullOrWhiteSpace(Me._lastSerialMessageReceived) AndAlso DateTime.Now > endTime

        Loop Until Not String.IsNullOrWhiteSpace(Me._lastSerialMessageReceived) OrElse Me._serialCommTimedOut

        If Me._serialCommTimedOut Then
            Return Me._lastSerialMessageReceived
        End If

        Dim receivedBuffer As New System.Text.StringBuilder
        receivedBuffer.Append(Me._lastSerialMessageReceived)

        ' loop until we no longer get a value from the device
        Do

            ' wait some time for the remote instrument to prepare a reply.
            Threading.Thread.Sleep(delayMilliseconds)

            ' read any reply from the remote instrument.
            Me._lastSerialMessageReceived = SystemSubsystem.SerialRequestLineTrimEnd(MyBase.Controller.Port, delayMilliseconds)

            If Not String.IsNullOrWhiteSpace(Me._lastSerialMessageReceived) Then
                receivedBuffer.Append(Me._lastSerialMessageReceived)
            End If

            ' exit if the message is empty.
        Loop Until String.IsNullOrWhiteSpace(Me._lastSerialMessageReceived)

        ' save the entire message.
        Me._lastSerialMessageReceived = receivedBuffer.ToString

        Return Me._lastSerialMessageReceived

    End Function

    ''' <summary>
    ''' Sends a message to the remote instrument by way of serial communication.
    ''' </summary>
    ''' <param name="remoteCommand">The command which to send to the remote instrument.</param>
    Public Sub SerialSend(ByVal remoteCommand As String)
        Me._lastSerialMessageSent = remoteCommand
        SystemSubsystem.SerialSend(MyBase.Controller.Port, remoteCommand)
    End Sub

#End Region

#Region " SHARED ERROR "

    ''' <summary>Returns the last error from the instrument.</summary>
    ''' <param name="port">Reference to a <see cref="Scpi.IPort">SCPI Port IO</see></param>
    ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
    Public Shared Function ReadLastError(ByVal port As Scpi.IPort) As String

        If port Is Nothing Then
            Throw New ArgumentNullException("port")
        End If
        Return port.QueryTrimEnd(":SYST:ERR?")
    End Function

#End Region

#Region " IRESETTABLE "

    ''' <summary>Clears the queues and resets all registers to zero.
    ''' Sets the subsystem properties to the following CLS default values:<para>
    ''' 
    ''' </para>
    ''' </summary>
    Public Overrides Function ClearExecutionState() As Boolean
        Return MyBase.ClearExecutionState()
    End Function

    ''' <summary>
    ''' Sets subsystem to the following default system preset values:<para>
    ''' </para>
    ''' </summary>
    Public Overrides Function PresetKnownState() As Boolean
        Return MyBase.PresetKnownState()
    End Function

    ''' <summary>Restore member properties to the following RST or System Reset values:<para>
    ''' </para>
    ''' </summary>
    ''' <history>
    ''' </history>
    Public Overrides Function ResetKnownState() As Boolean
        Me.LineFrequencyGetter()
        Return MyBase.ResetKnownState()
    End Function

#End Region

#Region " PASSWORD "

    Private _passwordLocked As ScpiValue(Of Boolean)
    ''' <summary>Gets or sets the condition for auto zero is enabled.</summary>
    ''' <param name="access">The access <see cref="isr.Scpi.ResourceAccessLevels">level</see> to the instrument.</param>
    Public ReadOnly Property PasswordLocked(ByVal access As ResourceAccessLevels) As Boolean
        Get
            MyBase.Controller.Getter(Me._passwordLocked, Syntax.PasswordModalityName, Syntax.LockCommand, Core.EnumeratedBooleanBase.OneZero, access)
            Return Me._passwordLocked.Value.Value
        End Get
    End Property

    ''' <summary>Unlocks the instrument using the provided password.</summary>
    ''' <param name="password"></param>
    ''' <returns>True if unlocked.</returns>
    ''' <param name="access">The access <see cref="isr.Scpi.ResourceAccessLevels">level</see> to the instrument.</param>
    Public Function UnlockPassword(ByVal password As String, ByVal access As ResourceAccessLevels) As Boolean
        If access = ResourceAccessLevels.Device Then
            MyBase.Controller.Port.WriteLine(Syntax.BuildCommand(
                                    Me.SyntaxHeader, Syntax.PasswordModalityName, Syntax.UnlockCommand,
                                    String.Format(Globalization.CultureInfo.CurrentCulture, """{0}""", password)))
        End If
        Return Me.PasswordLocked(access)
    End Function

#End Region

#Region " BEEPER "

    Private _beeperEnabled As ResettableValue(Of Boolean)
    Public ReadOnly Property BeeperEnabled() As ResettableValue(Of Boolean)
        Get
            Return Me._beeperEnabled
        End Get
    End Property
    ''' <summary>
    ''' Gets or sets the Beeper enabled state.
    ''' </summary>
    ''' <param name="access">The access <see cref="isr.Scpi.ResourceAccessLevels">level</see> to the instrument.</param>
    Public Property BeeperEnabled(ByVal access As ResourceAccessLevels) As Boolean
        Get
            MyBase.Controller.Getter(Me._beeperEnabled, "BEEP:STAT", Core.EnumeratedBooleanBase.OneZero, access)
            Return Me._beeperEnabled.Value.Value
        End Get
        Set(ByVal Value As Boolean)
            MyBase.Controller.Setter(Me._beeperEnabled, "BEEP:STAT", Value, Core.EnumeratedBooleanBase.OneZero, access)
        End Set
    End Property

    ''' <summary>
    ''' Commands the instrument to issue a Beep on the instrument.
    ''' </summary>
    ''' <param name="frequency">Specifies the frequency of the beep</param>
    ''' <param name="duration">Specifies the duration of the beep.</param>
    Public Sub BeepImmediately(ByVal frequency As Integer, ByVal duration As Single)

        MyBase.Controller.Port.WriteLine("{0}:BEEP:IMM {1}, {2}", MyBase.SyntaxHeader, frequency, duration)

    End Sub

#End Region

#Region " CARDS "

    ''' <summary>
    ''' Gets a list of cards.
    ''' </summary>
    Public Function EnumerateCards() As String()
        Dim cardList As String = MyBase.Controller.Port.QueryTrimEnd("*OPT?")
        If String.IsNullOrWhiteSpace(cardList) Then
            Return New String() {}
        Else
            Return cardList.Split(","c)
        End If
    End Function

    ''' <summary>
    ''' Return the number of analog outputs on the specified slot.
    ''' </summary>
    ''' <param name="slotNumber"></param>
    Public Function ReadCardAnalogOuts(ByVal slotNumber As Integer) As Integer
        Dim analogOuts As String = MyBase.Controller.QueryTrimEnd(String.Format(Globalization.CultureInfo.CurrentCulture,
              "SYST:CARD{0}:AOUT?", slotNumber))
        Dim cardAnalogOuts As Integer = 0
        If Not String.IsNullOrWhiteSpace(analogOuts) AndAlso Not Integer.TryParse(analogOuts, Globalization.NumberStyles.Integer,
                                Globalization.CultureInfo.InvariantCulture, cardAnalogOuts) Then
            Return 0
        End If
        Return cardAnalogOuts
    End Function

    ''' <summary>
    ''' Returns the firmware version of the card located in the 
    ''' specified <paramref name="slotNumber">slot</paramref>.
    ''' </summary>
    ''' <param name="slotNumber"></param>
    Public Function ReadCardFirmwareVersion(ByVal slotNumber As Integer) As String
        Return MyBase.Controller.Port.QueryTrimEnd(String.Format(Globalization.CultureInfo.CurrentCulture,
              "SYST:CARD{0}:SWR?", slotNumber))
    End Function

    ''' <summary>
    ''' Gets the card calibration count.
    ''' </summary>
    ''' <param name="slotNumber"></param>
    Public Function ReadCardCalCount(ByVal slotNumber As Integer) As Nullable(Of Int32)
        Return MyBase.Controller.Port.QueryInteger(String.Format(Globalization.CultureInfo.CurrentCulture,
            "CAL:PROT:CARD{0}:COUNT?", slotNumber))
    End Function

    ''' <summary>
    ''' Gets the card calibration date.
    ''' </summary>
    ''' <param name="slotNumber"></param>
    Public Function ReadCardCalDate(ByVal slotNumber As Integer) As String
        Return MyBase.Controller.Port.QueryTrimEnd(String.Format(Globalization.CultureInfo.CurrentCulture,
            "CAL:PROT:CARD{0}:DATE?", slotNumber)).Replace(",", "/")
    End Function

    ''' <summary>
    ''' Returns the serial number of the card located in the 
    ''' specified <paramref name="slotNumber">slot</paramref>.
    ''' </summary>
    ''' <param name="slotNumber"></param>
    Public Function ReadCardSerialNumber(ByVal slotNumber As Integer) As String
        Return MyBase.Controller.Port.QueryTrimEnd(String.Format(Globalization.CultureInfo.CurrentCulture,
              "SYST:CARD{0}:SNUM?", slotNumber))
    End Function

    ''' <summary>
    ''' Returns true if card has thermocouple compensation.
    ''' </summary>
    ''' <param name="slotNumber"></param>
    Public Function ReadCardTempCompensated(ByVal slotNumber As Integer) As Boolean
        Return MyBase.Controller.Port.QueryBoolean(String.Format(Globalization.CultureInfo.CurrentCulture,
              "SYST:CARD{0}:TCOM?", slotNumber), Core.EnumeratedBooleanBase.OneZero).Value
    End Function

    ''' <summary>
    ''' Sets a psuedo card for the specific slot.
    ''' </summary>
    ''' <param name="slotNumber"></param>
    ''' <param name="cardName"></param>
    Public Sub WritePseudoCard(ByVal slotNumber As Integer, ByVal cardName As String)
        MyBase.Controller.Port.WriteLine(String.Format(Globalization.CultureInfo.CurrentCulture,
              "SYST:PCAR{0} {1}", slotNumber, cardName))
    End Sub

#End Region

#Region " CONTACT CHECK "

    Private _contactCheckEnabled As ResettableValue(Of Boolean)
    Public ReadOnly Property ContactCheckEnabled() As ResettableValue(Of Boolean)
        Get
            Return Me._contactCheckEnabled
        End Get
    End Property

    ''' <summary>
    ''' Gets or sets the Contack check enabled state.
    ''' </summary>
    ''' <param name="access">The access <see cref="isr.Scpi.ResourceAccessLevels">level</see> to the instrument.</param>
    Public Property ContactCheckEnabled(ByVal access As ResourceAccessLevels) As Boolean
        Get
            MyBase.Controller.Getter(Me._contactCheckEnabled, Syntax.ContactCheck, Core.EnumeratedBooleanBase.OneZero, access)
            Return Me._contactCheckEnabled.Value.Value
        End Get
        Set(ByVal Value As Boolean)
            MyBase.Controller.Setter(Me._contactCheckEnabled, Syntax.ContactCheck, Value, Core.EnumeratedBooleanBase.OneZero, access)
        End Set
    End Property

    Private _contactCheckResistance As ResettableValue(Of Integer)
    Public ReadOnly Property ContactCheckResistance() As ResettableValue(Of Integer)
        Get
            Return Me._contactCheckResistance
        End Get
    End Property

    ''' <summary>
    ''' Gets or sets the contact check resistance.
    ''' </summary>
    ''' <param name="access"></param>
    Public Property ContactCheckResistance(ByVal access As ResourceAccessLevels) As Integer
        Get
            MyBase.Controller.Getter(Me._contactCheckResistance, Syntax.ContactCheck, Syntax.Resistance, access)
            Return Me._contactCheckResistance.Value.Value
        End Get
        Set(ByVal Value As Integer)
            MyBase.Controller.Setter(Me._contactCheckResistance, Syntax.ContactCheck, Syntax.Resistance, Value, access)
        End Set
    End Property

#End Region

#Region " CONTROL "

    Private _isRemote As ResettableValue(Of Boolean)
    ''' <summary>
    ''' Places or takes out the instrument of remote. Non-GPIB Only.
    ''' </summary>
    Public Property Remote() As Boolean
        Get
            Return Me._isRemote.Value.Value
        End Get
        Set(ByVal value As Boolean)
            Me._isRemote.Value = value
            If value Then
                MyBase.Controller.Port.WriteLine(":SYST:REM")
            Else
                MyBase.Controller.Port.WriteLine(":SYST:LOC")
            End If
        End Set
    End Property

    ''' <summary>
    ''' Lock out the front panel.  Non-GPIB.
    ''' </summary>
    Public Sub LocalLockout()
        MyBase.Controller.Port.WriteLine(":SYST:RWL")
    End Sub

#End Region

#Region " ERROR MANAGEMENT "

    ''' <summary>Clears the messages from the error queue.</summary>
    Public Sub ClearErrorQueue()
        SystemSubsystem.ClearErrorQueue(MyBase.Controller.Port)
    End Sub

    Private _lastError As ResettableString
    ''' <summary>
    ''' Gets or sets the last error.
    ''' </summary>
    ''' <value>
    ''' The last error.
    ''' </value>
    Public Property LastError() As String
        Get
            Return Me._lastError.Value
        End Get
        Set(ByVal value As String)
            Me._lastError.Value = value
        End Set
    End Property


    ''' <summary>
    ''' Gets the last error (cached).
    ''' </summary>
    Public ReadOnly Property LastError(ByVal access As ResourceAccessLevels) As String
        Get
            MyBase.Controller.Getter(Me._lastError, access)
            Return Me._lastError.Value
        End Get
    End Property

#End Region

#Region " PROPERTIES "

    Private _autoZero As ResettableValue(Of Boolean)
    Public ReadOnly Property AutoZero() As ResettableValue(Of Boolean)
        Get
            Return Me._autoZero
        End Get
    End Property
    ''' <summary>Gets or sets the condition for auto zero is enabled.</summary>
    Public Property AutoZero(ByVal access As ResourceAccessLevels) As Boolean
        Get
            MyBase.Controller.Getter(Me._autoZero, Syntax.AutoZeroCommand, Core.EnumeratedBooleanBase.OnOff, access)
            Return Me._autoZero.Value.Value
        End Get
        Set(ByVal value As Boolean)
            MyBase.Controller.Setter(Me._autoZero, Syntax.AutoZeroCommand, value, Core.EnumeratedBooleanBase.OnOff, access)
        End Set
    End Property

    Private _cableGuard As ResettableValue(Of Boolean)
    Public ReadOnly Property CableGuard() As ResettableValue(Of Boolean)
        Get
            Return Me._cableGuard
        End Get
    End Property
    ''' <summary>
    ''' Gets or sets the guard type as <see cref="Syntax.Cable">Cable</see> (true)
    ''' or <see cref="Syntax.Ohms">OHMS</see> (false).
    ''' </summary>
    ''' <param name="access"></param>
    Public Property CableGuard(ByVal access As ResourceAccessLevels) As Boolean
        Get
            MyBase.Controller.Getter(Me._cableGuard, Syntax.Guard, Syntax.CableOhms, access)
            Return Me._cableGuard.Value.Value
        End Get
        Set(ByVal Value As Boolean)
            MyBase.Controller.Setter(Me._cableGuard, Syntax.Guard, Value, Syntax.CableOhms, access)
        End Set
    End Property

    Private _frontSwitchedCache As ScpiValue(Of Boolean)
    ''' <summary>
    ''' Gets or sets the cached value of the front switched state.
    ''' </summary>
    ''' <value>
    ''' The front switched cached.
    ''' </value>
    Public Property FrontSwitchedCache() As ScpiValue(Of Boolean)
        Get
            Return Me._frontSwitchedCache
        End Get
        Set(ByVal value As ScpiValue(Of Boolean))
            Me._frontSwitchedCache = value
        End Set
    End Property

    ''' <summary>Gets or sets the status of the front (true) read (false) switch.</summary>
    ''' <param name="access">The access <see cref="isr.Scpi.ResourceAccessLevels">level</see> to the instrument.</param>
    Public ReadOnly Property FrontSwitched(ByVal access As ResourceAccessLevels) As Boolean
        Get
            MyBase.Controller.Getter(Me._frontSwitchedCache, Core.EnumeratedBooleanBase.OneZero, access)
            Return Me.FrontSwitchedCache.Value.Value
        End Get
    End Property

    Private _scpiRevision As ScpiValue(Of Double)
    ''' <summary>Returns the version level of the SCPI standard implemented by the instrument..</summary>
    ''' <param name="access">The access <see cref="isr.Scpi.ResourceAccessLevels">level</see> to the instrument.</param>
    Public ReadOnly Property ScpiRevision(ByVal access As ResourceAccessLevels) As Double
        Get
            MyBase.Controller.Getter(Me._scpiRevision, access)
            Return Me._scpiRevision.Value.Value
        End Get
    End Property

#End Region

#Region " LINE FREQUENCY "

    Private Shared _systemLineFrequency As Integer?
    ''' <summary>
    ''' Gets the system line frequency.
    ''' </summary>
    ''' 
    Public Shared ReadOnly Property SystemLineFrequency() As Integer?
        Get
            Return SystemSubsystem._systemLineFrequency
        End Get
    End Property

    Private _lineFrequency As ScpiValue(Of Integer)
    ''' <summary>Gets or sets the line frequency for this instrument.
    ''' Setting the stored value is useful to address the case when the instrument does not 
    ''' support the line frequency command.
    ''' </summary>
    Public ReadOnly Property LineFrequency() As ScpiValue(Of Integer)
        Get
            Return Me._lineFrequency
        End Get
    End Property

    ''' <summary>
    ''' Returns the line frequency.
    ''' </summary>
    Public Function LineFrequencyGetter() As Integer
        If Me._lineFrequency.Value.HasValue Then
            Return Me._lineFrequency.Value.Value
        Else
            If String.IsNullOrWhiteSpace(Me._lineFrequency.GetterCommand) Then
                SystemSubsystem._systemLineFrequency = 60
                Me._lineFrequency.Value = 60
                Me._lineFrequency.ActualValue = 60
            Else
                SystemSubsystem._systemLineFrequency = MyBase.Controller.Getter(Me._lineFrequency).Value.GetValueOrDefault(60)
            End If
            Return Me._lineFrequency.Value.Value
        End If
    End Function

#End Region

#Region " REMOTE SENSE "

    Private _remoteSenseMode As ResettableValue(Of Boolean)
    ''' <summary>
    ''' Gets the remote sense mode.
    ''' </summary>
    ''' 
    Public ReadOnly Property RemoteSenseMode() As ResettableValue(Of Boolean)
        Get
            Return Me._remoteSenseMode
        End Get
    End Property

    ''' <summary>
    ''' Gets or sets the measurement sense mode as four-wire (remote) or
    ''' Two-Wire</summary>
    Public Function RemoteSenseGetter() As Boolean
        MyBase.Controller.Getter(Me._remoteSenseMode)
        Return Me.RemoteSenseMode.Value.Value
    End Function

    ''' <summary>
    ''' Gets or sets the measurement sense mode as four-wire (remote) or
    ''' Two-Wire</summary>
    Public Function RemoteSenseSetter(ByVal value As Boolean) As Boolean
        MyBase.Controller.Setter(Me._remoteSenseMode, value)
        Return Me.RemoteSenseMode.Value.Value
    End Function

#End Region

End Class

#Region " UNUSED "
#If False Then
  ''' <summary>Gets or sets the measurement <see cref="SenseMode">sense mode</see></summary>
  ''' <param name="port">Reference to a <see cref="Scpi.IPort">SCPI Port IO</see></param>
  ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
  Public Shared Property SenseMode(ByVal controller As Scpi.IDevice) As SenseMode
    Get
      If controller Is Nothing Then
        Throw New ArgumentNullException("port")
      End If
      If controller.QueryBoolean(Syntax.BuildQuery(Me.SyntaxHeader, Syntax.RemoteSenseCommand)) Then
        Return SenseMode.FourWire
      Else
        Return SenseMode.TwoWire
      End If
    End Get
    Set(ByVal value As SenseMode)
      If controller Is Nothing Then
        Throw New ArgumentNullException("port")
      End If
      MyBase.Controller.Port.WriteLine(Syntax.BuildCommand(ScpiSyntaxHeader, Syntax.RemoteSenseCommand, value = Scpi.SenseMode.FourWire, BooleanDataFormat.OnOff))
    End Set
  End Property


  ''' <summary>Gets or sets the measurement <see cref="SenseMode">sense mode</see></summary>
  Public Property SenseMode(ByVal access As ResourceAccessLevels) As SenseMode
    Get
      If Me.RemoteSenseMode(access) Then
        Return SenseMode.FourWire
      Else
        Return SenseMode.TwoWire
      End If
    End Get
    Set(ByVal value As SenseMode)
      Me.RemoteSenseMode(access) = value = Scpi.SenseMode.FourWire
    End Set
  End Property
#End If
#End Region