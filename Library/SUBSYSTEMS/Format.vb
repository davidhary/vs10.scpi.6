''' <summary>Defines a Scpi Format Subsystem.</summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="01/15/05" by="David" revision="1.0.1841.x">
''' Created
''' </history>
Public Class FormatSubsystem
    Inherits Subsystem

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>
    ''' The SCPI Syntax Header for this subsystem.
    ''' </summary>
    Public Const ScpiSyntaxHeader As String = "FORM"

    ''' <summary>Constructs this class.</summary>
    ''' <param name="port">Reference to an open <see cref="Scpi.IPort">SCPI Port IO</see>.</param>
    Public Sub New(ByVal port As Scpi.IPort)

        ' instantiate the base class
        MyBase.New(ScpiSyntaxHeader, port)

        Me._ReadingElementsCache = New isr.Scpi.ResettableString
        MyBase.ResettableValues.Add(Me._ReadingElementsCache)


    End Sub

    ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
    ''' <param name="disposing">True if this method releases both managed and unmanaged 
    '''   resources; False if this method releases only unmanaged resources.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by
    '''   its disposing parameter.  If True, the method has been called directly or 
    '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
    '''   If disposing equals False, the method has been called by the 
    '''   runtime from inside the finalizer and you should not reference 
    '''   other objects--only unmanaged resources can be disposed.</remarks>
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)

        If Not MyBase.IsDisposed Then

            Try

                If disposing Then

                    ' Free managed resources when explicitly called

                End If

                ' Free shared unmanaged resources
                ' onDisposeUnmanagedResources
            Finally

                ' Invoke the base class dispose method
                MyBase.Dispose(disposing)

            End Try

        End If
    End Sub

#End Region

#Region " PARSERS AND BUILDERS "

#Region " READING ELEMENTS  "

    ''' <summary>
    ''' Builds the elements record for the specified elements.
    ''' </summary>
    ''' <param name="elements"></param>
    Public Shared Function BuildElementsRecord(ByVal elements As Nullable(Of ReadingElements)) As String
        If elements.HasValue Then
            Return BuildElementsRecord(elements.Value)
        Else
            Return BuildElementsRecord(ReadingElements.None)
        End If
    End Function
    ''' <summary>
    ''' Builds the elements record for the specified elements.
    ''' </summary>
    ''' <param name="elements"></param>
    Public Shared Function BuildElementsRecord(ByVal elements As ReadingElements) As String
        If elements = ReadingElements.None Then
            Return String.Empty
        Else
            Dim reply As New System.Text.StringBuilder
            For Each code As Integer In [Enum].GetValues(GetType(ReadingElements))
                If (elements And code) <> 0 Then
                    Dim value As String = Syntax.ExtractScpi(CType(code, ReadingElements))
                    If Not String.IsNullOrWhiteSpace(value) Then
                        Syntax.AddWord(reply, value)
                    End If
                End If
            Next
            Return reply.ToString
        End If
    End Function

    ''' <summary>
    ''' Get the composite reading elements based on the message from the instrument.
    ''' </summary>
    ''' <param name="record">Specifies the comma delimited elements record.</param>
    Public Shared Function ParseElements(ByVal record As String) As ReadingElements
        Dim parsed As ReadingElements = ReadingElements.None
        If Not String.IsNullOrWhiteSpace(record) Then
            For Each elementValue As String In record.Split(","c)
                parsed = parsed Or Syntax.ParseReadingElements(elementValue)
            Next
        End If
        Return parsed
    End Function

#End Region

#End Region

#Region " IRESETTABLE "

    ''' <summary>Clears the queues and resets all registers to zero.
    ''' Sets the subsystem properties to the following CLS default values:<para>
    ''' 
    ''' </para>
    ''' </summary>
    Public Overrides Function ClearExecutionState() As Boolean
        Return MyBase.ClearExecutionState()
    End Function

    ''' <summary>
    ''' Sets subsystem to the following default system preset values:<para>
    ''' </para>
    ''' </summary>
    Public Overrides Function PresetKnownState() As Boolean
        Return MyBase.PresetKnownState()
    End Function

    ''' <summary>Restore member properties to the following RST or System Reset values:<para>
    ''' </para>
    ''' </summary>
    ''' <history>
    ''' </history>
    Public Overrides Function ResetKnownState() As Boolean
        Return MyBase.ResetKnownState()
    End Function

#End Region

#Region " METHODS  and  PROPERTIES "

    Private _Elements As ReadingElements
    ''' <summary>
    ''' Gets the elements.
    ''' </summary>
    ''' 
    Public Property Elements() As ReadingElements
        Get
            Return Me._Elements
        End Get
        Protected Set(ByVal value As ReadingElements)
            Me._Elements = value
        End Set
    End Property

    Private _ReadingElementsCache As isr.Scpi.ResettableString
    Public ReadOnly Property ReadingElementsCache() As isr.Scpi.ResettableString
        Get
            Return Me._ReadingElementsCache
        End Get
    End Property

    ''' <summary>
    ''' Gets or sets the <see cref="ReadingElements">reading elements</see>.</summary>
    ''' <remarks>
    ''' Use the following code in the calling methd if verification is used:
    ''' <code>
    ''' If Not Me.Elements.IsVerified.GetValueOrDefault(False) Then
    '''   If Me.Elements.Value.HasValue AndAlso Me.Elements.ActualValue.HasValue Then
    '''     return String.Format(Globalization.CultureInfo.CurrentCulture, "Instrument settings applied using the command '{0}' did not verify. Expected={1}. Actual={2}.", Me.Elements.Value, Me.Elements.ActualValue.Value)
    '''   Else
    '''     return "Failed reading values from the instrument."
    '''   End If
    ''' End If
    ''' </code>
    ''' </remarks>
    Public Property Elements(ByVal access As ResourceAccessLevels) As ReadingElements
        Get
            ' MyBase.controller.Getter(Me.Elements, Syntax.Elements, FormatSubsystem.BuildElementsRecord(Me.Elements), access)
            Me.Elements = FormatSubsystem.ParseElements(MyBase.Controller.Getter(Me._ReadingElementsCache, Syntax.Elements, access).Value)
            Return Me.Elements
        End Get
        Set(ByVal value As ReadingElements)
            Dim value1 As String = MyBase.Controller.Setter(Me._ReadingElementsCache, Syntax.Elements, FormatSubsystem.BuildElementsRecord(value), access).Value
            ' MyBase.controller.Setter(Me.Elements, Syntax.Elements, FormatSubsystem.BuildElementsRecord(Me.Elements.Value), FormatSubsystem.BuildElementsRecord(value), access)
            Me.Elements = FormatSubsystem.ParseElements(value1)
        End Set
    End Property

#End Region

End Class
