''' <summary>Defines a SCPI Route Subsystem.</summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="01/15/05" by="David" revision="1.0.1841.x">
''' Created
''' </history>
Public Class RouteSubsystem
    Inherits Subsystem

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>
    ''' The SCPI Syntax Header for this subsystem.
    ''' </summary>
    Public Const ScpiSyntaxHeader As String = "ROUT"

    ''' <summary>Constructs this class.</summary>
    ''' <param name="port">Reference to an open <see cref="Scpi.IPort">SCPI Port IO</see>.</param>
    Public Sub New(ByVal port As Scpi.IPort)

        ' instantiate the base class
        MyBase.New(ScpiSyntaxHeader, port)

        Me._frontRouteTerminals = New ResettableValue(Of Boolean)
        MyBase.ResettableValues.Add(Me.FrontRouteTerminals)

        Me._scanListCache = New ScpiString
        Me._scanListCache.GetterCommand = Syntax.BuildQuery(Me.SyntaxHeader, Syntax.ScanModalityName) ' ":ROUT:SCAN?"
        Me._scanListCache.SetterCommandFormat = Syntax.BuildCommandFormat(Me.SyntaxHeader, Syntax.ScanModalityName)  ' ":ROUT:SCAN {0}"

    End Sub

    ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
    ''' <param name="disposing">True if this method releases both managed and unmanaged 
    '''   resources; False if this method releases only unmanaged resources.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by
    '''   its disposing parameter.  If True, the method has been called directly or 
    '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
    '''   If disposing equals False, the method has been called by the 
    '''   runtime from inside the finalizer and you should not reference 
    '''   other objects--only unmanaged resources can be disposed.</remarks>
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)

        If Not MyBase.IsDisposed Then

            Try

                If disposing Then

                    ' Free managed resources when explicitly called


                End If

                ' Free shared unmanaged resources
                ' onDisposeUnmanagedResources
            Finally

                ' Invoke the base class dispose method
                MyBase.Dispose(disposing)

            End Try

        End If

    End Sub

#End Region

#Region " SHARED "

    ''' <summary>
    ''' Returns true if the specified channels are closed.
    ''' </summary>
    ''' <param name="port">Reference to a <see cref="Scpi.IPort">SCPI Port IO</see></param>
    ''' <param name="channelList"></param>
    Public Shared Function AreClosedChannels(ByVal port As Scpi.IPort, ByVal channelList As String) As Boolean
        Return port.QueryBoolean(String.Format(Globalization.CultureInfo.CurrentCulture, ":ROUT:CLOS:STAT? {0}", channelList), Core.EnumeratedBooleanBase.OneZero).Value
    End Function

    ''' <summary>
    ''' Returns true if the specified channels are closed.
    ''' </summary>
    ''' <param name="port">Reference to a <see cref="Scpi.IPort">SCPI Port IO</see></param>
    ''' <param name="channelList"></param>
    Public Shared Function AreClosedMultipleChannels(ByVal port As Scpi.IPort, ByVal channelList As String) As Boolean
        Return port.QueryBoolean(String.Format(Globalization.CultureInfo.CurrentCulture, ":ROUT:MULT:CLOS:STAT? {0}", channelList), Core.EnumeratedBooleanBase.OneZero).Value
    End Function

    ''' <summary>Closes the specified channels in the list.
    ''' Use <see cref="CloseMultipleChannels">close multiple</see> to close only the
    ''' specified channels.
    ''' </summary>
    ''' <param name="port">Reference to a <see cref="Scpi.IPort">SCPI Port IO</see></param>
    ''' <param name="channelList"></param>
    ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
    ''' <remarks>
    ''' On the Keithley 2700 series, this command can only close a single
    ''' channel opening all other channels. Using a scan list with multiple channels causes an 
    ''' instrument error.
    ''' </remarks>
    Public Shared Sub CloseChannels(ByVal port As Scpi.IPort, ByVal channelList As String)
        If port Is Nothing Then
            Throw New ArgumentNullException("port")
        End If
        port.WriteLine(String.Format(Globalization.CultureInfo.CurrentCulture, ":ROUT:CLOS {0}", channelList))
    End Sub

    ''' <summary>Closes the specified channels in the list.
    ''' </summary>
    ''' <param name="port">Reference to a <see cref="Scpi.IPort">SCPI Port IO</see></param>
    ''' <param name="channelList"></param>
    ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
    Public Shared Sub CloseMultipleChannels(ByVal port As Scpi.IPort, ByVal channelList As String)
        If port Is Nothing Then
            Throw New ArgumentNullException("port")
        End If
        port.WriteLine(String.Format(Globalization.CultureInfo.CurrentCulture, ":ROUT:MULT:CLOS {0}", channelList))
    End Sub

    ''' <summary>Recalls channel pattern from a memory location.</summary>
    ''' <param name="port">Reference to a <see cref="Scpi.IPort">SCPI Port IO</see></param>
    ''' <param name="memoryLocation">Specifies a memory location between 1 and 100.</param>
    ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
    Public Shared Sub MemoryRecall(ByVal port As Scpi.IPort, ByVal memoryLocation As Int32)
        If port Is Nothing Then
            Throw New ArgumentNullException("port")
        End If
        port.WriteLine(String.Format(Globalization.CultureInfo.CurrentCulture, ":ROUT:MEM:REC M{0}", memoryLocation))
    End Sub

    ''' <summary>Saves existing channel pattern into a memory location.</summary>
    ''' <param name="port">Reference to a <see cref="Scpi.IPort">SCPI Port IO</see></param>
    ''' <param name="memoryLocation">Specifies a memory location between 1 and 100.</param>
    ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
    Public Shared Sub MemorySave(ByVal port As Scpi.IPort, ByVal memoryLocation As Int32)
        If port Is Nothing Then
            Throw New ArgumentNullException("port")
        End If
        port.WriteLine(String.Format(Globalization.CultureInfo.CurrentCulture, ":ROUT:MEM:SAVE M{0}", memoryLocation))
    End Sub

    ''' <summary>Opens all channels</summary>
    ''' <param name="port">Reference to a <see cref="Scpi.IPort">SCPI Port IO</see></param>
    ''' <param name="queryCommand">Specifies the open command.</param>
    ''' <remarks>An open command needs to be specified for the following instruments:<para>
    ''' Keithley 7000 Series: ":ROUT:OPEN ALL"
    ''' </para><para>
    ''' Keithley 2700 Series: ":ROUT:OPEN:ALL"
    ''' </para>
    ''' </remarks>
    ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
    Public Shared Sub OpenAll(ByVal port As Scpi.IPort, ByVal queryCommand As String)
        If port Is Nothing Then
            Throw New ArgumentNullException("port")
        End If
        port.WriteLine(queryCommand)
    End Sub

    ''' <summary>Opens the specified channels in the list.
    ''' On instruments such as 2700, which do not support <see cref="OpenChannels">open</see> 
    ''' Use <see cref="OpenMultipleChannels">open multiple</see> to open only the specified channels.
    ''' </summary>
    ''' <param name="port">Reference to a <see cref="Scpi.IPort">SCPI Port IO</see></param>
    ''' <param name="channelList"></param>
    ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
    Public Shared Sub OpenChannels(ByVal port As Scpi.IPort, ByVal channelList As String)
        If port Is Nothing Then
            Throw New ArgumentNullException("port")
        End If
        port.WriteLine(String.Format(Globalization.CultureInfo.CurrentCulture, ":ROUT:OPEN {0}", channelList))
    End Sub

    ''' <summary>Opens the specified channels in the list.
    ''' Some instruments (e.g., 2700) do not support <see cref="OpenChannels">open</see> this opens all other channels.  
    ''' Use <see cref="CloseMultipleChannels">close multiple</see> to close only the
    ''' specified channels.
    ''' </summary>
    ''' <param name="port">Reference to a <see cref="Scpi.IPort">SCPI Port IO</see></param>
    ''' <param name="channelList"></param>
    ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
    Public Shared Sub OpenMultipleChannels(ByVal port As Scpi.IPort, ByVal channelList As String)
        If port Is Nothing Then
            Throw New ArgumentNullException("port")
        End If
        port.WriteLine(String.Format(Globalization.CultureInfo.CurrentCulture, ":ROUT:MULT:OPEN {0}", channelList))
    End Sub

    ''' <summary>Gets or sets the slot type.</summary>
    ''' <param name="port">Reference to a <see cref="Scpi.IPort">SCPI Port IO</see></param>
    ''' <param name="slotNumber">Specifies the slot number.</param>
    ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
    Public Shared Property SlotCardType(ByVal port As Scpi.IPort, ByVal slotNumber As Int32) As String
        Get
            If port Is Nothing Then
                Throw New ArgumentNullException("port")
            End If
            Return port.QueryTrimEnd(String.Format(Globalization.CultureInfo.CurrentCulture, ":ROUT:CONF:SLOT{0}:CTYPE?", slotNumber))
        End Get
        Set(ByVal value As String)
            If port Is Nothing Then
                Throw New ArgumentNullException("port")
            End If
            port.WriteLine(String.Format(Globalization.CultureInfo.CurrentCulture, ":ROUT:CONF:SLOT{0}:CTYPE {1}", slotNumber, value))
        End Set
    End Property

    ''' <summary>Gets or sets the slot settling time.</summary>
    ''' <param name="port">Reference to a <see cref="Scpi.IPort">SCPI Port IO</see></param>
    ''' <param name="slotNumber">Specifies the slot number.</param>
    ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
    Public Shared Property SlotSettlingTime(ByVal port As Scpi.IPort, ByVal slotNumber As Int32) As Double
        Get
            If port Is Nothing Then
                Throw New ArgumentNullException("port")
            End If
            Return port.QueryDouble(String.Format(Globalization.CultureInfo.CurrentCulture,
                                                     ":ROUT:CONF:SLOT{0}:STIME?", slotNumber)).Value
        End Get
        Set(ByVal value As Double)
            If port Is Nothing Then
                Throw New ArgumentNullException("port")
            End If
            port.WriteLine(String.Format(Globalization.CultureInfo.CurrentCulture, ":ROUT:CONF:SLOT{0}:STIME {1}", slotNumber, value))
        End Set
    End Property

#End Region

#Region " IRESETTABLE "

    ''' <summary>Clears the queues and resets all registers to zero.
    ''' Sets the subsystem properties to the following CLS default values:<para>
    ''' 
    ''' </para>
    ''' </summary>
    Public Overrides Function ClearExecutionState() As Boolean
        Return MyBase.ClearExecutionState()
    End Function

    ''' <summary>
    ''' Sets subsystem to the following default system preset values:<para>
    ''' </para>
    ''' </summary>
    Public Overrides Function PresetKnownState() As Boolean
        Return MyBase.PresetKnownState()
    End Function

    ''' <summary>Restore member properties to the following RST or System Reset values:<para>
    ''' ROUTE: Front</para><para>
    ''' </para>
    ''' </summary>
    ''' <history>
    ''' </history>
    Public Overrides Function ResetKnownState() As Boolean
        Me._scanListCache = ""
        Return MyBase.ResetKnownState()
    End Function

#End Region

#Region " ROUTE "

    Private _frontRouteTerminals As ResettableValue(Of Boolean)
    Public ReadOnly Property FrontRouteTerminals() As ResettableValue(Of Boolean)
        Get
            Return Me._frontRouteTerminals
        End Get
    End Property

    ''' <summary>
    ''' Gets or sets the output route terminals state as front or rear.
    ''' </summary>
    ''' <param name="access"></param>
    Public Property FrontRouteTerminals(ByVal access As ResourceAccessLevels) As Boolean
        Get
            MyBase.Controller.Getter(Me.FrontRouteTerminals, Syntax.TerminalsCommand, Syntax.FrontRear, access)
            Return Me._frontRouteTerminals.Value.Value
        End Get
        Set(ByVal Value As Boolean)
            MyBase.Controller.Setter(Me._frontRouteTerminals, Syntax.TerminalsCommand, Value, Syntax.FrontRear, access)
        End Set
    End Property

#End Region

#Region " CHANNELS "

    ''' <summary>
    ''' Gets or sets the closed channels using the 
    ''' <see cref="CloseChannels">close SCPI command.</see>
    ''' </summary>
    Public Property ClosedChannels() As String
        Get
            Return MyBase.Controller.Port.QueryTrimEnd(":ROUT:CLOS?")
        End Get
        Set(ByVal value As String)
            MyBase.Controller.Port.WriteLine(String.Format(Globalization.CultureInfo.CurrentCulture,
                                                  ":ROUT:CLOS {0}", value))
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the closed channels using the 
    ''' <see cref="CloseMultipleChannels">multiple close SCPI command.</see>
    ''' </summary>
    Public Property ClosedMultipleChannels() As String
        Get
            Return MyBase.Controller.Port.QueryTrimEnd(":ROUT:MULT:CLOS?")
        End Get
        Set(ByVal value As String)
            MyBase.Controller.Port.WriteLine(String.Format(Globalization.CultureInfo.CurrentCulture,
                                                  ":ROUT:MULT:CLOS {0}", value))
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the open channels using the 
    ''' <see cref="OpenMultipleChannels">multiple open SCPI command.</see>
    ''' </summary>
    Public Property OpenedMultipleChannels() As String
        Get
            Return MyBase.Controller.Port.QueryTrimEnd(":ROUT:MULT:OPEN?")
        End Get
        Set(ByVal value As String)
            MyBase.Controller.Port.WriteLine(String.Format(Globalization.CultureInfo.CurrentCulture,
                                                  ":ROUT:MULT:OPEN {0}", value))
        End Set
    End Property

    Private _scanListCache As ScpiString
    ''' <summary>
    ''' Gets or sets the scan list cache.
    ''' </summary>
    ''' <value>
    ''' The scan list cache.
    ''' </value>
    Public Property ScanListCache() As ScpiString
        Get
            Return Me._scanListCache
        End Get
        Set(ByVal value As ScpiString)
            Me._scanListCache = value
        End Set
    End Property

    ''' <summary>Gets or sets the scan list.</summary>
    Public Property ScanList(ByVal access As ResourceAccessLevels) As String
        Get
            MyBase.Controller.Getter(Me.ScanListCache, access)
            Return Me.ScanListCache.Value
        End Get
        Set(ByVal value As String)
            MyBase.Controller.Setter(Me.ScanListCache, value, access)
        End Set
    End Property

#End Region

End Class

#Region " UNUSED "
#If False Then
  ''' <summary>Gets or sets the terminal <see cref="SCPI.RouteTerminalMode">mode</see>.</summary>
  ''' <param name="port">Reference to a <see cref="Scpi.IPort">SCPI Port IO</see></param>
  ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
  Public Shared Property TerminalMode(ByVal controller As Scpi.IDevice) As Scpi.RouteTerminalMode
    Get
      If controller Is Nothing Then
        Throw New ArgumentNullException("port")
      End If
      Dim mode As String = controller.QueryTrimEnd(":ROUT:TERM?")
      Return CType(mode, Scpi.RouteTerminalMode)
    End Get
    Set(ByVal value As Scpi.RouteTerminalMode)
      If controller Is Nothing Then
        Throw New ArgumentNullException("port")
      End If
      MyBase.Controller.Port.WriteLine( 
                    String.Format(Globalization.CultureInfo.CurrentCulture, ":ROUT:TERM {0}", value))
    End Set
  End Property

  ''' <summary>Gets or sets the scan list.</summary>
  ''' <param name="port">Reference to a <see cref="Scpi.IPort">SCPI Port IO</see></param>
  ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
  Public Shared Property ScanList(ByVal controller As Scpi.IDevice) As String
    Get
      If controller Is Nothing Then
        Throw New ArgumentNullException("port")
      End If
      Return controller.QueryTrimEnd(":ROUT:SCAN?")
    End Get
    Set(ByVal value As String)
      If controller Is Nothing Then
        Throw New ArgumentNullException("port")
      End If
      MyBase.Controller.Port.WriteLine(String.Format(Globalization.CultureInfo.CurrentCulture, ":ROUT:SCAN {0}", value))
    End Set
  End Property


#End If
#End Region
