''' <summary>Defines a SCPI Output Subsystem.</summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="01/15/05" by="David" revision="1.0.1841.x">
''' Created
''' </history>
Public Class OutputSubsystem
    Inherits Subsystem

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>
    ''' The SCPI Syntax Header for this subsystem.
    ''' </summary>
    Public Const ScpiSyntaxHeader As String = "OUTP"

    ''' <summary>Constructs this class.</summary>
    ''' <param name="port">Reference to an open <see cref="Scpi.IPort">SCPI Port IO</see>.</param>
    Public Sub New(ByVal port As Scpi.IPort)

        ' instantiate the base class
        MyBase.New(ScpiSyntaxHeader, port)

        Me._frontRouteTerminals = New ResettableValue(Of Boolean)
        MyBase.ResettableValues.Add(Me._frontRouteTerminals)
        Me._isOn = New ResettableValue(Of Boolean)
        MyBase.ResettableValues.Add(Me._isOn)
        Me._offMode = New ResettableValue(Of OutputOffMode)
        MyBase.ResettableValues.Add(Me._offMode)

    End Sub

    ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
    ''' <param name="disposing">True if this method releases both managed and unmanaged 
    '''   resources; False if this method releases only unmanaged resources.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by
    '''   its disposing parameter.  If True, the method has been called directly or 
    '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
    '''   If disposing equals False, the method has been called by the 
    '''   runtime from inside the finalizer and you should not reference 
    '''   other objects--only unmanaged resources can be disposed.</remarks>
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)

        If Not MyBase.IsDisposed Then

            Try

                If disposing Then

                    ' Free managed resources when explicitly called


                End If

                ' Free shared unmanaged resources
                ' onDisposeUnmanagedResources
            Finally

                ' Invoke the base class dispose method
                MyBase.Dispose(disposing)

            End Try

        End If

    End Sub

#End Region

#Region " IRESETTABLE "

    ''' <summary>Clears the queues and resets all registers to zero.
    ''' Sets the subsystem properties to the following CLS default values:<para>
    ''' 
    ''' </para>
    ''' </summary>
    Public Overrides Function ClearExecutionState() As Boolean
        Return MyBase.ClearExecutionState()
    End Function

    ''' <summary>
    ''' Sets subsystem to the following default system preset values:<para>
    ''' </para>
    ''' </summary>
    Public Overrides Function PresetKnownState() As Boolean
        Return MyBase.PresetKnownState()
    End Function

    ''' <summary>Restore member properties to the following RST or System Reset values:<para>
    ''' ON: False
    ''' OffNode = Normal.
    ''' </para>
    ''' </summary>
    ''' <history>
    ''' </history>
    Public Overrides Function ResetKnownState() As Boolean
        Return MyBase.ResetKnownState()
    End Function

#End Region

#Region " PROPERTIES "

    Private _isOn As ResettableValue(Of Boolean)
    Public ReadOnly Property IsOn() As ResettableValue(Of Boolean)
        Get
            Return Me._isOn
        End Get
    End Property

    ''' <summary>Sets or gets the source output state.</summary>
    Public Property IsOn(ByVal access As ResourceAccessLevels) As Boolean
        Get
            MyBase.Controller.Getter(Me._isOn, Syntax.StateCommand, Core.EnumeratedBooleanBase.OneZero, access)
            Return Me._isOn.Value.Value
        End Get
        Set(ByVal value As Boolean)
            MyBase.Controller.Setter(Me._isOn, Syntax.StateCommand, value, Core.EnumeratedBooleanBase.OneZero, access)
        End Set
    End Property

    Private _frontRouteTerminals As ResettableValue(Of Boolean)
    Public ReadOnly Property FrontRouteTerminals() As ResettableValue(Of Boolean)
        Get
            Return Me._frontRouteTerminals
        End Get
    End Property

    ''' <summary>
    ''' Gets or sets the output route terminals state as front or rear.
    ''' </summary>
    ''' <param name="access"></param>
    Public Property FrontRouteTerminals(ByVal access As ResourceAccessLevels) As Boolean
        Get
            MyBase.Controller.Getter(Me._frontRouteTerminals, Syntax.RouteModalityName, Syntax.TerminalsCommand, Syntax.FrontRear, access)
            Return Me._frontRouteTerminals.Value.Value
        End Get
        Set(ByVal Value As Boolean)
            MyBase.Controller.Setter(Me._frontRouteTerminals, Syntax.RouteModalityName, Syntax.TerminalsCommand, Value, Syntax.FrontRear, access)
        End Set
    End Property

    Private _offMode As ResettableValue(Of OutputOffMode)
    Public ReadOnly Property OffMode() As ResettableValue(Of OutputOffMode)
        Get
            Return Me._offMode
        End Get
    End Property

    ''' <summary>
    ''' Gets or sets the output off mode as NORMal, HIMP, ZERO, GUARd
    ''' </summary>
    Public Property OffMode(ByVal access As ResourceAccessLevels) As OutputOffMode
        Get
            Dim value As String = MyBase.Controller.Getter(Syntax.OffModeCommand, Syntax.ExtractScpi(Me.OffMode.Value), access)
            Me.OffMode.Value = Syntax.ParseOutputOffMode(value)
            Me.OffMode.ActualValue = Me.OffMode.Value
            Return Me.OffMode.Value.Value
        End Get
        Set(ByVal value As OutputOffMode)
            Dim setValue As String = MyBase.Controller.Setter(Syntax.OffModeCommand,
                                     Syntax.ExtractScpi(Me._offMode.Value),
                                     Syntax.ExtractScpi(value), access)
            Me._offMode.Value = Syntax.ParseOutputOffMode(setValue)
        End Set
    End Property

#End Region

End Class
