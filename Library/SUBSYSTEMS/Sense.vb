''' <summary>Defines a SCPI Sense Subsystem.</summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="01/15/05" by="David" revision="1.0.1841.x">
''' Created
''' </history>
Public Class SenseSubsystem
    Inherits Subsystem

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>
    ''' The SCPI Syntax Header for this subsystem.
    ''' </summary>
    Public Const ScpiSyntaxHeader As String = "SENS"

    ''' <summary>Constructs this class.</summary>
    ''' <param name="port">Reference to an open <see cref="Scpi.IPort">SCPI Port IO</see>.</param>
    Public Sub New(ByVal port As Scpi.IPort)

        ' instantiate the base class
        MyBase.New(ScpiSyntaxHeader, port)

        ' create a new function collection
        Me._functionCollection = New ResettableKeyableCollection(Of ScpiFunction)

        Me._averageCount = New ResettableValue(Of Integer)
        MyBase.ResettableValues.Add(Me._averageCount)
        Me._averageState = New ResettableValue(Of Boolean)
        MyBase.ResettableValues.Add(Me._averageState)
        Me._deltaAperture = New ResettableDouble
        MyBase.ResettableValues.Add(Me._deltaAperture)
        Me._deltaVoltageAutoRange = New ResettableValue(Of Boolean)
        MyBase.ResettableValues.Add(Me._deltaVoltageAutoRange)
        Me._deltaVoltageRange = New ResettableDouble
        MyBase.ResettableValues.Add(Me._deltaVoltageRange)
        Me._functionMode = New ResettableValue(Of SenseFunctionModes)
        MyBase.ResettableValues.Add(Me._functionMode)
        Me._functionsDisabled = New ResettableValue(Of SenseFunctionModes)
        MyBase.ResettableValues.Add(Me._functionsDisabled)
        Me._functionsEnabled = New ResettableValue(Of SenseFunctionModes)
        MyBase.ResettableValues.Add(Me._functionsEnabled)
        Me._isFunctionConcurrent = New ResettableValue(Of Boolean)
        MyBase.ResettableValues.Add(Me._isFunctionConcurrent)

    End Sub

    ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
    ''' <param name="disposing">True if this method releases both managed and unmanaged 
    '''   resources; False if this method releases only unmanaged resources.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by
    '''   its disposing parameter.  If True, the method has been called directly or 
    '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
    '''   If disposing equals False, the method has been called by the 
    '''   runtime from inside the finalizer and you should not reference 
    '''   other objects--only unmanaged resources can be disposed.</remarks>
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)

        If Not MyBase.IsDisposed Then

            Try

                If disposing Then


                End If

                ' Free shared unmanaged resources
                ' onDisposeUnmanagedResources
            Finally

                ' Invoke the base class dispose method
                MyBase.Dispose(disposing)

            End Try

        End If

    End Sub

#End Region

#Region " IRESETTABLE "

    ''' <summary>Clears the queues and resets all registers to zero.
    ''' Sets the subsystem properties to the following CLS default values:<para>
    ''' 
    ''' </para>
    ''' </summary>
    Public Overrides Function ClearExecutionState() As Boolean
        MyBase.ClearExecutionState()
        Return Me._functionCollection.ClearExecutionState()
    End Function

    ''' <summary>
    ''' Sets subsystem to the following default system preset values:<para>
    ''' </para>
    ''' </summary>
    Public Overrides Function PresetKnownState() As Boolean
        MyBase.PresetKnownState()
        Return Me._functionCollection.PresetKnownState()
    End Function

    ''' <summary>Restore member properties to the following RST or System Reset values:<para>
    ''' </para>
    ''' </summary>
    ''' <history>
    ''' </history>
    Public Overrides Function ResetKnownState() As Boolean
        MyBase.ResetKnownState()
        Return Me._functionCollection.ResetKnownState()
    End Function

#End Region

#Region " PARSERS AND BUILDERS "

    ''' <summary>
    ''' Returns the string function corresponding to the enumerated value.
    ''' Unlike SelectFunctionMode, this method can return a list of functions.
    ''' </summary>
    ''' <param name="value">specifies the function.</param>
    Public Shared Function BuildFunctionModesRecord(ByVal value As Nullable(Of Scpi.SenseFunctionModes)) As String
        If value.HasValue Then
            Return BuildFunctionModesRecord(value.Value)
        Else
            Return BuildFunctionModesRecord(SenseFunctionModes.None)
        End If
    End Function

    ''' <summary>
    ''' Returns the string function corresponding to the enumerated value.
    ''' Unlike SelectFunctionMode, this method can return a list of functions.
    ''' </summary>
    ''' <param name="value">specifies the function.</param>
    Public Shared Function BuildFunctionModesRecord(ByVal value As Scpi.SenseFunctionModes) As String
        If value = SenseFunctionModes.None Then
            Return String.Empty
        Else
            Dim builder As New System.Text.StringBuilder
            For Each element As SenseFunctionModes In [Enum].GetValues(GetType(SenseFunctionModes))
                If (value And element) <> 0 Then
                    Syntax.AddWord(builder, Syntax.ExtractScpi(element))
                End If
            Next
            Return builder.ToString
        End If
    End Function

#Region " LEGACY "

#If False Then
  ''' <summary>
  ''' Returns the enumerated function corresponding to the function name.
  ''' </summary>
  ''' <param name="functionMode">Specifies the function.</param>
      Public Shared Function ParseFunction(ByVal functionMode As String) As Scpi.SenseFunctionModes

    ' get the function mode
    Select Case functionMode
      Case Syntax.None
        Return Scpi.SenseFunctionModes.None
      Case Syntax.Resistance
        Return Scpi.SenseFunctionModes.Resistance
      Case Syntax.Volt
        Return Scpi.SenseFunctionModes.Voltage
      Case Syntax.Curr
        Return Scpi.SenseFunctionModes.Current
      Case Syntax.VoltDC
        Return Scpi.SenseFunctionModes.VoltageDC
      Case Syntax.CurrDC
        Return Scpi.SenseFunctionModes.CurrentDC
      Case Syntax.VoltAC
        Return Scpi.SenseFunctionModes.VoltageAC
      Case Syntax.CurrAC
        Return Scpi.SenseFunctionModes.CurrentAC
      Case Syntax.Fresistance
        Return Scpi.SenseFunctionModes.FourWireResistance
      Case Syntax.Temperature
        Return Scpi.SenseFunctionModes.Temperature
      Case Syntax.Frequency
        Return Scpi.SenseFunctionModes.Frequency
      Case Syntax.Period
        Return Scpi.SenseFunctionModes.Period
      Case Syntax.Continuity
        Return Scpi.SenseFunctionModes.Continuity
      Case Else
        Throw New BaseException("Function '" & functionMode & "' is not known.  Please contract developer")
    End Select
  End Function

  ''' <summary>
  ''' Returns the enumerated function corresponding to the function name.
  ''' </summary>
  ''' <param name="functionMode">Specifies the function.</param>
      Public Shared Function ParseFunctionOn(ByVal functionMode As String) As Scpi.SenseFunctionModes

    Dim value As Scpi.SenseFunctionModes
    ' clear value
    value = Scpi.SenseFunctionModes.None
    ' add voltage functionality
    If functionMode.IndexOf(Syntax.VoltDC, StringComparison.OrdinalIgnoreCase) > 0 Then
      value = value Or Scpi.SenseFunctionModes.VoltageDC
      If functionMode.IndexOf(Syntax.VoltAC, StringComparison.OrdinalIgnoreCase) > 0 Then
        value = value Or Scpi.SenseFunctionModes.VoltageAC
      End If
    ElseIf functionMode.IndexOf(Syntax.VoltAC, StringComparison.OrdinalIgnoreCase) > 0 Then
      value = value Or Scpi.SenseFunctionModes.VoltageAC
    ElseIf functionMode.IndexOf(Syntax.Volt, StringComparison.OrdinalIgnoreCase) > 0 Then
      value = value Or Scpi.SenseFunctionModes.Voltage
    End If
    ' add current functionality
    If functionMode.IndexOf(Syntax.CurrDC, StringComparison.OrdinalIgnoreCase) > 0 Then
      value = value Or Scpi.SenseFunctionModes.CurrentDC
      If functionMode.IndexOf(Syntax.CurrAC, StringComparison.OrdinalIgnoreCase) > 0 Then
        value = value Or Scpi.SenseFunctionModes.CurrentAC
      End If
    ElseIf functionMode.IndexOf(Syntax.CurrAC, StringComparison.OrdinalIgnoreCase) > 0 Then
      value = value Or Scpi.SenseFunctionModes.CurrentAC
    ElseIf functionMode.IndexOf(Syntax.Curr, StringComparison.OrdinalIgnoreCase) > 0 Then
      value = value Or Scpi.SenseFunctionModes.Current
    End If
    ' add resistance functionality
    If functionMode.IndexOf(Syntax.Fresistance, StringComparison.OrdinalIgnoreCase) > 0 Then
      value = value Or Scpi.SenseFunctionModes.FourWireResistance
    ElseIf functionMode.IndexOf(Syntax.Resistance, StringComparison.OrdinalIgnoreCase) > 0 Then
      value = value Or Scpi.SenseFunctionModes.Resistance
    End If
    ' add the rest of the functions
    If functionMode.IndexOf(Syntax.Memory, StringComparison.OrdinalIgnoreCase) > 0 Then
      value = value Or Scpi.SenseFunctionModes.Memory
    End If
    If functionMode.IndexOf(Syntax.Temperature, StringComparison.OrdinalIgnoreCase) > 0 Then
      value = value Or Scpi.SenseFunctionModes.Temperature
    End If
    If functionMode.IndexOf(Syntax.Frequency, StringComparison.OrdinalIgnoreCase) > 0 Then
      value = value Or Scpi.SenseFunctionModes.Frequency
    End If
    If functionMode.IndexOf(Syntax.Period, StringComparison.OrdinalIgnoreCase) > 0 Then
      value = value Or Scpi.SenseFunctionModes.Period
    End If
    If functionMode.IndexOf(Syntax.Continuity, StringComparison.OrdinalIgnoreCase) > 0 Then
      value = value Or Scpi.SenseFunctionModes.Continuity
    End If
    If value = Scpi.SenseFunctionModes.None Then
      Throw New BaseException("Function '" & functionMode & "' is not known.  Please contract developer")
    End If
    ' set the return value
    Return value
  End Function

  ''' <summary>
  ''' Returns the string function corresponding to the enumerated function.
  ''' </summary>
  ''' <param name="value">specifies the function.</param>
      Public Shared Function SelectFunctionMode(ByVal value As Scpi.SenseFunctionModes) As String

    ' get the function mode
    Select Case value
      Case Scpi.SenseFunctionModes.None
        Return String.Empty
      Case Scpi.SenseFunctionModes.Voltage
        Return Syntax.Volt
      Case Scpi.SenseFunctionModes.Current
        Return Syntax.Curr
      Case Scpi.SenseFunctionModes.Memory
        Return Syntax.Memory
      Case Scpi.SenseFunctionModes.Resistance
        Return Syntax.Resistance
      Case Scpi.SenseFunctionModes.VoltageDC
        Return Syntax.VoltDC
      Case Scpi.SenseFunctionModes.CurrentDC
        Return Syntax.CurrDC
      Case Scpi.SenseFunctionModes.VoltageAC
        Return Syntax.VoltAC
      Case Scpi.SenseFunctionModes.CurrentAC
        Return Syntax.CurrAC
      Case Scpi.SenseFunctionModes.FourWireResistance
        Return Syntax.Fresistance
      Case Scpi.SenseFunctionModes.Temperature
        Return Syntax.Temperature
      Case Scpi.SenseFunctionModes.Frequency
        Return Syntax.Frequency
      Case Scpi.SenseFunctionModes.Period
        Return Syntax.Period
      Case Scpi.SenseFunctionModes.Continuity
        Return Syntax.Continuity
      Case Else
        Throw New BaseException("Function '" & value.ToString() & "' is not known.  Please contract developer")
    End Select
    Return String.Empty ' CStr(Nothing)
  End Function

  ''' <summary>
  ''' Returns the string function corresponding to the enumerated value.
  ''' Unlike SelectFunctionMode, this method can return a list of functions.
  ''' </summary>
  ''' <param name="value">specifies the function.</param>
  ''' <param name="isAddQuote">Set true to contain the function strings in quotes.</param>
      Public Shared Function BuildFunctionsRecord(ByVal value As Scpi.SenseFunctionModes, 
                                              ByVal isAddQuote As Boolean) As String
    Dim returnedValue As String
    Dim addedValue As String
    returnedValue = String.Empty
    addedValue = String.Empty
    ' get the function mode
    If (value And Scpi.SenseFunctionModes.Voltage) <> 0 Then
      addedValue = Syntax.Volt
    Else
      addedValue = String.Empty ' CStr(Nothing)
    End If
    returnedValue = buildFunction(returnedValue, addedValue, isAddQuote)
    If (value And Scpi.SenseFunctionModes.Current) <> 0 Then
      addedValue = Syntax.Curr
    Else
      addedValue = String.Empty ' CStr(Nothing)
    End If
    returnedValue = buildFunction(returnedValue, addedValue, isAddQuote)
    If (value And Scpi.SenseFunctionModes.Memory) <> 0 Then
      addedValue = Syntax.Memory
    Else
      addedValue = String.Empty ' CStr(Nothing)
    End If
    returnedValue = buildFunction(returnedValue, addedValue, isAddQuote)
    If (value And Scpi.SenseFunctionModes.Resistance) <> 0 Then
      addedValue = Syntax.Resistance
    Else
      addedValue = String.Empty ' CStr(Nothing)
    End If
    returnedValue = buildFunction(returnedValue, addedValue, isAddQuote)
    If (value And Scpi.SenseFunctionModes.VoltageDC) <> 0 Then
      addedValue = Syntax.VoltDC
    Else
      addedValue = String.Empty ' CStr(Nothing)
    End If
    returnedValue = buildFunction(returnedValue, addedValue, isAddQuote)
    If (value And Scpi.SenseFunctionModes.CurrentDC) <> 0 Then
      addedValue = Syntax.CurrDC
    Else
      addedValue = String.Empty ' CStr(Nothing)
    End If
    returnedValue = buildFunction(returnedValue, addedValue, isAddQuote)
    If (value And Scpi.SenseFunctionModes.VoltageAC) <> 0 Then
      addedValue = Syntax.VoltAC
    Else
      addedValue = String.Empty ' CStr(Nothing)
    End If
    returnedValue = buildFunction(returnedValue, addedValue, isAddQuote)
    If (value And Scpi.SenseFunctionModes.CurrentAC) <> 0 Then
      addedValue = Syntax.CurrAC
    Else
      addedValue = String.Empty ' CStr(Nothing)
    End If
    returnedValue = buildFunction(returnedValue, addedValue, isAddQuote)
    If (value And Scpi.SenseFunctionModes.FourWireResistance) <> 0 Then
      addedValue = Syntax.Fresistance
    Else
      addedValue = String.Empty ' CStr(Nothing)
    End If
    returnedValue = buildFunction(returnedValue, addedValue, isAddQuote)
    If (value And Scpi.SenseFunctionModes.Temperature) <> 0 Then
      addedValue = Syntax.Temperature
    Else
      addedValue = String.Empty ' CStr(Nothing)
    End If
    returnedValue = buildFunction(returnedValue, addedValue, isAddQuote)
    If (value And Scpi.SenseFunctionModes.Frequency) <> 0 Then
      addedValue = Syntax.Frequency
    Else
      addedValue = String.Empty ' CStr(Nothing)
    End If
    returnedValue = buildFunction(returnedValue, addedValue, isAddQuote)
    If (value And Scpi.SenseFunctionModes.Period) <> 0 Then
      addedValue = Syntax.Period
    Else
      addedValue = String.Empty ' CStr(Nothing)
    End If
    returnedValue = buildFunction(returnedValue, addedValue, isAddQuote)
    If (value And Scpi.SenseFunctionModes.Continuity) <> 0 Then
      addedValue = Syntax.Continuity
    Else
      addedValue = String.Empty ' CStr(Nothing)
    End If
    returnedValue = buildFunction(returnedValue, addedValue, isAddQuote)
    Throw New FunctionNotDefinedException("Function " & value.ToString() & " not known. Please contract developer")
    ' set the return value
    Return returnedValue
  End Function

  ''' <summary>
  ''' Appends a function value to the function list.
  ''' </summary>
    ''' <param name="toAdd"></param>
  ''' <param name="isAddQuote"></param>
      Private Shared Function buildFunction(ByVal value As String, ByVal toAdd As String, ByVal isAddQuote As Boolean) As String

    If String.IsNullOrWhiteSpace(toAdd) Then
      Return value
    Else
      If isAddQuote Then
        toAdd = "'" & toAdd & "'"
      End If
      If String.IsNullOrWhiteSpace(value) Then
        Return toAdd
      Else
        Return value & "," & toAdd
      End If
    End If
  End Function
#End If
#End Region

#End Region

#Region " DATA "

    ''' <summary>Returns the latest post-processed reading stored in the sample buffer.</summary>
    Public Function FetchLatestData() As String

        Me._lastFetch = MyBase.Controller.Port.QueryTrimEnd(":SENSE:DATA:LAT?")
        Return Me._lastFetch

    End Function

    ''' <summary>Returns the latest post-processed reading stored in the sample buffer.</summary>
    Public Function FetchData() As String

        Me._lastFetch = MyBase.Controller.Port.QueryTrimEnd(":FETC?")
        Return Me._lastFetch

    End Function

    Private _lastFetch As String
    ''' <summary>Gets or sets the last fetch data string.</summary>
    Public ReadOnly Property LastFetch() As String
        Get
            Return Me._lastFetch
        End Get
    End Property

    ''' <summary>Returns the most recent reading.</summary>
    Public ReadOnly Property LatestData() As String
        Get
            Me._lastFetch = MyBase.Controller.Port.QueryTrimEnd(Syntax.BuildQuery(MyBase.SyntaxHeader, "DATA"))
            ' Me._lastFetch = MyBase.Controller.Port.QueryTrimEnd(":DATA:LAT?") ' this does not work with the 2700.
            Return Me._lastFetch
        End Get
    End Property

    ''' <summary>Take a measurement using the Read query.</summary>
    Public Function Read() As String
        Me._lastFetch = MyBase.Controller.Port.QueryTrimEnd(":READ?")
        Return Me._lastFetch
    End Function

#End Region

#Region " AVERAGE "

    Private _averageCount As ResettableValue(Of Integer)
    Public ReadOnly Property AverageCount() As ResettableValue(Of Integer)
        Get
            Return Me._averageCount
        End Get
    End Property
    ''' <summary>Gets or sets the average count.  This is the total number of 
    ''' elements used in the filter.
    ''' </summary>
    Public Property AverageCount(ByVal access As ResourceAccessLevels) As Integer
        Get
            MyBase.Controller.Getter(Me._averageCount, Syntax.AverageModalityName, Syntax.CountCommand, access)
            Return Me._averageCount.Value.Value
        End Get
        Set(ByVal value As Integer)
            MyBase.Controller.Setter(Me._averageCount, Syntax.AverageModalityName, Syntax.CountCommand, value, access)
        End Set
    End Property

    Private _averageState As ResettableValue(Of Boolean)
    Public ReadOnly Property AverageState() As ResettableValue(Of Boolean)
        Get
            Return Me._averageState
        End Get
    End Property
    ''' <summary>Gets or sets the condition for averaging.</summary>
    Public Property AverageState(ByVal access As ResourceAccessLevels) As Boolean
        Get
            MyBase.Controller.Getter(Me._averageState, Syntax.StateCommand, Syntax.AverageModalityName, Core.EnumeratedBooleanBase.OnOff, access)
            Return Me._averageState.Value.Value
        End Get
        Set(ByVal value As Boolean)
            MyBase.Controller.Setter(Me._averageState, Syntax.AverageModalityName, Syntax.StateCommand, value, Core.EnumeratedBooleanBase.OnOff, access)
        End Set
    End Property

#End Region

#Region " DELTA "

    Private Const serialCommTimeoutInterval As Integer = 200
    Private Const serialCommPollDelay As Integer = 10

    ''' <summary>
    ''' Returns a double or no value.
    ''' </summary>
    Private Shared Function parseDouble(ByVal value As String) As Nullable(Of Double)

        Dim numericValue As Double
        If String.IsNullOrWhiteSpace(value) Then
            Return New Nullable(Of Double)
        ElseIf Double.TryParse(value, Globalization.NumberStyles.Number Or Globalization.NumberStyles.AllowExponent, Globalization.CultureInfo.CurrentCulture, numericValue) Then
            Return numericValue
        Else
            Return New Nullable(Of Double)
        End If

    End Function

    Private _deltaAperture As ResettableDouble
    ''' <summary>
    ''' Gets the delta aperture.
    ''' </summary>
    ''' 
    Public ReadOnly Property DeltaAperture() As ResettableDouble
        Get
            Return Me._deltaAperture
        End Get
    End Property

    ''' <summary>Gets or sets the sense current integration period in NPLC.</summary>
    Public Property DeltaAperture(ByVal access As ResourceAccessLevels) As Double
        Get
            If access.IsDeviceAccess OrElse Not Me._deltaAperture.Value.HasValue Then
                ' send the serial command.
                Dim scpiCommand As String = Syntax.BuildQuery(MyBase.SyntaxHeader, Syntax.VoltageModalityName, Syntax.IntegrationPeriodCommand)
                Dim reply As String = SystemSubsystem.SerialQuery(MyBase.Controller.Port, scpiCommand, serialCommTimeoutInterval)
                If String.IsNullOrWhiteSpace(reply) Then
                    Throw New TimeoutException("Timeout fetching Delta Integration period using the command '" & scpiCommand & "'")
                Else
                    Me._deltaAperture.Value = parseDouble(reply)
                    Me._deltaAperture.ActualValue = Me._deltaAperture.Value
                End If
            End If
            Return Me._deltaAperture.Value.Value
        End Get
        Set(ByVal value As Double)
            If Not access.IsCacheAccess AndAlso
                (access.IsDeviceAccess OrElse (value <> DeltaAperture)) Then
                Dim scpiCommand As String = Syntax.BuildCommand(MyBase.SyntaxHeader, Syntax.VoltageModalityName,
                                                                Syntax.IntegrationPeriodCommand,
                                                                value)
                SystemSubsystem.SerialSend(MyBase.Controller.Port, scpiCommand)
            End If
            Me._deltaAperture.Value = value
        End Set
    End Property

    Private _deltaVoltageRange As ResettableDouble
    Public ReadOnly Property DeltaVoltageRange() As ResettableDouble
        Get
            Return Me._deltaVoltageRange
        End Get
    End Property

    ''' <summary>Gets or sets the delta sense voltage Range.   Set to
    ''' <see cref="SCPI.Syntax.Infinity">infinity</see> to set to maximum or to 
    ''' <see cref="SCPI.Syntax.Infinity">negative infinity</see> for minimum.</summary>
    Public Property DeltaVoltageRange(ByVal access As ResourceAccessLevels) As Double
        Get
            If access.IsDeviceAccess OrElse Not Me._deltaVoltageRange.Value.HasValue Then
                Dim scpiCommand As String = ":SENS:VOLT:RANG?"
                Dim reply As String = SystemSubsystem.SerialQuery(MyBase.Controller.Port, scpiCommand, serialCommTimeoutInterval)
                If String.IsNullOrWhiteSpace(reply) Then
                    Throw New TimeoutException("Timeout fetching Delta Voltage Range using the command '" & scpiCommand & "'")
                Else
                    Me._deltaVoltageRange.Value = parseDouble(reply)
                    Me._deltaVoltageRange.ActualValue = Me._deltaVoltageRange.Value
                End If
            End If
            Return Me._deltaVoltageRange.Value.Value
        End Get
        Set(ByVal value As Double)
            If Not access.IsCacheAccess AndAlso (access.IsDeviceAccess OrElse (value <> DeltaVoltageRange)) Then
                If value >= (Scpi.Syntax.Infinity - 1) Then
                    SystemSubsystem.SerialSend(MyBase.Controller.Port, ":SENS:VOLT:RANG MAX")
                ElseIf value <= (Scpi.Syntax.NegativeInfinity + 1) Then
                    SystemSubsystem.SerialSend(MyBase.Controller.Port, ":SENS:VOLT:RANG MIN")
                Else
                    SystemSubsystem.SerialSend(MyBase.Controller.Port, ":SENS:VOLT:RANG " & value.ToString(Globalization.CultureInfo.CurrentCulture))
                End If
            End If
            Me._deltaVoltageRange.Value = value
        End Set
    End Property

    Private _deltaVoltageAutoRange As ResettableValue(Of Boolean)
    Public ReadOnly Property DeltaVoltageAutoRange() As ResettableValue(Of Boolean)
        Get
            Return Me._deltaVoltageAutoRange
        End Get
    End Property


    ''' <summary>Gets or sets the delta sense voltage Range.   Set to
    ''' <see cref="SCPI.Syntax.Infinity">infinity</see> to set to maximum or to 
    ''' <see cref="SCPI.Syntax.Infinity">negative infinity</see> for minimum.</summary>
    Public Property DeltaVoltageAutoRange(ByVal access As ResourceAccessLevels) As Boolean
        Get
            If access.IsDeviceAccess OrElse Not Me._deltaVoltageAutoRange.Value.HasValue Then
                Dim scpiCommand As String = ":SENS:VOLT:RANG:AUTO?"
                Dim reply As String = SystemSubsystem.SerialQuery(MyBase.Controller.Port, scpiCommand, serialCommTimeoutInterval)
                If String.IsNullOrWhiteSpace(reply) Then
                    Throw New TimeoutException("Timeout fetching Delta Voltage Auto Range using the command '" & scpiCommand & "'")
                Else
                    Me._deltaVoltageAutoRange.Value = reply.Equals("1", StringComparison.OrdinalIgnoreCase)
                    Me._deltaVoltageAutoRange.ActualValue = Me._deltaVoltageAutoRange.Value
                End If
            End If
            Return Me._deltaVoltageAutoRange.Value.Value
        End Get
        Set(ByVal value As Boolean)
            If Not access.IsCacheAccess AndAlso (access.IsDeviceAccess OrElse (value <> DeltaVoltageAutoRange)) Then
                SystemSubsystem.SerialSend(MyBase.Controller.Port, ":SENS:VOLT:RANG:AUTO {0}", Core.EnumeratedBooleanBase.OneZero.Text(value))
            End If
            Me._deltaVoltageAutoRange.Value = value
        End Set
    End Property

#End Region

#Region " FUNCTIONS "

    Private _activeFunction As ScpiFunction
    ''' <summary>
    ''' Gets or sets reference to the selected function.
    ''' </summary>
    Public Property ActiveFunction() As ScpiFunction
        Get
            Return Me._activeFunction
        End Get
        Set(ByVal value As ScpiFunction)
            Me._activeFunction = value
        End Set
    End Property

    ''' <summary>
    ''' Activate the specified function.
    ''' </summary>
    Public Sub ActivateFunction(ByVal value As ScpiFunction, ByVal access As ResourceAccessLevels)
        Me._activeFunction = value
        If Not Me._supportsMultiFunctions Then
            Me.FunctionMode(access) = Me._activeFunction.SenseModality
        End If
    End Sub

    ''' <summary>
    ''' Activate the specified function.
    ''' </summary>
    Public Sub ActivateFunction(ByVal value As SenseFunctionModes, ByVal access As ResourceAccessLevels)
        If value <> SenseFunctionModes.None Then
            For Each scpiFunction As ScpiFunction In Me._functionCollection
                If scpiFunction.SenseModality = value Then
                    Me.ActivateFunction(scpiFunction, access)
                    Return
                End If
            Next
            Throw New FunctionNotDefinedException(value)
        End If
    End Sub

    ''' <summary>
    ''' Activate the specified function.
    ''' </summary>
    Public Sub ActivateFunction(ByVal access As ResourceAccessLevels)
        Me.ActivateFunction(Me.FunctionMode(access), access)
    End Sub

    ''' <summary>
    ''' Adds a sense function to the collection of sense functions.
    ''' Makes the function the 
    ''' <see cref="ActiveFunction">active function.</see>
    ''' </summary>
    ''' <param name="syntaxHeader">Specifies the function header that is used when addressing 
    ''' this instrument function using the SCPI commands.</param>
    ''' <param name="modality">Defines the <see cref="SenseFunctionModes">SCPI function or element</see>.</param>
    Public Function AddFunction(ByVal syntaxHeader As String, ByVal modality As SenseFunctionModes) As ScpiFunction

        Me._activeFunction = Me._functionCollection.Add(New ScpiFunction(syntaxHeader, modality))
        Return Me._activeFunction

    End Function

    Private _functionCollection As ResettableKeyableCollection(Of ScpiFunction)
    ''' <summary>
    ''' Gets reference to the collection of sense functions
    ''' </summary>
    Public ReadOnly Property FunctionCollection() As ResettableKeyableCollection(Of ScpiFunction)
        Get
            Return Me._functionCollection
        End Get
    End Property

    Private _functionMode As ResettableValue(Of SenseFunctionModes)
    Public ReadOnly Property FunctionMode() As ResettableValue(Of SenseFunctionModes)
        Get
            Return Me._functionMode
        End Get
    End Property

    ''' <summary>Gets or sets the <see cref="senseFunctionModes">sense function mode</see>.</summary>
    ''' <remarks>This method is useful with instruments having a single sense function.</remarks>
    Public Property FunctionMode(ByVal access As ResourceAccessLevels) As SenseFunctionModes
        Get
            Me._functionMode.Value = Syntax.ParseSenseFunctionMode(MyBase.Controller.Getter(Syntax.FunctionCommand,
                                           SenseSubsystem.BuildFunctionModesRecord(Me._functionMode.Value), access))
            Me._functionMode.ActualValue = Me._functionMode.Value.Value
            Return Me._functionMode.Value.Value
        End Get
        Set(ByVal value As SenseFunctionModes)
            Me._functionMode.Value = Syntax.ParseSenseFunctionMode(MyBase.Controller.Setter(Syntax.FunctionCommand,
                                             Syntax.ExtractScpi(Me._functionMode.Value),
                                            Syntax.ExtractScpi(value), access))
        End Set
    End Property

    Private _functionsDisabled As ResettableValue(Of SenseFunctionModes)
    Public ReadOnly Property FunctionsDisabled() As ResettableValue(Of SenseFunctionModes)
        Get
            Return Me._functionsDisabled
        End Get
    End Property
    ''' <summary>
    ''' Gets or sets the 'off' functions.
    ''' Unlike <see cref="FunctionMode">function mode</see>  this property is capable of turning 
    ''' on multiple sense functions.
    ''' </summary>
    ''' <param name="access"></param>
    Public Property FunctionsDisabled(ByVal access As ResourceAccessLevels) As SenseFunctionModes
        Get
            Me._functionsDisabled.Value = Syntax.ParseSenseFunctionMode(MyBase.Controller.Getter(Syntax.FunctionCommand,
                                            Syntax.Off,
                                            SenseSubsystem.BuildFunctionModesRecord(Me._functionsDisabled.Value),
                                            access))
            Me._functionsDisabled.ActualValue = Me._functionsDisabled.Value.Value
            Return Me._functionsDisabled.Value.Value
        End Get
        Set(ByVal Value As SenseFunctionModes)
            If Value = SenseFunctionModes.None Then
                Me._functionsDisabled.Value = Value
            Else
                Me._functionsDisabled.Value = Syntax.ParseSenseFunctionMode(MyBase.Controller.Setter(Syntax.FunctionCommand,
                                                Syntax.Off,
                                                SenseSubsystem.BuildFunctionModesRecord(Me._functionsDisabled.Value),
                                                SenseSubsystem.BuildFunctionModesRecord(Value), access))
            End If
        End Set
    End Property

    Private _functionsEnabled As ResettableValue(Of SenseFunctionModes)
    Public ReadOnly Property FunctionsEnabled() As ResettableValue(Of SenseFunctionModes)
        Get
            Return Me._functionsEnabled
        End Get
    End Property
    ''' <summary>
    ''' Gets or sets the 'on' functions.
    ''' Unlike <see cref="FunctionMode">function mode</see>  this property is capable of turning 
    ''' on multiple sense functions.
    ''' </summary>
    ''' <param name="access"></param>
    Public Property FunctionsEnabled(ByVal access As ResourceAccessLevels) As SenseFunctionModes
        Get
            Me._functionsEnabled.Value = Syntax.ParseSenseFunctionMode(MyBase.Controller.Getter(Syntax.FunctionCommand,
                                           SenseSubsystem.BuildFunctionModesRecord(Me._functionsEnabled.Value), access))
            Me._functionsEnabled.ActualValue = Me._functionsEnabled.Value.Value
            Return Me._functionsEnabled.Value.Value
        End Get
        Set(ByVal Value As SenseFunctionModes)
            If Value = SenseFunctionModes.None Then
                Me._functionsEnabled.Value = Value
            Else
                Me._functionsEnabled.Value = Syntax.ParseSenseFunctionMode(MyBase.Controller.Setter(Syntax.FunctionCommand,
                                                 SenseSubsystem.BuildFunctionModesRecord(Me._functionsEnabled.Value),
                                                SenseSubsystem.BuildFunctionModesRecord(Value), access))
            End If
        End Set
    End Property

    Private _isFunctionConcurrent As ResettableValue(Of Boolean)
    Public ReadOnly Property IsFunctionConcurrent() As ResettableValue(Of Boolean)
        Get
            Return Me._isFunctionConcurrent
        End Get
    End Property

    ''' <summary>
    ''' Gets or set the control of concurrent measurements.
    ''' </summary>
    ''' <param name="access"></param>
    Public Property IsFunctionConcurrent(ByVal access As ResourceAccessLevels) As Boolean
        Get
            MyBase.Controller.Getter(Me._isFunctionConcurrent, "FUNC:CONC", Core.EnumeratedBooleanBase.OneZero, access)
            Return Me.IsFunctionConcurrent.Value.Value
        End Get
        Set(ByVal Value As Boolean)
            MyBase.Controller.Setter(Me._isFunctionConcurrent, "FUNC:CONC", Value, Core.EnumeratedBooleanBase.OneZero, access)
        End Set
    End Property

    Private _supportsMultiFunctions As Boolean
    ''' <summary>
    ''' Gets or sets the condition telling if the instrument supports multi-functions.
    ''' For example, the 2400 source-measure instrument support measuring voltage, current, and resistance concurrently
    ''' whereas the 2700 supports a single function at a time.
    ''' </summary>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="Multi")>
    Public Property SupportsMultiFunctions() As Boolean
        Get
            Return Me._supportsMultiFunctions
        End Get
        Set(ByVal value As Boolean)
            Me._supportsMultiFunctions = value
        End Set
    End Property

    ''' <summary>
    ''' Sets integration periods on all functions.
    ''' </summary>
    Public Sub UpdateIntegrationPeriods(ByVal value As Double, ByVal access As ResourceAccessLevels)
        For Each scpiFunction As ScpiFunction In Me._functionCollection
            Me.IntegrationPeriod(scpiFunction, access) = value
        Next
    End Sub

    ''' <summary>
    ''' Sets auto range on all functions.
    ''' </summary>
    Public Sub UpdateAutoRanges(ByVal value As Boolean, ByVal access As ResourceAccessLevels)
        For Each scpiFunction As ScpiFunction In Me._functionCollection
            Me.AutoRange(scpiFunction, access) = value
        Next
    End Sub

#End Region

#Region " ANY FUNCTION "

    ''' <summary>Gets or sets the condition for auto current range.</summary>
    Public Property AutoMode(ByVal scpiFunction As ScpiFunction, ByVal access As ResourceAccessLevels) As Boolean
        Get
            MyBase.Controller.Getter(scpiFunction.AutoMode, scpiFunction.SyntaxHeader, Syntax.AutoModeCommand, Syntax.AutoManual, access)
            Return scpiFunction.AutoMode.Value.Value
        End Get
        Set(ByVal value As Boolean)
            MyBase.Controller.Setter(scpiFunction.AutoMode, scpiFunction.SyntaxHeader, Syntax.AutoRangeCommand, value, Syntax.AutoManual, access)
        End Set
    End Property

    ''' <summary>Gets or sets the condition for auto current range.</summary>
    Public Property AutoRange(ByVal scpiFunction As ScpiFunction, ByVal access As ResourceAccessLevels) As Boolean
        Get
            MyBase.Controller.Getter(scpiFunction.AutoRange, scpiFunction.SyntaxHeader, Syntax.AutoRangeCommand, Core.EnumeratedBooleanBase.OnOff, access)
            Return scpiFunction.AutoRange.Value.Value
        End Get
        Set(ByVal value As Boolean)
            MyBase.Controller.Setter(scpiFunction.AutoRange, scpiFunction.SyntaxHeader, Syntax.AutoRangeCommand, value, Core.EnumeratedBooleanBase.OnOff, access)
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the integration period of the specified function in seconds.
    ''' </summary>
    ''' <param name="scpiFunction"></param>
    ''' <param name="access"></param>
    Public Property IntegrationPeriod(ByVal scpiFunction As ScpiFunction, ByVal access As ResourceAccessLevels) As Double
        Get
            MyBase.Controller.Getter(scpiFunction.PowerLineCycles, scpiFunction.SyntaxHeader, "NPLC", access)
            Return scpiFunction.IntegrationPeriod.Value.Value
        End Get
        Set(ByVal Value As Double)
            MyBase.Controller.Setter(scpiFunction.PowerLineCycles, scpiFunction.SyntaxHeader, "NPLC", Value * scpiFunction.LineFrequency, access)
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the sense protection (compliance) of the active sense function.
    ''' </summary>
    ''' <param name="access"></param>
    ''' <remarks>
    ''' Specifies the sense protection level (compliance) for the active sense function. 
    ''' A current sense compliance specifies the current limit of a voltage source and conversely, 
    ''' the voltage sense compliance specifies the voltage limit on a current source.
    ''' </remarks>
    Public Property ProtectionLevel(ByVal scpiFunction As ScpiFunction, ByVal access As ResourceAccessLevels) As Double
        Get
            MyBase.Controller.Getter(scpiFunction.ProtectionLevel, scpiFunction.SyntaxHeader, "PROT", access)
            Return scpiFunction.ProtectionLevel.Value.Value
        End Get
        Set(ByVal Value As Double)
            MyBase.Controller.Setter(scpiFunction.ProtectionLevel, scpiFunction.SyntaxHeader, "PROT", Value, access)
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the upper range of the active sense function.
    ''' </summary>
    ''' <param name="access"></param>
    Public Property Range(ByVal scpiFunction As ScpiFunction, ByVal access As ResourceAccessLevels) As Double
        Get
            MyBase.Controller.Getter(scpiFunction.Range, scpiFunction.SyntaxHeader, "RANG", access)
            Return scpiFunction.Range.Value.Value
        End Get
        Set(ByVal Value As Double)
            MyBase.Controller.Setter(scpiFunction.Range, scpiFunction.SyntaxHeader, "RANG", Value, access)
        End Set
    End Property


#End Region

#Region " ACTIVE FUNCTION "

    ''' <summary>Gets or sets the condition for auto current range.</summary>
    Public Property AutoRange(ByVal access As ResourceAccessLevels) As Boolean
        Get
            Return Me.AutoRange(Me._activeFunction, access)
        End Get
        Set(ByVal value As Boolean)
            Me.AutoRange(Me._activeFunction, access) = value
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the integration period of the active function in seconds.
    ''' </summary>
    ''' <param name="access"></param>
    Public Property IntegrationPeriod(ByVal access As ResourceAccessLevels) As Double
        Get
            Return Me.IntegrationPeriod(Me._activeFunction, access)
        End Get
        Set(ByVal Value As Double)
            Me.IntegrationPeriod(Me._activeFunction, access) = Value
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the sense protection (compliance) of the active sense function.
    ''' </summary>
    ''' <param name="access"></param>
    ''' <remarks>
    ''' Specifies the sense protection level (compliance) for the active sense function. 
    ''' A current sense compliance specifies the current limit of a voltage source and conversely, 
    ''' the voltage sense compliance specifies the voltage limit on a current source.
    ''' </remarks>
    Public Property ProtectionLevel(ByVal access As ResourceAccessLevels) As Double
        Get
            Return Me.ProtectionLevel(Me._activeFunction, access)
        End Get
        Set(ByVal Value As Double)
            Me.ProtectionLevel(Me._activeFunction, access) = Value
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the upper range of the active sense function.
    ''' </summary>
    ''' <param name="access"></param>
    Public Property Range(ByVal access As ResourceAccessLevels) As Double
        Get
            Return Me.Range(Me._activeFunction, access)
        End Get
        Set(ByVal Value As Double)
            Me.Range(Me._activeFunction, access) = Value
        End Set
    End Property

#End Region

#Region " MEASUREMENTS "

    ''' <summary>
    ''' Configures the sense system for measurement.
    ''' </summary>
    ''' <param name="functions">Specifies the function mode.</param>
    ''' <param name="range">Specifies the measurement range.</param>
    ''' <param name="digits">Specifies the display digits</param>
    ''' <remarks>
    ''' Use this method to configure a measurement using the SCPI single-oriented measurement commands.
    ''' </remarks>
    Public Sub Configure(ByVal functions As SenseFunctionModes, ByVal range As String, ByVal digits As String)

        ' get the function based on the enumerated value
        Dim queryCommand As String = Syntax.ExtractScpi(functions)

        If Not String.IsNullOrWhiteSpace(queryCommand) Then

            ' set the configure command.
            queryCommand = ":CONF:" & queryCommand

            ' if we have a command check if we need to add the range
            If Not String.IsNullOrWhiteSpace(range) Then
                queryCommand = queryCommand & " " & range
            End If

            If Not String.IsNullOrWhiteSpace(digits) Then
                queryCommand = queryCommand & " ," & digits
            End If

            ' configure
            MyBase.Controller.Port.WriteLine(queryCommand)

        End If

    End Sub

#End Region

End Class

#Region " UNUSED "
#If False Then
  Private _isAllOn As ResettableValue(Of Boolean)
  Public ReadOnly Property IsAllOn() As ResettableValue(Of Boolean)
    Get
      Return Me._isAllOn
    End Get
  End Property
  Public Property isAllOn(ByVal access As ResourceAccessLevels) As Boolean
    Get
      Return Me._isAllOn.Value.Value
    End Get
    Set(ByVal value As Boolean)
      If value Then
        If Not access.IsCacheAccess Then
          If access.IsDeviceAccess Then
            Dim queryCommand As String = MyBase.SyntaxHeader & ":FUNC:ON:ALL"
            ' send the display enable command to the device.
            MyBase.Controller.Port.WriteLine(queryCommand)
          End If
        End If
        Me._functionsDisabled.Value = SenseFunctionModes.None
      Else
      End If
      Me._isAllOn.Value = value
      ' empty the function properties to make sure we
      ' do not have a false stored procedure.
    End Set
  End Property

  Me._isFunctionAll = New ScpiValue(Of Boolean)
  Me._isFunctionAll.settercommandformat = MyBase.SyntaxHeader & ":FUNC:{0}:ALL"

  Private _isFunctionAll As ScpiValue(Of Boolean) 
  ''' <summary>
  ''' Enables or disables all functions.
  ''' Set true to turn all sense functions on.
  ''' </summary>
  ''' <param name="access"></param>
      Public Property IsFunctionAll(ByVal access As ResourceAccessLevels) As Boolean
    Get
        Return Me._isFunctionAll.Value
    End Get
    Set(ByVal Value As Boolean)
      If Not access.IsCacheAccess Then
        Dim queryCommand As String
        If Value Then
          queryCommand = MyBase.SyntaxHeader & ":FUNC:ON:ALL"
        Else
          queryCommand = MyBase.SyntaxHeader & ":FUNC:OFF:ALL"
        End If
        If access.IsDeviceAccess Then
          ' send the display enable command to the device.
          MyBase.Controller.Port.WriteLine(queryCommand)
        End If
      End If
      Me._isFunctionAll = Value
      ' empty the function properties to make sure we
      ' do not have a false stored procedure.
      Me._functionRecord = String.Empty
      Me._disabledFunctions = String.Empty
    End Set
  End Property

  ''' <summary>
  ''' This property sets or get the sense function.
  ''' </summary>
  ''' <param name="access"></param>
      Public Property SenseFunctions(ByVal access As ResourceAccessLevels) As SenseFunctionModes
    Get
      Return Syntax.ParseSenseFunctions(Me.FunctionRecord(access))
    End Get
    Set(ByVal Value As SenseFunctionModes)
      ' set the function mode
      Me.FunctionRecord(access) = Syntax.SelectFunctionMode(Value)
    End Set
  End Property


#End If
#End Region