''' <summary>Defines a SCPI Status Subsystem.</summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="01/15/05" by="David" revision="1.0.1841.x">
''' Created
''' </history>
Public Class StatusSubsystem
    Inherits Subsystem

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructs this class.</summary>
    ''' <param name="port">Reference to an open <see cref="Scpi.IPort">SCPI Port IO</see>.</param>
    Public Sub New(ByVal port As Scpi.IPort)

        ' instantiate the base class
        MyBase.New(ScpiSyntaxHeader, port)

        Me._enabledMeasurementEvents = New ResettableValue(Of Integer)
        MyBase.ResettableValues.Add(Me._enabledMeasurementEvents)
        Me._enabledOperationEvents = New ResettableValue(Of Integer)
        MyBase.ResettableValues.Add(Me._enabledOperationEvents)
        Me._enabledOperationNegativeTransitions = New ResettableValue(Of Integer)
        MyBase.ResettableValues.Add(Me._enabledOperationNegativeTransitions)
        Me._enabledOperationPositiveTransitions = New ResettableValue(Of Integer)
        MyBase.ResettableValues.Add(Me._enabledOperationPositiveTransitions)
        Me._enabledQuestionableEvents = New ResettableValue(Of Integer)
        MyBase.ResettableValues.Add(Me._enabledQuestionableEvents)

        Me._enabledServiceRequests = New isr.Scpi.ResettableValue(Of Integer)
        MyBase.ResettableValues.Add(Me._enabledServiceRequests)

        Me._enabledStandardEvents = New ResettableValue(Of Integer)
        MyBase.ResettableValues.Add(Me._enabledStandardEvents)

        Me._measurementEventStatus = New ResettableValue(Of Integer)
        MyBase.ResettableValues.Add(Me._measurementEventStatus)
        Me._operationEventStatus = New ResettableValue(Of Integer)
        MyBase.ResettableValues.Add(Me._operationEventStatus)
        Me._questionableEventStatus = New ResettableValue(Of Integer)
        MyBase.ResettableValues.Add(Me._questionableEventStatus)
        Me._ServiceRequestEventStatusCache = New ResettableValue(Of Integer)
        MyBase.ResettableValues.Add(Me._ServiceRequestEventStatusCache)
        Me._standardEventStatus = New ResettableValue(Of Integer)
        MyBase.ResettableValues.Add(Me._standardEventStatus)

        Me._errorQueue = New ResettableString
        MyBase.ResettableValues.Add(Me._errorQueue)

        Me._errorQueue.ClearValue = ""

        ' COMMANDS: 
        Me._enabledServiceRequests.GetterCommand = Syntax.ServiceRequestEnableQueryCommand
        Me._enabledServiceRequests.SetterCommandFormat = Syntax.ServiceRequestEnableCommand
        Me._ServiceRequestEventStatusCache.GetterCommand = Syntax.ServiceRequestStatusQueryCommand
        Me._standardEventStatus.GetterCommand = Syntax.StandardEventStatusQueryCommand


        ' *CLS
        Me._measurementEventStatus.ClearValue = 0
        Me._operationEventStatus.ClearValue = 0
        Me._questionableEventStatus.ClearValue = 0
        Me._ServiceRequestEventStatusCache.ClearValue = 0
        Me._standardEventStatus.ClearValue = 0

        ' Preset
        Me._enabledMeasurementEvents.PresetValue = 0
        Me._enabledOperationEvents.PresetValue = 0
        Me._enabledOperationNegativeTransitions.PresetValue = 0
        Me._enabledOperationPositiveTransitions.PresetValue = 0
        Me._enabledQuestionableEvents.PresetValue = 0
        Me._enabledServiceRequests.PresetValue = 0
        Me._enabledStandardEvents.PresetValue = 0

        Me._measurementEventCondition = New ScpiValue(Of Integer)
        Me._operationEventCondition = New ScpiValue(Of Integer)
        Me._questionableCondition = New ScpiValue(Of Integer)


    End Sub

    ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
    ''' <param name="disposing">True if this method releases both managed and unmanaged 
    '''   resources; False if this method releases only unmanaged resources.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by
    '''   its disposing parameter.  If True, the method has been called directly or 
    '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
    '''   If disposing equals False, the method has been called by the 
    '''   runtime from inside the finalizer and you should not reference 
    '''   other objects--only unmanaged resources can be disposed.</remarks>
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)

        If Not MyBase.IsDisposed Then

            Try

                If disposing Then

                    ' Free managed resources when explicitly called


                End If

                ' Free shared unmanaged resources
                ' onDisposeUnmanagedResources
            Finally

                ' Invoke the base class dispose method
                MyBase.Dispose(disposing)

            End Try

        End If

    End Sub

#End Region

#Region " SHARED "

    ''' <summary>
    ''' The SCPI Syntax Header for this subsystem.
    ''' </summary>
    Public Const ScpiSyntaxHeader As String = "STAT"

    ''' <summary>Reads the error queue from the instrument.</summary>
    ''' <param name="requestServer">The <see cref="isr.Core.IServicingRequest">request server</see>.</param>
    ''' <remarks>This reflects the real time condition of the instrument.</remarks>
    Public Shared Function FetchErrorQueue(ByVal requestServer As isr.Core.IServicingRequest) As String
        If requestServer Is Nothing Then
            Throw New ArgumentNullException("requestServer")
        End If
        Dim srqEventArgs As New ServiceEventArgs(requestServer)
        Dim messageBuilder As New System.Text.StringBuilder
        Do
            srqEventArgs.ProcessServiceRequestRegister()
            If srqEventArgs.HasError Then
                If messageBuilder.Length > 0 Then
                    messageBuilder.Append(Environment.NewLine)
                End If
                messageBuilder.Append(requestServer.ReadErrorQueue())
            End If
        Loop Until Not srqEventArgs.HasError
        Return messageBuilder.ToString
    End Function

    ''' <summary>Returns the instrument registers to there preset power on state.</summary>
    ''' <param name="port">Reference to a <see cref="Scpi.IPort">SCPI Port IO</see></param>
    ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
    ''' <remarks>When this command is sent, the SCPI event registers are affected as follows:<p>
    '''   1. All bits of the positive transition filter registers are set to one (1).</p><p>
    '''   2. All bits of the negative transition filter registers are cleared to zero (0).</p><p>
    '''   3. All bits of the following registers are cleared to zero (0):</p><p>
    '''     a. Operation Event Enable Register.</p><p>
    '''     b. Questionable Event Enable Register.</p><p>
    '''   4. All bits of the following registers are set to one (1):</p><p>
    '''     a. Trigger Event Enable Register.</p><p>
    '''     b. Arm Event Enable Register.</p><p>
    '''     c. Sequence Event Enable Register.</p><p>
    '''   Note: Registers not included in the above list are not affected by this command.</p></remarks>
    Public Shared Sub Preset(ByVal port As Scpi.IPort)

        If port Is Nothing Then
            Throw New ArgumentNullException("port")
        End If
        port.WriteLine(Syntax.BuildExecute(ScpiSyntaxHeader, Syntax.PresetCommand))
    End Sub

#End Region

#Region " IRESETTABLE "

    ''' <summary>Clears the queues and resets all registers to zero.
    ''' Sets the subsystem properties to the following CLS default values:<para>
    ''' 
    ''' </para>
    ''' </summary>
    Public Overrides Function ClearExecutionState() As Boolean
        Return MyBase.ClearExecutionState()
    End Function

    ''' <summary>
    ''' Sets subsystem to the following default system preset values:<para>
    ''' </para>
    ''' </summary>
    Public Overrides Function PresetKnownState() As Boolean
        StatusSubsystem.Preset(MyBase.Controller.Port)
        Return MyBase.PresetKnownState()
    End Function

    ''' <summary>Restore member properties to the following RST or System Reset values:<para>
    ''' </para>
    ''' </summary>
    ''' <history>
    ''' </history>
    Public Overrides Function ResetKnownState() As Boolean
        Return MyBase.ResetKnownState()
    End Function

#End Region

#Region " ERROR QUEUE "

    ''' <summary>Clears the messages from the error queue.</summary>
    Public Sub ClearErrorQueue()
        MyBase.Controller.Port.WriteLine(Syntax.BuildExecute(ScpiSyntaxHeader, Syntax.ClearCommand))
        Me._errorQueue.Value = ""
        Me._errorQueue.ActualValue = Me._errorQueue.Value
    End Sub

    Private _errorQueue As ResettableString
    ''' <summary>
    ''' Fetches the error queue.
    ''' </summary>
    Public Function FetchErrorQueue() As String
        Me._errorQueue.Value = StatusSubsystem.FetchErrorQueue(MyBase.Controller.Port.RequestServer)
        Me._errorQueue.ActualValue = Me._errorQueue.Value
        Return Me._errorQueue.Value
    End Function

#End Region

#Region " MEASUREMENT EVENTS "

    ''' <summary>Reads the measurement event register condition from the instrument.  This 
    ''' needs to be cast to the event status of the specific instrument type</summary>
    ''' <remarks>This reflects the real time condition of the instrument.</remarks>
    Public Shared ReadOnly Property MeasurementEventCondition(ByVal port As Scpi.IPort) As Nullable(Of Integer)
        Get
            If port Is Nothing Then
                Throw New ArgumentNullException("port")
            End If
            Return port.QueryInteger(Syntax.BuildQuery(ScpiSyntaxHeader, Syntax.Measurement, Syntax.EventConditionCommand))
        End Get
    End Property

    Private _measurementEventCondition As ScpiValue(Of Integer)
    ''' <summary>Reads the measurement event register condition from the instrument.  This 
    '''   needs to be cast to the event status of the specific instrument type</summary>
    ''' <remarks>This reflects the real time condition of the instrument.</remarks>
    Public ReadOnly Property MeasurementEventCondition(ByVal access As ResourceAccessLevels) As Integer
        Get
            MyBase.Controller.Getter(Syntax.Measurement, Syntax.EventConditionCommand, Me._measurementEventCondition.Value, access)
            Return Me._measurementEventCondition.Value.Value
        End Get
    End Property

    ''' <summary>Reads the measurement event status from the instrument.
    '''   This needs to be cast to the event status of the specific instrument type</summary>
    Public Shared ReadOnly Property MeasurementEventStatus(ByVal port As Scpi.IPort) As Nullable(Of Integer)
        Get
            If port Is Nothing Then
                Throw New ArgumentNullException("port")
            End If
            Return port.QueryInteger(Syntax.BuildQuery(ScpiSyntaxHeader, Syntax.Measurement, Syntax.EventStatusCommand))
        End Get
    End Property

    Private _measurementEventStatus As ResettableValue(Of Integer)
    ''' <summary>Reads the measurement event status from the instrument.
    '''   This needs to be cast to the event status of the specific instrument type</summary>
    Public ReadOnly Property MeasurementEventStatus(ByVal access As ResourceAccessLevels) As Integer
        Get
            MyBase.Controller.Getter(Me._measurementEventStatus, Syntax.Measurement, Syntax.EventStatusCommand, access)
            Return Me._measurementEventStatus.Value.Value
        End Get
    End Property

    Private _enabledMeasurementEvents As ResettableValue(Of Integer)
    ''' <summary>Programs or reads back the measurement event request.
    '''   This needs to be cast to the event status of the specific instrument type.</summary>
    ''' <returns>The mask to use for enabling the events.</returns>
    Public Property EnabledMeasurementEvents(ByVal access As ResourceAccessLevels) As Integer
        Get
            MyBase.Controller.Getter(Me._enabledMeasurementEvents, Syntax.Measurement, Syntax.Enable, access)
            Return Me._enabledMeasurementEvents.Value.Value
        End Get
        Set(ByVal value As Integer)
            MyBase.Controller.Setter(Me._enabledMeasurementEvents, Syntax.Measurement, Syntax.Enable, value, access)
            ' MyBase.Controller.Port.WriteLine(String.Format(Globalization.CultureInfo.CurrentCulture, ":STAT:MEAS:ENAB {0:D}", value))
        End Set
    End Property

#End Region

#Region " OPERATION EVENTS "

    ''' <summary>Reads the Operation event register condition from the instrument.  This 
    '''   needs to be cast to the event status of the specific instrument type</summary>
    ''' <remarks>This reflects the real time condition of the instrument.</remarks>
    Public Shared ReadOnly Property OperationEventCondition(ByVal port As Scpi.IPort) As Integer
        Get
            If port Is Nothing Then
                Throw New ArgumentNullException("port")
            End If
            Dim value As Nullable(Of Integer) = port.QueryInteger(Syntax.BuildQuery(ScpiSyntaxHeader, Syntax.Operation, Syntax.EventConditionCommand))
            If value.HasValue Then
                Return value.Value
            Else
                Return -1
            End If
        End Get
    End Property

    Private _operationEventCondition As ScpiValue(Of Integer)
    ''' <summary>Reads the Operation event register condition from the instrument.  This 
    '''   needs to be cast to the event status of the specific instrument type</summary>
    ''' <remarks>This reflects the real time condition of the instrument.</remarks>
    Public ReadOnly Property OperationEventCondition(ByVal access As ResourceAccessLevels) As Integer
        Get
            MyBase.Controller.Getter(Syntax.Operation, Syntax.EventConditionCommand, Me._operationEventCondition.Value, access)
            Return Me._operationEventCondition.Value.Value
        End Get
    End Property

    ''' <summary>Reads the Operation event status from the instrument.
    '''   This needs to be cast to the event status of the specific instrument type</summary>
    Public Shared ReadOnly Property OperationEventStatus(ByVal port As Scpi.IPort) As Integer
        Get
            If port Is Nothing Then
                Throw New ArgumentNullException("port")
            End If
            Dim value As Nullable(Of Integer) = port.QueryInteger(Syntax.BuildQuery(ScpiSyntaxHeader, Syntax.Operation, Syntax.EventStatusCommand))
            If value.HasValue Then
                Return value.Value
            Else
                Return -1
            End If
        End Get
    End Property

    ''' <summary>Programs or reads back the Operation event request.
    '''   This needs to be cast to the event status of the specific instrument type.</summary>
    ''' <returns>The mask to use for enabling the events.</returns>
    Public Shared Property EnabledOperationEvents(ByVal port As Scpi.IPort) As Integer
        Get
            If port Is Nothing Then
                Throw New ArgumentNullException("port")
            End If
            Dim value As Nullable(Of Integer) = port.QueryInteger(Syntax.BuildQuery(ScpiSyntaxHeader, Syntax.Operation, Syntax.EventEnableCommand))
            If value.HasValue Then
                Return value.Value
            Else
                Return -1
            End If
        End Get
        Set(ByVal value As Integer)
            port.WriteLine(Syntax.BuildCommand(ScpiSyntaxHeader, Syntax.Operation, Syntax.EventEnableCommand, value))
            ' MyBase.Controller.Port.WriteLine(String.Format(Globalization.CultureInfo.CurrentCulture, ":STAT:OPER:ENAB {0:D}", value))
        End Set
    End Property

    ''' <summary>Programs or reads back the operation negative transition event request. 
    '''   This needs to be cast to the event status of the specific instrument type</summary>
    ''' <returns>The Operation Transition Events mask to use for enabling the events.</returns>
    ''' <remarks>At this time this reads back the event status rather than the enable status.</remarks>
    Public Shared Property EnabledOperationNegativeTransitions(ByVal port As Scpi.IPort) As Integer
        Get
            If port Is Nothing Then
                Throw New ArgumentNullException("port")
            End If
            Dim value As Nullable(Of Integer) = port.QueryInteger(Syntax.BuildQuery(ScpiSyntaxHeader, Syntax.Operation, Syntax.NegativeTransitionEventEnableCommand))
            If value.HasValue Then
                Return value.Value
            Else
                Return -1
            End If
        End Get
        Set(ByVal value As Integer)
            port.WriteLine(Syntax.BuildCommand(ScpiSyntaxHeader, Syntax.Operation, Syntax.NegativeTransitionEventEnableCommand, value))
        End Set
    End Property

    ''' <summary>Programs or reads back the operation Positive transition event request. 
    '''   This needs to be cast to the event status of the specific instrument type</summary>
    ''' <returns>The Operation Transition Events mask to use for enabling the events.</returns>
    ''' <remarks>At this time this reads back the event status rather than the enable status.</remarks>
    Public Shared Property EnabledOperationPositiveTransitions(ByVal port As Scpi.IPort) As Integer
        Get
            If port Is Nothing Then
                Throw New ArgumentNullException("port")
            End If
            Dim value As Nullable(Of Integer) = port.QueryInteger(Syntax.BuildQuery(ScpiSyntaxHeader, Syntax.Operation, Syntax.PositiveTransitionEventEnableCommand))
            If value.HasValue Then
                Return value.Value
            Else
                Return -1
            End If
        End Get
        Set(ByVal value As Integer)
            port.WriteLine(Syntax.BuildCommand(ScpiSyntaxHeader, Syntax.Operation, Syntax.PositiveTransitionEventEnableCommand, value))
        End Set
    End Property

    Private _operationEventStatus As ResettableValue(Of Integer)
    ''' <summary>Reads the Operation event status from the instrument.
    '''   This needs to be cast to the event status of the specific instrument type</summary>
    Public ReadOnly Property OperationEventStatus(ByVal access As ResourceAccessLevels) As Integer
        Get
            MyBase.Controller.Getter(Me._operationEventStatus, Syntax.Operation, Syntax.EventStatusCommand, access)
            Return Me._operationEventStatus.Value.Value
        End Get
    End Property

    Private _enabledOperationEvents As ResettableValue(Of Integer)
    ''' <summary>Programs or reads back the Operation event request.
    '''   This needs to be cast to the event status of the specific instrument type.</summary>
    ''' <returns>The mask to use for enabling the events.</returns>
    Public Property EnabledOperationEvents(ByVal access As ResourceAccessLevels) As Integer
        Get
            MyBase.Controller.Getter(Me._enabledOperationEvents, Syntax.Operation, Syntax.EventEnableCommand, access)
            Return Me._enabledOperationEvents.Value.Value
        End Get
        Set(ByVal value As Integer)
            MyBase.Controller.Setter(Me._enabledOperationEvents, Syntax.Operation, Syntax.EventEnableCommand, value, access)
            ' MyBase.Controller.Port.WriteLine(String.Format(Globalization.CultureInfo.CurrentCulture, ":STAT:OPER:ENAB {0:D}", value))
        End Set
    End Property

    Private _enabledOperationNegativeTransitions As ResettableValue(Of Integer)
    ''' <summary>Programs or reads back the operation negative transition event request. 
    '''   This needs to be cast to the event status of the specific instrument type</summary>
    ''' <returns>The Operation Transition Events mask to use for enabling the events.</returns>
    ''' <remarks>At this time this reads back the event status rather than the enable status.</remarks>
    Public Property EnabledOperationNegativeTransitions(ByVal access As ResourceAccessLevels) As Integer
        Get
            MyBase.Controller.Getter(Me._enabledOperationNegativeTransitions, Syntax.Operation, Syntax.NegativeTransitionEventEnableCommand, access)
            Return Me._enabledOperationNegativeTransitions.Value.Value
        End Get
        Set(ByVal value As Integer)
            MyBase.Controller.Setter(Me._enabledOperationNegativeTransitions, Syntax.Operation, Syntax.NegativeTransitionEventEnableCommand, value, access)
        End Set
    End Property

    Private _enabledOperationPositiveTransitions As ResettableValue(Of Integer)
    ''' <summary>Programs or reads back the operation Positive transition event request. 
    '''   This needs to be cast to the event status of the specific instrument type</summary>
    ''' <returns>The Operation Transition Events mask to use for enabling the events.</returns>
    ''' <remarks>At this time this reads back the event status rather than the enable status.</remarks>
    Public Property EnabledOperationPositiveTransitions(ByVal access As ResourceAccessLevels) As Integer
        Get
            MyBase.Controller.Getter(Me._enabledOperationPositiveTransitions, Syntax.Operation, Syntax.PositiveTransitionEventEnableCommand, access)
            Return Me._enabledOperationPositiveTransitions.Value.Value
        End Get
        Set(ByVal value As Integer)
            MyBase.Controller.Setter(Me._enabledOperationPositiveTransitions, Syntax.Operation, Syntax.PositiveTransitionEventEnableCommand, value, access)
        End Set
    End Property

#End Region

#Region " QUESTIONABLE EVENTS "

    ''' <summary>Reads the Questionable event register condition from the instrument.  This 
    '''   needs to be cast to the event status of the specific instrument type</summary>
    ''' <remarks>This reflects the real time condition of the instrument.</remarks>
    Public Shared ReadOnly Property QuestionableEventCondition(ByVal port As Scpi.IPort) As Integer
        Get
            If port Is Nothing Then
                Throw New ArgumentNullException("port")
            End If
            Dim value As Nullable(Of Integer) = port.QueryInteger(Syntax.BuildQuery(ScpiSyntaxHeader, Syntax.Questionable, Syntax.EventConditionCommand))
            If value.HasValue Then
                Return value.Value
            Else
                Return -1
            End If
        End Get
    End Property

    ''' <summary>Reads the Questionable event status from the instrument.
    '''   This needs to be cast to the event status of the specific instrument type</summary>
    Public Shared ReadOnly Property QuestionableEventStatus(ByVal port As Scpi.IPort) As Integer
        Get
            If port Is Nothing Then
                Throw New ArgumentNullException("port")
            End If
            Dim value As Nullable(Of Integer) = port.QueryInteger(Syntax.BuildQuery(ScpiSyntaxHeader, Syntax.Questionable, Syntax.EventStatusCommand))
            If value.HasValue Then
                Return value.Value
            Else
                Return -1
            End If
        End Get
    End Property

    ''' <summary>Programs or reads back the Questionable event request.
    '''   This needs to be cast to the event status of the specific instrument type.</summary>
    ''' <returns>The mask to use for enabling the events.</returns>
    Public Shared Property EnabledQuestionableEvents(ByVal port As Scpi.IPort) As Integer
        Get
            If port Is Nothing Then
                Throw New ArgumentNullException("port")
            End If
            Dim value As Nullable(Of Integer) = port.QueryInteger(Syntax.BuildQuery(ScpiSyntaxHeader, Syntax.Questionable, Syntax.EventEnableCommand))
            If value.HasValue Then
                Return value.Value
            Else
                Return -1
            End If
        End Get
        Set(ByVal value As Integer)
            port.WriteLine(Syntax.BuildCommand(ScpiSyntaxHeader, Syntax.Questionable, Syntax.EventEnableCommand, value))
            ' MyBase.Controller.Port.WriteLine(String.Format(Globalization.CultureInfo.CurrentCulture, ":STAT:QUES:ENAB {0:D}", value))
        End Set
    End Property

    Private _questionableCondition As ScpiValue(Of Integer)
    ''' <summary>Reads the Questionable event register condition from the instrument.  This 
    '''   needs to be cast to the event status of the specific instrument type</summary>
    ''' <remarks>This reflects the real time condition of the instrument.</remarks>
    Public ReadOnly Property QuestionableEventCondition(ByVal access As ResourceAccessLevels) As Integer
        Get
            MyBase.Controller.Getter(Me._questionableCondition, Syntax.Questionable, Syntax.EventConditionCommand, access)
            Return Me._questionableCondition.Value.Value
        End Get
    End Property

    Private _questionableEventStatus As ResettableValue(Of Integer)
    ''' <summary>Reads the Questionable event status from the instrument.
    '''   This needs to be cast to the event status of the specific instrument type</summary>
    Public ReadOnly Property QuestionableEventStatus(ByVal access As ResourceAccessLevels) As Integer
        Get
            MyBase.Controller.Getter(Me._questionableEventStatus, Syntax.Questionable, Syntax.EventStatusCommand, access)
            Return Me._questionableEventStatus.Value.Value
        End Get
    End Property

    Private _enabledQuestionableEvents As ResettableValue(Of Integer)
    ''' <summary>Programs or reads back the Questionable event request.
    '''   This needs to be cast to the event status of the specific instrument type.</summary>
    ''' <returns>The mask to use for enabling the events.</returns>
    Public Property EnabledQuestionableEvents(ByVal access As ResourceAccessLevels) As Integer
        Get
            MyBase.Controller.Getter(Me._enabledQuestionableEvents, Syntax.Questionable, Syntax.EventEnableCommand, access)
            Return Me._enabledQuestionableEvents.Value.Value
        End Get
        Set(ByVal value As Integer)
            MyBase.Controller.Setter(Me._enabledQuestionableEvents, Syntax.Questionable, Syntax.EventEnableCommand, value, access)
            ' MyBase.Controller.Port.WriteLine(String.Format(Globalization.CultureInfo.CurrentCulture, ":STAT:QUES:ENAB {0:D}", value))
        End Set
    End Property

#End Region

#Region " SERVICE REQUESTS "

    Private _infiniteTimeoutSeconds As Single = 100
    Public Property InfiniteTimeoutSeconds() As Single
        Get
            Return Me._infiniteTimeoutSeconds
        End Get
        Set(ByVal value As Single)
            Me._infiniteTimeoutSeconds = value
        End Set
    End Property

    ''' <summary>
    ''' Awaits for service request up to the specified timeout.
    ''' </summary>
    ''' <param name="serviceRequestBits">Specifies the status byte which to check.</param>
    Public Sub AwaitServiceRequested(ByVal serviceRequestBits As ServiceRequests)
        AwaitServiceRequested(serviceRequestBits, 2.01, 0)
    End Sub

    ''' <summary>
    ''' Awaits for service request up to the specified timeout.
    ''' </summary>
    ''' <param name="serviceRequestBits">Specifies the status byte which to check.</param>
    ''' <param name="timeoutSeconds">Specifies how many seconds to wait for the service request before exiting this function.
    ''' Use a negative value for a <see cref="InfiniteTimeoutSeconds">large timeout (e.g., 100 seconds)</see> </param>
    ''' <param name="secondsBetweenPolls">Specifies how many seconds to wait between serial polls when looking for the SRQ.</param>
    Public Sub AwaitServiceRequested(ByVal serviceRequestBits As ServiceRequests,
                      ByVal timeoutSeconds As Single, ByVal secondsBetweenPolls As Single)

        Dim countdownTimer As System.Diagnostics.Stopwatch

        ' check if time out is negative
        If timeoutSeconds < 0 Then
            ' if negative, set to 'infinite' value.
            timeoutSeconds = Me._infiniteTimeoutSeconds
        End If

        ' Clear the SRQ flag
        Me._isServiceRequestLatched = False

        ' Set a new instance of the tmer
        countdownTimer = System.Diagnostics.Stopwatch.StartNew()

        ' Loop until SQR or Time out
        Do
            ' await the given time delay
            If secondsBetweenPolls > 0 Then
                Threading.Thread.Sleep(CInt(1000 * secondsBetweenPolls))
                ' countdownTimer.AwaitTimerByTime(timeDelaySeconds)
            Else
                Threading.Thread.Sleep(1)
                ' Application.DoEvents()
            End If
        Loop Until Me.IsLatchedRequestAvailable(serviceRequestBits) OrElse (countdownTimer.Elapsed.TotalSeconds > timeoutSeconds)

        ' Set the time out flag to true if the SRQ flag was
        ' not set
        Me._isTimedOut = Not Me._isServiceRequestLatched

    End Sub

    ''' <summary>
    ''' Checks the SCPI device for an error bit on the service request register.
    ''' Polls the device status and checks if the device reported and error since the last error test.
    ''' </summary>
    Public Function IsErrorAvailable() As Boolean

        Me._lastSerialPoll = MyBase.Controller.Port.SerialPoll
        Return (Me._lastSerialPoll And ServiceRequests.ErrorAvailable) = ServiceRequests.ErrorAvailable

    End Function

    ''' <summary>
    ''' Checks the SCPI device for a message bit on the service request register.
    ''' Polls the device status and checks if the device reported a new message since the last check.
    ''' </summary>
    Public Function IsMessageAvailable() As Boolean

        Me._lastSerialPoll = MyBase.Controller.Port.SerialPoll
        IsMessageAvailable = (Me._lastSerialPoll And ServiceRequests.MessageAvailable) = ServiceRequests.MessageAvailable

    End Function

    Private _isServiceRequestLatched As Boolean
    ''' <summary>
    ''' Gets or sets the status of the service request for cached serial poll requests.
    ''' Clearing this property allows the <see cref="IsLatchedRequestAvailable">request available</see>
    ''' method to poll the device until detecting a service request.  
    ''' Otherwise, the request assume that the device was already
    ''' read and would use last <see cref="LastSerialPoll">last serial poll</see> to determine
    ''' the reading.
    ''' </summary>
    Public Property IsServiceRequestLatched() As Boolean
        Get
            Return Me._isServiceRequestLatched
        End Get
        Set(ByVal Value As Boolean)
            ' Set the service request tag.
            Me._isServiceRequestLatched = Value
        End Set
    End Property

    ''' <summary>
    ''' Checks the SCPI device for the specified bitson the service request register.
    ''' Returns true if any of the requested bits is set on the serial poll.
    ''' </summary>
    ''' <param name="requestBits">Specifies the bits to look for.</param>
    Private Function _isRequestAvailable(ByVal requestBits As ServiceRequests) As Boolean

        ' If not requested check the SRQ bit
        Me._lastSerialPoll = MyBase.Controller.Port.SerialPoll

        ' get the SRQ status
        Return (Me._lastSerialPoll And requestBits) <> 0

    End Function

    ''' <summary>
    ''' Checks the SCPI device for the specified bits on the service request register.
    ''' Sets the <see cref="IsServiceRequestLatched">latched service request tag</see> if
    ''' not already set and if any one of the specified bits is set on the SRQ serial poll bit. 
    ''' Returns the status of the the <see cref="IsServiceRequestLatched">latched service request tag</see>.
    ''' The the <see cref="IsServiceRequestLatched">latched service request tag</see> must be
    ''' cleared (set to False) before inquiring about the service requests again.
    ''' </summary>
    ''' <param name="requestBits">Specifies the bits to look for.</param>
    Public Function IsLatchedRequestAvailable(ByVal requestBits As ServiceRequests) As Boolean

        ' Check if already done
        If Not Me._isServiceRequestLatched Then
            Me._isServiceRequestLatched = Me._isRequestAvailable(requestBits)
        End If
        ' return the value
        Return Me._isServiceRequestLatched

    End Function

    Private _isTimedOut As Boolean
    ''' <summary>
    '''  Get the timeout status of the last operation.
    ''' </summary>
    Public ReadOnly Property IsTimedOut() As Boolean
        Get
            Return Me._isTimedOut
        End Get
    End Property

    Private _lastSerialPoll As ServiceRequests
    ''' <summary>
    ''' Gets the last serial pool byte.
    ''' </summary>
    Public ReadOnly Property LastSerialPoll() As ServiceRequests
        Get
            Return Me._lastSerialPoll
        End Get
    End Property

#End Region

#Region " IEEE488.2 REGISTER COMMANDS: SERVICE REQUESTS "

    ''' <summary>Returns a True is the service request register 
    '''   status byte reports error available.</summary>
    Public Function HasError(ByVal errorBits As Integer) As Boolean
        Return (Me.ReadStatusByte() And errorBits) <> 0
    End Function

    ''' <summary>Returns the service request register status byte.</summary>
    Public Overloads Function ReadStatusByte() As ServiceRequests
        Dim status As Integer = CType(MyBase.Controller.Port.ReadStatusByte(), Integer)
        If status >= 0 Then
            Return CType(status, ServiceRequests)
        Else
            Return CType(256 + status, ServiceRequests)
        End If
    End Function

    Private _enabledServiceRequests As ResettableValue(Of Integer)
    ''' <summary>Programs or reads back the service request event request.</summary>
    ''' <param name="access">Specifies if the property updates the device, uses a stored value, 
    ''' and/or verified by reading back the device value.</param>
    ''' <returns>The <see cref="ServiceRequests">mask</see>
    '''    to use for enabling the events.</returns>
    ''' <remarks>
    ''' This enable register is used along with the Status Byte Register to generate service requests (SRQ). 
    ''' With a bit in the Service Request Enable Register set, an SRQ occurs when the corresponding bit 
    ''' in the Status Byte Register is set by an appropriate event.
    ''' For exampled, to set the ESB (Event Summary Bit) and MAV (Message Available) bits of the 
    ''' Service Request Enable Register, send the following command: *SRE 48 where 48 is bit B5 (32), 
    ''' for ESB, plus bit B4 (16), for MAV). The contents of the Service Request Enable Register can 
    ''' be read using the SRE? quesy command.
    ''' </remarks>
    Public Property EnabledServiceRequests(ByVal access As ResourceAccessLevels) As ServiceRequests
        Get
            MyBase.Controller.Getter(Me._enabledServiceRequests, access)
            Return CType(Me._enabledServiceRequests.Value, ServiceRequests)
        End Get
        Set(ByVal value As ServiceRequests)
            MyBase.Controller.Setter(Me._enabledServiceRequests, value, access)
        End Set
    End Property

    Private _ServiceRequestEventStatusCache As ResettableValue(Of Integer)
    ''' <summary>
    ''' Gets the service request event status cache.
    ''' </summary>
    ''' 
    Public ReadOnly Property ServiceRequestEventStatusCache() As ResettableValue(Of Integer)
        Get
            Return Me._ServiceRequestEventStatusCache
        End Get
    End Property

    ''' <summary>
    ''' Gets the service request event status.
    ''' </summary>
    ''' 
    Public ReadOnly Property ServiceRequestEventStatus() As ServiceRequests
        Get
            Return CType(Me._ServiceRequestEventStatusCache.Value.GetValueOrDefault(ServiceRequests.None), ServiceRequests)
        End Get
    End Property

    ''' <summary>Reads the service request event status from the instrument.</summary>
    Public ReadOnly Property ServiceRequestEventStatus(ByVal access As ResourceAccessLevels) As ServiceRequests
        Get
            MyBase.Controller.Getter(Me._ServiceRequestEventStatusCache, access)
            Return CType(Me._ServiceRequestEventStatusCache.Value, ServiceRequests)
        End Get
    End Property

#End Region

#Region " IEEE488.2 REGISTER COMMANDS: STANDARD EVENTS "

    Private _standardEventStatus As ResettableValue(Of Integer)
    ''' <summary>Reads the standard event status from the instrument.</summary>
    Public ReadOnly Property StandardEventStatus(ByVal access As ResourceAccessLevels) As StandardEvents
        Get
            If access.IsDeviceAccess OrElse Not Me._standardEventStatus.Value.HasValue Then
                Me._standardEventStatus.Value = MyBase.Controller.Port.QueryInteger(Syntax.StandardEventStatusQueryCommand)
            End If
            Return CType(Me._standardEventStatus.Value, StandardEvents)
        End Get
    End Property

    Private _enabledStandardEvents As ResettableValue(Of Integer)
    ''' <summary>
    ''' Gets or sets the value of the Status Event Enable Register.
    ''' </summary>
    ''' <param name="access">Specifies if the property updates the device, uses a stored value, 
    ''' and/or verified by reading back the device value.</param>
    ''' <remarks>
    ''' When a standard event is enabled (unmasked), the occurrence of that event will set the 
    ''' event summary bit ESB in the Status Byte Register.  A set bit (1) in the enable register allows 
    ''' (enables) the ESB bit to set when the corresponding standard event occurs.
    ''' For example, to set the CME and QYE bits of the Standard Event Enable Register, send the 
    ''' command *ESE 63 (32+4).  Thereafter, if a command error (CME) occurs, bit B5 (32) of the 
    ''' Standard Event Status Register sets.  If a query error QYE occurs, bit B2 (4) of the 
    ''' Standard Event Register sets. Because both these events are enabled, the occurrense of any 
    ''' one of them causes the ESB bit in the Status Byte Register to set.
    ''' </remarks>
    Public Property EnabledStandardEvents(ByVal access As ResourceAccessLevels) As StandardEvents
        Get
            MyBase.Controller.Getter(Me._enabledStandardEvents, access)
            Return CType(Me._enabledStandardEvents.Value, StandardEvents)
        End Get
        Set(ByVal Value As StandardEvents)
            Me._enabledStandardEvents.SetterCommand = String.Format(Globalization.CultureInfo.CurrentCulture, Syntax.StandardEventEnableCommand, Value)
            MyBase.Controller.Setter(Me._enabledStandardEvents, Value, access)
        End Set
    End Property

#End Region

#Region " OPERATION COMPLETE "

    ''' <summary>
    '''  Awaits the completion of the last operation.
    ''' </summary>
    Public Sub AwaitOperationComplete()
        AwaitOperationComplete(2.01, 0)
    End Sub

    ''' <summary>
    ''' Awaits the completion of the last operation.
    ''' </summary>
    ''' <param name="timeoutSeconds">Specifies how many seconds to wait for the service request before exiting this function.</param>
    ''' <param name="secondsBetweenPolls">Specifies how many seconds to wait between serial polls when looking for the SRQ.</param>
    ''' <remarks>
    ''' Waits for a completion of a measurement from the Source Meter. The time between polls facilites 
    ''' other operations while polling.  Sets the <see cref="IsServiceRequestLatched">latched request</see>.
    ''' </remarks>
    ''' <history>
    ''' 06/23/03  David Hary  1.0.03  Test for device support fo OPC.
    ''' </history>
    Public Sub AwaitOperationComplete(ByVal timeoutSeconds As Single, ByVal secondsBetweenPolls As Single)

        If MyBase.Controller.Port.SupportsOperationComplete Then
            ' prepare for SRE on OPC
            Me.IsServiceRequestLatched = False
            MyBase.Controller.Port.ClearExecutionState()
            Me.EnabledStandardEvents(ResourceAccessLevels.Device) = StandardEvents.All
            Me.EnabledServiceRequests(ResourceAccessLevels.Device) = ServiceRequests.StandardEvent

            ' Enable Service Request on end of operation.
            Me.EnableOperationCompleteServiceRequest()

            ' await the end of all processes.
            AwaitServiceRequested(ServiceRequests.RequestingService, timeoutSeconds, secondsBetweenPolls)

        End If

    End Sub

    ''' <summary>
    ''' Checks the instrument to determine if operation completed.
    ''' Uses the *OPC? query rather then using a service request.
    ''' </summary>
    ''' <history>
    ''' 06/23/03  David Hary  1.0.03  Test for device support fo OPC.
    ''' </history>
    Public Function IsOperationCompleted() As Boolean

        If MyBase.Controller.Port.SupportsOperationComplete Then
            Return MyBase.Controller.Port.QueryString(Syntax.OperationCompletedQueryCommand) = "1"
        Else
            Return True
        End If

    End Function

    ''' <summary>
    ''' Requests the instrument to set the OPC bit of the status byte register after all operations 
    ''' are complete and issue an SRQ.
    ''' </summary>
    ''' <remarks>
    ''' The standard byte register must be cleared before issueing this command. 
    ''' Also, the standard event register OPC bit must be enabled before issueing this command.
    ''' </remarks>
    Public Sub EnableOperationCompleteServiceRequest()

        ' clear the SRQ boolean
        Me._isServiceRequestLatched = False

        ' update: Check if necessary.
        ' Clear the service request bit
        Me._lastSerialPoll = MyBase.Controller.Port.SerialPoll

        ' Issue service request
        MyBase.Controller.Port.WriteLine(Syntax.OperationCompletedCommand)

    End Sub

#End Region

End Class
