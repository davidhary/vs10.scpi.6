''' <summary>Defines a Scpi Calculate3 Subsystem.</summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="01/15/05" by="David" revision="1.0.1841.x">
''' Created
''' </history>
Public Class Calculate3Subsystem
    Inherits Subsystem

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>
    ''' The SCPI Syntax Header for this subsystem.
    ''' </summary>
    Public Const ScpiSyntaxHeader As String = "CALC3"

    ''' <summary>Constructs this class.</summary>
    ''' <param name="port">Reference to an open <see cref="Scpi.IPort">SCPI Port IO</see>.</param>
    Public Sub New(ByVal port As Scpi.IPort)

        ' instantiate the base class
        MyBase.New(ScpiSyntaxHeader, port)

        Me._forcedDigitalOutputPattern = New ResettableValue(Of Integer)
        MyBase.ResettableValues.Add(Me._forcedDigitalOutputPattern)
        Me._forcedDigitalOutputPatternEnabled = New ResettableValue(Of Boolean)
        MyBase.ResettableValues.Add(Me._forcedDigitalOutputPatternEnabled)

    End Sub

    ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
    ''' <param name="disposing">True if this method releases both managed and unmanaged 
    '''   resources; False if this method releases only unmanaged resources.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by
    '''   its disposing parameter.  If True, the method has been called directly or 
    '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
    '''   If disposing equals False, the method has been called by the 
    '''   runtime from inside the finalizer and you should not reference 
    '''   other objects--only unmanaged resources can be disposed.</remarks>
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)

        If Not MyBase.IsDisposed Then

            Try

                If disposing Then

                    ' Free managed resources when explicitly called

                End If

                ' Free shared unmanaged resources
                ' onDisposeUnmanagedResources
            Finally

                ' Invoke the base class dispose method
                MyBase.Dispose(disposing)

            End Try

        End If

    End Sub

#End Region

#Region " IRESETTABLE "

    ''' <summary>Clears the queues and resets all registers to zero.
    ''' Sets the subsystem properties to the following CLS default values:<para>
    ''' 
    ''' </para>
    ''' </summary>
    Public Overrides Function ClearExecutionState() As Boolean
        Return MyBase.ClearExecutionState()
    End Function

    ''' <summary>
    ''' Sets subsystem to the following default system preset values:<para>
    ''' </para>
    ''' </summary>
    Public Overrides Function PresetKnownState() As Boolean
        Return MyBase.PresetKnownState()
    End Function

    ''' <summary>Restore member properties to the following RST or System Reset values:<para>
    ''' </para>
    ''' </summary>
    ''' <history>
    ''' </history>
    Public Overrides Function ResetKnownState() As Boolean
        Return MyBase.ResetKnownState()
    End Function

#End Region

#Region " DIGITAL I/O "

    Private _forcedDigitalOutputPatternEnabled As ResettableValue(Of Boolean)
    Public ReadOnly Property ForcedDigitalOutputPatternEnabled() As ResettableValue(Of Boolean)
        Get
            Return Me._forcedDigitalOutputPatternEnabled
        End Get
    End Property
    ''' <summary>Gets or sets the condition for auto current range.</summary>
    Public Property ForcedDigitalOutputPatternEnabled(ByVal access As ResourceAccessLevels) As Boolean
        Get
            MyBase.Controller.Getter(Me.ForcedDigitalOutputPatternEnabled, Syntax.Forced, Syntax.Status, Core.EnumeratedBooleanBase.OnOff, access)
            Return Me._forcedDigitalOutputPatternEnabled.Value.Value
        End Get
        Set(ByVal value As Boolean)
            MyBase.Controller.Setter(Me.ForcedDigitalOutputPatternEnabled, Syntax.Forced, Syntax.Status, value, Core.EnumeratedBooleanBase.OnOff, access)
        End Set
    End Property

    Private _forcedDigitalOutputPattern As ResettableValue(Of Integer)
    Public ReadOnly Property ForcedDigitalOutputPattern() As ResettableValue(Of Integer)
        Get
            Return Me._forcedDigitalOutputPattern
        End Get
    End Property

    ''' <summary>Gets or sets the condition for auto current range.</summary>
    Public Property ForcedDigitalOutputPattern(ByVal access As ResourceAccessLevels) As Integer
        Get
            MyBase.Controller.Getter(Me.ForcedDigitalOutputPattern, Syntax.Forced, Syntax.Pattern, access)
            Return Me._forcedDigitalOutputPattern.Value.Value
        End Get
        Set(ByVal value As Integer)
            MyBase.Controller.Setter(Me.ForcedDigitalOutputPattern, Syntax.Forced, Syntax.Pattern, value, access)
        End Set
    End Property

#End Region

End Class


