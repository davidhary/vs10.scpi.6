﻿''' <summary>
''' Defines the contract implemented by SCPI Controller.
''' The SCPI controller uses the <see cref="IPort">Port I/O</see> to control the instrument and
''' read or write SCPI structures to the instrument.
''' The <see cref="IPort">Port I/O</see> could VISA device such as SERIAL, ETHRENET or GPIB.</summary>
''' <license>
''' (c) 2011 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="02/12/11" by="David" revision="6.0.4060.x">
''' Created
''' </history>
Public Interface IController
  Inherits IDisposable

#Region " I PORT "

  ''' <summary>
  ''' Gets reference to the Port I/O system that implements the <see cref="Scpi.IPort">SCPI Port IO</see> interface
  ''' for accessing the instrument.
  ''' </summary>
      ReadOnly Property Port() As IPort

#End Region

#Region " SUBSYSTEM HEADER "

  ''' <summary>
  ''' Gets or sets the subsystem syntax header.
  ''' </summary>
      Property SyntaxHeader() As String

#End Region

#Region " EXECUTE "

  ''' <summary>
  ''' Executes a command based on the <paramref name="command">SCPI command</paramref> syntax.
  ''' </summary>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}' where 
  ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see> and 
  ''' the second item is a <paramref name="command">command</paramref>.
  ''' </param>
    Sub Execute(ByVal subHeader As String)

  ''' <summary>
  ''' Executes a command based on the <paramref name="command">SCPI command</paramref> syntax.
  ''' </summary>
  ''' <param name="modalityName">Specifies the modality name.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2}' where 
  ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see>, 
  ''' the second item is the <paramref name="modalityName">modality name</paramref> and 
  ''' the second item is a <paramref name="command">command</paramref>.
  ''' </param>
    Sub Execute(ByVal modalityName As String, ByVal subHeader As String)


  ''' <summary>
  ''' Executes a command constructered from the specified subsystem elements.
  ''' </summary>
  ''' <param name="modalityName">Name of the modality.</param>
  ''' <param name="subHeader">The sub header.</param>
  ''' <param name="isVerifyOperationComplete">if set to <c>True</c> [is verify operation complete].</param>
  ''' <param name="isRaiseDeviceErrors">if set to <c>True</c> [is raise device errors].</param>
  Sub Execute(ByVal modalityName As String, ByVal subHeader As String, ByVal isVerifyOperationComplete As Boolean, ByVal isRaiseDeviceErrors As Boolean)

#End Region

#Region " STRING "

#Region " QUERRIES "

  ''' <summary>
  ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
  ''' </summary>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}?' where 
  ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see> and 
  ''' the second item is a <paramref name="command">command</paramref>.
  ''' </param>
      Function QueryTrimEnd(ByVal subHeader As String) As String

  ''' <summary>
  ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
  ''' </summary>
  ''' <param name="modalityName">Specifies the modality name.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2}?' where 
  ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see>, 
  ''' the second item is the <paramref name="modalityName">modality name</paramref> and 
  ''' the thrid item is a <paramref name="command">command</paramref>.
  ''' </param>
      Function QueryTrimEnd(ByVal modalityName As String, ByVal subHeader As String) As String

#End Region

#Region " GETTERS w/ ACCESS "

  ''' <summary>
  ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
  ''' </summary>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}?' where 
  ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see> and 
  ''' the second item is a <paramref name="command">command</paramref>
  ''' </param>
  ''' <param name="existingValue">The existing value to use for forcing instrument access if null or empty.</param>
  ''' <param name="access">The access <see cref="isr.Scpi.ResourceAccessLevels">level</see> to the instrument.</param>
      Function Getter(ByVal subHeader As String, 
                  ByVal existingValue As String, 
                  ByVal access As ResourceAccessLevels) As String

  ''' <summary>
  ''' Gets a value from the instrument <see cref="SyntaxHeader">SCPI subsystem</see> <paramref name="modalityName">modality</paramref> and <paramref name="subHeader">element</paramref>.
  ''' </summary>
  ''' <param name="modalityName">Specifies the modality name.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2}?' where 
  ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see>, 
  ''' the second item is the <paramref name="modalityName">modality name</paramref> and 
  ''' the third item is a <paramref name="command">command</paramref>
  ''' </param>
  ''' <param name="existingValue">The existing value to use for forcing instrument access if null or empty.</param>
      Function Getter(ByVal modalityName As String, ByVal subHeader As String, 
                  ByVal existingValue As String, 
                  ByVal access As ResourceAccessLevels) As String

#End Region

#Region " GETTERS w/ ACCESS w/ ELEMENT  "

  ''' <summary>
  ''' Gets a value from the instrument <see cref="SyntaxHeader">SCPI subsystem</see> <paramref name="subHeader">element</paramref>.
  ''' Uses or updates the nullable value getter command.
  ''' </summary>
  ''' <param name="element">Specifies the <see cref="IScpiValueBase">existing element</see> where the getter command is stored.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}?' where 
  ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see> and 
  ''' the second item is a <paramref name="command">command</paramref>
  ''' </param>
  ''' <param name="existingValue">The existing value to use for forcing instrument access if null or empty.</param>
  ''' <param name="access">Specifies the desired access level to the instrument.</param>
      Function GetterString(ByVal element As IScpiValueBase, ByVal subHeader As String, 
                        ByVal existingValue As String, 
                        ByVal access As ResourceAccessLevels) As String

  ''' <summary>
  ''' Gets a value from the instrument <see cref="SyntaxHeader">SCPI subsystem</see> <paramref name="modalityName">modality</paramref> and <paramref name="subHeader">element</paramref>.
  ''' </summary>
  ''' <param name="element">Specifies the <see cref="IScpiValueBase">existing element</see> where the getter command is stored.</param>
  ''' <param name="modalityName">Specifies the modality name.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2}?' where 
  ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see>, 
  ''' the second item is the <paramref name="modalityName">modality name</paramref> and 
  ''' the third item is a <paramref name="command">command</paramref>
  ''' </param>
  ''' <param name="existingValue">The existing value to use for forcing instrument access if null or empty.</param>
      Function GetterString(ByVal element As IScpiValueBase, ByVal modalityName As String, ByVal subHeader As String, 
                        ByVal existingValue As String, 
                        ByVal access As ResourceAccessLevels) As String

#End Region

#Region " GETTERS w/ ACCESS w/ SCPI VALUE "

  ''' <summary>
  ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
  ''' Uses or updates the nullable value getter command.
  ''' </summary>
  ''' <param name="element">Specifies the <see cref="IScpiValueBase">existing element</see> where the getter command is stored.</param>
  ''' <param name="access">The desired <see cref="ResourceAccessLevels">access level</see> to the instrument.</param>
      Function Getter(ByVal element As IScpiString, ByVal access As ResourceAccessLevels) As IScpiString

  ''' <summary>
  ''' Gets the instrument <see cref="SyntaxHeader">SCPI subsystem</see> <paramref name="subHeader">element</paramref> value.
  ''' Applies readings to the <paramref name="element">existing element</paramref>
  ''' </summary>
  ''' <param name="element">Specifies the existing element where the instrument readings are applied.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}?' where 
  ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see> and 
  ''' the second item is a <paramref name="command">command</paramref>
  ''' </param>
  ''' <param name="access">The desired <see cref="ResourceAccessLevels">access level</see> to the instrument.</param>
      Function Getter(ByVal element As IScpiString, ByVal subHeader As String, ByVal access As ResourceAccessLevels) As IScpiString

  ''' <summary>
  ''' Gets the instrument <see cref="SyntaxHeader">SCPI subsystem</see> <paramref name="modalityName">modality</paramref> <paramref name="subHeader">element</paramref> value.
  ''' Applies readings to the <paramref name="element">existing element</paramref>
  ''' </summary>
  ''' <param name="element">Specifies the existing element where the instrument readings are applied.</param>
  ''' <param name="modalityName">Specifies the modality name.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2}?' where 
  ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see>, 
  ''' the second item is the <paramref name="modalityName">modality name</paramref> and 
  ''' the third item is a <paramref name="command">command</paramref>
  ''' </param>
  ''' <param name="access">The desired <see cref="ResourceAccessLevels">access level</see> to the instrument.</param>
      Function Getter(ByVal element As IScpiString, ByVal modalityName As String, ByVal subHeader As String, 
                         ByVal access As ResourceAccessLevels) As IScpiString

#End Region

#Region " SETTERS "

  ''' <summary>
  ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
  ''' </summary>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1} {2}' where 
  ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see>, 
  ''' the second item is a <paramref name="command">command</paramref>
  ''' the third item represents a <paramref name="value">numeric value</paramref>.
  ''' </param>
  ''' <param name="value">The value to set in the instrument.</param>
      Function Setter(ByVal subHeader As String, ByVal value As String, ByVal verify As Boolean) As Boolean

  ''' <summary>
  ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
  ''' </summary>
  ''' <param name="modalityName">Specifies the modality name.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2} {3}' where 
  ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see>, 
  ''' the second item is the <paramref name="modalityName">modality name</paramref>, 
  ''' the third item is a <paramref name="command">command</paramref>
  ''' the fourth item represents a <paramref name="value">numeric value</paramref>.
  ''' </param>
  ''' <param name="value">The value to set in the instrument.</param>
      Function Setter(ByVal modalityName As String, ByVal subHeader As String, ByVal value As String, ByVal verify As Boolean) As Boolean

#End Region

#Region " SETTERS w/ ACCESS "

  ''' <summary>
  ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
  ''' </summary>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1} {2}' where 
  ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see> 
  ''' the second item is a <paramref name="command">command</paramref> and 
  ''' the third item is the <paramref name="value">value</paramref>.
  ''' </param>
  ''' <param name="existingValue">Specifies the existing element value where the instrument.</param>
  ''' <param name="access">The desired <see cref="ResourceAccessLevels">access level</see> to the instrument.</param>
      Function Setter(ByVal subHeader As String, 
                  ByVal existingValue As String, ByVal value As String, 
                  ByVal access As ResourceAccessLevels) As String

  ''' <summary>
  ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
  ''' </summary>
  ''' <param name="modalityName">Specifies the modality name.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2} {3}' where 
  ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see>, 
  ''' the second item is the <paramref name="modalityName">modality name</paramref>, 
  ''' the second item is a <paramref name="command">command</paramref> and
  ''' the fourth item represents a <paramref name="value">numeric value</paramref>.
  ''' </param>
  ''' <param name="existingValue">Specifies the existing element value where the instrument.</param>
  ''' <param name="access">The desired <see cref="ResourceAccessLevels">access level</see> to the instrument.</param>
      Function Setter(ByVal modalityName As String, ByVal subHeader As String, 
                  ByVal existingValue As String, ByVal value As String, 
                  ByVal access As ResourceAccessLevels) As String

#End Region

#Region " SETTERS w/ ACCESS w/ ELEMENT "

  ''' <summary>
  ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
  ''' </summary>
  ''' <param name="element">Specifies the <see cref="IScpiValueBase">existing element</see> where the getter command is stored.</param>
  ''' <param name="existingValue">Specifies the existing element value where the instrument.</param>
  ''' <param name="value">Specifies the value to be set using the element <see cref="IScpiValueBase.SetterCommandFormat">command</see>.</param>
  ''' <param name="access">The desired <see cref="ResourceAccessLevels">access level</see> to the instrument.</param>
      Function Setter(ByVal element As IScpiValueBase, 
                  ByVal existingValue As String, ByVal value As String, 
                  ByVal access As ResourceAccessLevels) As String

  ''' <summary>
  ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
  ''' </summary>
  ''' <param name="element">Specifies the <see cref="IScpiValueBase">existing element</see> where the getter command is stored.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1} {2}' where 
  ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see> 
  ''' the second item is a <paramref name="command">command</paramref> and 
  ''' the third item is the <paramref name="value">value</paramref>.
  ''' </param>
  ''' <param name="existingValue">Specifies the existing element value where the instrument.</param>
  ''' <param name="access">The desired <see cref="ResourceAccessLevels">access level</see> to the instrument.</param>
      Function Setter(ByVal element As IScpiValueBase, ByVal subHeader As String, 
                  ByVal existingValue As String, ByVal value As String, 
                  ByVal access As ResourceAccessLevels) As String

  ''' <summary>
  ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
  ''' </summary>
  ''' <param name="element">Specifies the <see cref="IScpiValueBase">existing element</see> where the getter command is stored.</param>
  ''' <param name="modalityName">Specifies the modality name.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2} {3}' where 
  ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see>, 
  ''' the second item is the <paramref name="modalityName">modality name</paramref>, 
  ''' the second item is a <paramref name="command">command</paramref> and
  ''' the fourth item represents a <paramref name="value">numeric value</paramref>.
  ''' </param>
  ''' <param name="existingValue">Specifies the existing element value where the instrument.</param>
  ''' <param name="access">The desired <see cref="ResourceAccessLevels">access level</see> to the instrument.</param>
      Function Setter(ByVal element As IScpiValueBase, ByVal modalityName As String, ByVal subHeader As String, 
                  ByVal existingValue As String, ByVal value As String, 
                  ByVal access As ResourceAccessLevels) As String

#End Region

#Region " SETTERS w/ ACCESS w/ SCPI VALUE  "

  ''' <summary>
  ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
  ''' Applies readings to the <paramref name="element">existing element</paramref>
  ''' Assume element has both <see cref="IScpiValueBase.GetterCommand">getter</see> and 
  ''' <see cref="IScpiValueBase.SetterCommand">setter</see> commands specified.
  ''' </summary>
  ''' <param name="element">Specifies the element holding the existing value, which is updated.</param>
  ''' <param name="value">The value to set in the instrument.</param>
  ''' <param name="access">The desired <see cref="ResourceAccessLevels">access level</see> to the instrument.</param>
      Function Setter(ByVal element As IScpiString, ByVal value As String, ByVal access As ResourceAccessLevels) As IScpiString


  ''' <summary>
  ''' Sets the instrument <see cref="SyntaxHeader">SCPI subsystem</see> <paramref name="subHeader">element</paramref> value.
  ''' Applies readings to the <paramref name="element">existing element</paramref>
  ''' </summary>
  ''' <param name="element">Specifies the existing element where the instrument readings are applied.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1} {2}' where 
  ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see> 
  ''' the second item is a <paramref name="command">command</paramref> and 
  ''' the third item is the <paramref name="value">value</paramref>.
  ''' </param>
  ''' <param name="value">The value to set in the instrument.</param>
  ''' <param name="access">The desired <see cref="ResourceAccessLevels">access level</see> to the instrument.</param>
    ''' <remarks>
  ''' Use the following code in the calling methd if verification is used:
  ''' <code>
  ''' If Not Me.Elements.IsVerified.GetValueOrDefault(False) Then
  '''   If String.IsNullOrWhiteSpace(element.Value) OrElse String.IsNullOrWhiteSpace(element.ActualValue) Then
  '''     return "Failed reading values from the instrument."
  '''   Else
  '''     return String.Format(Globalization.CultureInfo.CurrentCulture, "Instrument settings applied using the command '{0}' did not verify. Expected={1}. Actual={2}.", Me.Elements.Value, Me.Elements.ActualValue.Value)
  '''   End If
  ''' End If
  ''' </code>
  ''' </remarks>
  Function Setter(ByVal element As IScpiString, 
                         ByVal subHeader As String, ByVal value As String, ByVal access As ResourceAccessLevels) As IScpiString

  ''' <summary>
  ''' Sets the instrument <see cref="SyntaxHeader">SCPI subsystem</see> <paramref name="modalityName">modality</paramref> <paramref name="subHeader">element</paramref> value.
  ''' Applies readings to the <paramref name="element">existing element</paramref>
  ''' </summary>
  ''' <param name="element">Specifies the existing element where the instrument readings are applied.</param>
  ''' <param name="modalityName">Specifies the modality name.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2} {3}' where 
  ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see>, 
  ''' the second item is the <paramref name="modalityName">modality name</paramref>, 
  ''' the second item is a <paramref name="command">command</paramref> and
  ''' the fourth item represents a <paramref name="value">numeric value</paramref>.
  ''' </param>
  ''' <param name="value">The value to set in the instrument.</param>
  ''' <param name="access">The desired <see cref="ResourceAccessLevels">access level</see> to the instrument.</param>
    ''' <remarks>
  ''' Use the following code in the calling methd if verification is used:
  ''' <code>
  ''' If Not Me.Elements.IsVerified.GetValueOrDefault(False) Then
  '''   If String.IsNullOrWhiteSpace(element.Value) OrElse String.IsNullOrWhiteSpace(element.ActualValue) Then
  '''     return "Failed reading values from the instrument."
  '''   Else
  '''     return String.Format(Globalization.CultureInfo.CurrentCulture, "Instrument settings applied using the command '{0}' did not verify. Expected={1}. Actual={2}.", Me.Elements.Value, Me.Elements.ActualValue.Value)
  '''   End If
  ''' End If
  ''' </code>
  ''' </remarks>
  Function Setter(ByVal element As IScpiString, 
                         ByVal modalityName As String, ByVal subHeader As String, ByVal value As String, 
                         ByVal access As ResourceAccessLevels) As IScpiString

#End Region

#End Region

#Region " BOOLEAN "

#Region " QUERRIES "

  ''' <summary>
  ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
  ''' </summary>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}?' where 
  ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see> and 
  ''' the second item is a <paramref name="command">command</paramref>.
  ''' </param>
  ''' <param name="parser">The <see cref="isr.Core.IParser(of boolean)">boolean parser.</see></param>
      Function QueryBoolean(ByVal subHeader As String, ByVal parser As isr.Core.IParser(Of Boolean)) As Nullable(Of Boolean)

  ''' <summary>
  ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
  ''' </summary>
  ''' <param name="modalityName">Specifies the modality name.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2}?' where 
  ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see>, 
  ''' the second item is the <paramref name="modalityName">modality name</paramref> and 
  ''' the thrid item is a <paramref name="command">command</paramref>.
  ''' </param>
  ''' <param name="parser">The <see cref="isr.Core.IParser(of boolean)">boolean parser.</see></param>
      Function QueryBoolean(ByVal modalityName As String, ByVal subHeader As String, ByVal parser As isr.Core.IParser(Of Boolean)) As Nullable(Of Boolean)

#End Region

#Region " GETTERS w/ ACCESS "

  ''' <summary>
  ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
  ''' </summary>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}?' where 
  ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see> and 
  ''' the second item is a <paramref name="command">command</paramref>
  ''' </param>
  ''' <param name="existingValue">The existing value. The instrument is accessed if this value has no value.</param>
  ''' <param name="parser">The <see cref="isr.Core.IParser(of boolean)">boolean parser.</see></param>
  ''' <param name="access">The desired <see cref="ResourceAccessLevels">access level</see> to the instrument.</param>
      Function Getter(ByVal subHeader As String, 
                  ByVal existingValue As Nullable(Of Boolean), ByVal parser As isr.Core.IParser(Of Boolean), 
                  ByVal access As ResourceAccessLevels) As Nullable(Of Boolean)

  ''' <summary>
  ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
  ''' </summary>
  ''' <param name="modalityName">Specifies the modality name.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2}?' where 
  ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see>, 
  ''' the second item is the <paramref name="modalityName">modality name</paramref> and
  ''' the second item is a <paramref name="command">command</paramref>.
  ''' </param>
  ''' <param name="existingValue">The existing value. The instrument is accessed if this value has no value.</param>
  ''' <param name="parser">The <see cref="isr.Core.IParser(of boolean)">boolean parser.</see></param>
  ''' <param name="access">The desired <see cref="ResourceAccessLevels">access level</see> to the instrument.</param>
      Function Getter(ByVal modalityName As String, ByVal subHeader As String, 
                  ByVal existingValue As Nullable(Of Boolean), ByVal parser As isr.Core.IParser(Of Boolean), 
                  ByVal access As ResourceAccessLevels) As Nullable(Of Boolean)

#End Region

#Region " GETTERS w/ ACCESS w/ ELEMENT "

  ''' <summary>
  ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
  ''' Uses or updates the nullable value getter command.
  ''' </summary>
  ''' <param name="element">Specifies the <see cref="IScpiValueBase">existing element</see> where the getter command is stored.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}?' where 
  ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see> and 
  ''' the second item is a <paramref name="command">command</paramref>
  ''' </param>
  ''' <param name="existingValue">The existing value. The instrument is accessed if this value has no value.</param>
  ''' <param name="parser">The <see cref="isr.Core.IParser(of boolean)">boolean parser.</see></param>
  ''' <param name="access">The desired <see cref="ResourceAccessLevels">access level</see> to the instrument.</param>
      Function Getter(ByVal element As IScpiValueBase, ByVal subHeader As String, 
                  ByVal existingValue As Nullable(Of Boolean), ByVal parser As isr.Core.IParser(Of Boolean), 
                  ByVal access As ResourceAccessLevels) As Nullable(Of Boolean)

  ''' <summary>
  ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
  ''' Uses or updates the nullable value getter command.
  ''' </summary>
  ''' <param name="element">Specifies the <see cref="IScpiValueBase">existing element</see> where the getter command is stored.</param>
  ''' <param name="modalityName">Specifies the modality name.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2}?' where 
  ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see>, 
  ''' the second item is the <paramref name="modalityName">modality name</paramref> and
  ''' the second item is a <paramref name="command">command</paramref>.
  ''' </param>
  ''' <param name="existingValue">The existing value. The instrument is accessed if this value has no value.</param>
  ''' <param name="parser">The <see cref="isr.Core.IParser(of boolean)">boolean parser.</see></param>
  ''' <param name="access">The desired <see cref="ResourceAccessLevels">access level</see> to the instrument.</param>
      Function Getter(ByVal element As IScpiValueBase, ByVal modalityName As String, ByVal subHeader As String, 
                  ByVal existingValue As Nullable(Of Boolean), ByVal parser As isr.Core.IParser(Of Boolean), 
                  ByVal access As ResourceAccessLevels) As Nullable(Of Boolean)

#End Region

#Region " GETTERS w/ SCPI VALUE "

  ''' <summary>
  ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
  ''' Uses the nullable value getter command.
  ''' </summary>
  ''' <param name="element">Specifies the <see cref="IScpiValueBase">existing element</see> where the getter command is stored.</param>
      Function Getter(ByVal element As IScpiValue(Of Boolean)) As IScpiValue(Of Boolean)

  ''' <summary>
  ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
  ''' Uses the nullable value getter command.
  ''' </summary>
  ''' <param name="element">Specifies the <see cref="IScpiValueBase">existing element</see> where the getter command is stored.</param>
  ''' <param name="parser">The <see cref="isr.Core.IFormatter(of boolean)">boolean parser.</see></param>
      Function Getter(ByVal element As IScpiValue(Of Boolean), ByVal parser As isr.Core.IParser(Of Boolean)) As IScpiValue(Of Boolean)

#End Region

#Region " GETTERS w/ ACCESS w/ SCPI VALUE "

  ''' <summary>
  ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
  ''' Uses the nullable value getter command.
  ''' </summary>
  ''' <param name="element">Specifies the <see cref="IScpiValueBase">existing element</see> where the getter command is stored.</param>
  ''' <param name="parser">The <see cref="isr.Core.IFormatter(of boolean)">boolean parser.</see></param>
  ''' <param name="access">The desired <see cref="ResourceAccessLevels">access level</see> to the instrument.</param>
      Function Getter(ByVal element As IScpiValue(Of Boolean), 
                  ByVal parser As isr.Core.IParser(Of Boolean), ByVal access As ResourceAccessLevels) As IScpiValue(Of Boolean)

  ''' <summary>
  ''' Gets a value from the instrument <see cref="SyntaxHeader">SCPI subsystem</see> <paramref name="subHeader">element</paramref>.
  ''' Applies readings to the <paramref name="element">existing element</paramref>
  ''' </summary>
  ''' <param name="element">Specifies the existing element where the instrument readings are applied.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}?' where 
  ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see> and 
  ''' the second item is a <paramref name="command">command</paramref>
  ''' </param>
  ''' <param name="parser">The <see cref="isr.Core.IParser(of boolean)">boolean parser.</see></param>
  ''' <param name="access">The desired <see cref="ResourceAccessLevels">access level</see> to the instrument.</param>
      Function Getter(ByVal element As IScpiValue(Of Boolean), 
                  ByVal subHeader As String, 
                  ByVal parser As isr.Core.IParser(Of Boolean), ByVal access As ResourceAccessLevels) As IScpiValue(Of Boolean)

  ''' <summary>
  ''' Gets a value from the instrument <see cref="SyntaxHeader">SCPI subsystem</see> <paramref name="modalityName">modality</paramref> and <paramref name="subHeader">element</paramref>.
  ''' Applies readings to the <paramref name="element">existing element</paramref>
  ''' </summary>
  ''' <param name="element">Specifies the existing element where the instrument readings are applied.</param>
  ''' <param name="modalityName">Specifies the modality name.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2}?' where 
  ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see>, 
  ''' the second item is the <paramref name="modalityName">modality name</paramref> and
  ''' the second item is a <paramref name="command">command</paramref>.
  ''' </param>
  ''' <param name="parser">The <see cref="isr.Core.IParser(of boolean)">boolean parser.</see></param>
  ''' <param name="access">The desired <see cref="ResourceAccessLevels">access level</see> to the instrument.</param>
      Function Getter(ByVal element As IScpiValue(Of Boolean), 
                  ByVal modalityName As String, ByVal subHeader As String, 
                  ByVal parser As isr.Core.IParser(Of Boolean), ByVal access As ResourceAccessLevels) As IScpiValue(Of Boolean)

#End Region

#Region " SETTERS "

  ''' <summary>
  ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
  ''' </summary>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1} {2}' where 
  ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see>, 
  ''' the second item is a <paramref name="command">command</paramref>
  ''' the third item represents a <paramref name="value">numeric value</paramref>.
  ''' </param>
  ''' <param name="value">The value to set in the instrument.</param>
  ''' <param name="formatterParser">The <see cref="isr.Core.IFormatterParser(of boolean)">boolean formatter and parser.</see></param>
      Function Setter(ByVal subHeader As String, ByVal value As Boolean, ByVal formatterParser As isr.Core.IFormatterParser(Of Boolean), ByVal verify As Boolean) As Boolean

  ''' <summary>
  ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
  ''' </summary>
  ''' <param name="modalityName">Specifies the modality name.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2} {3}' where 
  ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see>, 
  ''' the second item is the <paramref name="modalityName">modality name</paramref>, 
  ''' the third item is a <paramref name="command">command</paramref>
  ''' the fourth item represents a <paramref name="value">numeric value</paramref>.
  ''' </param>
  ''' <param name="value">The value to set in the instrument.</param>
  ''' <param name="formatterParser">The <see cref="isr.Core.IFormatterParser(of boolean)">boolean formatter and parser.</see></param>
      Function Setter(ByVal modalityName As String, ByVal subHeader As String, ByVal formatterParser As isr.Core.IFormatterParser(Of Boolean), ByVal value As Boolean, ByVal verify As Boolean) As Boolean

#End Region

#Region " SETTERS w/ ACCESS "

  ''' <summary>
  ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
  ''' </summary>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1} {2}' where 
  ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see> 
  ''' the second item is a <paramref name="command">command</paramref> and 
  ''' the third item is the <paramref name="value">value</paramref>.
  ''' </param>
  ''' <param name="existingValue">The existing value. The instrument is accessed if this value has no value or is not equal to the specified <paramref name="value"/>.</param>
  ''' <param name="formatter">The <see cref="isr.Core.IFormatter(of boolean)">boolean formatter.</see></param>
  ''' <param name="access">The desired <see cref="ResourceAccessLevels">access level</see> to the instrument.</param>
      Function Setter(ByVal subHeader As String, 
                  ByVal existingValue As Nullable(Of Boolean), ByVal value As Boolean, 
                  ByVal formatter As isr.Core.IFormatter(Of Boolean), 
                  ByVal access As ResourceAccessLevels) As Nullable(Of Boolean)

  ''' <summary>
  ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
  ''' </summary>
  ''' <param name="modalityName">Specifies the modality name.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2} {3}' where 
  ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see>, 
  ''' the second item is the <paramref name="modalityName">modality name</paramref>, 
  ''' the second item is a <paramref name="command">command</paramref> and
  ''' the fourth item represents a <paramref name="value">numeric value</paramref>.
  ''' </param>
  ''' <param name="existingValue">The existing value. The instrument is accessed if this value has no value or is not equal to the specified <paramref name="value"/>.</param>
  ''' <param name="value">The value to set in the instrument.</param>
  ''' <param name="formatter">The <see cref="isr.Core.IFormatter(of boolean)">boolean formatter.</see></param>
  ''' <param name="access">The desired <see cref="ResourceAccessLevels">access level</see> to the instrument.</param>
      Function Setter(ByVal modalityName As String, ByVal subHeader As String, 
                  ByVal existingValue As Nullable(Of Boolean), ByVal value As Boolean, 
                  ByVal formatter As isr.Core.IFormatter(Of Boolean), 
                  ByVal access As ResourceAccessLevels) As Nullable(Of Boolean)

#End Region

#Region " SETTERS w/ ELEMENT "

  ''' <summary>
  ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
  ''' </summary>
  ''' <param name="element">Specifies the <see cref="IScpiValueBase">existing element</see> where the getter command is stored.</param>
  ''' <param name="value">Specifies the value to be set using the element <see cref="IScpiValueBase.SetterCommandFormat">command</see>.</param>
  ''' <param name="existingValue">The existing value. The instrument is accessed if this value has no value or is not equal to the specified <paramref name="value"/>.</param>
  ''' <param name="formatter">The <see cref="isr.Core.IFormatter(of boolean)">boolean formatter.</see></param>
      Function Setter(ByVal element As IScpiValueBase, 
                  ByVal existingValue As Boolean, ByVal value As Boolean, 
                  ByVal formatter As isr.Core.IFormatter(Of Boolean)) As Boolean

#End Region

#Region " SETTERS w/ ACCESS w/ ELEMENT "

  ''' <summary>
  ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
  ''' </summary>
  ''' <param name="element">Specifies the <see cref="IScpiValueBase">existing element</see> where the getter command is stored.</param>
  ''' <param name="value">Specifies the value to be set using the element <see cref="IScpiValueBase.SetterCommandFormat">command</see>.</param>
  ''' <param name="existingValue">The existing value. The instrument is accessed if this value has no value or is not equal to the specified <paramref name="value"/>.</param>
  ''' <param name="formatter">The <see cref="isr.Core.IFormatter(of boolean)">boolean formatter.</see></param>
  ''' <param name="access">The desired <see cref="ResourceAccessLevels">access level</see> to the instrument.</param>
      Function Setter(ByVal element As IScpiValueBase, 
                  ByVal existingValue As Boolean, ByVal value As Boolean, 
                  ByVal formatter As isr.Core.IFormatter(Of Boolean), 
                  ByVal access As ResourceAccessLevels) As Boolean

  ''' <summary>
  ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
  ''' Uses or updates the nullable value getter command.
  ''' </summary>
  ''' <param name="element">Specifies the <see cref="IScpiValueBase">existing element</see> where the getter command is stored.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1} {2}' where 
  ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see> 
  ''' the second item is a <paramref name="command">command</paramref> and 
  ''' the third item is the <paramref name="value">value</paramref>.
  ''' </param>
  ''' <param name="value">The value which to set.</param>
  ''' <param name="existingValue">The existing value. The instrument is accessed if this value has no value or is not equal to the specified <paramref name="value"/>.</param>
  ''' <param name="formatter">The <see cref="isr.Core.IFormatter(of boolean)">boolean formatter.</see></param>
  ''' <param name="access">The desired <see cref="ResourceAccessLevels">access level</see> to the instrument.</param>
      Function Setter(ByVal element As IScpiValueBase, ByVal subHeader As String, 
                  ByVal existingValue As Nullable(Of Boolean), ByVal value As Boolean, 
                  ByVal formatter As isr.Core.IFormatter(Of Boolean), 
                  ByVal access As ResourceAccessLevels) As Nullable(Of Boolean)

  ''' <summary>
  ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
  ''' Uses or updates the nullable value getter command.
  ''' </summary>
  ''' <param name="element">Specifies the <see cref="IScpiValueBase">existing element</see> where the getter command is stored.</param>
  ''' <param name="modalityName">Specifies the modality name.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2} {3}' where 
  ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see>, 
  ''' the second item is the <paramref name="modalityName">modality name</paramref>, 
  ''' the second item is a <paramref name="command">command</paramref> and
  ''' the fourth item represents a <paramref name="value">numeric value</paramref>.
  ''' </param>
  ''' <param name="existingValue">The existing value. The instrument is accessed if this value has no value or is not equal to the specified <paramref name="value"/>.</param>
  ''' <param name="value">The value to set in the instrument.</param>
  ''' <param name="formatter">The <see cref="isr.Core.IFormatter(of boolean)">boolean formatter.</see></param>
  ''' <param name="access">The desired <see cref="ResourceAccessLevels">access level</see> to the instrument.</param>
      Function Setter(ByVal element As IScpiValueBase, ByVal modalityName As String, ByVal subHeader As String, 
                         ByVal existingValue As Nullable(Of Boolean), ByVal value As Boolean, 
                         ByVal formatter As isr.Core.IFormatter(Of Boolean), 
                         ByVal access As ResourceAccessLevels) As Nullable(Of Boolean)

#End Region

#Region " SETTERS w/ SCPI VALUE "

  ''' <summary>
  ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
  ''' Applies readings to the <paramref name="element">existing element</paramref>
  ''' Assume element has both <see cref="IScpiValueBase.GetterCommand">getter</see> and 
  ''' <see cref="IScpiValueBase.SetterCommand">setter</see> commands specified.
  ''' </summary>
  ''' <param name="element">Specifies the element holding the existing value, which is updated.</param>
  ''' <param name="value">The value to set in the instrument.</param>
      Function Setter(ByVal element As IScpiValue(Of Boolean), ByVal value As Boolean) As IScpiValue(Of Boolean)

  ''' <summary>
  ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
  ''' Applies readings to the <paramref name="element">existing element</paramref>
  ''' Assume element has both <see cref="IScpiValueBase.GetterCommand">getter</see> and 
  ''' <see cref="IScpiValueBase.SetterCommand">setter</see> commands specified.
  ''' </summary>
  ''' <param name="element">Specifies the element holding the existing value, which is updated.</param>
  ''' <param name="value">The value to set in the instrument.</param>
  ''' <param name="formatterParser">The <see cref="isr.Core.IFormatterParser(of boolean)">boolean formatter and parser.</see></param>
      Function Setter(ByVal element As IScpiValue(Of Boolean), ByVal value As Boolean, 
                  ByVal formatterParser As isr.Core.IFormatterParser(Of Boolean)) As IScpiValue(Of Boolean)

#End Region

#Region " SETTERS w/ ACCESS w/ SCPI VALUE "

  ''' <summary>
  ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
  ''' Applies readings to the <paramref name="element">existing element</paramref>
  ''' Assume element has both <see cref="IScpiValueBase.GetterCommand">getter</see> and 
  ''' <see cref="IScpiValueBase.SetterCommand">setter</see> commands specified.
  ''' </summary>
  ''' <param name="element">Specifies the element holding the existing value, which is updated.</param>
  ''' <param name="value">The value to set in the instrument.</param>
  ''' <param name="formatterParser">The <see cref="isr.Core.IFormatterParser(of boolean)">boolean formatter and parser.</see></param>
  ''' <param name="access">The desired <see cref="ResourceAccessLevels">access level</see> to the instrument.</param>
      Function Setter(ByVal element As IScpiValue(Of Boolean), ByVal value As Boolean, 
                         ByVal formatterParser As isr.Core.IFormatterParser(Of Boolean), ByVal access As ResourceAccessLevels) As IScpiValue(Of Boolean)

  ''' <summary>
  ''' Sets the instrument <see cref="SyntaxHeader">SCPI subsystem</see> <paramref name="subHeader">element</paramref> value.
  ''' Applies readings to the <paramref name="element">existing element</paramref>
  ''' </summary>
  ''' <param name="element">Specifies the existing element where the instrument readings are applied.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1} {2}' where 
  ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see> 
  ''' the second item is a <paramref name="command">command</paramref> and 
  ''' the third item is the <paramref name="value">value</paramref>.
  ''' </param>
  ''' <param name="formatterParser">The <see cref="isr.Core.IFormatterParser(of boolean)">boolean formatter and parser.</see></param>
  ''' <param name="access">The desired <see cref="ResourceAccessLevels">access level</see> to the instrument.</param>
    ''' <remarks>
  ''' Use the following code in the calling methd if verification is used:
  ''' <code>
  ''' If Not element.IsVerified.GetValueOrDefault(False) Then
  '''   If element.Value.HasValue AndAlso element.ActualValue.HasValue Then
  '''     return String.Format(Globalization.CultureInfo.CurrentCulture, 
  '''            "Instrument settings applied using the command '{0}' did not verify. Expected={1}. Actual={2}.", 
  '''            element.Value, element.ActualValue.Value)
  '''   Else
  '''     return "Failed reading values from the instrument."
  '''   End If
  ''' End If
  ''' </code>
  ''' </remarks>
  Function Setter(ByVal element As IScpiValue(Of Boolean), 
                         ByVal subHeader As String, 
                         ByVal value As Boolean, 
                         ByVal formatterParser As isr.Core.IFormatterParser(Of Boolean), ByVal access As ResourceAccessLevels) As IScpiValue(Of Boolean)

  ''' <summary>
  ''' Sets the instrument <see cref="SyntaxHeader">SCPI subsystem</see> <paramref name="modalityName">modality</paramref> <paramref name="subHeader">element</paramref> value.
  ''' </summary>
  ''' <param name="element">Specifies the existing element where the instrument readings are applied.</param>
  ''' <param name="modalityName">Specifies the modality name.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2} {3}' where 
  ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see>, 
  ''' the second item is the <paramref name="modalityName">modality name</paramref>, 
  ''' the second item is a <paramref name="command">command</paramref> and
  ''' the fourth item represents a <paramref name="value">numeric value</paramref>.
  ''' </param>
  ''' <param name="formatterParser">The <see cref="isr.Core.IFormatterParser(of boolean)">boolean formatter and parser.</see></param>
  ''' <param name="access">The desired <see cref="ResourceAccessLevels">access level</see> to the instrument.</param>
    ''' <remarks>
  ''' Use the following code in the calling methd if verification is used:
  ''' <code>
  ''' If Not element.IsVerified.GetValueOrDefault(False) Then
  '''   If element.Value.HasValue AndAlso element.ActualValue.HasValue Then
  '''     return String.Format(Globalization.CultureInfo.CurrentCulture, 
  '''            "Instrument settings applied using the command '{0}' did not verify. Expected={1}. Actual={2}.", 
  '''            element.Value, element.ActualValue.Value)
  '''   Else
  '''     return "Failed reading values from the instrument."
  '''   End If
  ''' End If
  ''' </code>
  ''' </remarks>
  Function Setter(ByVal element As IScpiValue(Of Boolean), 
                         ByVal modalityName As String, ByVal subHeader As String, 
                         ByVal value As Boolean, 
                         ByVal formatterParser As isr.Core.IFormatterParser(Of Boolean), ByVal access As ResourceAccessLevels) As IScpiValue(Of Boolean)

#End Region

#End Region

#Region " DOUBLE "

#Region " QUERRIES: DOUBLE "

  ''' <summary>
  ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
  ''' </summary>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}?' where 
  ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see> and 
  ''' the second item is a <paramref name="command">command</paramref>.
  ''' </param>
      Function QueryDouble(ByVal subHeader As String) As Nullable(Of Double)

  ''' <summary>
  ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
  ''' </summary>
  ''' <param name="modalityName">Specifies the modality name.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2}?' where 
  ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see>, 
  ''' the second item is the <paramref name="modalityName">modality name</paramref> and 
  ''' the thrid item is a <paramref name="command">command</paramref>.
  ''' </param>
      Function QueryDouble(ByVal modalityName As String, ByVal subHeader As String) As Nullable(Of Double)

#End Region

#Region " GETTERS w/ ACCESS "

  ''' <summary>
  ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
  ''' </summary>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}?' where 
  ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see> and 
  ''' the second item is a <paramref name="command">command</paramref>
  ''' </param>
  ''' <param name="existingValue">Specifies the existing value.</param>
      Function Getter(ByVal subHeader As String, 
                  ByVal existingValue As Nullable(Of Double), 
                  ByVal access As ResourceAccessLevels) As Nullable(Of Double)

  ''' <summary>
  ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
  ''' </summary>
  ''' <param name="modalityName">Specifies the modality name.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2}?' where 
  ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see>, 
  ''' the second item is the <paramref name="modalityName">modality name</paramref> and
  ''' the second item is a <paramref name="command">command</paramref>.
  ''' </param>
  ''' <param name="existingValue">Specifies the existing value.</param>
      Function Getter(ByVal modalityName As String, ByVal subHeader As String, 
                  ByVal existingValue As Nullable(Of Double), 
                  ByVal access As ResourceAccessLevels) As Nullable(Of Double)
#End Region

#Region " GETTERS w/ ACCESS w/ ELEMENT "

  ''' <summary>
  ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
  ''' Uses or updates the nullable value getter command.
  ''' </summary>
  ''' <param name="element">Specifies the <see cref="IScpiValueBase">existing element</see> where the getter command is stored.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}?' where 
  ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see> and 
  ''' the second item is a <paramref name="command">command</paramref>
  ''' </param>
  ''' <param name="existingValue">Specifies the existing value.</param>
      Function Getter(ByVal element As IScpiValueBase, ByVal subHeader As String, 
                  ByVal existingValue As Nullable(Of Double), 
                  ByVal access As ResourceAccessLevels) As Nullable(Of Double)

  ''' <summary>
  ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
  ''' Uses or updates the nullable value getter command.
  ''' </summary>
  ''' <param name="element">Specifies the <see cref="IScpiValueBase">existing element</see> where the getter command is stored.</param>
  ''' <param name="modalityName">Specifies the modality name.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2}?' where 
  ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see>, 
  ''' the second item is the <paramref name="modalityName">modality name</paramref> and
  ''' the second item is a <paramref name="command">command</paramref>.
  ''' </param>
  ''' <param name="existingValue">Specifies the existing value.</param>
      Function Getter(ByVal element As IScpiValueBase, ByVal modalityName As String, ByVal subHeader As String, 
                  ByVal existingValue As Nullable(Of Double), 
                  ByVal access As ResourceAccessLevels) As Nullable(Of Double)

#End Region

#Region " GETTERS w/ ACCESS w/ SCPI VALUE "

  ''' <summary>
  ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
  ''' Uses or updates the nullable value getter command.
  ''' </summary>
  ''' <param name="element">Specifies the <see cref="IScpiValueBase">existing element</see> where the getter command is stored.</param>
      Function Getter(ByVal element As IScpiValue(Of Double), ByVal access As ResourceAccessLevels) As IScpiValue(Of Double)

  ''' <summary>
  ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
  ''' Applies readings to the <paramref name="element">existing element</paramref>
  ''' </summary>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}?' where 
  ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see> and 
  ''' the second item is a <paramref name="command">command</paramref>
  ''' </param>
  ''' <param name="element">Specifies the element holding the existing value, which is updated.</param>
      Function Getter(ByVal element As IScpiValue(Of Double), 
                         ByVal subHeader As String, 
                         ByVal access As ResourceAccessLevels) As IScpiValue(Of Double)
  ''' <summary>
  ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
  ''' Applies readings to the <paramref name="element">existing element</paramref>
  ''' </summary>
  ''' <param name="modalityName">Specifies the modality name.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2}?' where 
  ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see>, 
  ''' the second item is the <paramref name="modalityName">modality name</paramref> and
  ''' the second item is a <paramref name="command">command</paramref>.
  ''' </param>
  ''' <param name="element">Specifies the element holding the existing value, which is updated.</param>
      Function Getter(ByVal element As IScpiValue(Of Double), 
                         ByVal modalityName As String, ByVal subHeader As String, 
                         ByVal access As ResourceAccessLevels) As IScpiValue(Of Double)

#End Region

#Region " SETTERS "

  ''' <summary>
  ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
  ''' </summary>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1} {2}' where 
  ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see>, 
  ''' the second item is a <paramref name="command">command</paramref>
  ''' the third item represents a <paramref name="value">numeric value</paramref>.
  ''' </param>
  ''' <param name="value">The value to set in the instrument.</param>
      Function Setter(ByVal subHeader As String, ByVal value As Double, ByVal verify As Boolean) As Boolean

  ''' <summary>
  ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
  ''' </summary>
  ''' <param name="modalityName">Specifies the modality name.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2} {3}' where 
  ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see>, 
  ''' the second item is the <paramref name="modalityName">modality name</paramref>, 
  ''' the third item is a <paramref name="command">command</paramref>
  ''' the fourth item represents a <paramref name=" value">numeric value</paramref>.
  ''' </param>
  ''' <param name="value">The value to set in the instrument.</param>
      Function Setter(ByVal modalityName As String, ByVal subHeader As String, ByVal value As Double, ByVal verify As Boolean) As Boolean

#End Region

#Region " SETTERS w/ ACCESS "

  ''' <summary>
  ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
  ''' </summary>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1} {2}' where 
  ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see> 
  ''' the second item is a <paramref name="command">command</paramref> and 
  ''' the third item is the <paramref name="value">value</paramref>.
  ''' </param>
  ''' <param name="existingValue">Specifies the existing value.</param>
      Function Setter(ByVal subHeader As String, 
                  ByVal existingValue As Nullable(Of Double), ByVal value As Double, 
                  ByVal access As ResourceAccessLevels) As Nullable(Of Double)

  ''' <summary>
  ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
  ''' </summary>
  ''' <param name="modalityName">Specifies the modality name.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2} {3}' where 
  ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see>, 
  ''' the second item is the <paramref name="modalityName">modality name</paramref>, 
  ''' the second item is a <paramref name="command">command</paramref> and
  ''' the fourth item represents a <paramref name="value">numeric value</paramref>.
  ''' </param>
  ''' <param name="existingValue">Specifies the existing value.</param>
      Function Setter(ByVal modalityName As String, ByVal subHeader As String, 
                  ByVal existingValue As Nullable(Of Double), ByVal value As Double, 
                  ByVal access As ResourceAccessLevels) As Nullable(Of Double)

#End Region

#Region " SETTERS w/ ACCESS w/ ELEMENT "

  ''' <summary>
  ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
  ''' </summary>
  ''' <param name="element">Specifies the <see cref="IScpiValueBase">existing element</see> where the getter command is stored.</param>
  ''' <param name="existingValue">Specifies the existing element value where the instrument.</param>
  ''' <param name="value">The value to be set using the element <see cref="IScpiValueBase.SetterCommandFormat">command</see>.</param>
  ''' <param name="access">The desired <see cref="ResourceAccessLevels">access level</see> to the instrument.</param>
      Function Setter(ByVal element As IScpiValueBase, 
                  ByVal existingValue As Double, ByVal value As Double, 
                  ByVal access As ResourceAccessLevels) As Double

  ''' <summary>
  ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
  ''' Uses or updates the nullable value getter command.
  ''' </summary>
  ''' <param name="element">Specifies the <see cref="IScpiValueBase">existing element</see> where the getter command is stored.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1} {2}' where 
  ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see> 
  ''' the second item is a <paramref name="command">command</paramref> and 
  ''' the third item is the <paramref name="value">value</paramref>.
  ''' </param>
  ''' <param name="existingValue">Specifies the existing value.</param>
  ''' <param name="value">The value to set in the instrument.</param>
  ''' <param name="access">The desired <see cref="ResourceAccessLevels">access level</see> to the instrument.</param>
      Function Setter(ByVal element As IScpiValueBase, ByVal subHeader As String, 
                  ByVal existingValue As Nullable(Of Double), ByVal value As Double, 
                  ByVal access As ResourceAccessLevels) As Nullable(Of Double)

  ''' <summary>
  ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
  ''' Uses or updates the nullable value getter command.
  ''' </summary>
  ''' <param name="element">Specifies the <see cref="IScpiValueBase">existing element</see> where the getter command is stored.</param>
  ''' <param name="modalityName">Specifies the modality name.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2} {3}' where 
  ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see>, 
  ''' the second item is the <paramref name="modalityName">modality name</paramref>, 
  ''' the second item is a <paramref name="command">command</paramref> and
  ''' the fourth item represents a <paramref name="value">numeric value</paramref>.
  ''' </param>
  ''' <param name="existingValue">Specifies the existing value.</param>
  ''' <param name="value">The value to set in the instrument.</param>
  ''' <param name="access">The desired <see cref="ResourceAccessLevels">access level</see> to the instrument.</param>
      Function Setter(ByVal element As IScpiValueBase, ByVal modalityName As String, ByVal subHeader As String, 
                  ByVal existingValue As Nullable(Of Double), ByVal value As Double, 
                  ByVal access As ResourceAccessLevels) As Nullable(Of Double)
#End Region

#Region " SETTERS w/ ACCESS w/ SCPI VALUE "

  ''' <summary>
  ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
  ''' Applies readings to the <paramref name="element">existing element</paramref>
  ''' Assume element has both <see cref="IScpiValueBase.GetterCommand">getter</see> and 
  ''' <see cref="IScpiValueBase.SetterCommand">setter</see> commands specified.
  ''' </summary>
  ''' <param name="element">Specifies the element holding the existing value, which is updated.</param>
  ''' <param name="value">The value to be set using the element <see cref="IScpiValueBase.SetterCommandFormat">command</see>.</param>
  ''' <param name="access">The desired <see cref="ResourceAccessLevels">access level</see> to the instrument.</param>
      Function Setter(ByVal element As IScpiValue(Of Double), ByVal value As Double, ByVal access As ResourceAccessLevels) As IScpiValue(Of Double)

  ''' <summary>
  ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
  ''' Applies readings to the <paramref name="element">existing element</paramref>
  ''' </summary>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1} {2}' where 
  ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see> 
  ''' the second item is a <paramref name="command">command</paramref> and 
  ''' the third item is the <paramref name="value">value</paramref>.
  ''' </param>
  ''' <param name="element">Specifies the element holding the existing value, which is updated.</param>
      Function Setter(ByVal element As IScpiValue(Of Double), 
                         ByVal subHeader As String, ByVal value As Double, 
                         ByVal access As ResourceAccessLevels) As IScpiValue(Of Double)

  ''' <summary>
  ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
  ''' Applies readings to the <paramref name="element">existing element</paramref>
  ''' </summary>
  ''' <param name="modalityName">Specifies the modality name.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2} {3}' where 
  ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see>, 
  ''' the second item is the <paramref name="modalityName">modality name</paramref>, 
  ''' the second item is a <paramref name="command">command</paramref> and
  ''' the fourth item represents a <paramref name="value">numeric value</paramref>.
  ''' </param>
  ''' <param name="element">Specifies the element holding the existing value, which is updated.</param>
      Function Setter(ByVal element As IScpiValue(Of Double), 
                         ByVal modalityName As String, ByVal subHeader As String, ByVal value As Double, 
                         ByVal access As ResourceAccessLevels) As IScpiValue(Of Double)
#End Region

#End Region

#Region " INTEGER "

#Region " QUERRIES "

  ''' <summary>
  ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
  ''' </summary>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}?' where 
  ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see> and 
  ''' the second item is a <paramref name="command">command</paramref>.
  ''' </param>
      Function QueryInteger(ByVal subHeader As String) As Nullable(Of Integer)

  ''' <summary>
  ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
  ''' </summary>
  ''' <param name="modalityName">Specifies the modality name.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2}?' where 
  ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see>, 
  ''' the second item is the <paramref name="modalityName">modality name</paramref> and 
  ''' the thrid item is a <paramref name="command">command</paramref>.
  ''' </param>
      Function QueryInteger(ByVal modalityName As String, ByVal subHeader As String) As Nullable(Of Integer)

#End Region

#Region " QUERRIES - INFINITY "

  ''' <summary>
  ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
  ''' </summary>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}?' where 
  ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see> and 
  ''' the second item is a <paramref name="command">command</paramref>.
  ''' </param>
      Function QueryInfInteger(ByVal subHeader As String) As Nullable(Of Integer)

  ''' <summary>
  ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
  ''' </summary>
  ''' <param name="modalityName">Specifies the modality name.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2}?' where 
  ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see>, 
  ''' the second item is the <paramref name="modalityName">modality name</paramref> and 
  ''' the thrid item is a <paramref name="command">command</paramref>.
  ''' </param>
      Function QueryInfInteger(ByVal modalityName As String, ByVal subHeader As String) As Nullable(Of Integer)

#End Region

#Region " GETTERS "

  ''' <summary>
  ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
  ''' </summary>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}?' where 
  ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see> and 
  ''' the second item is a <paramref name="command">command</paramref>
  ''' </param>
  ''' <param name="existingValue">Specifies the existing value.</param>
      Function Getter(ByVal subHeader As String, 
                  ByVal existingValue As Nullable(Of Integer), 
                  ByVal access As ResourceAccessLevels) As Nullable(Of Int32)

  ''' <summary>
  ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
  ''' </summary>
  ''' <param name="modalityName">Specifies the modality name.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2}?' where 
  ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see>, 
  ''' the second item is the <paramref name="modalityName">modality name</paramref> and
  ''' the second item is a <paramref name="command">command</paramref>.
  ''' </param>
  ''' <param name="existingValue">Specifies the existing value.</param>
      Function Getter(ByVal modalityName As String, ByVal subHeader As String, 
                  ByVal existingValue As Nullable(Of Integer), 
                  ByVal access As ResourceAccessLevels) As Nullable(Of Integer)

  ''' <summary>
  ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
  ''' </summary>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}?' where 
  ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see> and 
  ''' the second item is a <paramref name="command">command</paramref>
  ''' </param>
  ''' <param name="existingValue">Specifies the existing value.</param>
      Function GetterInfinity(ByVal subHeader As String, 
                          ByVal existingValue As Nullable(Of Integer), 
                          ByVal access As ResourceAccessLevels) As Nullable(Of Integer)

  ''' <summary>
  ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
  ''' </summary>
  ''' <param name="modalityName">Specifies the modality name.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2}?' where 
  ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see>, 
  ''' the second item is the <paramref name="modalityName">modality name</paramref> and
  ''' the second item is a <paramref name="command">command</paramref>.
  ''' </param>
  ''' <param name="existingValue">Specifies the existing value.</param>
      Function GetterInfinity(ByVal modalityName As String, ByVal subHeader As String, 
                          ByVal existingValue As Nullable(Of Integer), 
                          ByVal access As ResourceAccessLevels) As Nullable(Of Integer)

#End Region

#Region " GETTERS w/ ELEMENT "

  ''' <summary>
  ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
  ''' Uses or updates the nullable value getter command.
  ''' </summary>
  ''' <param name="element">Specifies the <see cref="IScpiValueBase">existing element</see> where the getter command is stored.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}?' where 
  ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see> and 
  ''' the second item is a <paramref name="command">command</paramref>
  ''' </param>
  ''' <param name="existingValue">Specifies the existing value.</param>
      Function Getter(ByVal element As IScpiValueBase, ByVal subHeader As String, 
                  ByVal existingValue As Nullable(Of Integer), 
                  ByVal access As ResourceAccessLevels) As Nullable(Of Int32)

  ''' <summary>
  ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
  ''' Uses or updates the nullable value getter command.
  ''' </summary>
  ''' <param name="element">Specifies the <see cref="IScpiValueBase">existing element</see> where the getter command is stored.</param>
  ''' <param name="modalityName">Specifies the modality name.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2}?' where 
  ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see>, 
  ''' the second item is the <paramref name="modalityName">modality name</paramref> and
  ''' the second item is a <paramref name="command">command</paramref>.
  ''' </param>
  ''' <param name="existingValue">Specifies the existing value.</param>
      Function Getter(ByVal element As IScpiValueBase, ByVal modalityName As String, ByVal subHeader As String, 
                  ByVal existingValue As Nullable(Of Integer), 
                  ByVal access As ResourceAccessLevels) As Nullable(Of Integer)

  ''' <summary>
  ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
  ''' Uses or updates the nullable value getter command.
  ''' </summary>
  ''' <param name="element">Specifies the <see cref="IScpiValueBase">existing element</see> where the getter command is stored.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}?' where 
  ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see> and 
  ''' the second item is a <paramref name="command">command</paramref>
  ''' </param>
  ''' <param name="existingValue">Specifies the existing value.</param>
      Function GetterInfinity(ByVal element As IScpiValueBase, ByVal subHeader As String, 
                          ByVal existingValue As Nullable(Of Integer), 
                          ByVal access As ResourceAccessLevels) As Nullable(Of Integer)

  ''' <summary>
  ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
  ''' Uses or updates the nullable value getter command.
  ''' </summary>
  ''' <param name="element">Specifies the <see cref="IScpiValueBase">existing element</see> where the getter command is stored.</param>
  ''' <param name="modalityName">Specifies the modality name.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2}?' where 
  ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see>, 
  ''' the second item is the <paramref name="modalityName">modality name</paramref> and
  ''' the second item is a <paramref name="command">command</paramref>.
  ''' </param>
  ''' <param name="existingValue">Specifies the existing value.</param>
      Function GetterInfinity(ByVal element As IScpiValueBase, ByVal modalityName As String, ByVal subHeader As String, 
                          ByVal existingValue As Nullable(Of Integer), 
                          ByVal access As ResourceAccessLevels) As Nullable(Of Integer)
#End Region

#Region " GETTERS w/ SCPI VALUE "

  ''' <summary>
  ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax
  ''' specified by the element.
  ''' Uses the nullable value getter command.
  ''' </summary>
  ''' <param name="element">Specifies the <see cref="IScpiValue">existing element</see> where the getter command is stored.</param>
      Function Getter(ByVal element As IScpiValue(Of Integer)) As IScpiValue(Of Integer)

  ''' <summary>
  ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
  ''' Uses the nullable value getter command.
  ''' </summary>
  ''' <param name="element">Specifies the <see cref="IScpiValueBase">existing element</see> where the getter command is stored.</param>
      Function Getter(ByVal element As IScpiValue(Of Integer), ByVal access As ResourceAccessLevels) As IScpiValue(Of Integer)

  ''' <summary>
  ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
  ''' Uses or updates the nullable value getter command.
  ''' </summary>
  ''' <param name="element">Specifies the <see cref="IScpiValueBase">existing element</see> where the getter command is stored.</param>
      Function GetterInfinity(ByVal element As IScpiValue(Of Integer), ByVal access As ResourceAccessLevels) As IScpiValue(Of Integer)

  ''' <summary>
  ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
  ''' Applies readings to the <paramref name="element">existing element</paramref>
  ''' </summary>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}?' where 
  ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see> and 
  ''' the second item is a <paramref name="command">command</paramref>
  ''' </param>
  ''' <param name="element">Specifies the element holding the existing value, which is updated.</param>
      Function Getter(ByVal element As IScpiValue(Of Integer), 
                         ByVal subHeader As String, 
                         ByVal access As ResourceAccessLevels) As IScpiValue(Of Integer)

  ''' <summary>
  ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
  ''' Applies readings to the <paramref name="element">existing element</paramref>
  ''' </summary>
  ''' <param name="modalityName">Specifies the modality name.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2}?' where 
  ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see>, 
  ''' the second item is the <paramref name="modalityName">modality name</paramref> and
  ''' the second item is a <paramref name="command">command</paramref>.
  ''' </param>
  ''' <param name="element">Specifies the element holding the existing value, which is updated.</param>
      Function Getter(ByVal element As IScpiValue(Of Integer), 
                         ByVal modalityName As String, ByVal subHeader As String, 
                         ByVal access As ResourceAccessLevels) As IScpiValue(Of Integer)

  ''' <summary>
  ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
  ''' Applies readings to the <paramref name="element">existing element</paramref>
  ''' </summary>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}?' where 
  ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see> and 
  ''' the second item is a <paramref name="command">command</paramref>
  ''' </param>
  ''' <param name="element">Specifies the element holding the existing value, which is updated.</param>
      Function GetterInfinity(ByVal element As IScpiValue(Of Integer), 
                                 ByVal subHeader As String, ByVal access As ResourceAccessLevels) As IScpiValue(Of Integer)

  ''' <summary>
  ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
  ''' Applies readings to the <paramref name="element">existing element</paramref>
  ''' </summary>
  ''' <param name="modalityName">Specifies the modality name.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2}?' where 
  ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see>, 
  ''' the second item is the <paramref name="modalityName">modality name</paramref> and
  ''' the second item is a <paramref name="command">command</paramref>.
  ''' </param>
  ''' <param name="element">Specifies the element holding the existing value, which is updated.</param>
      Function GetterInfinity(ByVal element As IScpiValue(Of Integer), 
                                 ByVal modalityName As String, ByVal subHeader As String, 
                                 ByVal access As ResourceAccessLevels) As IScpiValue(Of Integer)

#End Region

#Region " SETTERS "

  ''' <summary>
  ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
  ''' </summary>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1} {2}' where 
  ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see>, 
  ''' the second item is a <paramref name="command">command</paramref>
  ''' the third item represents a <paramref name="value">numeric value</paramref>.
  ''' </param>
  ''' <param name="value">Specifies the command value.</param>
      Function Setter(ByVal subHeader As String, ByVal value As Integer, ByVal verify As Boolean) As Boolean

  ''' <summary>
  ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
  ''' </summary>
  ''' <param name="modalityName">Specifies the modality name.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2} {3}' where 
  ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see>, 
  ''' the second item is the <paramref name="modalityName">modality name</paramref>, 
  ''' the third item is a <paramref name="command">command</paramref>
  ''' the fourth item represents a <paramref name="value">numeric value</paramref>.
  ''' </param>
  ''' <param name="value">Specifies the command value.</param>
      Function Setter(ByVal modalityName As String, ByVal subHeader As String, ByVal value As Integer, ByVal verify As Boolean) As Boolean

  ''' <summary>
  ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
  ''' </summary>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1} {2}' where 
  ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see>, 
  ''' the second item is a <paramref name="command">command</paramref>
  ''' the third item represents a <paramref name="value">numeric value</paramref>.
  ''' </param>
  ''' <param name="value">Specifies the command value.</param>
      Function SetterInfinity(ByVal subHeader As String, ByVal value As Integer, ByVal verify As Boolean) As Boolean

  ''' <summary>
  ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
  ''' </summary>
  ''' <param name="modalityName">Specifies the modality name.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2} {3}' where 
  ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see>, 
  ''' the second item is the <paramref name="modalityName">modality name</paramref>, 
  ''' the third item is a <paramref name="command">command</paramref>
  ''' the fourth item represents a <paramref name="value">numeric value</paramref>.
  ''' </param>
  ''' <param name="value">Specifies the command value.</param>
      Function SetterInfinity(ByVal modalityName As String, ByVal subHeader As String, ByVal value As Integer, ByVal verify As Boolean) As Boolean

#End Region

#Region " SETTERS w/ ACCESS "

  ''' <summary>
  ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
  ''' </summary>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1} {2}' where 
  ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see> 
  ''' the second item is a <paramref name="command">command</paramref> and 
  ''' the third item is the <paramref name="value">value</paramref>.
  ''' </param>
  ''' <param name="existingValue">Specifies the existing value.</param>
      Function Setter(ByVal subHeader As String, 
                  ByVal existingValue As Nullable(Of Integer), ByVal value As Integer, 
                  ByVal access As ResourceAccessLevels) As Nullable(Of Integer)

  ''' <summary>
  ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
  ''' </summary>
  ''' <param name="modalityName">Specifies the modality name.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2} {3}' where 
  ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see>, 
  ''' the second item is the <paramref name="modalityName">modality name</paramref>, 
  ''' the second item is a <paramref name="command">command</paramref> and
  ''' the fourth item represents a <paramref name="value">numeric value</paramref>.
  ''' </param>
  ''' <param name="existingValue">Specifies the existing value.</param>
      Function Setter(ByVal modalityName As String, ByVal subHeader As String, 
                  ByVal existingValue As Nullable(Of Integer), ByVal value As Integer, 
                  ByVal access As ResourceAccessLevels) As Nullable(Of Integer)

  ''' <summary>
  ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
  ''' </summary>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1} {2}' where 
  ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see> 
  ''' the second item is a <paramref name="command">command</paramref> and 
  ''' the third item is the <paramref name="value">value</paramref>.
  ''' </param>
  ''' <param name="existingValue">Specifies the existing value.</param>
      Function SetterInfinity(ByVal subHeader As String, 
                          ByVal existingValue As Nullable(Of Integer), ByVal value As Integer, 
                          ByVal access As ResourceAccessLevels) As Nullable(Of Integer)

  ''' <summary>
  ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
  ''' </summary>
  ''' <param name="modalityName">Specifies the modality name.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2} {3}' where 
  ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see>, 
  ''' the second item is the <paramref name="modalityName">modality name</paramref>, 
  ''' the second item is a <paramref name="command">command</paramref> and
  ''' the fourth item represents a <paramref name="value">numeric value</paramref>.
  ''' </param>
  ''' <param name="existingValue">Specifies the existing value.</param>
      Function SetterInfinity(ByVal modalityName As String, ByVal subHeader As String, 
                          ByVal existingValue As Nullable(Of Integer), ByVal value As Integer, 
                          ByVal access As ResourceAccessLevels) As Nullable(Of Integer)

#End Region

#Region " SETTERS w/ ACCESS w/ ELEMENT "

  ''' <summary>
  ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
  ''' </summary>
  ''' <param name="element">Specifies the <see cref="IScpiValueBase">existing element</see> where the getter command is stored.</param>
  ''' <param name="existingValue">Specifies the existing element value where the instrument.</param>
  ''' <param name="value">Specifies the value to be set using the element <see cref="IScpiValueBase.SetterCommandFormat">command</see>.</param>
      Function Setter(ByVal element As IScpiValueBase, 
                  ByVal existingValue As Integer, ByVal value As Integer, 
                  ByVal access As ResourceAccessLevels) As Integer

  ''' <summary>
  ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
  ''' Uses or updates the nullable value getter command.
  ''' </summary>
  ''' <param name="element">Specifies the <see cref="IScpiValueBase">existing element</see> where the getter command is stored.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1} {2}' where 
  ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see> 
  ''' the second item is a <paramref name="command">command</paramref> and 
  ''' the third item is the <paramref name="value">value</paramref>.
  ''' </param>
  ''' <param name="existingValue">Specifies the existing value.</param>
      Function Setter(ByVal element As IScpiValueBase, ByVal subHeader As String, 
                  ByVal existingValue As Nullable(Of Integer), ByVal value As Integer, 
                  ByVal access As ResourceAccessLevels) As Nullable(Of Integer)

  ''' <summary>
  ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
  ''' Uses or updates the nullable value getter command.
  ''' </summary>
  ''' <param name="element">Specifies the <see cref="IScpiValueBase">existing element</see> where the getter command is stored.</param>
  ''' <param name="modalityName">Specifies the modality name.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2} {3}' where 
  ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see>, 
  ''' the second item is the <paramref name="modalityName">modality name</paramref>, 
  ''' the second item is a <paramref name="command">command</paramref> and
  ''' the fourth item represents a <paramref name="value">numeric value</paramref>.
  ''' </param>
  ''' <param name="existingValue">Specifies the existing value.</param>
      Function Setter(ByVal element As IScpiValueBase, ByVal modalityName As String, ByVal subHeader As String, 
                  ByVal existingValue As Nullable(Of Integer), ByVal value As Integer, 
                  ByVal access As ResourceAccessLevels) As Nullable(Of Integer)

  ''' <summary>
  ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
  ''' Uses or updates the nullable value getter command.
  ''' </summary>
  ''' <param name="element">Specifies the <see cref="IScpiValueBase">existing element</see> where the getter command is stored.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1} {2}' where 
  ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see> 
  ''' the second item is a <paramref name="command">command</paramref> and 
  ''' the third item is the <paramref name="value">value</paramref>.
  ''' </param>
  ''' <param name="existingValue">Specifies the existing value.</param>
      Function SetterInfinity(ByVal element As IScpiValueBase, ByVal subHeader As String, 
                          ByVal existingValue As Nullable(Of Integer), ByVal value As Integer, 
                          ByVal access As ResourceAccessLevels) As Nullable(Of Integer)

  ''' <summary>
  ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
  ''' Uses or updates the nullable value getter command.
  ''' </summary>
  ''' <param name="element">Specifies the <see cref="IScpiValueBase">existing element</see> where the getter command is stored.</param>
  ''' <param name="modalityName">Specifies the modality name.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2} {3}' where 
  ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see>, 
  ''' the second item is the <paramref name="modalityName">modality name</paramref>, 
  ''' the second item is a <paramref name="command">command</paramref> and
  ''' the fourth item represents a <paramref name="value">numeric value</paramref>.
  ''' </param>
  ''' <param name="existingValue">Specifies the existing value.</param>
      Function SetterInfinity(ByVal element As IScpiValueBase, ByVal modalityName As String, ByVal subHeader As String, 
                          ByVal existingValue As Nullable(Of Integer), ByVal value As Integer, 
                          ByVal access As ResourceAccessLevels) As Nullable(Of Integer)

#End Region

#Region " SETTERS w/ SCPI VALUE "

  ''' <summary>
  ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
  ''' Applies readings to the <paramref name="element">existing element</paramref>
  ''' Assume element has both <see cref="IScpiValueBase.GetterCommand">getter</see> and 
  ''' <see cref="IScpiValueBase.SetterCommand">setter</see> commands specified.
  ''' </summary>
  ''' <param name="element">Specifies the element holding the existing value, which is updated.</param>
      Function Setter(ByVal element As IScpiValue(Of Integer), ByVal value As Integer, ByVal access As ResourceAccessLevels) As IScpiValue(Of Integer)

  ''' <summary>
  ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
  ''' Applies readings to the <paramref name="element">existing element</paramref>
  ''' </summary>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1} {2}' where 
  ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see> 
  ''' the second item is a <paramref name="command">command</paramref> and 
  ''' the third item is the <paramref name="value">value</paramref>.
  ''' </param>
  ''' <param name="element">Specifies the element holding the existing value, which is updated.</param>
      Function Setter(ByVal element As IScpiValue(Of Integer), 
                         ByVal subHeader As String, ByVal value As Integer, 
                         ByVal access As ResourceAccessLevels) As IScpiValue(Of Integer)

  ''' <summary>
  ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
  ''' Applies readings to the <paramref name="element">existing element</paramref>
  ''' </summary>
  ''' <param name="modalityName">Specifies the modality name.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2} {3}' where 
  ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see>, 
  ''' the second item is the <paramref name="modalityName">modality name</paramref>, 
  ''' the second item is a <paramref name="command">command</paramref> and
  ''' the fourth item represents a <paramref name="value">numeric value</paramref>.
  ''' </param>
  ''' <param name="element">Specifies the element holding the existing value, which is updated.</param>
      Function Setter(ByVal element As IScpiValue(Of Integer), 
                         ByVal modalityName As String, ByVal subHeader As String, ByVal value As Integer, 
                         ByVal access As ResourceAccessLevels) As IScpiValue(Of Integer)

  ''' <summary>
  ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
  ''' Applies readings to the <paramref name="element">existing element</paramref>
  ''' </summary>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1} {2}' where 
  ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see> 
  ''' the second item is a <paramref name="command">command</paramref> and 
  ''' the third item is the <paramref name="value">value</paramref>.
  ''' </param>
  ''' <param name="element">Specifies the element holding the existing value, which is updated.</param>
      Function SetterInfinity(ByVal element As IScpiValue(Of Integer), 
                                 ByVal subHeader As String, ByVal value As Integer, 
                                 ByVal access As ResourceAccessLevels) As IScpiValue(Of Integer)

  ''' <summary>
  ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
  ''' Applies readings to the <paramref name="element">existing element</paramref>
  ''' </summary>
  ''' <param name="modalityName">Specifies the modality name.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2} {3}' where 
  ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see>, 
  ''' the second item is the <paramref name="modalityName">modality name</paramref>, 
  ''' the second item is a <paramref name="command">command</paramref> and
  ''' the fourth item represents a <paramref name="value">numeric value</paramref>.
  ''' </param>
  ''' <param name="element">Specifies the element holding the existing value, which is updated.</param>
      Function SetterInfinity(ByVal element As IScpiValue(Of Integer), 
                                 ByVal modalityName As String, ByVal subHeader As String, ByVal value As Integer, 
                                 ByVal access As ResourceAccessLevels) As IScpiValue(Of Integer)
#End Region

#End Region

End Interface
