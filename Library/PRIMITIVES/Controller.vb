''' <summary>
''' The SCPI Controller for controlling, reading from and writing to the decies..
''' The SCPI controller uses the <see cref="IPort">Port I/O</see> to control the instrument and
''' read or write SCPI structures to the instrument.
''' The <see cref="IPort">Port I/O</see> could VISA device such as SERIAL, ETHRENET or GPIB.
''' </summary>
''' <license>
''' (c) 2011 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="01/15/05" by="David" revision="1.0.1841.x">
''' Created
''' </history>
''' <history date="02/12/11" by="David" revision="6.0.4060.x">
''' Based on the subsystem class of the previous version.
''' </history>
Public Class Controller
    Implements IController

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="port" /> class.
    ''' </summary>
    ''' <param name="io">The element implementing the interface to the instrument.</param>
    Public Sub New(ByVal io As Scpi.IPort)
        MyBase.New()
        Me._port = io
    End Sub

    ''' <summary>Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup.</summary>
    ''' <remarks>Do not make this method Overridable (virtual) because a derived 
    '''   class should not be able to override this method.</remarks>
    Public Sub Dispose() Implements IDisposable.Dispose

        ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

        ' this disposes all child classes.
        Dispose(True)

        ' Take this object off the finalization(Queue) and prevent finalization code 
        ' from executing a second time.
        GC.SuppressFinalize(Me)

    End Sub

    ''' <summary>
    ''' Gets or sets the dispose status sentinel of the base class.  This applies to the derived class
    ''' provided proper implementation.
    ''' </summary>
    Public Property IsDisposed() As Boolean


    ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
    ''' <param name="disposing">True if this method releases both managed and unmanaged 
    '''   resources; False if this method releases only unmanaged resources.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by
    '''   its disposing parameter.  If True, the method has been called directly or 
    '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
    '''   If disposing equals False, the method has been called by the 
    '''   runtime from inside the finalizer and you should not reference 
    '''   other objects--only unmanaged resources can be disposed.</remarks>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)

        If Not Me.IsDisposed Then

            Try

                If disposing Then

                    ' Free managed resources when explicitly called
                    Me._port = Nothing

                End If

                ' Free shared unmanaged resources

            Finally

                ' set the sentinel indicating that the class was disposed.
                Me.IsDisposed = True

            End Try

        End If

    End Sub

    ''' <summary>This destructor will run only if the Dispose method 
    '''   does not get called. It gives the base class the opportunity to 
    '''   finalize. Do not provide destructors in types derived from this class.</summary>
    Protected Overrides Sub Finalize()
        Try
            ' Do not re-create Dispose clean-up code here.
            ' Calling Dispose(false) is optimal for readability and maintainability.
            Dispose(False)
        Finally
            ' The compiler automatically adds a call to the base class finalizer 
            ' that satisfies the rule: FinalizersShouldCallBaseClassFinalizer.
            MyBase.Finalize()
        End Try
    End Sub

#End Region

#Region " I IO "

    Private _port As IPort
    ''' <summary>
    ''' Gets reference to the Port I/O  system that implements the <see cref="Scpi.IPort">SCPI Port IO</see> interface
    ''' for accessing the instrument.
    ''' </summary>
    Public ReadOnly Property Port() As IPort Implements IController.Port
        Get
            Return Me._port
        End Get
    End Property

#End Region

#Region " SUBSYSTEM HEADER "

    Private _syntaxHeader As String
    ''' <summary>
    ''' Gets or sets the subsystem syntax header.
    ''' </summary>
    Public Property SyntaxHeader() As String Implements IController.SyntaxHeader
        Get
            Return Me._syntaxHeader
        End Get
        Set(ByVal value As String)
            Me._syntaxHeader = value
        End Set
    End Property

#End Region

#Region " EXECUTE "

    ''' <summary>
    ''' Executes a command based on the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}' where 
    ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see> and 
    ''' the second item is a <paramref name="command">command</paramref>.
    ''' </param>
    Public Sub Execute(ByVal subHeader As String) Implements IController.Execute
        Me.Port.WriteLine(Syntax.BuildExecute(Me.SyntaxHeader, subHeader))
    End Sub

    ''' <summary>
    ''' Executes a command based on the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="modalityName">Specifies the modality name.</param>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2}' where 
    ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see>, 
    ''' the second item is the <paramref name="modalityName">modality name</paramref> and 
    ''' the second item is a <paramref name="command">command</paramref>.
    ''' </param>
    Public Sub Execute(ByVal modalityName As String, ByVal subHeader As String) Implements IController.Execute
        Me.Port.WriteLine(Syntax.BuildExecute(Me.SyntaxHeader, modalityName, subHeader))
    End Sub

    ''' <summary>
    ''' Executes a command constructered from the specified subsystem elements.
    ''' </summary>
    ''' <param name="modalityName">Name of the modality.</param>
    ''' <param name="subHeader">The sub header.</param>
    ''' <param name="isVerifyOperationComplete">if set to <c>True</c> [is verify operation complete].</param>
    ''' <param name="isRaiseDeviceErrors">if set to <c>True</c> [is raise device errors].</param>
    Public Sub Execute(ByVal modalityName As String, ByVal subHeader As String,
                       ByVal isVerifyOperationComplete As Boolean, ByVal isRaiseDeviceErrors As Boolean) Implements IController.Execute
        Me.Port.WriteLine(Syntax.BuildExecute(Me.SyntaxHeader, modalityName, subHeader), isVerifyOperationComplete, isRaiseDeviceErrors)
    End Sub

#End Region

#Region " STRING "

#Region " QUERRIES "

    ''' <summary>
    ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}?' where 
    ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see> and 
    ''' the second item is a <paramref name="command">command</paramref>.
    ''' </param>
    Public Function QueryTrimEnd(ByVal subHeader As String) As String Implements IController.QueryTrimEnd
        Return Me.Port.QueryTrimEnd(Syntax.BuildQuery(Me.SyntaxHeader, subHeader))
    End Function

    ''' <summary>
    ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="modalityName">Specifies the modality name.</param>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2}?' where 
    ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see>, 
    ''' the second item is the <paramref name="modalityName">modality name</paramref> and 
    ''' the thrid item is a <paramref name="command">command</paramref>.
    ''' </param>
    Public Function QueryTrimEnd(ByVal modalityName As String, ByVal subHeader As String) As String Implements IController.QueryTrimEnd
        Return Me.Port.QueryTrimEnd(Syntax.BuildQuery(Me.SyntaxHeader, modalityName, subHeader))
    End Function

#End Region

#Region " GETTERS w/ ACCESS "

    ''' <summary>
    ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}?' where 
    ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see> and 
    ''' the second item is a <paramref name="command">command</paramref>
    ''' </param>
    ''' <param name="existingValue">The existing value to use for forcing instrument access if null or empty.</param>
    ''' <param name="access">The access <see cref="isr.Scpi.ResourceAccessLevels">level</see> to the instrument.</param>
    Public Function Getter(ByVal subHeader As String,
                       ByVal existingValue As String,
                       ByVal access As ResourceAccessLevels) As String Implements IController.Getter
        If access.IsDeviceAccess OrElse String.IsNullOrWhiteSpace(existingValue) Then
            existingValue = Me.QueryTrimEnd(subHeader)
        End If
        Return existingValue
    End Function

    ''' <summary>
    ''' Gets a value from the instrument <see cref="SyntaxHeader">SCPI subsystem</see> <paramref name="modalityName">modality</paramref> and <paramref name="subHeader">element</paramref>.
    ''' </summary>
    ''' <param name="modalityName">Specifies the modality name.</param>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2}?' where 
    ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see>, 
    ''' the second item is the <paramref name="modalityName">modality name</paramref> and 
    ''' the third item is a <paramref name="command">command</paramref>
    ''' </param>
    ''' <param name="existingValue">The existing value to use for forcing instrument access if null or empty.</param>
    Public Function Getter(ByVal modalityName As String, ByVal subHeader As String,
                       ByVal existingValue As String,
                       ByVal access As ResourceAccessLevels) As String Implements IController.Getter
        If access.IsDeviceAccess OrElse String.IsNullOrWhiteSpace(existingValue) Then
            existingValue = Me.QueryTrimEnd(modalityName, subHeader)
        End If
        Return existingValue
    End Function

#End Region

#Region " GETTERS w/ ACCESS w/ ELEMENT  "

    ''' <summary>
    ''' Gets a value from the instrument <see cref="SyntaxHeader">SCPI subsystem</see> <paramref name="subHeader">element</paramref>.
    ''' Uses or updates the nullable value getter command.
    ''' </summary>
    ''' <param name="element">Specifies the <see cref="IScpiValueBase">existing element</see> where the getter command is stored.</param>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}?' where 
    ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see> and 
    ''' the second item is a <paramref name="command">command</paramref>
    ''' </param>
    ''' <param name="existingValue">The existing value to use for forcing instrument access if null or empty.</param>
    ''' <param name="access">Specifies the desired access level to the instrument.</param>
    Public Function GetterString(ByVal element As IScpiValueBase,
                       ByVal subHeader As String,
                       ByVal existingValue As String,
                       ByVal access As ResourceAccessLevels) As String Implements IController.GetterString
        If access.IsDeviceAccess OrElse String.IsNullOrWhiteSpace(existingValue) Then
            If String.IsNullOrWhiteSpace(element.GetterCommand) Then
                element.GetterCommand = Syntax.BuildQuery(Me.SyntaxHeader, subHeader)
            End If
            existingValue = Me.Port.QueryTrimEnd(element.GetterCommand)
            ' value = Me.QueryTrimEnd(subHeader)
        End If
        Return existingValue
    End Function

    ''' <summary>
    ''' Gets a value from the instrument <see cref="SyntaxHeader">SCPI subsystem</see> <paramref name="modalityName">modality</paramref> and <paramref name="subHeader">element</paramref>.
    ''' </summary>
    ''' <param name="element">Specifies the <see cref="IScpiValueBase">existing element</see> where the getter command is stored.</param>
    ''' <param name="modalityName">Specifies the modality name.</param>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2}?' where 
    ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see>, 
    ''' the second item is the <paramref name="modalityName">modality name</paramref> and 
    ''' the third item is a <paramref name="command">command</paramref>
    ''' </param>
    ''' <param name="existingValue">The existing value to use for forcing instrument access if null or empty.</param>
    Public Function GetterString(ByVal element As IScpiValueBase,
                       ByVal modalityName As String, ByVal subHeader As String,
                       ByVal existingValue As String,
                       ByVal access As ResourceAccessLevels) As String Implements IController.GetterString
        If access.IsDeviceAccess OrElse String.IsNullOrWhiteSpace(existingValue) Then
            If String.IsNullOrWhiteSpace(element.GetterCommand) Then
                element.GetterCommand = Syntax.BuildQuery(Me.SyntaxHeader, modalityName, subHeader)
            End If
            ' value = Me.QueryTrimEnd(modalityName, subHeader)
            existingValue = Me.Port.QueryTrimEnd(element.GetterCommand)
        End If
        Return existingValue
    End Function

#End Region

#Region " GETTERS w/ ACCESS w/ SCPI VALUE "

    ''' <summary>
    ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
    ''' Uses or updates the nullable value getter command.
    ''' </summary>
    ''' <param name="element">Specifies the <see cref="IScpiValueBase">existing element</see> where the getter command is stored.</param>
    ''' <param name="access">The desired <see cref="ResourceAccessLevels">access level</see> to the instrument.</param>
    Public Function Getter(ByVal element As IScpiString, ByVal access As ResourceAccessLevels) As IScpiString Implements IController.Getter
        If access.IsDeviceAccess OrElse String.IsNullOrWhiteSpace(element.Value) Then
            element.Value = Me.Port.QueryTrimEnd(element.GetterCommand)
            element.ActualValue = element.Value
        End If
        Return element
    End Function

    ''' <summary>
    ''' Gets the instrument <see cref="SyntaxHeader">SCPI subsystem</see> <paramref name="subHeader">element</paramref> value.
    ''' Applies readings to the <paramref name="element">existing element</paramref>
    ''' </summary>
    ''' <param name="element">Specifies the existing element where the instrument readings are applied.</param>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}?' where 
    ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see> and 
    ''' the second item is a <paramref name="command">command</paramref>
    ''' </param>
    ''' <param name="access">The desired <see cref="ResourceAccessLevels">access level</see> to the instrument.</param>
    Public Function Getter(ByVal element As IScpiString, ByVal subHeader As String, ByVal access As ResourceAccessLevels) As IScpiString Implements IController.Getter
        element.Value = Me.GetterString(CType(element, IScpiValueBase), subHeader, element.Value, access)
        element.ActualValue = element.Value
        Return element
    End Function

    ''' <summary>
    ''' Gets the instrument <see cref="SyntaxHeader">SCPI subsystem</see> <paramref name="modalityName">modality</paramref> <paramref name="subHeader">element</paramref> value.
    ''' Applies readings to the <paramref name="element">existing element</paramref>
    ''' </summary>
    ''' <param name="element">Specifies the existing element where the instrument readings are applied.</param>
    ''' <param name="modalityName">Specifies the modality name.</param>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2}?' where 
    ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see>, 
    ''' the second item is the <paramref name="modalityName">modality name</paramref> and 
    ''' the third item is a <paramref name="command">command</paramref>
    ''' </param>
    ''' <param name="access">The desired <see cref="ResourceAccessLevels">access level</see> to the instrument.</param>
    Public Function Getter(ByVal element As IScpiString, ByVal modalityName As String, ByVal subHeader As String,
                       ByVal access As ResourceAccessLevels) As IScpiString Implements IController.Getter
        element.Value = Me.GetterString(element, modalityName, subHeader, element.Value, access)
        element.ActualValue = element.Value
        Return element
    End Function

#End Region

#Region " SETTERS "

    ''' <summary>
    ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1} {2}' where 
    ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see>, 
    ''' the second item is a <paramref name="command">command</paramref>
    ''' the third item represents a <paramref name="value">numeric value</paramref>.
    ''' </param>
    ''' <param name="value">The value to set in the instrument.</param>
    Public Function Setter(ByVal subHeader As String, ByVal value As String, ByVal verify As Boolean) As Boolean Implements IController.Setter
        Dim scpiMessage As String = Syntax.BuildCommand(Me.SyntaxHeader, subHeader, value)
        Me.Port.WriteLine(scpiMessage)
        If verify Then
            Dim actual As String = Me.QueryTrimEnd(subHeader)
            Return Not String.IsNullOrWhiteSpace(actual) AndAlso (value = actual)
        Else
            Return True
        End If
    End Function

    ''' <summary>
    ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="modalityName">Specifies the modality name.</param>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2} {3}' where 
    ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see>, 
    ''' the second item is the <paramref name="modalityName">modality name</paramref>, 
    ''' the third item is a <paramref name="command">command</paramref>
    ''' the fourth item represents a <paramref name="value">numeric value</paramref>.
    ''' </param>
    ''' <param name="value">The value to set in the instrument.</param>
    Public Function Setter(ByVal modalityName As String, ByVal subHeader As String, ByVal value As String, ByVal verify As Boolean) As Boolean Implements IController.Setter
        Dim scpiMessage As String = Syntax.BuildCommand(Me.SyntaxHeader, modalityName, subHeader, value)
        Me.Port.WriteLine(scpiMessage)
        If verify Then
            Dim actual As String = Me.QueryTrimEnd(modalityName, subHeader)
            Return Not String.IsNullOrWhiteSpace(actual) AndAlso (value = actual)
        Else
            Return True
        End If
    End Function

#End Region

#Region " SETTERS w/ ACCESS "

    ''' <summary>
    ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1} {2}' where 
    ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see> 
    ''' the second item is a <paramref name="command">command</paramref> and 
    ''' the third item is the <paramref name="value">value</paramref>.
    ''' </param>
    ''' <param name="existingValue">Specifies the existing element value where the instrument.</param>
    ''' <param name="access">The desired <see cref="ResourceAccessLevels">access level</see> to the instrument.</param>
    Public Function Setter(ByVal subHeader As String,
                       ByVal existingValue As String, ByVal value As String,
                       ByVal access As ResourceAccessLevels) As String Implements IController.Setter
        If access.IsVerifyAccess Then
            Throw New ArgumentException("Verification not supported by this method", "access")
        End If
        If access.IsDeviceAccess OrElse Not String.Equals(value, existingValue, StringComparison.OrdinalIgnoreCase) Then
            Me.Port.WriteLine(Syntax.BuildCommand(Me.SyntaxHeader, subHeader, value))
        End If
        Return value
    End Function

    ''' <summary>
    ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="modalityName">Specifies the modality name.</param>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2} {3}' where 
    ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see>, 
    ''' the second item is the <paramref name="modalityName">modality name</paramref>, 
    ''' the second item is a <paramref name="command">command</paramref> and
    ''' the fourth item represents a <paramref name="value">numeric value</paramref>.
    ''' </param>
    ''' <param name="existingValue">Specifies the existing element value where the instrument.</param>
    ''' <param name="access">The desired <see cref="ResourceAccessLevels">access level</see> to the instrument.</param>
    Public Function Setter(ByVal modalityName As String, ByVal subHeader As String,
                       ByVal existingValue As String, ByVal value As String,
                       ByVal access As ResourceAccessLevels) As String Implements IController.Setter
        If access.IsVerifyAccess Then
            Throw New ArgumentException("Verification not supported by this method", "access")
        End If
        If access.IsDeviceAccess OrElse Not String.Equals(value, existingValue, StringComparison.OrdinalIgnoreCase) Then
            Me.Port.WriteLine(Syntax.BuildCommand(Me.SyntaxHeader, modalityName, subHeader, value))
        End If
        Return value
    End Function

#End Region

#Region " SETTERS w/ ACCESS w/ ELEMENT "

    ''' <summary>
    ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="element">Specifies the <see cref="IScpiValueBase">existing element</see> where the getter command is stored.</param>
    ''' <param name="existingValue">Specifies the existing element value where the instrument.</param>
    ''' <param name="value">Specifies the value to be set using the element <see cref="IScpiValueBase.SetterCommandFormat">command</see>.</param>
    ''' <param name="access">The desired <see cref="ResourceAccessLevels">access level</see> to the instrument.</param>
    Public Function Setter(ByVal element As IScpiValueBase,
                       ByVal existingValue As String, ByVal value As String,
                       ByVal access As ResourceAccessLevels) As String Implements IController.Setter
        If access.IsVerifyAccess Then
            Throw New ArgumentException("Verification not supported by this method", "access")
        End If
        If access.IsDeviceAccess OrElse Not String.Equals(value, existingValue, StringComparison.OrdinalIgnoreCase) Then
            If Not String.IsNullOrWhiteSpace(element.SetterCommandFormat) Then
                element.SetterCommand = String.Format(Globalization.CultureInfo.InvariantCulture, element.SetterCommandFormat, value)
            ElseIf String.IsNullOrWhiteSpace(element.SetterCommand) Then
                Throw New ArgumentException("Element setter command is empty", "element")
            End If
            Me.Port.WriteLine(element.SetterCommand)
        End If
        Return value
    End Function

    ''' <summary>
    ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="element">Specifies the <see cref="IScpiValueBase">existing element</see> where the getter command is stored.</param>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1} {2}' where 
    ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see> 
    ''' the second item is a <paramref name="command">command</paramref> and 
    ''' the third item is the <paramref name="value">value</paramref>.
    ''' </param>
    ''' <param name="existingValue">Specifies the existing element value where the instrument.</param>
    ''' <param name="access">The desired <see cref="ResourceAccessLevels">access level</see> to the instrument.</param>
    Public Function Setter(ByVal element As IScpiValueBase,
                       ByVal subHeader As String,
                       ByVal existingValue As String, ByVal value As String,
                       ByVal access As ResourceAccessLevels) As String Implements IController.Setter
        If access.IsVerifyAccess Then
            Throw New ArgumentException("Verification not supported by this method", "access")
        End If
        If access.IsDeviceAccess OrElse Not String.Equals(value, existingValue, StringComparison.OrdinalIgnoreCase) Then
            If String.IsNullOrWhiteSpace(element.SetterCommandFormat) Then
                element.SetterCommandFormat = Syntax.BuildCommandFormat(Me.SyntaxHeader, subHeader)
            End If
            element.SetterCommand = Syntax.BuildCommand(Me.SyntaxHeader, subHeader, value)
            Me.Port.WriteLine(element.SetterCommand)
        End If
        Return value
    End Function

    ''' <summary>
    ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="element">Specifies the <see cref="IScpiValueBase">existing element</see> where the getter command is stored.</param>
    ''' <param name="modalityName">Specifies the modality name.</param>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2} {3}' where 
    ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see>, 
    ''' the second item is the <paramref name="modalityName">modality name</paramref>, 
    ''' the second item is a <paramref name="command">command</paramref> and
    ''' the fourth item represents a <paramref name="value">numeric value</paramref>.
    ''' </param>
    ''' <param name="existingValue">Specifies the existing element value where the instrument.</param>
    ''' <param name="access">The desired <see cref="ResourceAccessLevels">access level</see> to the instrument.</param>
    Public Function Setter(ByVal element As IScpiValueBase,
                       ByVal modalityName As String, ByVal subHeader As String,
                       ByVal existingValue As String, ByVal value As String,
                       ByVal access As ResourceAccessLevels) As String Implements IController.Setter
        If access.IsVerifyAccess Then
            Throw New ArgumentException("Verification not supported by this method", "access")
        End If
        If access.IsDeviceAccess OrElse Not String.Equals(value, existingValue, StringComparison.OrdinalIgnoreCase) Then
            If String.IsNullOrWhiteSpace(element.SetterCommandFormat) Then
                element.SetterCommandFormat = Syntax.BuildCommandFormat(Me.SyntaxHeader, modalityName, subHeader)
            End If
            element.SetterCommand = Syntax.BuildCommand(Me.SyntaxHeader, modalityName, subHeader, value)
            Me.Port.WriteLine(element.SetterCommand)
        End If
        Return value
    End Function

#End Region

#Region " SETTERS w/ ACCESS w/ SCPI VALUE  "

    ''' <summary>
    ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
    ''' Applies readings to the <paramref name="element">existing element</paramref>
    ''' Assume element has both <see cref="IScpiValueBase.GetterCommand">getter</see> and 
    ''' <see cref="IScpiValueBase.SetterCommand">setter</see> commands specified.
    ''' </summary>
    ''' <param name="element">Specifies the element holding the existing value, which is updated.</param>
    ''' <param name="value">The value to set in the instrument.</param>
    ''' <param name="access">The desired <see cref="ResourceAccessLevels">access level</see> to the instrument.</param>
    Public Function Setter(ByVal element As IScpiString, ByVal value As String, ByVal access As ResourceAccessLevels) As IScpiString Implements IController.Setter
        If access.IsDeviceAccess OrElse (value <> element.Value) Then
            element.Value = Me.Setter(CType(element, IScpiValueBase), element.Value, value, access And Not ResourceAccessLevels.Verify)
        End If
        If access.IsVerifyAccess Then
            Me.Getter(element, ResourceAccessLevels.Device)
            element.Value = value
        ElseIf access.IsCacheAccess Then
            element.ActualValue = value
        End If
        Return element
    End Function

    ''' <summary>
    ''' Sets the instrument <see cref="SyntaxHeader">SCPI subsystem</see> <paramref name="subHeader">element</paramref> value.
    ''' Applies readings to the <paramref name="element">existing element</paramref>
    ''' </summary>
    ''' <param name="element">Specifies the existing element where the instrument readings are applied.</param>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1} {2}' where 
    ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see> 
    ''' the second item is a <paramref name="command">command</paramref> and 
    ''' the third item is the <paramref name="value">value</paramref>.
    ''' </param>
    ''' <param name="value">The value to set in the instrument.</param>
    ''' <param name="access">The desired <see cref="ResourceAccessLevels">access level</see> to the instrument.</param>
    ''' <remarks>
    ''' Use the following code in the calling methd if verification is used:
    ''' <code>
    ''' If Not Me.Elements.IsVerified.GetValueOrDefault(False) Then
    '''   If String.IsNullOrWhiteSpace(element.Value) OrElse String.IsNullOrWhiteSpace(element.ActualValue) Then
    '''     return "Failed reading values from the instrument."
    '''   Else
    '''     return String.Format(Globalization.CultureInfo.CurrentCulture, "Instrument settings applied using the command '{0}' did not verify. Expected={1}. Actual={2}.", Me.Elements.Value, Me.Elements.ActualValue.Value)
    '''   End If
    ''' End If
    ''' </code>
    ''' </remarks>
    Public Function Setter(ByVal element As IScpiString,
                           ByVal subHeader As String, ByVal value As String, ByVal access As ResourceAccessLevels) As IScpiString Implements IController.Setter
        element.Value = Me.Setter(CType(element, IScpiValueBase), subHeader, element.Value, value, access And Not isr.Scpi.ResourceAccessLevels.Verify)
        If access.IsVerifyAccess Then
            Me.Getter(element, subHeader, ResourceAccessLevels.Device)
            element.Value = value
        ElseIf access.IsCacheAccess Then
            element.ActualValue = value
        End If
        Return element
    End Function

    ''' <summary>
    ''' Sets the instrument <see cref="SyntaxHeader">SCPI subsystem</see> <paramref name="modalityName">modality</paramref> <paramref name="subHeader">element</paramref> value.
    ''' Applies readings to the <paramref name="element">existing element</paramref>
    ''' </summary>
    ''' <param name="element">Specifies the existing element where the instrument readings are applied.</param>
    ''' <param name="modalityName">Specifies the modality name.</param>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2} {3}' where 
    ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see>, 
    ''' the second item is the <paramref name="modalityName">modality name</paramref>, 
    ''' the second item is a <paramref name="command">command</paramref> and
    ''' the fourth item represents a <paramref name="value">numeric value</paramref>.
    ''' </param>
    ''' <param name="value">The value to set in the instrument.</param>
    ''' <param name="access">The desired <see cref="ResourceAccessLevels">access level</see> to the instrument.</param>
    ''' <remarks>
    ''' Use the following code in the calling methd if verification is used:
    ''' <code>
    ''' If Not Me.Elements.IsVerified.GetValueOrDefault(False) Then
    '''   If String.IsNullOrWhiteSpace(element.Value) OrElse String.IsNullOrWhiteSpace(element.ActualValue) Then
    '''     return "Failed reading values from the instrument."
    '''   Else
    '''     return String.Format(Globalization.CultureInfo.CurrentCulture, "Instrument settings applied using the command '{0}' did not verify. Expected={1}. Actual={2}.", Me.Elements.Value, Me.Elements.ActualValue.Value)
    '''   End If
    ''' End If
    ''' </code>
    ''' </remarks>
    Public Function Setter(ByVal element As IScpiString,
                           ByVal modalityName As String, ByVal subHeader As String, ByVal value As String,
                           ByVal access As ResourceAccessLevels) As IScpiString Implements IController.Setter
        element.Value = Me.Setter(CType(element, IScpiValueBase), modalityName, subHeader, element.Value, value, access And Not isr.Scpi.ResourceAccessLevels.Verify)
        If access.IsVerifyAccess Then
            Me.Getter(element, modalityName, subHeader, ResourceAccessLevels.Device)
            element.Value = value
        ElseIf access.IsCacheAccess Then
            element.ActualValue = value
        End If
        Return element
    End Function

#End Region

#End Region

#Region " BOOLEAN "

#Region " QUERRIES "

    ''' <summary>
    ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}?' where 
    ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see> and 
    ''' the second item is a <paramref name="command">command</paramref>.
    ''' </param>
    ''' <param name="parser">The <see cref="isr.Core.IParser(of boolean)">boolean Parser.</see></param>
    Public Function QueryBoolean(ByVal subHeader As String, ByVal parser As isr.Core.IParser(Of Boolean)) As Nullable(Of Boolean) Implements IController.QueryBoolean
        Return Me.Port.QueryBoolean(Syntax.BuildQuery(Me.SyntaxHeader, subHeader), parser)
    End Function

    ''' <summary>
    ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="modalityName">Specifies the modality name.</param>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2}?' where 
    ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see>, 
    ''' the second item is the <paramref name="modalityName">modality name</paramref> and 
    ''' the thrid item is a <paramref name="command">command</paramref>.
    ''' </param>
    ''' <param name="Parser">The <see cref="isr.Core.IParser(of boolean)">boolean Parser.</see></param>
    Public Function QueryBoolean(ByVal modalityName As String, ByVal subHeader As String, ByVal parser As isr.Core.IParser(Of Boolean)) As Nullable(Of Boolean) Implements IController.QueryBoolean
        Return Me.Port.QueryBoolean(Syntax.BuildQuery(Me.SyntaxHeader, modalityName, subHeader), parser)
    End Function

#End Region

#Region " GETTERS w/ ACCESS "

    ''' <summary>
    ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}?' where 
    ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see> and 
    ''' the second item is a <paramref name="command">command</paramref>
    ''' </param>
    ''' <param name="existingValue">The existing value. The instrument is accessed if this value has no value.</param>
    ''' <param name="parser">The <see cref="isr.Core.IParser(of boolean)">boolean Parser.</see></param>
    ''' <param name="access">The desired <see cref="ResourceAccessLevels">access level</see> to the instrument.</param>
    Public Function Getter(ByVal subHeader As String,
                       ByVal existingValue As Nullable(Of Boolean), ByVal parser As isr.Core.IParser(Of Boolean),
                       ByVal access As ResourceAccessLevels) As Nullable(Of Boolean) Implements IController.Getter
        If access.IsDeviceAccess OrElse Not existingValue.HasValue Then
            existingValue = Me.QueryBoolean(subHeader, parser)
        End If
        Return existingValue
    End Function

    ''' <summary>
    ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="modalityName">Specifies the modality name.</param>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2}?' where 
    ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see>, 
    ''' the second item is the <paramref name="modalityName">modality name</paramref> and
    ''' the second item is a <paramref name="command">command</paramref>.
    ''' </param>
    ''' <param name="existingValue">The existing value. The instrument is accessed if this value has no value.</param>
    ''' <param name="Parser">The <see cref="isr.Core.IParser(of boolean)">boolean Parser.</see></param>
    ''' <param name="access">The desired <see cref="ResourceAccessLevels">access level</see> to the instrument.</param>
    Public Function Getter(ByVal modalityName As String, ByVal subHeader As String,
                       ByVal existingValue As Nullable(Of Boolean), ByVal parser As isr.Core.IParser(Of Boolean),
                       ByVal access As ResourceAccessLevels) As Nullable(Of Boolean) Implements IController.Getter
        If access.IsDeviceAccess OrElse Not existingValue.HasValue Then
            existingValue = Me.QueryBoolean(modalityName, subHeader, parser)
        End If
        Return existingValue
    End Function

#End Region

#Region " GETTERS w/ ACCESS w/ ELEMENT "

    ''' <summary>
    ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
    ''' Uses or updates the nullable value getter command.
    ''' </summary>
    ''' <param name="element">Specifies the <see cref="IScpiValueBase">existing element</see> where the getter command is stored.</param>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}?' where 
    ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see> and 
    ''' the second item is a <paramref name="command">command</paramref>
    ''' </param>
    ''' <param name="existingValue">The existing value. The instrument is accessed if this value has no value.</param>
    ''' <param name="Parser">The <see cref="isr.Core.IParser(of boolean)">boolean Parser.</see></param>
    ''' <param name="access">The desired <see cref="ResourceAccessLevels">access level</see> to the instrument.</param>
    Public Function Getter(ByVal element As IScpiValueBase,
                       ByVal subHeader As String,
                       ByVal existingValue As Nullable(Of Boolean), ByVal parser As isr.Core.IParser(Of Boolean),
                       ByVal access As ResourceAccessLevels) As Nullable(Of Boolean) Implements IController.Getter
        If access.IsDeviceAccess OrElse Not existingValue.HasValue Then
            If String.IsNullOrWhiteSpace(element.GetterCommand) Then
                element.GetterCommand = Syntax.BuildQuery(Me.SyntaxHeader, subHeader)
            End If
            ' existingValue = Me.QueryBoolean(subHeader)
            existingValue = Me.Port.QueryBoolean(element.GetterCommand, parser)
        End If
        Return existingValue
    End Function

    ''' <summary>
    ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
    ''' Uses or updates the nullable value getter command.
    ''' </summary>
    ''' <param name="element">Specifies the <see cref="IScpiValueBase">existing element</see> where the getter command is stored.</param>
    ''' <param name="modalityName">Specifies the modality name.</param>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2}?' where 
    ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see>, 
    ''' the second item is the <paramref name="modalityName">modality name</paramref> and
    ''' the second item is a <paramref name="command">command</paramref>.
    ''' </param>
    ''' <param name="existingValue">The existing value. The instrument is accessed if this value has no value.</param>
    ''' <param name="Parser">The <see cref="isr.Core.IParser(of boolean)">boolean Parser.</see></param>
    ''' <param name="access">The desired <see cref="ResourceAccessLevels">access level</see> to the instrument.</param>
    Public Function Getter(ByVal element As IScpiValueBase,
                       ByVal modalityName As String, ByVal subHeader As String,
                       ByVal existingValue As Nullable(Of Boolean), ByVal parser As isr.Core.IParser(Of Boolean),
                       ByVal access As ResourceAccessLevels) As Nullable(Of Boolean) Implements IController.Getter
        If access.IsDeviceAccess OrElse Not existingValue.HasValue Then
            If String.IsNullOrWhiteSpace(element.GetterCommand) Then
                element.GetterCommand = Syntax.BuildQuery(Me.SyntaxHeader, modalityName, subHeader)
            End If
            ' existingValue = Me.QueryBoolean(modalityName, subHeader)
            existingValue = Me.Port.QueryBoolean(element.GetterCommand, parser)
        End If
        Return existingValue
    End Function

#End Region

#Region " GETTERS w/ SCPI VALUE "

    ''' <summary>
    ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
    ''' Uses the nullable value getter command.
    ''' </summary>
    ''' <param name="element">Specifies the <see cref="IScpiValueBase">existing element</see> where the getter command is stored.</param>
    Public Function Getter(ByVal element As IScpiValue(Of Boolean)) As IScpiValue(Of Boolean) Implements IController.Getter
        Return Getter(element, CType(element.FormatterParser, Core.IParser(Of Boolean)))
    End Function

    ''' <summary>
    ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
    ''' Uses the nullable value getter command.
    ''' </summary>
    ''' <param name="element">Specifies the <see cref="IScpiValueBase">existing element</see> where the getter command is stored.</param>
    ''' <param name="Parser">The <see cref="isr.Core.IParser(of boolean)">boolean Parser.</see></param>
    Public Function Getter(ByVal element As IScpiValue(Of Boolean),
                       ByVal parser As Core.IParser(Of Boolean)) As IScpiValue(Of Boolean) Implements IController.Getter
        element.Value = Me.Port.QueryBoolean(element.GetterCommand, parser)
        element.ActualValue = element.Value
        Return element
    End Function

#End Region

#Region " GETTERS w/ ACCESS w/ SCPI VALUE "

    ''' <summary>
    ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
    ''' Uses the nullable value getter command.
    ''' </summary>
    ''' <param name="element">Specifies the <see cref="IScpiValueBase">existing element</see> where the getter command is stored.</param>
    ''' <param name="Parser">The <see cref="isr.Core.IParser(of boolean)">boolean Parser.</see></param>
    ''' <param name="access">The desired <see cref="ResourceAccessLevels">access level</see> to the instrument.</param>
    Public Function Getter(ByVal element As IScpiValue(Of Boolean),
                       ByVal parser As isr.Core.IParser(Of Boolean),
                       ByVal access As ResourceAccessLevels) As IScpiValue(Of Boolean) Implements IController.Getter
        If access.IsDeviceAccess OrElse Not element.Value.HasValue Then
            element.Value = Me.Port.QueryBoolean(element.GetterCommand, parser)
            element.ActualValue = element.Value
        End If
        Return element
    End Function

    ''' <summary>
    ''' Gets a value from the instrument <see cref="SyntaxHeader">SCPI subsystem</see> <paramref name="subHeader">element</paramref>.
    ''' Applies readings to the <paramref name="element">existing element</paramref>
    ''' </summary>
    ''' <param name="element">Specifies the existing element where the instrument readings are applied.</param>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}?' where 
    ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see> and 
    ''' the second item is a <paramref name="command">command</paramref>
    ''' </param>
    ''' <param name="Parser">The <see cref="isr.Core.IParser(of boolean)">boolean Parser.</see></param>
    ''' <param name="access">The desired <see cref="ResourceAccessLevels">access level</see> to the instrument.</param>
    Public Function Getter(ByVal element As IScpiValue(Of Boolean),
                       ByVal subHeader As String,
                       ByVal parser As isr.Core.IParser(Of Boolean),
                       ByVal access As ResourceAccessLevels) As IScpiValue(Of Boolean) Implements IController.Getter
        If access.IsDeviceAccess OrElse Not element.Value.HasValue Then
            If String.IsNullOrWhiteSpace(element.GetterCommand) Then
                element.GetterCommand = Syntax.BuildQuery(Me.SyntaxHeader, subHeader)
            End If
            ' element.Value = Me.QueryBoolean(subHeader)
            element.Value = Me.Port.QueryBoolean(element.GetterCommand, parser)
            element.ActualValue = element.Value
        End If
        Return element
    End Function

    ''' <summary>
    ''' Gets a value from the instrument <see cref="SyntaxHeader">SCPI subsystem</see> <paramref name="modalityName">modality</paramref> and <paramref name="subHeader">element</paramref>.
    ''' Applies readings to the <paramref name="element">existing element</paramref>
    ''' </summary>
    ''' <param name="element">Specifies the existing element where the instrument readings are applied.</param>
    ''' <param name="modalityName">Specifies the modality name.</param>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2}?' where 
    ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see>, 
    ''' the second item is the <paramref name="modalityName">modality name</paramref> and
    ''' the second item is a <paramref name="command">command</paramref>.
    ''' </param>
    ''' <param name="Parser">The <see cref="isr.Core.IParser(of boolean)">boolean Parser.</see></param>
    ''' <param name="access">The desired <see cref="ResourceAccessLevels">access level</see> to the instrument.</param>
    Public Function Getter(ByVal element As IScpiValue(Of Boolean),
                       ByVal modalityName As String, ByVal subHeader As String,
                       ByVal parser As isr.Core.IParser(Of Boolean), ByVal access As ResourceAccessLevels) As IScpiValue(Of Boolean) Implements IController.Getter
        element.Value = Me.Getter(element, modalityName, subHeader, element.Value, parser, access)
        element.ActualValue = element.Value
        Return element
    End Function

#End Region

#Region " SETTERS "

    ''' <summary>
    ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1} {2}' where 
    ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see>, 
    ''' the second item is a <paramref name="command">command</paramref>
    ''' the third item represents a <paramref name="value">numeric value</paramref>.
    ''' </param>
    ''' <param name="value">The value to set in the instrument.</param>
    ''' <param name="formatterParser">The <see cref="isr.Core.IFormatterParser(of boolean)">boolean formatter and parser.</see></param>
    Public Function Setter(ByVal subHeader As String, ByVal value As Boolean, ByVal formatterParser As isr.Core.IFormatterParser(Of Boolean), ByVal verify As Boolean) As Boolean Implements IController.Setter
        Dim scpiMessage As String = Syntax.BuildCommand(Me.SyntaxHeader, subHeader, value, formatterParser)
        Me.Port.WriteLine(scpiMessage)
        If verify Then
            Dim actual As Nullable(Of Boolean) = Me.QueryBoolean(scpiMessage, formatterParser)
            Return actual.HasValue AndAlso (value = actual.Value)
        Else
            Return True
        End If
    End Function

    ''' <summary>
    ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="modalityName">Specifies the modality name.</param>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2} {3}' where 
    ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see>, 
    ''' the second item is the <paramref name="modalityName">modality name</paramref>, 
    ''' the third item is a <paramref name="command">command</paramref>
    ''' the fourth item represents a <paramref name="value">numeric value</paramref>.
    ''' </param>
    ''' <param name="value">The value to set in the instrument.</param>
    ''' <param name="formatterParser">The <see cref="isr.Core.IFormatterParser(of boolean)">boolean formatter and parser.</see></param>
    Public Function Setter(ByVal modalityName As String, ByVal subHeader As String, ByVal formatterParser As isr.Core.IFormatterParser(Of Boolean), ByVal value As Boolean, ByVal verify As Boolean) As Boolean Implements IController.Setter
        Dim scpiMessage As String = Syntax.BuildCommand(Me.SyntaxHeader, modalityName, subHeader, value, formatterParser)
        Me.Port.WriteLine(scpiMessage)
        If verify Then
            Dim actual As Nullable(Of Boolean) = Me.QueryBoolean(modalityName, subHeader, formatterParser)
            Return actual.HasValue AndAlso (value = actual.Value)
        Else
            Return True
        End If
    End Function

#End Region

#Region " SETTERS w/ ACCESS "

    ''' <summary>
    ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1} {2}' where 
    ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see> 
    ''' the second item is a <paramref name="command">command</paramref> and 
    ''' the third item is the <paramref name="value">value</paramref>.
    ''' </param>
    ''' <param name="existingValue">The existing value. The instrument is accessed if this value has no value or is not equal to the specified <paramref name="value"/>.</param>
    ''' <param name="formatter">The <see cref="isr.Core.IFormatter(of boolean)">boolean formatter.</see></param>
    ''' <param name="access">The desired <see cref="ResourceAccessLevels">access level</see> to the instrument.</param>
    Public Function Setter(ByVal subHeader As String,
                       ByVal existingValue As Nullable(Of Boolean), ByVal value As Boolean,
                       ByVal formatter As isr.Core.IFormatter(Of Boolean),
                       ByVal access As ResourceAccessLevels) As Nullable(Of Boolean) Implements IController.Setter
        If access.IsVerifyAccess Then
            Throw New ArgumentException("Verification not supported by this method", "access")
        End If
        If access.IsDeviceAccess OrElse (value <> existingValue) Then
            Me.Port.WriteLine(Syntax.BuildCommand(Me.SyntaxHeader, subHeader, value, formatter))
        End If
        Return New Nullable(Of Boolean)(value)
    End Function

    ''' <summary>
    ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="modalityName">Specifies the modality name.</param>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2} {3}' where 
    ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see>, 
    ''' the second item is the <paramref name="modalityName">modality name</paramref>, 
    ''' the second item is a <paramref name="command">command</paramref> and
    ''' the fourth item represents a <paramref name="value">numeric value</paramref>.
    ''' </param>
    ''' <param name="existingValue">The existing value. The instrument is accessed if this value has no value or is not equal to the specified <paramref name="value"/>.</param>
    ''' <param name="value">The value to set in the instrument.</param>
    ''' <param name="formatter">The <see cref="isr.Core.IFormatter(of boolean)">boolean formatter.</see></param>
    ''' <param name="access">The desired <see cref="ResourceAccessLevels">access level</see> to the instrument.</param>
    Public Function Setter(ByVal modalityName As String, ByVal subHeader As String,
                       ByVal existingValue As Nullable(Of Boolean), ByVal value As Boolean,
                       ByVal formatter As isr.Core.IFormatter(Of Boolean),
                       ByVal access As ResourceAccessLevels) As Nullable(Of Boolean) Implements IController.Setter
        If access.IsVerifyAccess Then
            Throw New ArgumentException("Verification not supported by this method", "access")
        End If
        If access.IsDeviceAccess OrElse (value <> existingValue) Then
            Me.Port.WriteLine(Syntax.BuildCommand(Me.SyntaxHeader, modalityName, subHeader, value, formatter))
        End If
        Return New Nullable(Of Boolean)(value)
    End Function

#End Region

#Region " SETTERS w/ ELEMENT "

    ''' <summary>
    ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="element">Specifies the <see cref="IScpiValueBase">existing element</see> where the getter command is stored.</param>
    ''' <param name="value">Specifies the value to be set using the element <see cref="IScpiValueBase.SetterCommandFormat">command</see>.</param>
    ''' <param name="existingValue">The existing value. The instrument is accessed if this value has no value or is not equal to the specified <paramref name="value"/>.</param>
    ''' <param name="formatter">The <see cref="isr.Core.IFormatter(of boolean)">boolean formatter.</see></param>
    Public Function Setter(ByVal element As IScpiValueBase,
                       ByVal existingValue As Boolean, ByVal value As Boolean,
                       ByVal formatter As isr.Core.IFormatter(Of Boolean)) As Boolean Implements IController.Setter
        If value <> existingValue Then
            If Not String.IsNullOrWhiteSpace(element.SetterCommandFormat) Then
                element.SetterCommand = String.Format(Globalization.CultureInfo.InvariantCulture,
                                                element.SetterCommandFormat, formatter.Text(value))
            ElseIf String.IsNullOrWhiteSpace(element.SetterCommand) Then
                Throw New ArgumentException("Element setter command is empty", "element")
            End If
            Me.Port.WriteLine(element.SetterCommand)
        End If
        Return value
    End Function


#End Region

#Region " SETTERS w/ ACCESS w/ ELEMENT "

    ''' <summary>
    ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="element">Specifies the <see cref="IScpiValueBase">existing element</see> where the getter command is stored.</param>
    ''' <param name="value">Specifies the value to be set using the element <see cref="IScpiValueBase.SetterCommandFormat">command</see>.</param>
    ''' <param name="existingValue">The existing value. The instrument is accessed if this value has no value or is not equal to the specified <paramref name="value"/>.</param>
    ''' <param name="formatter">The <see cref="isr.Core.IFormatter(of boolean)">boolean formatter.</see></param>
    ''' <param name="access">The desired <see cref="ResourceAccessLevels">access level</see> to the instrument.</param>
    Public Function Setter(ByVal element As IScpiValueBase,
                       ByVal existingValue As Boolean, ByVal value As Boolean,
                       ByVal formatter As isr.Core.IFormatter(Of Boolean),
                       ByVal access As ResourceAccessLevels) As Boolean Implements IController.Setter
        If access.IsVerifyAccess Then
            Throw New ArgumentException("Verification not supported by this method", "access")
        End If
        If access.IsDeviceAccess OrElse value <> existingValue Then
            If Not String.IsNullOrWhiteSpace(element.SetterCommandFormat) Then
                element.SetterCommand = String.Format(Globalization.CultureInfo.InvariantCulture,
                                                element.SetterCommandFormat, formatter.Text(value))
            ElseIf String.IsNullOrWhiteSpace(element.SetterCommand) Then
                Throw New ArgumentException("Element setter command is empty", "element")
            End If
            Me.Port.WriteLine(element.SetterCommand)
        End If
        Return value
    End Function

    ''' <summary>
    ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
    ''' Uses or updates the nullable value getter command.
    ''' </summary>
    ''' <param name="element">Specifies the <see cref="IScpiValueBase">existing element</see> where the getter command is stored.</param>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1} {2}' where 
    ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see> 
    ''' the second item is a <paramref name="command">command</paramref> and 
    ''' the third item is the <paramref name="value">value</paramref>.
    ''' </param>
    ''' <param name="value">The value which to set.</param>
    ''' <param name="existingValue">The existing value. The instrument is accessed if this value has no value or is not equal to the specified <paramref name="value"/>.</param>
    ''' <param name="formatter">The <see cref="isr.Core.IFormatter(of boolean)">boolean formatter.</see></param>
    ''' <param name="access">The desired <see cref="ResourceAccessLevels">access level</see> to the instrument.</param>
    Public Function Setter(ByVal element As IScpiValueBase,
                       ByVal subHeader As String,
                       ByVal existingValue As Nullable(Of Boolean), ByVal value As Boolean,
                       ByVal formatter As isr.Core.IFormatter(Of Boolean),
                       ByVal access As ResourceAccessLevels) As Nullable(Of Boolean) Implements IController.Setter
        If access.IsVerifyAccess Then
            Throw New ArgumentException("Verification not supported by this method", "access")
        End If
        If access.IsDeviceAccess OrElse (value <> existingValue) Then
            If String.IsNullOrWhiteSpace(element.SetterCommandFormat) Then
                element.SetterCommandFormat = Syntax.BuildCommandFormat(Me.SyntaxHeader, subHeader)
            End If
            element.SetterCommand = Syntax.BuildCommand(Me.SyntaxHeader, subHeader, value, formatter)
            Me.Port.WriteLine(element.SetterCommand)
        End If
        Return New Nullable(Of Boolean)(value)
    End Function

    ''' <summary>
    ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
    ''' Uses or updates the nullable value getter command.
    ''' </summary>
    ''' <param name="element">Specifies the <see cref="IScpiValueBase">existing element</see> where the getter command is stored.</param>
    ''' <param name="modalityName">Specifies the modality name.</param>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2} {3}' where 
    ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see>, 
    ''' the second item is the <paramref name="modalityName">modality name</paramref>, 
    ''' the second item is a <paramref name="command">command</paramref> and
    ''' the fourth item represents a <paramref name="value">numeric value</paramref>.
    ''' </param>
    ''' <param name="existingValue">The existing value. The instrument is accessed if this value has no value or is not equal to the specified <paramref name="value"/>.</param>
    ''' <param name="value">The value to set in the instrument.</param>
    ''' <param name="formatter">The <see cref="isr.Core.IFormatter(of boolean)">boolean formatter.</see></param>
    ''' <param name="access">The desired <see cref="ResourceAccessLevels">access level</see> to the instrument.</param>
    Public Function Setter(ByVal element As IScpiValueBase,
                       ByVal modalityName As String, ByVal subHeader As String,
                       ByVal existingValue As Nullable(Of Boolean), ByVal value As Boolean,
                       ByVal formatter As isr.Core.IFormatter(Of Boolean),
                       ByVal access As ResourceAccessLevels) As Nullable(Of Boolean) Implements IController.Setter
        If access.IsVerifyAccess Then
            Throw New ArgumentException("Verification not supported by this method", "access")
        End If
        If access.IsDeviceAccess OrElse (value <> existingValue) Then
            If String.IsNullOrWhiteSpace(element.SetterCommandFormat) Then
                element.SetterCommandFormat = Syntax.BuildCommandFormat(Me.SyntaxHeader, modalityName, subHeader)
            End If
            element.SetterCommand = Syntax.BuildCommand(Me.SyntaxHeader, modalityName, subHeader, value, formatter)
            Me.Port.WriteLine(element.SetterCommand)
        End If
        Return New Nullable(Of Boolean)(value)
    End Function

#End Region

#Region " SETTERS w/ SCPI VALUE "

    ''' <summary>
    ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
    ''' Applies readings to the <paramref name="element">existing element</paramref>
    ''' Assume element has both <see cref="IScpiValueBase.GetterCommand">getter</see> and 
    ''' <see cref="IScpiValueBase.SetterCommand">setter</see> commands specified.
    ''' </summary>
    ''' <param name="element">Specifies the element holding the existing value, which is updated.</param>
    ''' <param name="value">The value to set in the instrument.</param>
    Public Function Setter(ByVal element As IScpiValue(Of Boolean), ByVal value As Boolean) As IScpiValue(Of Boolean) Implements IController.Setter
        element.Value = Me.Setter(CType(element, IScpiValueBase), element.Value.Value, value, CType(element.FormatterParser, Core.IFormatterParser(Of Boolean)))
        Return element
    End Function

    ''' <summary>
    ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
    ''' Applies readings to the <paramref name="element">existing element</paramref>
    ''' Assume element has both <see cref="IScpiValueBase.GetterCommand">getter</see> and 
    ''' <see cref="IScpiValueBase.SetterCommand">setter</see> commands specified.
    ''' </summary>
    ''' <param name="element">Specifies the element holding the existing value, which is updated.</param>
    ''' <param name="value">The value to set in the instrument.</param>
    ''' <param name="formatterParser">The <see cref="isr.Core.IFormatterParser(of boolean)">boolean formatter and parser.</see></param>
    Public Function Setter(ByVal element As IScpiValue(Of Boolean), ByVal value As Boolean,
                       ByVal formatterParser As isr.Core.IFormatterParser(Of Boolean)) As IScpiValue(Of Boolean) Implements IController.Setter
        element.Value = Me.Setter(CType(element, IScpiValueBase), element.Value.Value, value, formatterParser)
        Return element
    End Function

#End Region

#Region " SETTERS w/ ACCESS w/ SCPI VALUE "

    ''' <summary>
    ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
    ''' Applies readings to the <paramref name="element">existing element</paramref>
    ''' Assume element has both <see cref="IScpiValueBase.GetterCommand">getter</see> and 
    ''' <see cref="IScpiValueBase.SetterCommand">setter</see> commands specified.
    ''' </summary>
    ''' <param name="element">Specifies the element holding the existing value, which is updated.</param>
    ''' <param name="value">The value to set in the instrument.</param>
    ''' <param name="formatterParser">The <see cref="isr.Core.IFormatterParser(of boolean)">boolean formatter and parser.</see></param>
    ''' <param name="access">The desired <see cref="ResourceAccessLevels">access level</see> to the instrument.</param>
    Public Function Setter(ByVal element As IScpiValue(Of Boolean), ByVal value As Boolean,
                       ByVal formatterParser As isr.Core.IFormatterParser(Of Boolean), ByVal access As ResourceAccessLevels) As IScpiValue(Of Boolean) Implements IController.Setter
        If access.IsDeviceAccess OrElse (value <> element.Value) Then
            element.Value = Me.Setter(CType(element, IScpiValueBase), element.Value.Value, value, formatterParser, access And Not ResourceAccessLevels.Verify)
        End If
        If access.IsVerifyAccess Then
            Me.Getter(element, formatterParser, ResourceAccessLevels.Device)
            element.Value = value
        ElseIf access.IsCacheAccess Then
            element.ActualValue = value
        End If
        Return element
    End Function

    ''' <summary>
    ''' Sets the instrument <see cref="SyntaxHeader">SCPI subsystem</see> <paramref name="subHeader">element</paramref> value.
    ''' Applies readings to the <paramref name="element">existing element</paramref>
    ''' </summary>
    ''' <param name="element">Specifies the existing element where the instrument readings are applied.</param>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1} {2}' where 
    ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see> 
    ''' the second item is a <paramref name="command">command</paramref> and 
    ''' the third item is the <paramref name="value">value</paramref>.
    ''' </param>
    ''' <param name="formatterParser">The <see cref="isr.Core.IFormatterParser(of boolean)">boolean formatter and parser.</see></param>
    ''' <param name="access">The desired <see cref="ResourceAccessLevels">access level</see> to the instrument.</param>
    ''' <remarks>
    ''' Use the following code in the calling methd if verification is used:
    ''' <code>
    ''' If Not element.IsVerified.GetValueOrDefault(False) Then
    '''   If element.Value.HasValue AndAlso element.ActualValue.HasValue Then
    '''     return String.Format(Globalization.CultureInfo.CurrentCulture, 
    '''            "Instrument settings applied using the command '{0}' did not verify. Expected={1}. Actual={2}.", 
    '''            element.Value, element.ActualValue.Value)
    '''   Else
    '''     return "Failed reading values from the instrument."
    '''   End If
    ''' End If
    ''' </code>
    ''' </remarks>
    Public Function Setter(ByVal element As IScpiValue(Of Boolean),
                           ByVal subHeader As String,
                           ByVal value As Boolean,
                           ByVal formatterParser As isr.Core.IFormatterParser(Of Boolean), ByVal access As ResourceAccessLevels) As IScpiValue(Of Boolean) Implements IController.Setter
        element.Value = Me.Setter(CType(element, IScpiValueBase), subHeader, element.Value, value, formatterParser, access And Not ResourceAccessLevels.Verify)
        If access.IsVerifyAccess Then
            Me.Getter(element, subHeader, formatterParser, ResourceAccessLevels.Device)
            element.Value = value
        ElseIf access.IsCacheAccess Then
            element.ActualValue = value
        End If
        Return element
    End Function

    ''' <summary>
    ''' Sets the instrument <see cref="SyntaxHeader">SCPI subsystem</see> <paramref name="modalityName">modality</paramref> <paramref name="subHeader">element</paramref> value.
    ''' </summary>
    ''' <param name="element">Specifies the existing element where the instrument readings are applied.</param>
    ''' <param name="modalityName">Specifies the modality name.</param>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2} {3}' where 
    ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see>, 
    ''' the second item is the <paramref name="modalityName">modality name</paramref>, 
    ''' the second item is a <paramref name="command">command</paramref> and
    ''' the fourth item represents a <paramref name="value">numeric value</paramref>.
    ''' </param>
    ''' <param name="formatterParser">The <see cref="isr.Core.IFormatterParser(of boolean)">boolean formatter and parser</see></param>
    ''' <param name="access">The desired <see cref="ResourceAccessLevels">access level</see> to the instrument.</param>
    ''' <remarks>
    ''' Use the following code in the calling methd if verification is used:
    ''' <code>
    ''' If Not element.IsVerified.GetValueOrDefault(False) Then
    '''   If element.Value.HasValue AndAlso element.ActualValue.HasValue Then
    '''     return String.Format(Globalization.CultureInfo.CurrentCulture, 
    '''            "Instrument settings applied using the command '{0}' did not verify. Expected={1}. Actual={2}.", 
    '''            element.Value, element.ActualValue.Value)
    '''   Else
    '''     return "Failed reading values from the instrument."
    '''   End If
    ''' End If
    ''' </code>
    ''' </remarks>
    Public Function Setter(ByVal element As IScpiValue(Of Boolean),
                           ByVal modalityName As String, ByVal subHeader As String,
                           ByVal value As Boolean,
                           ByVal formatterParser As isr.Core.IFormatterParser(Of Boolean), ByVal access As ResourceAccessLevels) As IScpiValue(Of Boolean) Implements IController.Setter
        element.Value = Me.Setter(CType(element, IScpiValueBase), modalityName, subHeader, element.Value, value, formatterParser, access And Not ResourceAccessLevels.Verify)
        If access.IsVerifyAccess Then
            Me.Getter(element, modalityName, subHeader, formatterParser, ResourceAccessLevels.Device)
            element.Value = value
        ElseIf access.IsCacheAccess Then
            element.ActualValue = value
        End If
        Return element
    End Function

#End Region

#End Region

#Region " DOUBLE "

#Region " QUERRIES: DOUBLE "

    ''' <summary>
    ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}?' where 
    ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see> and 
    ''' the second item is a <paramref name="command">command</paramref>.
    ''' </param>
    Public Function QueryDouble(ByVal subHeader As String) As Nullable(Of Double) Implements IController.QueryDouble
        Return Me.Port.QueryDouble(Syntax.BuildQuery(Me.SyntaxHeader, subHeader))
    End Function

    ''' <summary>
    ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="modalityName">Specifies the modality name.</param>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2}?' where 
    ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see>, 
    ''' the second item is the <paramref name="modalityName">modality name</paramref> and 
    ''' the thrid item is a <paramref name="command">command</paramref>.
    ''' </param>
    Public Function QueryDouble(ByVal modalityName As String, ByVal subHeader As String) As Nullable(Of Double) Implements IController.QueryDouble
        Return Me.Port.QueryDouble(Syntax.BuildQuery(Me.SyntaxHeader, modalityName, subHeader))
    End Function

#End Region

#Region " GETTERS w/ ACCESS "

    ''' <summary>
    ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}?' where 
    ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see> and 
    ''' the second item is a <paramref name="command">command</paramref>
    ''' </param>
    ''' <param name="existingValue">Specifies the existing value.</param>
    Public Function Getter(ByVal subHeader As String,
                       ByVal existingValue As Nullable(Of Double),
                       ByVal access As ResourceAccessLevels) As Nullable(Of Double) Implements IController.Getter
        If access.IsDeviceAccess OrElse Not existingValue.HasValue Then
            existingValue = Me.QueryDouble(subHeader)
        End If
        Return existingValue
    End Function

    ''' <summary>
    ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="modalityName">Specifies the modality name.</param>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2}?' where 
    ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see>, 
    ''' the second item is the <paramref name="modalityName">modality name</paramref> and
    ''' the second item is a <paramref name="command">command</paramref>.
    ''' </param>
    ''' <param name="existingValue">Specifies the existing value.</param>
    Public Function Getter(ByVal modalityName As String, ByVal subHeader As String,
                       ByVal existingValue As Nullable(Of Double),
                       ByVal access As ResourceAccessLevels) As Nullable(Of Double) Implements IController.Getter
        If access.IsDeviceAccess OrElse Not existingValue.HasValue Then
            existingValue = Me.QueryDouble(modalityName, subHeader)
        End If
        Return existingValue
    End Function

#End Region

#Region " GETTERS w/ ACCESS w/ ELEMENT "

    ''' <summary>
    ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
    ''' Uses or updates the nullable value getter command.
    ''' </summary>
    ''' <param name="element">Specifies the <see cref="IScpiValueBase">existing element</see> where the getter command is stored.</param>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}?' where 
    ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see> and 
    ''' the second item is a <paramref name="command">command</paramref>
    ''' </param>
    ''' <param name="existingValue">Specifies the existing value.</param>
    Public Function Getter(ByVal element As IScpiValueBase,
                       ByVal subHeader As String,
                       ByVal existingValue As Nullable(Of Double),
                       ByVal access As ResourceAccessLevels) As Nullable(Of Double) Implements IController.Getter
        If access.IsDeviceAccess OrElse Not existingValue.HasValue Then
            If String.IsNullOrWhiteSpace(element.GetterCommand) Then
                element.GetterCommand = Syntax.BuildQuery(Me.SyntaxHeader, subHeader)
            End If
            'existingValue = Me.QueryDouble(subHeader)
            existingValue = Me.Port.QueryDouble(element.GetterCommand)
        End If
        Return existingValue
    End Function

    ''' <summary>
    ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
    ''' Uses or updates the nullable value getter command.
    ''' </summary>
    ''' <param name="element">Specifies the <see cref="IScpiValueBase">existing element</see> where the getter command is stored.</param>
    ''' <param name="modalityName">Specifies the modality name.</param>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2}?' where 
    ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see>, 
    ''' the second item is the <paramref name="modalityName">modality name</paramref> and
    ''' the second item is a <paramref name="command">command</paramref>.
    ''' </param>
    ''' <param name="existingValue">Specifies the existing value.</param>
    Public Function Getter(ByVal element As IScpiValueBase,
                       ByVal modalityName As String, ByVal subHeader As String,
                       ByVal existingValue As Nullable(Of Double),
                       ByVal access As ResourceAccessLevels) As Nullable(Of Double) Implements IController.Getter
        If access.IsDeviceAccess OrElse Not existingValue.HasValue Then
            If String.IsNullOrWhiteSpace(element.GetterCommand) Then
                element.GetterCommand = Syntax.BuildQuery(Me.SyntaxHeader, modalityName, subHeader)
            End If
            ' existingValue = Me.QueryDouble(subHeader)
            existingValue = Me.Port.QueryDouble(element.GetterCommand)
        End If
        Return existingValue
    End Function

#End Region

#Region " GETTERS w/ ACCESS w/ SCPI VALUE "

    ''' <summary>
    ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
    ''' Uses or updates the nullable value getter command.
    ''' </summary>
    ''' <param name="element">Specifies the <see cref="IScpiValueBase">existing element</see> where the getter command is stored.</param>
    Public Function Getter(ByVal element As IScpiValue(Of Double), ByVal access As ResourceAccessLevels) As IScpiValue(Of Double) Implements IController.Getter
        If access.IsDeviceAccess OrElse Not element.Value.HasValue Then
            element.Value = Me.Port.QueryDouble(element.GetterCommand)
            element.ActualValue = element.Value
        End If
        Return element
    End Function

    ''' <summary>
    ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
    ''' Applies readings to the <paramref name="element">existing element</paramref>
    ''' </summary>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}?' where 
    ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see> and 
    ''' the second item is a <paramref name="command">command</paramref>
    ''' </param>
    ''' <param name="element">Specifies the element holding the existing value, which is updated.</param>
    Public Function Getter(ByVal element As IScpiValue(Of Double),
                       ByVal subHeader As String,
                       ByVal access As ResourceAccessLevels) As IScpiValue(Of Double) Implements IController.Getter
        element.Value = Me.Getter(element, subHeader, element.Value, access)
        element.ActualValue = element.Value
        Return element
    End Function

    ''' <summary>
    ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
    ''' Applies readings to the <paramref name="element">existing element</paramref>
    ''' </summary>
    ''' <param name="modalityName">Specifies the modality name.</param>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2}?' where 
    ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see>, 
    ''' the second item is the <paramref name="modalityName">modality name</paramref> and
    ''' the second item is a <paramref name="command">command</paramref>.
    ''' </param>
    ''' <param name="element">Specifies the element holding the existing value, which is updated.</param>
    Public Function Getter(ByVal element As IScpiValue(Of Double),
                       ByVal modalityName As String, ByVal subHeader As String,
                       ByVal access As ResourceAccessLevels) As IScpiValue(Of Double) Implements IController.Getter
        element.Value = Me.Getter(element, modalityName, subHeader, element.Value, access)
        element.ActualValue = element.Value
        Return element
    End Function

#End Region

#Region " SETTERS "

    ''' <summary>
    ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1} {2}' where 
    ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see>, 
    ''' the second item is a <paramref name="command">command</paramref>
    ''' the third item represents a <paramref name="value">numeric value</paramref>.
    ''' </param>
    ''' <param name="value">The value to set in the instrument.</param>
    Public Function Setter(ByVal subHeader As String, ByVal value As Double, ByVal verify As Boolean) As Boolean Implements IController.Setter
        Dim scpiMessage As String = Syntax.BuildCommand(Me.SyntaxHeader, subHeader, value)
        Me.Port.WriteLine(scpiMessage)
        If verify Then
            Dim actual As Nullable(Of Double) = Me.QueryDouble(scpiMessage)
            Return actual.HasValue AndAlso (value = actual.Value)
        Else
            Return True
        End If
    End Function

    ''' <summary>
    ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="modalityName">Specifies the modality name.</param>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2} {3}' where 
    ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see>, 
    ''' the second item is the <paramref name="modalityName">modality name</paramref>, 
    ''' the third item is a <paramref name="command">command</paramref>
    ''' the fourth item represents a <paramref name=" value">numeric value</paramref>.
    ''' </param>
    ''' <param name="value">The value to set in the instrument.</param>
    Public Function Setter(ByVal modalityName As String, ByVal subHeader As String, ByVal value As Double, ByVal verify As Boolean) As Boolean Implements IController.Setter
        Dim scpiMessage As String = Syntax.BuildCommand(Me.SyntaxHeader, modalityName, subHeader, value)
        Me.Port.WriteLine(scpiMessage)
        If verify Then
            Dim actual As Nullable(Of Double) = Me.QueryDouble(modalityName, subHeader)
            Return actual.HasValue AndAlso (value = actual.Value)
        Else
            Return True
        End If
    End Function

#End Region

#Region " SETTERS w/ ACCESS "

    ''' <summary>
    ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1} {2}' where 
    ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see> 
    ''' the second item is a <paramref name="command">command</paramref> and 
    ''' the third item is the <paramref name="value">value</paramref>.
    ''' </param>
    ''' <param name="existingValue">Specifies the existing value.</param>
    Public Function Setter(ByVal subHeader As String,
                       ByVal existingValue As Nullable(Of Double), ByVal value As Double,
                       ByVal access As ResourceAccessLevels) As Nullable(Of Double) Implements IController.Setter
        If access.IsVerifyAccess Then
            Throw New ArgumentException("Verification not supported by this method", "access")
        End If
        If access.IsDeviceAccess OrElse Not ScpiDouble.Approximates(value, existingValue, Single.Epsilon) Then
            Me.Port.WriteLine(Syntax.BuildCommand(Me.SyntaxHeader, subHeader, value))
        End If
        Return New Nullable(Of Double)(value)
    End Function

    ''' <summary>
    ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="modalityName">Specifies the modality name.</param>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2} {3}' where 
    ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see>, 
    ''' the second item is the <paramref name="modalityName">modality name</paramref>, 
    ''' the second item is a <paramref name="command">command</paramref> and
    ''' the fourth item represents a <paramref name="value">numeric value</paramref>.
    ''' </param>
    ''' <param name="existingValue">Specifies the existing value.</param>
    Public Function Setter(ByVal modalityName As String, ByVal subHeader As String,
                       ByVal existingValue As Nullable(Of Double), ByVal value As Double,
                       ByVal access As ResourceAccessLevels) As Nullable(Of Double) Implements IController.Setter
        If access.IsVerifyAccess Then
            Throw New ArgumentException("Verification not supported by this method", "access")
        End If
        If access.IsDeviceAccess OrElse Not ScpiDouble.Approximates(value, existingValue, Single.Epsilon) Then
            Me.Port.WriteLine(Syntax.BuildCommand(Me.SyntaxHeader, modalityName, subHeader, value))
        End If
        Return New Nullable(Of Double)(value)
    End Function

#End Region

#Region " SETTERS w/ ACCESS w/ ELEMENT "

    ''' <summary>
    ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="element">Specifies the <see cref="IScpiValueBase">existing element</see> where the getter command is stored.</param>
    ''' <param name="existingValue">Specifies the existing element value where the instrument.</param>
    ''' <param name="value">The value to be set using the element <see cref="IScpiValueBase.SetterCommandFormat">command</see>.</param>
    ''' <param name="access">The desired <see cref="ResourceAccessLevels">access level</see> to the instrument.</param>
    Public Function Setter(ByVal element As IScpiValueBase,
                       ByVal existingValue As Double, ByVal value As Double,
                       ByVal access As ResourceAccessLevels) As Double Implements IController.Setter
        If access.IsVerifyAccess Then
            Throw New ArgumentException("Verification not supported by this method", "access")
        End If
        If access.IsDeviceAccess OrElse Not ScpiDouble.Approximates(value, existingValue, Single.Epsilon) Then
            If Not String.IsNullOrWhiteSpace(element.SetterCommandFormat) Then
                element.SetterCommand = String.Format(Globalization.CultureInfo.InvariantCulture, element.SetterCommandFormat, value)
            ElseIf String.IsNullOrWhiteSpace(element.SetterCommand) Then
                Throw New ArgumentException("Element setter command is empty", "element")
            End If
            Me.Port.WriteLine(element.SetterCommand)
        End If
        Return value
    End Function

    ''' <summary>
    ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
    ''' Uses or updates the nullable value getter command.
    ''' </summary>
    ''' <param name="element">Specifies the <see cref="IScpiValueBase">existing element</see> where the getter command is stored.</param>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1} {2}' where 
    ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see> 
    ''' the second item is a <paramref name="command">command</paramref> and 
    ''' the third item is the <paramref name="value">value</paramref>.
    ''' </param>
    ''' <param name="existingValue">Specifies the existing value.</param>
    ''' <param name="value">The value to set in the instrument.</param>
    ''' <param name="access">The desired <see cref="ResourceAccessLevels">access level</see> to the instrument.</param>
    Public Function Setter(ByVal element As IScpiValueBase,
                       ByVal subHeader As String,
                       ByVal existingValue As Nullable(Of Double), ByVal value As Double,
                       ByVal access As ResourceAccessLevels) As Nullable(Of Double) Implements IController.Setter
        If access.IsVerifyAccess Then
            Throw New ArgumentException("Verification not supported by this method", "access")
        End If
        If access.IsDeviceAccess OrElse Not ScpiDouble.Approximates(value, existingValue, Single.Epsilon) Then
            If String.IsNullOrWhiteSpace(element.SetterCommandFormat) Then
                element.SetterCommandFormat = Syntax.BuildCommandFormat(Me.SyntaxHeader, subHeader)
            End If
            element.SetterCommand = Syntax.BuildCommand(Me.SyntaxHeader, subHeader, value)
            Me.Port.WriteLine(element.SetterCommand)
        End If
        Return New Nullable(Of Double)(value)
    End Function

    ''' <summary>
    ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
    ''' Uses or updates the nullable value getter command.
    ''' </summary>
    ''' <param name="element">Specifies the <see cref="IScpiValueBase">existing element</see> where the getter command is stored.</param>
    ''' <param name="modalityName">Specifies the modality name.</param>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2} {3}' where 
    ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see>, 
    ''' the second item is the <paramref name="modalityName">modality name</paramref>, 
    ''' the second item is a <paramref name="command">command</paramref> and
    ''' the fourth item represents a <paramref name="value">numeric value</paramref>.
    ''' </param>
    ''' <param name="existingValue">Specifies the existing value.</param>
    ''' <param name="value">The value to set in the instrument.</param>
    ''' <param name="access">The desired <see cref="ResourceAccessLevels">access level</see> to the instrument.</param>
    Public Function Setter(ByVal element As IScpiValueBase,
                       ByVal modalityName As String, ByVal subHeader As String,
                       ByVal existingValue As Nullable(Of Double), ByVal value As Double,
                       ByVal access As ResourceAccessLevels) As Nullable(Of Double) Implements IController.Setter
        If access.IsVerifyAccess Then
            Throw New ArgumentException("Verification not supported by this method", "access")
        End If
        If access.IsDeviceAccess OrElse Not ScpiDouble.Approximates(value, existingValue, Single.Epsilon) Then
            If String.IsNullOrWhiteSpace(element.SetterCommandFormat) Then
                element.SetterCommandFormat = Syntax.BuildCommandFormat(Me.SyntaxHeader, modalityName, subHeader)
            End If
            element.SetterCommand = Syntax.BuildCommand(Me.SyntaxHeader, modalityName, subHeader, value)
            Me.Port.WriteLine(element.SetterCommand)
        End If
        Return New Nullable(Of Double)(value)
    End Function

#End Region

#Region " SETTERS w/ ACCESS w/ SCPI VALUE "

    ''' <summary>
    ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
    ''' Applies readings to the <paramref name="element">existing element</paramref>
    ''' Assume element has both <see cref="IScpiValueBase.GetterCommand">getter</see> and 
    ''' <see cref="IScpiValueBase.SetterCommand">setter</see> commands specified.
    ''' </summary>
    ''' <param name="element">Specifies the element holding the existing value, which is updated.</param>
    ''' <param name="value">The value to be set using the element <see cref="IScpiValueBase.SetterCommandFormat">command</see>.</param>
    ''' <param name="access">The desired <see cref="ResourceAccessLevels">access level</see> to the instrument.</param>
    Public Function Setter(ByVal element As IScpiValue(Of Double), ByVal value As Double, ByVal access As ResourceAccessLevels) As IScpiValue(Of Double) Implements IController.Setter
        If access.IsDeviceAccess OrElse (value <> element.Value) Then
            element.Value = Me.Setter(CType(element, IScpiValueBase), element.Value.Value, value, access And Not ResourceAccessLevels.Verify)
        End If
        If access.IsVerifyAccess Then
            Me.Getter(element, ResourceAccessLevels.Device)
            element.Value = value
        ElseIf access.IsCacheAccess Then
            element.ActualValue = value
        End If
        Return element
    End Function

    ''' <summary>
    ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
    ''' Applies readings to the <paramref name="element">existing element</paramref>
    ''' </summary>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1} {2}' where 
    ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see> 
    ''' the second item is a <paramref name="command">command</paramref> and 
    ''' the third item is the <paramref name="value">value</paramref>.
    ''' </param>
    ''' <param name="element">Specifies the element holding the existing value, which is updated.</param>
    Public Function Setter(ByVal element As IScpiValue(Of Double),
                       ByVal subHeader As String, ByVal value As Double,
                       ByVal access As ResourceAccessLevels) As IScpiValue(Of Double) Implements IController.Setter
        element.Value = Me.Setter(CType(element, IScpiValueBase), subHeader, element.Value, value, access And Not ResourceAccessLevels.Verify)
        ' element.Value = Me.Setter(CType(element, IScpiValueBase), subHeader, element.Value, element.BoundValue(value), access And Not ResourceAccessLevels.Verify)
        If access.IsVerifyAccess Then
            Me.Getter(element, subHeader, ResourceAccessLevels.Device)
            element.Value = value
        ElseIf access.IsCacheAccess Then
            element.ActualValue = value
        End If
        Return element
    End Function

    ''' <summary>
    ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
    ''' Applies readings to the <paramref name="element">existing element</paramref>
    ''' </summary>
    ''' <param name="modalityName">Specifies the modality name.</param>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2} {3}' where 
    ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see>, 
    ''' the second item is the <paramref name="modalityName">modality name</paramref>, 
    ''' the second item is a <paramref name="command">command</paramref> and
    ''' the fourth item represents a <paramref name="value">numeric value</paramref>.
    ''' </param>
    ''' <param name="element">Specifies the element holding the existing value, which is updated.</param>
    Public Function Setter(ByVal element As IScpiValue(Of Double),
                       ByVal modalityName As String, ByVal subHeader As String, ByVal value As Double,
                       ByVal access As ResourceAccessLevels) As IScpiValue(Of Double) Implements IController.Setter
        element.Value = Me.Setter(CType(element, IScpiValueBase), modalityName, subHeader, element.Value, value, access And Not ResourceAccessLevels.Verify)
        'element.Value = Me.Setter(CType(element, IScpiValueBase), modalityName, subHeader, element.Value, element.BoundValue(value), access And Not ResourceAccessLevels.Verify)
        If access.IsVerifyAccess Then
            Me.Getter(element, subHeader, ResourceAccessLevels.Device)
            element.Value = value
        ElseIf access.IsCacheAccess Then
            element.ActualValue = value
        End If
        Return element
    End Function

#End Region

#End Region

#Region " INTEGER "

#Region " QUERRIES "

    ''' <summary>
    ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}?' where 
    ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see> and 
    ''' the second item is a <paramref name="command">command</paramref>.
    ''' </param>
    Public Function QueryInteger(ByVal subHeader As String) As Nullable(Of Integer) Implements IController.QueryInteger
        Return Me.Port.QueryInteger(Syntax.BuildQuery(Me.SyntaxHeader, subHeader))
    End Function

    ''' <summary>
    ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="modalityName">Specifies the modality name.</param>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2}?' where 
    ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see>, 
    ''' the second item is the <paramref name="modalityName">modality name</paramref> and 
    ''' the thrid item is a <paramref name="command">command</paramref>.
    ''' </param>
    Public Function QueryInteger(ByVal modalityName As String, ByVal subHeader As String) As Nullable(Of Integer) Implements IController.QueryInteger
        Return Me.Port.QueryInteger(Syntax.BuildQuery(Me.SyntaxHeader, modalityName, subHeader))
    End Function

#End Region

#Region " QUERRIES - INFINITY "

    ''' <summary>
    ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}?' where 
    ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see> and 
    ''' the second item is a <paramref name="command">command</paramref>.
    ''' </param>
    Public Function QueryInfInteger(ByVal subHeader As String) As Nullable(Of Integer) Implements IController.QueryInfInteger
        Return Me.Port.QueryInfInteger(Syntax.BuildQuery(Me.SyntaxHeader, subHeader))
    End Function

    ''' <summary>
    ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="modalityName">Specifies the modality name.</param>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2}?' where 
    ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see>, 
    ''' the second item is the <paramref name="modalityName">modality name</paramref> and 
    ''' the thrid item is a <paramref name="command">command</paramref>.
    ''' </param>
    Public Function QueryInfInteger(ByVal modalityName As String, ByVal subHeader As String) As Nullable(Of Integer) Implements IController.QueryInfInteger
        Return Me.Port.QueryInfInteger(Syntax.BuildQuery(Me.SyntaxHeader, modalityName, subHeader))
    End Function

#End Region

#Region " GETTERS "

    ''' <summary>
    ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}?' where 
    ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see> and 
    ''' the second item is a <paramref name="command">command</paramref>
    ''' </param>
    ''' <param name="existingValue">Specifies the existing value.</param>
    Public Function Getter(ByVal subHeader As String,
                       ByVal existingValue As Nullable(Of Integer),
                       ByVal access As ResourceAccessLevels) As Nullable(Of Int32) Implements IController.Getter
        If access.IsDeviceAccess OrElse Not existingValue.HasValue Then
            existingValue = Me.QueryInteger(subHeader)
        End If
        Return existingValue
    End Function

    ''' <summary>
    ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="modalityName">Specifies the modality name.</param>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2}?' where 
    ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see>, 
    ''' the second item is the <paramref name="modalityName">modality name</paramref> and
    ''' the second item is a <paramref name="command">command</paramref>.
    ''' </param>
    ''' <param name="existingValue">Specifies the existing value.</param>
    Public Function Getter(ByVal modalityName As String, ByVal subHeader As String,
                       ByVal existingValue As Nullable(Of Integer),
                       ByVal access As ResourceAccessLevels) As Nullable(Of Integer) Implements IController.Getter
        If access.IsDeviceAccess OrElse Not existingValue.HasValue Then
            existingValue = Me.QueryInteger(modalityName, subHeader)
        End If
        Return existingValue
    End Function

    ''' <summary>
    ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}?' where 
    ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see> and 
    ''' the second item is a <paramref name="command">command</paramref>
    ''' </param>
    ''' <param name="existingValue">Specifies the existing value.</param>
    Public Function GetterInfinity(ByVal subHeader As String,
                               ByVal existingValue As Nullable(Of Integer),
                               ByVal access As ResourceAccessLevels) As Nullable(Of Integer) Implements IController.GetterInfinity
        If access.IsDeviceAccess OrElse Not existingValue.HasValue Then
            existingValue = Me.QueryInfInteger(subHeader)
        End If
        Return existingValue
    End Function

    ''' <summary>
    ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="modalityName">Specifies the modality name.</param>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2}?' where 
    ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see>, 
    ''' the second item is the <paramref name="modalityName">modality name</paramref> and
    ''' the second item is a <paramref name="command">command</paramref>.
    ''' </param>
    ''' <param name="existingValue">Specifies the existing value.</param>
    Public Function GetterInfinity(ByVal modalityName As String, ByVal subHeader As String,
                               ByVal existingValue As Nullable(Of Integer),
                               ByVal access As ResourceAccessLevels) As Nullable(Of Integer) Implements IController.GetterInfinity
        If access.IsDeviceAccess OrElse Not existingValue.HasValue Then
            existingValue = Me.QueryInfInteger(modalityName, subHeader)
        End If
        Return existingValue
    End Function

#End Region

#Region " GETTERS w/ ELEMENT "

    ''' <summary>
    ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
    ''' Uses or updates the nullable value getter command.
    ''' </summary>
    ''' <param name="element">Specifies the <see cref="IScpiValueBase">existing element</see> where the getter command is stored.</param>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}?' where 
    ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see> and 
    ''' the second item is a <paramref name="command">command</paramref>
    ''' </param>
    ''' <param name="existingValue">Specifies the existing value.</param>
    Public Function Getter(ByVal element As IScpiValueBase,
                       ByVal subHeader As String,
                       ByVal existingValue As Nullable(Of Integer),
                       ByVal access As ResourceAccessLevels) As Nullable(Of Int32) Implements IController.Getter
        If access.IsDeviceAccess OrElse Not existingValue.HasValue Then
            If String.IsNullOrWhiteSpace(element.GetterCommand) Then
                element.GetterCommand = Syntax.BuildQuery(Me.SyntaxHeader, subHeader)
            End If
            ' existingValue = Me.QueryInteger(subHeader)
            existingValue = Me.Port.QueryInteger(element.GetterCommand)
        End If
        Return existingValue
    End Function

    ''' <summary>
    ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
    ''' Uses or updates the nullable value getter command.
    ''' </summary>
    ''' <param name="element">Specifies the <see cref="IScpiValueBase">existing element</see> where the getter command is stored.</param>
    ''' <param name="modalityName">Specifies the modality name.</param>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2}?' where 
    ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see>, 
    ''' the second item is the <paramref name="modalityName">modality name</paramref> and
    ''' the second item is a <paramref name="command">command</paramref>.
    ''' </param>
    ''' <param name="existingValue">Specifies the existing value.</param>
    Public Function Getter(ByVal element As IScpiValueBase,
                       ByVal modalityName As String, ByVal subHeader As String,
                       ByVal existingValue As Nullable(Of Integer),
                       ByVal access As ResourceAccessLevels) As Nullable(Of Integer) Implements IController.Getter
        If access.IsDeviceAccess OrElse Not existingValue.HasValue Then
            If String.IsNullOrWhiteSpace(element.GetterCommand) Then
                element.GetterCommand = Syntax.BuildQuery(Me.SyntaxHeader, modalityName, subHeader)
            End If
            ' existingValue = Me.QueryInteger(modalityName, subHeader)
            existingValue = Me.Port.QueryInteger(element.GetterCommand)
        End If
        Return existingValue
    End Function

    ''' <summary>
    ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
    ''' Uses or updates the nullable value getter command.
    ''' </summary>
    ''' <param name="element">Specifies the <see cref="IScpiValueBase">existing element</see> where the getter command is stored.</param>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}?' where 
    ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see> and 
    ''' the second item is a <paramref name="command">command</paramref>
    ''' </param>
    ''' <param name="existingValue">Specifies the existing value.</param>
    Public Function GetterInfinity(ByVal element As IScpiValueBase,
                               ByVal subHeader As String,
                               ByVal existingValue As Nullable(Of Integer),
                               ByVal access As ResourceAccessLevels) As Nullable(Of Integer) Implements IController.GetterInfinity
        If access.IsDeviceAccess OrElse Not existingValue.HasValue Then
            If String.IsNullOrWhiteSpace(element.GetterCommand) Then
                element.GetterCommand = Syntax.BuildQuery(Me.SyntaxHeader, subHeader)
            End If
            ' existingValue = Me.QueryInfInteger(subHeader)
            existingValue = Me.Port.QueryInfInteger(element.GetterCommand)
        End If
        Return existingValue
    End Function

    ''' <summary>
    ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
    ''' Uses or updates the nullable value getter command.
    ''' </summary>
    ''' <param name="element">Specifies the <see cref="IScpiValueBase">existing element</see> where the getter command is stored.</param>
    ''' <param name="modalityName">Specifies the modality name.</param>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2}?' where 
    ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see>, 
    ''' the second item is the <paramref name="modalityName">modality name</paramref> and
    ''' the second item is a <paramref name="command">command</paramref>.
    ''' </param>
    ''' <param name="existingValue">Specifies the existing value.</param>
    Public Function GetterInfinity(ByVal element As IScpiValueBase,
                               ByVal modalityName As String, ByVal subHeader As String,
                               ByVal existingValue As Nullable(Of Integer),
                               ByVal access As ResourceAccessLevels) As Nullable(Of Integer) Implements IController.GetterInfinity
        If access.IsDeviceAccess OrElse Not existingValue.HasValue Then
            If String.IsNullOrWhiteSpace(element.GetterCommand) Then
                element.GetterCommand = Syntax.BuildQuery(Me.SyntaxHeader, modalityName, subHeader)
            End If
            ' existingValue = Me.QueryInfInteger(modalityName, subHeader)
            existingValue = Me.Port.QueryInfInteger(element.GetterCommand)
        End If
        Return existingValue
    End Function

#End Region

#Region " GETTERS w/ SCPI VALUE DEVICE ACCESS "

    ''' <summary>
    ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
    ''' Uses or updates the nullable value getter command.
    ''' </summary>
    ''' <param name="element">Specifies the <see cref="IScpiValueBase">existing element</see> where the getter command is stored.</param>
    Public Function Getter(ByVal element As IScpiValue(Of Integer)) As IScpiValue(Of Integer) Implements IController.Getter
        element.Value = Me.Port.QueryInteger(element.GetterCommand)
        element.ActualValue = element.Value
        Return element
    End Function

#End Region

#Region " GETTERS w/ SCPI VALUE "

    ''' <summary>
    ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
    ''' Uses or updates the nullable value getter command.
    ''' </summary>
    ''' <param name="element">Specifies the <see cref="IScpiValueBase">existing element</see> where the getter command is stored.</param>
    Public Function Getter(ByVal element As IScpiValue(Of Integer), ByVal access As ResourceAccessLevels) As IScpiValue(Of Integer) Implements IController.Getter
        If access.IsDeviceAccess OrElse Not element.Value.HasValue Then
            element.Value = Me.Port.QueryInteger(element.GetterCommand)
            element.ActualValue = element.Value
        End If
        Return element
    End Function

    ''' <summary>
    ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
    ''' Uses or updates the nullable value getter command.
    ''' </summary>
    ''' <param name="element">Specifies the <see cref="IScpiValueBase">existing element</see> where the getter command is stored.</param>
    Public Function GetterInfinity(ByVal element As IScpiValue(Of Integer), ByVal access As ResourceAccessLevels) As IScpiValue(Of Integer) Implements IController.GetterInfinity
        If access.IsDeviceAccess OrElse Not element.Value.HasValue Then
            element.Value = Me.Port.QueryInfInteger(element.GetterCommand)
            element.ActualValue = element.Value
        End If
        Return element
    End Function

    ''' <summary>
    ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
    ''' Applies readings to the <paramref name="element">existing element</paramref>
    ''' </summary>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}?' where 
    ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see> and 
    ''' the second item is a <paramref name="command">command</paramref>
    ''' </param>
    ''' <param name="element">Specifies the element holding the existing value, which is updated.</param>
    Public Function Getter(ByVal element As IScpiValue(Of Integer),
                       ByVal subHeader As String,
                       ByVal access As ResourceAccessLevels) As IScpiValue(Of Integer) Implements IController.Getter
        element.Value = Me.Getter(element, subHeader, element.Value, access)
        element.ActualValue = element.Value
        Return element
    End Function

    ''' <summary>
    ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
    ''' Applies readings to the <paramref name="element">existing element</paramref>
    ''' </summary>
    ''' <param name="modalityName">Specifies the modality name.</param>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2}?' where 
    ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see>, 
    ''' the second item is the <paramref name="modalityName">modality name</paramref> and
    ''' the second item is a <paramref name="command">command</paramref>.
    ''' </param>
    ''' <param name="element">Specifies the element holding the existing value, which is updated.</param>
    Public Function Getter(ByVal element As IScpiValue(Of Integer),
                       ByVal modalityName As String, ByVal subHeader As String,
                       ByVal access As ResourceAccessLevels) As IScpiValue(Of Integer) Implements IController.Getter
        element.Value = Me.Getter(element, modalityName, subHeader, element.Value, access)
        element.ActualValue = element.Value
        Return element
    End Function

    ''' <summary>
    ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
    ''' Applies readings to the <paramref name="element">existing element</paramref>
    ''' </summary>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}?' where 
    ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see> and 
    ''' the second item is a <paramref name="command">command</paramref>
    ''' </param>
    ''' <param name="element">Specifies the element holding the existing value, which is updated.</param>
    Public Function GetterInfinity(ByVal element As IScpiValue(Of Integer),
                               ByVal subHeader As String, ByVal access As ResourceAccessLevels) As IScpiValue(Of Integer) Implements IController.GetterInfinity
        element.Value = Me.GetterInfinity(element, subHeader, element.Value, access)
        element.ActualValue = element.Value
        Return element
    End Function

    ''' <summary>
    ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
    ''' Applies readings to the <paramref name="element">existing element</paramref>
    ''' </summary>
    ''' <param name="modalityName">Specifies the modality name.</param>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2}?' where 
    ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see>, 
    ''' the second item is the <paramref name="modalityName">modality name</paramref> and
    ''' the second item is a <paramref name="command">command</paramref>.
    ''' </param>
    ''' <param name="element">Specifies the element holding the existing value, which is updated.</param>
    Public Function GetterInfinity(ByVal element As IScpiValue(Of Integer),
                               ByVal modalityName As String, ByVal subHeader As String,
                               ByVal access As ResourceAccessLevels) As IScpiValue(Of Integer) Implements IController.GetterInfinity
        element.Value = Me.GetterInfinity(element, modalityName, subHeader, element.Value, access)
        element.ActualValue = element.Value
        Return element
    End Function

#End Region

#Region " SETTERS "

    ''' <summary>
    ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1} {2}' where 
    ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see>, 
    ''' the second item is a <paramref name="command">command</paramref>
    ''' the third item represents a <paramref name="value">numeric value</paramref>.
    ''' </param>
    ''' <param name="value">Specifies the command value.</param>
    Public Function Setter(ByVal subHeader As String, ByVal value As Integer, ByVal verify As Boolean) As Boolean Implements IController.Setter
        Dim scpiMessage As String = Syntax.BuildCommand(Me.SyntaxHeader, subHeader, value)
        Me.Port.WriteLine(scpiMessage)
        If verify Then
            Dim actual As Nullable(Of Integer) = Me.QueryInteger(scpiMessage)
            Return actual.HasValue AndAlso (value = actual.Value)
        Else
            Return True
        End If
    End Function

    ''' <summary>
    ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="modalityName">Specifies the modality name.</param>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2} {3}' where 
    ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see>, 
    ''' the second item is the <paramref name="modalityName">modality name</paramref>, 
    ''' the third item is a <paramref name="command">command</paramref>
    ''' the fourth item represents a <paramref name="value">numeric value</paramref>.
    ''' </param>
    ''' <param name="value">Specifies the command value.</param>
    Public Function Setter(ByVal modalityName As String, ByVal subHeader As String, ByVal value As Integer, ByVal verify As Boolean) As Boolean Implements IController.Setter
        Dim scpiMessage As String = Syntax.BuildCommand(Me.SyntaxHeader, modalityName, subHeader, value)
        Me.Port.WriteLine(scpiMessage)
        If verify Then
            Dim actual As Nullable(Of Integer) = Me.QueryInteger(modalityName, subHeader)
            Return actual.HasValue AndAlso (value = actual.Value)
        Else
            Return True
        End If
    End Function

    ''' <summary>
    ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1} {2}' where 
    ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see>, 
    ''' the second item is a <paramref name="command">command</paramref>
    ''' the third item represents a <paramref name="value">numeric value</paramref>.
    ''' </param>
    ''' <param name="value">Specifies the command value.</param>
    Public Function SetterInfinity(ByVal subHeader As String, ByVal value As Integer, ByVal verify As Boolean) As Boolean Implements IController.SetterInfinity
        Dim scpiMessage As String = Syntax.BuildCommand(Me.SyntaxHeader, subHeader, value)
        Me.Port.WriteLine(scpiMessage)
        If verify Then
            Dim actual As Nullable(Of Integer) = Me.QueryInteger(scpiMessage)
            Return actual.HasValue AndAlso (value = actual.Value)
        Else
            Return True
        End If
    End Function

    ''' <summary>
    ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="modalityName">Specifies the modality name.</param>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2} {3}' where 
    ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see>, 
    ''' the second item is the <paramref name="modalityName">modality name</paramref>, 
    ''' the third item is a <paramref name="command">command</paramref>
    ''' the fourth item represents a <paramref name="value">numeric value</paramref>.
    ''' </param>
    ''' <param name="value">Specifies the command value.</param>
    Public Function SetterInfinity(ByVal modalityName As String, ByVal subHeader As String, ByVal value As Integer, ByVal verify As Boolean) As Boolean Implements IController.SetterInfinity
        Dim scpiMessage As String = Syntax.BuildCommand(Me.SyntaxHeader, modalityName, subHeader, value)
        Me.Port.WriteLine(scpiMessage)
        If verify Then
            Dim actual As Nullable(Of Integer) = Me.QueryInteger(modalityName, subHeader)
            Return actual.HasValue AndAlso (value = actual.Value)
        Else
            Return True
        End If
    End Function

#End Region

#Region " SETTERS w/ ACCESS "

    ''' <summary>
    ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1} {2}' where 
    ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see> 
    ''' the second item is a <paramref name="command">command</paramref> and 
    ''' the third item is the <paramref name="value">value</paramref>.
    ''' </param>
    ''' <param name="existingValue">Specifies the existing value.</param>
    Public Function Setter(ByVal subHeader As String,
                       ByVal existingValue As Nullable(Of Integer), ByVal value As Integer,
                       ByVal access As ResourceAccessLevels) As Nullable(Of Integer) Implements IController.Setter
        If access.IsVerifyAccess Then
            Throw New ArgumentException("Verification not supported by this method", "access")
        End If
        If access.IsDeviceAccess OrElse (value <> existingValue) Then
            Me.Port.WriteLine(Syntax.BuildCommand(Me.SyntaxHeader, subHeader, value))
        End If
        Return New Nullable(Of Integer)(value)
    End Function

    ''' <summary>
    ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="modalityName">Specifies the modality name.</param>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2} {3}' where 
    ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see>, 
    ''' the second item is the <paramref name="modalityName">modality name</paramref>, 
    ''' the second item is a <paramref name="command">command</paramref> and
    ''' the fourth item represents a <paramref name="value">numeric value</paramref>.
    ''' </param>
    ''' <param name="existingValue">Specifies the existing value.</param>
    Public Function Setter(ByVal modalityName As String, ByVal subHeader As String,
                       ByVal existingValue As Nullable(Of Integer), ByVal value As Integer,
                       ByVal access As ResourceAccessLevels) As Nullable(Of Integer) Implements IController.Setter
        If access.IsVerifyAccess Then
            Throw New ArgumentException("Verification not supported by this method", "access")
        End If
        If access.IsDeviceAccess OrElse (value <> existingValue) Then
            Me.Port.WriteLine(Syntax.BuildCommand(Me.SyntaxHeader, modalityName, subHeader, value))
        End If
        Return New Nullable(Of Integer)(value)
    End Function

    ''' <summary>
    ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1} {2}' where 
    ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see> 
    ''' the second item is a <paramref name="command">command</paramref> and 
    ''' the third item is the <paramref name="value">value</paramref>.
    ''' </param>
    ''' <param name="existingValue">Specifies the existing value.</param>
    Public Function SetterInfinity(ByVal subHeader As String,
                               ByVal existingValue As Nullable(Of Integer), ByVal value As Integer,
                               ByVal access As ResourceAccessLevels) As Nullable(Of Integer) Implements IController.SetterInfinity
        If access.IsVerifyAccess Then
            Throw New ArgumentException("Verification not supported by this method", "access")
        End If
        If access.IsDeviceAccess OrElse (value <> existingValue) Then
            Me.Port.WriteLine(Syntax.BuildCommand(Me.SyntaxHeader, subHeader, value))
        End If
        Return New Nullable(Of Integer)(value)
    End Function

    ''' <summary>
    ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="modalityName">Specifies the modality name.</param>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2} {3}' where 
    ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see>, 
    ''' the second item is the <paramref name="modalityName">modality name</paramref>, 
    ''' the second item is a <paramref name="command">command</paramref> and
    ''' the fourth item represents a <paramref name="value">numeric value</paramref>.
    ''' </param>
    ''' <param name="existingValue">Specifies the existing value.</param>
    Public Function SetterInfinity(ByVal modalityName As String, ByVal subHeader As String,
                               ByVal existingValue As Nullable(Of Integer), ByVal value As Integer,
                               ByVal access As ResourceAccessLevels) As Nullable(Of Integer) Implements IController.SetterInfinity
        If access.IsVerifyAccess Then
            Throw New ArgumentException("Verification not supported by this method", "access")
        End If
        If access.IsDeviceAccess OrElse (value <> existingValue) Then
            Me.Port.WriteLine(Syntax.BuildCommand(Me.SyntaxHeader, modalityName, subHeader, value))
        End If
        Return New Nullable(Of Integer)(value)
    End Function

#End Region

#Region " SETTERS w/ ACCESS w/ ELEMENT "

    ''' <summary>
    ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="element">Specifies the <see cref="IScpiValueBase">existing element</see> where the getter command is stored.</param>
    ''' <param name="existingValue">Specifies the existing element value where the instrument.</param>
    ''' <param name="value">Specifies the value to be set using the element <see cref="IScpiValueBase.SetterCommandFormat">command</see>.</param>
    Public Function Setter(ByVal element As IScpiValueBase,
                       ByVal existingValue As Integer, ByVal value As Integer,
                       ByVal access As ResourceAccessLevels) As Integer Implements IController.Setter
        If access.IsVerifyAccess Then
            Throw New ArgumentException("Verification not supported by this method", "access")
        End If
        If access.IsDeviceAccess OrElse value <> existingValue Then
            If Not String.IsNullOrWhiteSpace(element.SetterCommandFormat) Then
                element.SetterCommand = String.Format(Globalization.CultureInfo.InvariantCulture,
                                                element.SetterCommandFormat, value)
            ElseIf String.IsNullOrWhiteSpace(element.SetterCommand) Then
                Throw New ArgumentException("Element setter command is empty", "element")
            End If
            Me.Port.WriteLine(element.SetterCommand)
        End If
        Return value
    End Function

    ''' <summary>
    ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
    ''' Uses or updates the nullable value getter command.
    ''' </summary>
    ''' <param name="element">Specifies the <see cref="IScpiValueBase">existing element</see> where the getter command is stored.</param>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1} {2}' where 
    ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see> 
    ''' the second item is a <paramref name="command">command</paramref> and 
    ''' the third item is the <paramref name="value">value</paramref>.
    ''' </param>
    ''' <param name="existingValue">Specifies the existing value.</param>
    Public Function Setter(ByVal element As IScpiValueBase,
                       ByVal subHeader As String,
                       ByVal existingValue As Nullable(Of Integer), ByVal value As Integer,
                       ByVal access As ResourceAccessLevels) As Nullable(Of Integer) Implements IController.Setter
        If access.IsVerifyAccess Then
            Throw New ArgumentException("Verification not supported by this method", "access")
        End If
        If access.IsDeviceAccess OrElse (value <> existingValue) Then
            If String.IsNullOrWhiteSpace(element.SetterCommandFormat) Then
                element.SetterCommandFormat = Syntax.BuildCommandFormat(Me.SyntaxHeader, subHeader)
            End If
            element.SetterCommand = Syntax.BuildCommand(Me.SyntaxHeader, subHeader, value)
            Me.Port.WriteLine(element.SetterCommand)
        End If
        Return New Nullable(Of Integer)(value)
    End Function

    ''' <summary>
    ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
    ''' Uses or updates the nullable value getter command.
    ''' </summary>
    ''' <param name="element">Specifies the <see cref="IScpiValueBase">existing element</see> where the getter command is stored.</param>
    ''' <param name="modalityName">Specifies the modality name.</param>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2} {3}' where 
    ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see>, 
    ''' the second item is the <paramref name="modalityName">modality name</paramref>, 
    ''' the second item is a <paramref name="command">command</paramref> and
    ''' the fourth item represents a <paramref name="value">numeric value</paramref>.
    ''' </param>
    ''' <param name="existingValue">Specifies the existing value.</param>
    Public Function Setter(ByVal element As IScpiValueBase,
                       ByVal modalityName As String, ByVal subHeader As String,
                       ByVal existingValue As Nullable(Of Integer), ByVal value As Integer,
                       ByVal access As ResourceAccessLevels) As Nullable(Of Integer) Implements IController.Setter
        If access.IsVerifyAccess Then
            Throw New ArgumentException("Verification not supported by this method", "access")
        End If
        If access.IsDeviceAccess OrElse (value <> existingValue) Then
            If String.IsNullOrWhiteSpace(element.SetterCommandFormat) Then
                element.SetterCommandFormat = Syntax.BuildCommandFormat(Me.SyntaxHeader, modalityName, subHeader)
            End If
            element.SetterCommand = Syntax.BuildCommand(Me.SyntaxHeader, modalityName, subHeader, value)
            Me.Port.WriteLine(element.SetterCommand)
        End If
        Return New Nullable(Of Integer)(value)
    End Function

    ''' <summary>
    ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
    ''' Uses or updates the nullable value getter command.
    ''' </summary>
    ''' <param name="element">Specifies the <see cref="IScpiValueBase">existing element</see> where the getter command is stored.</param>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1} {2}' where 
    ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see> 
    ''' the second item is a <paramref name="command">command</paramref> and 
    ''' the third item is the <paramref name="value">value</paramref>.
    ''' </param>
    ''' <param name="existingValue">Specifies the existing value.</param>
    Public Function SetterInfinity(ByVal element As IScpiValueBase,
                               ByVal subHeader As String,
                               ByVal existingValue As Nullable(Of Integer), ByVal value As Integer,
                               ByVal access As ResourceAccessLevels) As Nullable(Of Integer) Implements IController.SetterInfinity
        If access.IsVerifyAccess Then
            Throw New ArgumentException("Verification not supported by this method", "access")
        End If
        If access.IsDeviceAccess OrElse (value <> existingValue) Then
            If String.IsNullOrWhiteSpace(element.SetterCommandFormat) Then
                element.SetterCommandFormat = Syntax.BuildCommandFormat(Me.SyntaxHeader, subHeader)
            End If
            element.SetterCommand = Syntax.BuildCommand(Me.SyntaxHeader, subHeader, value)
            Me.Port.WriteLine(element.SetterCommand)
        End If
        Return New Nullable(Of Integer)(value)
    End Function

    ''' <summary>
    ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
    ''' Uses or updates the nullable value getter command.
    ''' </summary>
    ''' <param name="element">Specifies the <see cref="IScpiValueBase">existing element</see> where the getter command is stored.</param>
    ''' <param name="modalityName">Specifies the modality name.</param>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2} {3}' where 
    ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see>, 
    ''' the second item is the <paramref name="modalityName">modality name</paramref>, 
    ''' the second item is a <paramref name="command">command</paramref> and
    ''' the fourth item represents a <paramref name="value">numeric value</paramref>.
    ''' </param>
    ''' <param name="existingValue">Specifies the existing value.</param>
    Public Function SetterInfinity(ByVal element As IScpiValueBase,
                               ByVal modalityName As String, ByVal subHeader As String,
                               ByVal existingValue As Nullable(Of Integer), ByVal value As Integer,
                               ByVal access As ResourceAccessLevels) As Nullable(Of Integer) Implements IController.SetterInfinity
        If access.IsVerifyAccess Then
            Throw New ArgumentException("Verification not supported by this method", "access")
        End If
        If access.IsDeviceAccess OrElse (value <> existingValue) Then
            If String.IsNullOrWhiteSpace(element.SetterCommandFormat) Then
                element.SetterCommandFormat = Syntax.BuildCommandFormat(Me.SyntaxHeader, modalityName, subHeader)
            End If
            element.SetterCommand = Syntax.BuildCommand(Me.SyntaxHeader, modalityName, subHeader, value)
            Me.Port.WriteLine(element.SetterCommand)
        End If
        Return New Nullable(Of Integer)(value)
    End Function

#End Region

#Region " SETTERS w/ SCPI VALUE "

    ''' <summary>
    ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
    ''' Applies readings to the <paramref name="element">existing element</paramref>
    ''' Assume element has both <see cref="IScpiValueBase.GetterCommand">getter</see> and 
    ''' <see cref="IScpiValueBase.SetterCommand">setter</see> commands specified.
    ''' </summary>
    ''' <param name="element">Specifies the element holding the existing value, which is updated.</param>
    Public Function Setter(ByVal element As IScpiValue(Of Integer), ByVal value As Integer, ByVal access As ResourceAccessLevels) As IScpiValue(Of Integer) Implements IController.Setter
        If access.IsDeviceAccess OrElse (value <> element.Value) Then
            element.Value = Me.Setter(CType(element, IScpiValueBase), element.Value.Value, value, access And Not ResourceAccessLevels.Verify)
        End If
        If access.IsVerifyAccess Then
            Me.Getter(element, ResourceAccessLevels.Device)
            element.Value = value
        ElseIf access.IsCacheAccess Then
            element.ActualValue = value
        End If
        Return element
    End Function

    ''' <summary>
    ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
    ''' Applies readings to the <paramref name="element">existing element</paramref>
    ''' </summary>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1} {2}' where 
    ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see> 
    ''' the second item is a <paramref name="command">command</paramref> and 
    ''' the third item is the <paramref name="value">value</paramref>.
    ''' </param>
    ''' <param name="element">Specifies the element holding the existing value, which is updated.</param>
    Public Function Setter(ByVal element As IScpiValue(Of Integer),
                       ByVal subHeader As String, ByVal value As Integer,
                       ByVal access As ResourceAccessLevels) As IScpiValue(Of Integer) Implements IController.Setter
        element.Value = Me.Setter(CType(element, IScpiValueBase), subHeader, element.Value, value, access And Not ResourceAccessLevels.Verify)
        If access.IsVerifyAccess Then
            Me.Getter(element, subHeader, ResourceAccessLevels.Device)
            element.Value = value
        ElseIf access.IsCacheAccess Then
            element.ActualValue = value
        End If
        Return element
    End Function

    ''' <summary>
    ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
    ''' Applies readings to the <paramref name="element">existing element</paramref>
    ''' </summary>
    ''' <param name="modalityName">Specifies the modality name.</param>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2} {3}' where 
    ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see>, 
    ''' the second item is the <paramref name="modalityName">modality name</paramref>, 
    ''' the second item is a <paramref name="command">command</paramref> and
    ''' the fourth item represents a <paramref name="value">numeric value</paramref>.
    ''' </param>
    ''' <param name="element">Specifies the element holding the existing value, which is updated.</param>
    Public Function Setter(ByVal element As IScpiValue(Of Integer),
                       ByVal modalityName As String, ByVal subHeader As String, ByVal value As Integer,
                       ByVal access As ResourceAccessLevels) As IScpiValue(Of Integer) Implements IController.Setter
        element.Value = Me.Setter(CType(element, IScpiValueBase), modalityName, subHeader, element.Value, value, access And Not ResourceAccessLevels.Verify)
        If access.IsVerifyAccess Then
            Me.Getter(element, subHeader, ResourceAccessLevels.Device)
            element.Value = value
        ElseIf access.IsCacheAccess Then
            element.ActualValue = value
        End If
        Return element
    End Function

    ''' <summary>
    ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
    ''' Applies readings to the <paramref name="element">existing element</paramref>
    ''' </summary>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1} {2}' where 
    ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see> 
    ''' the second item is a <paramref name="command">command</paramref> and 
    ''' the third item is the <paramref name="value">value</paramref>.
    ''' </param>
    ''' <param name="element">Specifies the element holding the existing value, which is updated.</param>
    Public Function SetterInfinity(ByVal element As IScpiValue(Of Integer),
                               ByVal subHeader As String, ByVal value As Integer,
                               ByVal access As ResourceAccessLevels) As IScpiValue(Of Integer) Implements IController.SetterInfinity
        element.Value = Me.SetterInfinity(CType(element, IScpiValueBase), subHeader, element.Value, value, access And Not ResourceAccessLevels.Verify)
        If access.IsVerifyAccess Then
            Me.Getter(element, subHeader, ResourceAccessLevels.Device)
            element.Value = value
        ElseIf access.IsCacheAccess Then
            element.ActualValue = value
        End If
        Return element
    End Function

    ''' <summary>
    ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
    ''' Applies readings to the <paramref name="element">existing element</paramref>
    ''' </summary>
    ''' <param name="modalityName">Specifies the modality name.</param>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2} {3}' where 
    ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see>, 
    ''' the second item is the <paramref name="modalityName">modality name</paramref>, 
    ''' the second item is a <paramref name="command">command</paramref> and
    ''' the fourth item represents a <paramref name="value">numeric value</paramref>.
    ''' </param>
    ''' <param name="element">Specifies the element holding the existing value, which is updated.</param>
    Public Function SetterInfinity(ByVal element As IScpiValue(Of Integer),
                               ByVal modalityName As String, ByVal subHeader As String, ByVal value As Integer,
                               ByVal access As ResourceAccessLevels) As IScpiValue(Of Integer) Implements IController.SetterInfinity
        element.Value = Me.SetterInfinity(CType(element, IScpiValueBase), modalityName, subHeader, element.Value, value, access And Not ResourceAccessLevels.Verify)
        If access.IsVerifyAccess Then
            Me.Getter(element, subHeader, ResourceAccessLevels.Device)
            element.Value = value
        ElseIf access.IsCacheAccess Then
            element.ActualValue = value
        End If
        Return element
    End Function

#End Region

#End Region

End Class

Public Module [SystemExtensions]

#Region " ACCESS LEVEL "

  ''' <summary>
  ''' Returns true if cache only access level is requested.
  ''' </summary>
  ''' <param name="access"></param>
      <Runtime.CompilerServices.Extension()> 
  Public Function IsCacheAccess(ByVal access As ResourceAccessLevels) As Boolean
    Return (access And ResourceAccessLevels.Cache) <> 0
  End Function

  ''' <summary>
  ''' Returns true if device access level is requested.
  ''' </summary>
  ''' <param name="access"></param>
      <Runtime.CompilerServices.Extension()> 
  Public Function IsDeviceAccess(ByVal access As ResourceAccessLevels) As Boolean
    Return (access And ResourceAccessLevels.Device) <> 0
  End Function

  ''' <summary>
  ''' Returns true if device verification access level is requested.
  ''' </summary>
  ''' <param name="access"></param>
      <Runtime.CompilerServices.Extension()> 
  Public Function IsVerifyAccess(ByVal access As ResourceAccessLevels) As Boolean
    Return (access And ResourceAccessLevels.Verify) <> 0
  End Function
#End Region

End Module

#Region " UNUSED "
#If False Then

#Region " REMOVE "
  ''' <summary>
  ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
  ''' Applies readings to the <paramref name="element">existing element</paramref>
  ''' </summary>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}?' where 
  ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see> and 
  ''' the second item is a <paramref name="command">command</paramref>
  ''' </param>
  ''' <param name="element">Specifies the element holding the existing value, which is updated.</param>
      Public Function Getter(ByVal subHeader As String, ByVal element As ResettableValue(Of Boolean), 
                         ByVal access As ResourceAccessLevels) As ResettableValue(Of Boolean)
    element.Value = Me.Getter(element, subHeader, element.Value, access)
    element.ActualValue = element.Value
    Return element
  End Function

  ''' <summary>
  ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
  ''' Applies readings to the <paramref name="element">existing element</paramref>
  ''' </summary>
  ''' <param name="modalityName">Specifies the modality name.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2}?' where 
  ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see>, 
  ''' the second item is the <paramref name="modalityName">modality name</paramref> and
  ''' the second item is a <paramref name="command">command</paramref>.
  ''' </param>
  ''' <param name="element">Specifies the element holding the existing value, which is updated.</param>
      Public Function Getter(ByVal modalityName As String, ByVal subHeader As String, 
                         ByVal element As ResettableValue(Of Boolean), 
                         ByVal access As ResourceAccessLevels) As ResettableValue(Of Boolean)
    If access.IsDeviceAccess OrElse Not element.Value.HasValue Then
      element.Value = Me.QueryBoolean(modalityName, subHeader)
      element.ActualValue = element.Value
    End If
    Return element
  End Function

#End Region
  ''' <summary>Gets the line frequency for this instrument.
  ''' The line frequency can be read from the instrument.  Instruments that do 
  ''' not support this command must set the value when initialized.</summary>
  Public ReadOnly Property ControllerLineFrequency() As Double
    Get
      Return Me._controller.SystemSubsystem.LineFrequency
    End Get
  End Property

#Region " REMOVE "

  ''' <summary>
  ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
  ''' Applies readings to the <paramref name="element">existing element</paramref>
  ''' </summary>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1} {2}' where 
  ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see> 
  ''' the second item is a <paramref name="command">command</paramref> and 
  ''' the third item is the <paramref name="value">value</paramref>.
  ''' </param>
  ''' <param name="element">Specifies the element holding the existing value, which is updated.</param>
      Public Function Setter(ByVal subHeader As String, ByVal element As ResettableValue(Of Boolean), 
                         ByVal value As Boolean, ByVal boolFormat As BooleanDataFormat, 
                         ByVal access As ResourceAccessLevels) As ResettableValue(Of Boolean)
    If access.IsDeviceAccess OrElse (value <> element) Then
      MyBase.Controller.Port.WriteLine(Syntax.BuildCommand(MyBase.SyntaxHeader, subHeader, value, boolFormat))
    End If
    If access.IsVerifyAccess Then
      Dim actual As New ResettableValue(Of Boolean)
      Me.Getter(actual, subHeader, ResourceAccessLevels.Device)
      If value <> actual.Value Then
        If actual.Value.HasValue Then
          Throw New VerificationException( 
                  Syntax.BuildCommand(MyBase.SyntaxHeader, subHeader, value, boolFormat), value, actual.Value.Value)
        Else
          Throw New VerificationException("Getter failed reading.")
        End If
      End If
    End If
    element.Value = value
    Return element
  End Function

  ''' <summary>
  ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
  ''' Applies readings to the <paramref name="element">existing element</paramref>
  ''' </summary>
  ''' <param name="modalityName">Specifies the modality name.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2} {3}' where 
  ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see>, 
  ''' the second item is the <paramref name="modalityName">modality name</paramref>, 
  ''' the second item is a <paramref name="command">command</paramref> and
  ''' the fourth item represents a <paramref name="value">numeric value</paramref>.
  ''' </param>
  ''' <param name="element">Specifies the element holding the existing value, which is updated.</param>
      Public Function Setter(ByVal modalityName As String, ByVal subHeader As String, 
                         ByVal element As IScpiValue(Of Boolean), ByVal value As Boolean, 
                         ByVal boolFormat As BooleanDataFormat, ByVal access As ResourceAccessLevels) As IScpiValue(Of Boolean)
    If access.IsDeviceAccess OrElse (value <> element) Then
      MyBase.Controller.Port.WriteLine(Syntax.BuildCommand(MyBase.SyntaxHeader, modalityName, subHeader, value, boolFormat))
    End If
    If access.IsVerifyAccess Then
      Dim actual As ResettableValue(Of Boolean)
      actual.Value = Me.Getter(actual, modalityName, subHeader, ResourceAccessLevels.Device).Value
      If value <> actual Then

        If actual.Value.HasValue Then
          Throw New VerificationException( 
                  Syntax.BuildCommand(MyBase.SyntaxHeader, modalityName, subHeader, value, boolFormat), value, actual.Value.Value)
        Else
          Throw New VerificationException("Getter failed reading.")
        End If
      End If
    End If
    element.Value = value
    Return element
  End Function

#End Region

  ''' <summary>
  ''' Gets the instrument <see cref="SyntaxHeader">SCPI subsystem</see> <paramref name="modalityName">modality</paramref> <paramref name="subHeader">element</paramref> value.
  ''' Applies readings to the <paramref name="element">existing element</paramref>
  ''' </summary>
  ''' <param name="element">Specifies the existing element where the instrument readings are applied.</param>
  ''' <param name="modalityName">Specifies the modality name.</param>
  ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2}?' where 
  ''' the first item is the <see cref="SyntaxHeader">SCPI subsystem syntax header</see>, 
  ''' the second item is the <paramref name="modalityName">modality name</paramref> and 
  ''' the third item is a <paramref name="command">command</paramref>
  ''' </param>
  ''' <param name="access">The desired <see cref="ResourceAccessLevels">access level</see> to the instrument.</param>
      Public Function Getter(ByVal modalityName As String, ByVal subHeader As String, 
                         ByVal element As IScpiString, 
                         ByVal access As ResourceAccessLevels) As IScpiString
    element.Value = Me.Getter(element, modalityName, subHeader, element.Value, access)
    element.ActualValue = element.Value
    Return element
  End Function

#End If
#End Region

