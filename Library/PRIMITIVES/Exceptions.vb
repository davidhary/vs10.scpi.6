''' <summary>Handles data conversion exceptions.</summary>
''' <license>
''' (c) 2005 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="01/15/05" by="David" revision="1.0.1841.x">
''' Created
''' </history>
<Serializable()> Public Class CastException
    Inherits BaseException

    Private Const _defMessage As String = "failed converting setting value to the expected type."

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructor with no parameters.</summary>
    Public Sub New()
        MyBase.New(CastException._defMessage)
    End Sub

    ''' <summary>Constructor specifying the Message to be set.</summary>
    ''' <param name="message">Specifies the exception message.</param>
    Public Sub New(ByVal message As String)
        MyBase.New(message)
    End Sub

    ''' <summary>Constructor specifying the Message and InnerException property to be set.</summary>
    ''' <param name="message">Specifies the exception message.</param>
    ''' <param name="innerException">Sets a reference to the InnerException.</param>
    Public Sub New(ByVal message As String, ByVal innerException As System.Exception)
        MyBase.New(message, innerException)
    End Sub

    ''' <summary>Constructor used for deserialization of the exception class.</summary>
    ''' <param name="info">Represents the SerializationInfo of the exception.</param>
    ''' <param name="context">Represents the context information of the exception.</param>
    '''-----------------------------------------------------------------------------
    Protected Sub New(ByVal info As System.Runtime.Serialization.SerializationInfo,
        ByVal context As System.Runtime.Serialization.StreamingContext)
        MyBase.New(info, context)
    End Sub

#End Region

End Class

''' <summary>Handles function not defined exceptions.</summary>
''' <license>
''' (c) 2005 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="01/08/05" by="David" revision="1.00.1834.x">
''' Created
''' </history>
''' <history date="01/23/08" by="David" revision="1.00.2944.x">
''' Update to .NET 3.5
''' </history>
<Serializable()> Public Class FunctionNotDefinedException
    Inherits BaseException

    Private Const _defMessage As String = "function not defined."

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructor with no parameters.</summary>
    Public Sub New()
        MyBase.New(FunctionNotDefinedException._defMessage)
    End Sub

    ''' <summary>Constructor specifying the Message to be set.</summary>
    ''' <param name="message">Specifies the exception message.</param>
    Public Sub New(ByVal message As String)
        MyBase.New(message)
    End Sub

    ''' <summary>Constructor specifying the Message and InnerException property to be set.</summary>
    ''' <param name="message">Specifies the exception message.</param>
    ''' <param name="innerException">Sets a reference to the InnerException.</param>
    Public Sub New(ByVal message As String, ByVal innerException As System.Exception)
        MyBase.New(message, innerException)
    End Sub

    ''' <summary>Constructor used for deserialization of the exception class.</summary>
    ''' <param name="info">Represents the SerializationInfo of the exception.</param>
    ''' <param name="context">Represents the context information of the exception.</param>
    Protected Sub New(ByVal info As System.Runtime.Serialization.SerializationInfo,
        ByVal context As System.Runtime.Serialization.StreamingContext)
        MyBase.New(info, context)
    End Sub

    Private Const _functionModeMessage As String = "failed selecting function {0}."
    ''' <summary>Constructor specifying the Message to be set.</summary>
    ''' <param name="functionMode">Specifies the exception message.</param>
    Public Sub New(ByVal functionMode As SourceFunctionMode)
        MyBase.New(String.Format(Globalization.CultureInfo.CurrentCulture, _functionModeMessage, functionMode))
    End Sub

    ''' <summary>Constructor specifying the Message to be set.</summary>
    ''' <param name="functionMode">Specifies the exception message.</param>
    Public Sub New(ByVal functionMode As SenseFunctionModes)
        MyBase.New(String.Format(Globalization.CultureInfo.CurrentCulture, _functionModeMessage, functionMode))
    End Sub

#End Region

End Class

