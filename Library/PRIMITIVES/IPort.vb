''' <summary>
''' Defines the contract implemented by VISA Port I/O accessing the device.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="303/25/2008" by="David" revision="5.0.3004.x">
''' Created
''' </history>
Public Interface IPort
  Inherits Core.IResettableDevice, IInitializable

#Region " CONTROLLER INFORMATION "

  ''' <summary>Gets the resource name.</summary>
  Property ResourceName() As String

  ''' <summary>
  ''' Gets or sets the version infrmation.  Allow a Protected way to set the
  ''' version information to an instrument-specific version information.
  ''' </summary>
  Property VersionInfo() As isr.Scpi.IVersionInfo

  ''' <summary>
  ''' Queries the instrument and returns the Identity string..
  ''' </summary>
  Function ReadIdentity() As String

#End Region

#Region " QUERY "

  ''' <summary>Queries the instrument and returns a Boolean value.</summary>
  ''' <param name="question">The query to use.</param>
  ''' <param name="parser">The <see cref="isr.Core.IParser(of boolean)">boolean Parser.</see></param>
  Function QueryBoolean(ByVal question As String, ByVal parser As isr.Core.IParser(Of Boolean)) As Nullable(Of Boolean)

  ''' <summary>Queries the instrument and returns a double value.</summary>
  ''' <param name="question">The query to use.</param>
  Function QueryDouble(ByVal question As String) As Nullable(Of Double)

  ''' <summary>Queries the instrument and returns an integer value.</summary>
  ''' <param name="question">The query to use.</param>
  Function QueryInteger(ByVal question As String) As Nullable(Of Integer)

  ''' <summary>Queries the instrument and returns an integer value.
  ''' The instrument may return INF in which case the returned value is the maximum integer.</summary>
  ''' <param name="question">The query to use.</param>
  Function QueryInfInteger(ByVal question As String) As Nullable(Of Integer)

  ''' <summary>
  ''' Sends the query and reads the reply.
  ''' </summary>
  ''' <param name="query"></param>
      Function QueryString(ByVal query As String) As String

  ''' <summary>Queries the instrument and returns a string save the termination character.</summary>
  ''' <param name="question">The query to use.</param>
  Function QueryTrimEnd(ByVal question As String) As String

  ''' <summary>Queries the instrument and returns a string save the new line and carriage return characters.</summary>
  ''' <param name="question">The query to use.</param>
  Function QueryTrimNewLine(ByVal question As String) As String

#End Region

#Region " READ "

  ''' <summary>
  ''' Gets or sets the default string size to read.
  ''' </summary>
  Property DefaultStringSize() As Integer

  ''' <summary>Reads and discard all unread data till the END character.</summary>
  Sub DiscardUnreadData()

  ''' <summary>Reads the instrument and returns a Boolean value.</summary>
  ''' <param name="parser">The <see cref="isr.Core.IFormatter(of boolean)">boolean formatter.</see></param>
  Function ReadBoolean(ByVal parser As isr.Core.IParser(Of Boolean)) As Nullable(Of Boolean)

  ''' <summary>Reads the instrument and returns a double value.</summary>
  Function ReadDouble() As Nullable(Of Double)

  ''' <summary>Reads the instrument and returns an integer value.</summary>
  Function ReadInteger() As Nullable(Of Integer)

  ''' <summary>Reads the instrument and returns an integer value.
  ''' The instrument may return INF in which case the returned value is the maximum integer.</summary>
  Function ReadInfInteger() As Nullable(Of Integer)

  ''' <summary>Queries the instrument and returns a string save the termination character.</summary>
  Overloads Function ReadLineTrimEnd(ByVal terminationCharacters As Char()) As String

  ''' <summary>Queries the instrument and returns a string save the new line and carriage return characters.</summary>
  Overloads Function ReadLineTrimEnd(ByVal terminationCharacter As Byte) As String

  ''' <summary>Queries the instrument and returns a string save the new line and carriage return characters.</summary>
  Overloads Function ReadLineTrimEnd() As String

  ''' <summary>Queries the instrument and returns a string save the new line and carriage return characters.</summary>
  Function ReadLineTrimNewLine() As String

  ''' <summary>Queries the instrument and returns a string save the new line and carriage return characters.</summary>
  Function ReadLineTrimLinefeed() As String

  ''' <summary>
  ''' Receives data from the SCPI instrument.
  ''' </summary>
    Function ReadLine() As String

#If False Then
  ''' <summary>
  ''' Receives data from the SCPI instrument.
  ''' </summary>
  ''' <param name="maximumLength">Specifies the maximum number of characters to read.  
  ''' A negative value specifies using the the exiting value.</param>
  ''' <param name="expectDataInQueue">Specify if expecting to have data in the 
  ''' queue before calling this routine.</param>
  ''' <param name="isRaiseDeviceErrors">Specifies if detecting and raising errors after reading.</param>
    ''' <remarks>
  ''' Reports errors to the operator if the instrument is not ready to receive a command, 
  ''' or if the instrument does not have data to send. Typically, you should not expect to have 
  ''' data in the queue immediately after querying the instrument. Rather, you could check if all 
  ''' operations were complete in such cases.
  ''' </remarks>
  Function ReadLine(ByVal maximumLength As Integer, ByVal expectDataInQueue As Boolean, ByVal isRaiseDeviceErrors As Boolean) As String
#End If

  ''' <summary>Gets the last message that was received from the instrument.
  ''' Note that not all messages get recorded.
  ''' </summary>
  ReadOnly Property ReceiveBuffer() As String

  ''' <summary>
  ''' Gets or sets the termination character.
  ''' </summary>
      Property TerminationCharacter() As Byte

#End Region

#Region " REGISTERS "

  ''' <summary>Returns True if the last operation reported an error by
  '''   way of a service request.</summary>
  ReadOnly Property HadError() As Boolean

  ''' <summary>Gets or sets the last service event arguments
  '''   <see cref="BaseServiceEventArgs">status</see></summary>
  ''' <remarks>Also used to hold values read from instrument by way of static methods
  '''   such as *OPC?</remarks>
  Property LastServiceEventArgs() As BaseServiceEventArgs

  ''' <summary>
  ''' Serial polls the device and returns the <see cref="ServiceRequests">service requests bits.</see>
  ''' </summary>
      Function SerialPoll() As ServiceRequests

  ''' <summary>
  ''' Serial polls the device and returns the <see cref="ServiceRequests">service requests bits.</see>
  ''' </summary>
      Function ReadStatusByte() As ServiceRequests

  ''' <summary>
  ''' Gets or sets the condition that determines if the device supports OPC.
  ''' </summary>
    ''' <remarks>
  ''' Some instruments provide only partial support of IEEE488.2 not supporting the query for 
  ''' determining completion of operations. For these instruments, another method needs to be 
  ''' used such as polling.
  ''' </remarks>
  Property SupportsOperationComplete() As Boolean

#If False Then
  Function StandardEventStatus() As StandardEvents
#End If
  ''' <summary>
  ''' Reads the standard event register status.
  ''' </summary>
  Function ReadStandardEventStatus() As Integer

#End Region

#Region " SERVICE REQUEST SERVER "

  ''' <summary>
  ''' Gets the request server.
  ''' </summary>
  ''' 
  ReadOnly Property RequestServer() As isr.Core.IServicingRequest

#End Region

#Region " WRITE "

  ''' <summary>Gets the last message that was received from the instrument.
  ''' Note that not all messages get recorded.
  ''' </summary>
  ReadOnly Property TransmitBuffer() As String

  ''' <summary>Writes boolean command to the instrument.</summary>
  ''' <param name="queryCommand">The main command.</param>
  ''' <param name="value">The boolean value to write.</param>
  ''' <param name="formatter">The <see cref="isr.Core.IFormatter(of boolean)">boolean formatter.</see></param>
  Sub WriteLine(ByVal formatter As isr.Core.IFormatter(Of Boolean), ByVal queryCommand As String, ByVal value As Boolean)

  ''' <summary>
  ''' Outputs a command based on the format string and the specified v alues.
  ''' </summary>
  ''' <param name="format"></param>
  ''' <param name="values"></param>
    Sub WriteLine(ByVal format As String, ByVal ParamArray values() As Object)

  ''' <summary>
  ''' Writes a string to the instrument.
  ''' </summary>
  ''' <param name="value">Specifies the data to send.</param>
  ''' <remarks>
  ''' This method makes no assumptions on the status of 
  ''' the GPIB instrument other than it must not have data
  ''' in the queue when sending new data to the instrument.
  ''' Because this method does not wait for operation to
  ''' complete, it is unable to report device errors as
  ''' they occur as the device may report an error way
  ''' after the message was sent to the GPIB.  Therefore,
  ''' in debug situation it is preferable to use the
  ''' full <see cref="WriteLine"/> and <see cref="ReadLine"/> methods.
  ''' </remarks>
  Sub WriteLine(ByVal value As String)

#If False Then
  ''' <summary>
  ''' Writes a string to the instrument.
  ''' </summary>
  ''' <param name="value">Specifies the data to send.</param>
  ''' <param name="isVerifyOperationComplete">Specifies if the write procedure verifies 
  ''' instrument OPC before returning control to the calling application.</param>
  ''' <param name="isRaiseDeviceErrors">Specifies to have the write procedure raise error 
  ''' in case of instrument errors after verifying OPC</param>
  ''' <remarks>
  ''' Report errors if the instrument is not ready to receive a command, or if the instrument does not have data to send.
  ''' </remarks>
  Sub WriteLine(ByVal value As String, ByVal isVerifyOperationComplete As Boolean, ByVal isRaiseDeviceErrors As Boolean)
#End If

#End Region

End Interface

Public Interface IInitializable

  ''' <summary>Initialize resettable values, units, scales.
  ''' This is to be called once upon connecting but before reset and clear..</summary>
  Function InitializeExecutionState() As Boolean

End Interface

#Region " UNUSED "
#If False Then

#Region " SUB SYSTEMS "

  ''' <summary>
  ''' Reference to the SCPI <see cref="isr.Scpi.SystemSubsystem">System subsystem</see>.
  ''' Every controller must have a System subsystem. 
  ''' </summary>
      ReadOnly Property SystemSubsystem() As isr.Scpi.SystemSubsystem

#End Region


#Region " ACCESS LEVEL "

  ''' <summary>
  ''' Gets or sets reference to the level of <see cref="ResourceAccessLevels">resource access.</see>
  ''' </summary>
      Property AccessLevel() As ResourceAccessLevels

  ''' <summary>
  ''' Saves the access level.
  ''' </summary>
    Sub StoreResourceAccessLevel()

  ''' <summary>
  ''' Restores the access level.
  ''' </summary>
    Sub RestoreResourceAccessLevel()
#End Region

#End If

#End Region