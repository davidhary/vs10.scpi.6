''' <summary>
''' Parses and holds the instrument version information.</summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="01/15/08" by="David" revision="2.0.2936.x">
''' derived from previous Scpi Instrument implementation.
''' Created
''' </history>
Public MustInherit Class VersionInfo
    Implements IVersionInfo

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructs this class.</summary>
    Protected Sub New()
        MyBase.New()
    End Sub

#End Region

#Region " INFORMATION PROPERTIES "

    Private _identity As String
    ''' <summary>
    ''' Gets the identity information that was used to parse the version information.
    ''' </summary>
    Public ReadOnly Property Identity() As String
        Get
            Return Me._identity
        End Get
    End Property

    Private _firmwareRevision As String
    ''' <summary>
    ''' Gets the extended version information.
    ''' </summary>
    Public ReadOnly Property FirmwareRevision() As String Implements IVersionInfo.FirmwareRevision
        Get
            Return Me._firmwareRevision
        End Get
    End Property

    Private _manufacturerName As String
    ''' <summary>Returns the instrument manufacturer name .</summary>
    Public ReadOnly Property ManufacturerName() As String Implements IVersionInfo.ManufacturerName
        Get
            Return Me._manufacturerName
        End Get
    End Property

    Private _model As String
    ''' <summary>Returns the instrument model number.</summary>
    ''' <value>A string property that may include additional precursors such as
    '''   'Model' to the relevant information.</value>
    Public ReadOnly Property Model() As String Implements IVersionInfo.Model
        Get
            Return Me._model
        End Get
    End Property

    Private _serialNumber As String
    ''' <summary>returns the instrument serial number.</summary>
    Public ReadOnly Property SerialNumber() As String Implements IVersionInfo.SerialNumber
        Get
            Return Me._serialNumber
        End Get
    End Property

#End Region

#Region " PARSE "

    ''' <summary>Parses the instrument firmware revision.</summary>
    ''' <param name="revision">Specifies the instrument firmware revision..</param>
    MustOverride Sub ParseFirmwareRevision(ByVal revision As String) Implements IVersionInfo.ParseFirmwareRevision

    ''' <summary>
    ''' Parses the instrument ID.
    ''' </summary>
    ''' <param name="id">Specifies the instrument ID, which includes at a minimum the following information:
    ''' <see cref="ManufacturerName">manufacturer</see>, <see cref="Model">model</see>,
    ''' <see cref="SerialNumber">serial number</see>, e.g.,
    ''' <c>KEITHLEY INSTRUMENTS INC.,MODEL 2420,0669977,C11 Oct 10 1997 09:51:36/A02 /D/B/E.</c>.</param>
    ''' <exception cref="System.ArgumentNullException">id</exception>
    ''' <remarks>The firmware revision can be further interpreted by the child instruments.</remarks>
    Public Overridable Sub ParseInstrumentId(ByVal id As String) Implements IVersionInfo.ParseInstrumentId

        If String.IsNullOrWhiteSpace(id) Then
            Throw New ArgumentNullException("id")
        End If

        ' save the identity.
        Me._identity = id

        ' Parse the id to get the revision number
        Dim idItems() As String = id.Split(","c)

        ' company, e.g., KEITHLEY INSTRUMENTS INC.,
        Me._manufacturerName = idItems(0)

        ' model: MODEL 2420
        Me._model = idItems(1)

        ' Serial Number: 0669977
        Me._serialNumber = idItems(2)

        ' firmware: C11 Oct 10 1997 09:51:36/A02 /D/B/E
        Me._firmwareRevision = idItems(3)

        ' parse thee firmware revision
        ParseFirmwareRevision(Me._firmwareRevision)

    End Sub

#End Region

End Class
