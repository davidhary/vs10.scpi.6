''' <summary>Provides event arguments for instrument events.</summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="02/20/2006" by="David" revision="1.0.2242.x">
''' Created
''' </history>
Public Class ServiceEventArgs
    Inherits isr.Scpi.BaseServiceEventArgs

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    Private _requestServer As isr.Core.IServicingRequest
    ''' <summary>
    ''' Initializes a new instance of the <see cref="ServiceEventArgs" /> class.
    ''' </summary>
    ''' <param name="requestServer">The <see cref="isr.Core.IServicingRequest">request server</see>.</param>
    Public Sub New(ByVal requestServer As isr.Core.IServicingRequest)

        ' instantiate the base class
        MyBase.New()
        Me._requestServer = requestServer
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="ServiceEventArgs" /> class.
    ''' </summary>
    ''' <param name="eventMessage">The event message.</param>
    ''' <param name="requestServer">The request server.</param>
    Public Sub New(ByVal eventMessage As String, ByVal requestServer As isr.Core.IServicingRequest)

        ' instantiate the base class
        MyBase.New(eventMessage)
        Me._requestServer = requestServer

    End Sub

#End Region

#Region " READ REGISTERS AND MESSAGES "

    ''' <summary>
    ''' Reads a message from the instrument.
    ''' </summary>
    Protected Overrides Function ReadLineTrimEnd() As String
        Return Me._requestServer.ReadLineTrimEnd
    End Function

    ''' <summary>
    ''' Reads the measurement events register status.
    ''' </summary>
    ''' <remarks>Override to get the event status.</remarks>
    Protected Overrides Function ReadMeasurementEventStatus() As Integer
        Return Me._requestServer.ReadMeasurementEventStatus() ' .QueryInteger("STAT:MEAS:EVEN?").Value()
    End Function

    ''' <summary>
    ''' Reads the operation event register status.
    ''' </summary>
    ''' <remarks>Override to get the event status.</remarks>
    Protected Overrides Function ReadOperationEventStatus() As Integer
        Return Me._requestServer.ReadOperationEventStatus() 'QueryInteger("STAT:OPER:EVEN?").Value
    End Function

    ''' <summary>
    ''' Reads the questionable event register status.
    ''' </summary>
    ''' <remarks>Override to get the event status.</remarks>
    Protected Overrides Function ReadQuestionableEventStatus() As Integer
        Return Me._requestServer.ReadQuestionableEventStatus() ' .QueryInteger("STAT:QUES:EVEN?").Value
    End Function

    ''' <summary>
    ''' Reads the standard event register status.
    ''' </summary>
    ''' <remarks>Override to get the event status.</remarks>
    Protected Overrides Function ReadStandardEventStatus() As Integer
        Return Me._requestServer.ReadStandardEventStatus() ' .QueryInteger("*ESR?").Value
    End Function

    ''' <summary>
    ''' Reads the event status register.
    ''' </summary>
    Protected Overrides Function ReadStatusByte() As Integer
        Return Me._requestServer.ReadStatusByte1
    End Function

    Protected Overrides Function ReadLastError() As String
        Return Me._requestServer.ReadLastError() '.QueryTrimEnd(":SYST:ERR?")
    End Function

    Protected Overrides Function ReadErrorQueue() As String
        Return Me._requestServer.ReadErrorQueue
#If False Then
      Dim messageBuilder As New System.Text.StringBuilder
      Do
        ProcessServiceRequestRegister()
        If Me.HasError Then
          If messageBuilder.Length > 0 Then
            messageBuilder.Append(Environment.NewLine)
          End If
          messageBuilder.Append(me.session.Query(":STAT:QUE?"))
        End If
      Loop Until Not Me.HasError
      Return messageBuilder.ToString
#End If
    End Function

#End Region

End Class
